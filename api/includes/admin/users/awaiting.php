<?php
$users_manager = managers_store::locate('users');
$data = array(
    'is_active' => 0
);
if (isset($_REQUEST['data'])) {
    $_REQUEST['data'] = array_merge($_REQUEST['data'], $data);
} else {
    $_REQUEST['data'] = $data;
}
try {
    $assign['users'] = $users_manager->search();
} catch (search_exception $e) {
    $core->session->setError($e->getMessage());
}
