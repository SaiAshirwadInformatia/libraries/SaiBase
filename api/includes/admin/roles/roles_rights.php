<?php
$roles_rights_manager = managers_store::locate('roles_rights');
$rights_manager = managers_store::locate('rights');
$roles_manager = managers_store::locate('roles');
try {
    
    if (isset($_GET['id'])) {
        $all_rights = $rights_manager->all();
        $role = $roles_manager->get($_GET['id']);
        $assign['role'] = $role->jsonSerialize();
        $assign['current_rights'] = $roles_rights_manager->get($role->getId());
        $currentrights = $assign['current_rights'];
        $newrights = array();
        foreach ($all_rights as $a) {
            $hasRight = false;
            foreach ($currentrights as $c) {
                if ($c['name'] == $a['name']) {
                    $hasRight = true;
                    break;
                }
            }
            if (! $hasRight) {
                $newrights[] = $a;
            }
        }
        $assign['newrights'] = $newrights;
    }
    // var_dump(count($all_rights));
    // var_dump(count($newrights));
    // var_dump(count($assign['current_rights']));
    // die();
} catch (sai_exception $e) {
    $assign['newrights'] = $all_rights;
}