<?php
use Sai\Modules\Albums;

$albums_manager = managers_store::locate('albums');
$photos = new albums_photos(false);
try {
    $assign['albums'] = $albums_manager->all();
    if (isset($_POST) and count($_POST) > 0 and isset($_FILES['files'])) {
        $photos->loadArgs();
        $albums_photos_manager = managers_store::locate('albums_photos');
        $albums_photos_manager->setObject($photos);
        $ret = $albums_photos_manager->create();
        if (isset($ret)) {
            if (isset($ret['msg'])) {
                $core->session->setSuccess($ret['msg']);
            } elseif (isset($ret['error_msg'])) {
                $core->session->setError($ret['error_msg']);
            }
        }
    }
} catch (Albums\Exception $e) {
    $core->session->setError(
        "Please add atleast one album so that you can add photos to it");
    redirect_url($core->basehref . '/admincp/albums/create');
    exit();
} catch (albums_photos_exception $e) {
    $core->session->setError($e->getMessage());
} catch (files_exception $e) {
    $core->session->setError($e->getMessage());
}

$assign['photos'] = $photos->jsonSerialize();