<?php
if (isset($_POST) and count($_POST) > 0) {
    foreach ($_POST as $_k => $_v) {
        if (strpos($_k, "config_") === 0) {
            $_k = str_replace("config_", "", $_k);
            $core->configs->$_k = $_v;
        }
    }
    $core->configs->save();
    $core->session->setSuccess("Saved configuration");
}
$assign['configs'] = $core->configs->getAll();
foreach ($assign['configs'] as $k => $v) {
    if ($v['oftype'] == 'list') {
        $assign['configs'][$k]['options'] = explode("|", $v['options']);
    }
}
