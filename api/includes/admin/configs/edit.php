<?php
if (isset($_POST) and count($_POST) > 0) {
    if (isset($_POST['name']) and isset($_POST['value']) and
         isset($_POST['oftype'])) {
        if (! empty($_POST['name']) and ! empty($_POST['value'])) {
            $new_config = array(
                'options' => ''
            );
            $c_name = $_POST['name'];
            $new_config['name'] = $c_name;
            $new_config['val'] = $core->db->escapeString($_POST['value']);
            $new_config['oftype'] = $_POST['oftype'];
            if (isset($_POST['options'])) {
                $new_config['options'] = $_POST['options'];
            }
            $core->configs->$c_name = $new_config;
            $core->configs->save();
            $core->session->setSuccess("Successfully saved configuration");
            header("Location: edit?name=" . $c_name);
            exit();
        } else {
            $core->session->setError(
                "Please fill valid config name & its value");
        }
    } else {
        $core->session->setError(
            "Please fill name, value, type of config as they are required fields");
    }
}
$config = array(
    "name" => "",
    "val" => "",
    "oftype" => "",
    "options" => ""
);
if (isset($_GET['name'])) {
    $config = $core->configs->get($_GET['name']);
}
$assign['config'] = $config;
