<?php
if (isset($_GET['name'])) {
    $assign['config'] = $core->configs->get($_GET['name']);
}
if (isset($_POST['deleteYes'])) {
    $core->configs->remove($_GET['name']);
    $core->session->setSuccess("Removed configuration");
    header("Location: viewall");
    exit();
} elseif (isset($_POST['deleteNo'])) {
    header("Location: viewall");
    exit();
}
