<?php
if (class_exists($module)) {
    try {
        $manager = managers_store::locate($module);
        if (method_exists($manager, "search")) {
            $r = $manager->search();
        } elseif (method_exists($manager, "all")) {
            $_r = $manager->all();
            $r = array(
                "results" => $_r,
                "found" => count($_r),
                "totalFound" => count($_r),
                "page" => 1,
                "totalPages" => 1
            );
        } else {
            $core->session->setError(
                "Something went wrong with <code>$module</code> view all mechanism");
        }
        if (isset($r['results'])) {
            $assign['module_results'] = $r;
        }
    } catch (sai_exception $e) {
        $core->session->setError($e->getMessage());
    }
} else {
    $core->session->setError("<code>$module</code> named class doesn't exists");
}
