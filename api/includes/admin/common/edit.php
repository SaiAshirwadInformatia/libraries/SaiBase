<?php
use Symfony\Component\Validator\Validation;
if (class_exists($module)) {
    $object = new $module(false);
    if (isset($_GET['id'])) {
        $object->loadObject($_GET['id']);
    }
    try {
        if (isset($_POST) and count($_POST) > 0 or
             isset($_FILES) and count($_FILES) > 0) {
            $object->loadArgs();
            $object_manager = managers_store::locate($module);
            $object_manager->setObject($object);
            $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
            $violations = $validator->validate($object);
            if (count($violations) == 0) {
                if ($assign['action'] == 'create') {
                    $ret = $object_manager->create();
                } elseif ($assign['action'] = 'edit') {
                    $ret = $object_manager->update();
                }
            } else {
                $obj_e = $module . "_exception";
                $const = $violations->get(0)->getMessage();
                throw new $obj_e(constant($obj_e . '::' . $const));
            }
            if (isset($ret)) {
                if (isset($ret['msg'])) {
                    $core->session->setSuccess($ret['msg']);
                } elseif (isset($ret['error_msg'])) {
                    $core->session->setError($ret['error_msg']);
                }
                header("Location: edit?id=" . $ret['id']);
                exit();
            }
        }
    } catch (sai_exception $e) {
        $core->session->setError($e->getExceptionMessage());
    }
    catch (Exception $e){
        $core->session->setError($e->getMessage());
    }
    $assign[$module] = $object->jsonSerialize();
} else {
    $core->session->setError("<code>$module</code> named class doesn't exists");
}
