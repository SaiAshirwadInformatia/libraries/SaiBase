<?php
try {
    if (! isset($_GET['a']) or ! isset($_GET['u']) or ! isset($_GET['t'])) {
        throw new users_exception(users_exception::FORGOT_INVALID_LINK);
    }
    $result = $core->db->selectAll(
        array(
            'tablename' => 'users',
            'where' => array(
                'condition' => 'AND',
                'data' => array(
                    'access_token' => $_GET['a'],
                    'id' => $_GET['u']
                )
            )
        ));
    if (count($result) == 0) {
        throw new users_exception(users_exception::FORGOT_INVALID_LINK);
    }
} catch (users_exception $e) {
    $core->session->setParam('error_msg', $e->getExceptionMessage());
    header("Location: forgotpassword");
}
