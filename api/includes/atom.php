<?php
header("Content-Type: application/xml");

$rss_modules = array(
    'events',
    'shradhanjali',
    'temples',
    'recipes',
    'users'
);

if (isset($_GET['category'])) {
    $rss_modules = explode(",", $_GET['category']);
}

$xmlDoc = new DOMDocument("1.0", "UTF-8");
$feed = $xmlDoc->createElement('feed');
$feed->setAttribute("xmlns", "http://www.w3.org/2005/Atom");

$feed->appendChild($xmlDoc->createElement('title', 'Ghanerao RSS Feeds'));
$feed->appendChild($xmlDoc->createElement('link', 'http://ghanerao.in'));

$feed->appendChild(
    $xmlDoc->createElement('updated', 
        (new DateTime())->format("D, d M Y H:i:s P")));
$feed->appendChild(
    $xmlDoc->createElement('generator', 
        'Sai Ashirwad Informatia Atom Feeds Generator (http://saiashirwad.com)'));

foreach ($rss_modules as $module) {
    $manager = managers_store::locate($module);
    $items = $manager->all();
    foreach ($items as $item) {
        $xml_item = $xmlDoc->createElement('entry');
        
        $title = (isset($item['name']) ? $item['name'] : (isset($item['title']) ? $item['title'] : ''));
        if ($module == 'users') {
            $title = $item['fname'] . ' ' . $item['mname'] . ' ' . $item['lname'];
        }
        $xml_item->appendChild($xmlDoc->createElement('title', $title));
        
        $description = (isset($item['description']) ? $item['description'] : '');
        $description = $xmlDoc->createCDATASection($description);
        $descriptionNode = $xmlDoc->createElement('summary');
        $descriptionNode->appendChild($description);
        $xml_item->appendChild($descriptionNode);
        
        $module_link = (strrpos($module, 's') == strlen($module) - 1) ? substr(
            $module, 0, strlen($module) - 1) : $module;
        $link = $core->basehref . '/' . $module_link . '/' . name_for_url(
            $title);
        if ($module == 'users') {
            $link = $core->basehref . '/profile/' . $item['username'];
        }
        $xml_item->appendChild($xmlDoc->createElement('link', $link));
        
        $pubDate = new DateTime();
        $pubDate->setTimestamp(strtotime($item['creation_ts']));
        $xml_item->appendChild(
            $xmlDoc->createElement('updated', 
                $pubDate->format("D, d M Y H:i:s P")));
        
        $feed->appendChild($xml_item);
    }
}
$xmlDoc->appendChild($feed);

echo $xmlDoc->saveXML();
exit();
