<?php
$users_manager = managers_store::locate('users');
$users_workexperience_manager = managers_store::locate('users_workexperience');
$users_addresses_manager = managers_store::locate('users_addresses');
$user = new users(true);
$logged = true;
if (! $auth_manager->validate()) {
    $logged = false;
}
if (isset($_GET['id']) or isset($params[0])) {
    $data = '';
    if (isset($_GET['id'])) {
        $data = $_GET['id'];
    } else {
        $data = $params[0];
    }
    
    try {
        $user = $users_manager->get($data);
    } catch (auth_exception $e) {
        redirect_url($core->basehref . '/login');
    } catch (users_exception $e) {
        $user = null;
    }
    if (! is_null($user)) {
        if (! $logged and ! $user->is_public) {
            redirect_url($core->basehref . '/login');
        }
    }
} elseif ($logged) {
    $user = $users_manager->my();
} else {
    redirect_url($core->basehref . '/login');
}

$title = "Not Found - My Profile";

$myself = false;
if (! is_null($user) and ! is_null($core->session->getUser())) {
    if ($user->getId() == $core->session->getUser()->getId()) {
        $myself = true;
    }
}

if (! is_null($user)) {
    $title = $user->getName() . ' - My Profile';
    $user = $user->jsonSerialize();
    try {
        $users_workexperience = $users_workexperience_manager->get($user['id']);
        if (isset($users_workexperience) and
             isset($users_workexperience['workexperience'])) {
            $user['workexperience'] = $users_workexperience['workexperience'];
        }
    } catch (sai_exception $e) {}
    try {
        $users_address = $users_addresses_manager->get($user['id']);
        if (isset($users_address) and isset($users_address['addresses'])) {
            $user['addresses'] = $users_address['addresses'];
        }
    } catch (sai_exception $e) {}
}

$page->slug = 'noprofile';
$page->title = $title;

if (! is_null($user) and is_array($user)) {
    $assign['user'] = $user;
    $assign['myself'] = $myself;
    $page->slug = 'profile';
}