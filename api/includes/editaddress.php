<?php
$users_addresses_manager = managers_store::locate('users_addresses');
if (isset($_POST) and count($_POST) > 0) {
    try {
        $address->loadArgs();
        $users_addresses_manager->setObject($address);
        if (isset($_GET['id'])) {
            $ret = $users_addresses_manager->update();
        } else {
            $ret = $users_addresses_manager->create();
        }
        if (isset($ret['msg'])) {
            $core->session->setSuccess($ret['msg']);
        } else {
            $core->session->setError($ret['error_msg']);
        }
    } catch (users_addresses_exception $e) {
        $core->session->setError($e->getExceptionMessage());
    }
}
$my_address = $users_addresses_manager->my();
$final_address = array();
if (isset($my_address['addresses'])) {
    foreach ($my_address['addresses'] as $key => $value) {
        $final_address[$value['address_type']] = $value;
    }
}
$assign['addresses'] = json_encode($final_address);
?>