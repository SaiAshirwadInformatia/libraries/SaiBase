<?php
define('GUI_ADMIN', GUI_TEMPLATE . DS . 'admin' . DS);
define('API_ADMIN', SAI_ABS_PATH . 'api' . DS . 'includes' . DS . 'admin' . DS);
$core->i18n->changeLocale('en-US');
$sai_smarty->template = 'admin';
$sai_smarty->setTemplateDir(GUI_TEMPLATE . '/admin');

if (! is_null($core->session->getUser()) and
     ! is_null($core->session->getUser()->creation_ts)) {
    $admin_nav = const_get('admin_nav');
    $assign['nav'] = $admin_nav;
    $auth_manager = managers_store::locate('auth');
    if (! $auth_manager->validateAdmin()) {
        $page->slug = 'login';
    } else {
        $module = '';
        $action = '';
        if (isset($params[0])) {
            $module = $params[0];
            $assign['module'] = $module;
        }
        if (isset($params[1])) {
            $action = $params[1];
        }
        $assign['action'] = $action;
        if ($action == 'create') {
            $action = 'edit';
        }
        if ($module != '') {
            $slug = $module;
            if (! file_exists(GUI_ADMIN . $slug)) {
                $assign['module_req'] = $slug;
                $slug = 'filenotfound';
            } else {
                if (file_exists(API_ADMIN . $slug . '.php')) {
                    require_once API_ADMIN . $slug . '.php';
                } else {
                    if ($action == '') {
                        $action = 'home';
                    }
                    $slug .= DS . $action;
                    if (file_exists(API_ADMIN . $slug . '.php')) {
                        require_once API_ADMIN . $slug . '.php';
                    } elseif (file_exists(
                        API_ADMIN . 'common' . DS . $action . '.php')) {
                        require_once API_ADMIN . 'common' . DS . $action . '.php';
                    }
                    if (! file_exists(GUI_ADMIN . $slug . '.tpl')) {
                        if (file_exists(
                            GUI_ADMIN . 'common' . DS . $action . '.tpl')) {
                            $slug = 'common' . DS . $action;
                        } else {
                            $assign['module_req'] = $module;
                            $assign['file_req'] = $action . '.tpl';
                            $slug = 'filenotfound';
                        }
                    }
                }
            }
        } else {
            $slug = 'home';
        }
        $page->slug = $slug;
    }
    $assign['nav'] = const_get('admin_nav');
} else {
    header("Location: " . $core->basehref . "/login");
}
/**
 * Forcefully change template details to admin
 * This avoid invalid dynamically changed value errors
 *
 * Like save configuration affected template name
 */
$assign['template'] = $core->basehref . $core->configs->templates . '/' .
     $sai_smarty->template;