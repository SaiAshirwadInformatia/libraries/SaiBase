<?php
$users = new users(false, false);
$users->loadObject($core->session->getUser()
    ->getId());
if (isset($_POST) and count($_POST) > 0) {
    $users->loadArgs();
    $users_manager = managers_store::locate('users');
    $users_manager->setObject($users);
    $ret = $users_manager->update();
    if (! isset($ret['error_code'])) {
        $core->session->setSuccess($ret['msg']);
        $assign['current_user'] = $users->jsonSerialize();
    } else {
        $core->session->setError($ret['msg']);
    }
}
$assign['user'] = $users->jsonSerialize();