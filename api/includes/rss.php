<?php
header("Content-Type: application/xml");

$rss_modules = array(
    'events',
    'shradhanjali',
    'temples',
    'recipes',
    'users'
);

if (isset($_GET['category'])) {
    $rss_modules = explode(",", $_GET['category']);
}

$xmlDoc = new DOMDocument("1.0", "UTF-8");
$root = $xmlDoc->createElement('rss');
$root->setAttribute("version", "2.0");
$channel = $xmlDoc->createElement('channel');

$channel->appendChild($xmlDoc->createElement('title', 'Ghanerao RSS Feeds'));
$channel->appendChild(
    $xmlDoc->createElement('description', 
        'Ghanerao a Village based Community for connecting every individual together with the help of web'));
$channel->appendChild($xmlDoc->createElement('link', 'http://ghanerao.in'));
$channel->appendChild(
    $xmlDoc->createElement('category', implode(" ", $rss_modules)));
$channel->appendChild(
    $xmlDoc->createElement('lastBuildDate', 
        (new DateTime())->format("D, d M Y H:i:s P")));
$channel->appendChild(
    $xmlDoc->createElement('generator', 
        'Sai Ashirwad Informatia RSS Feeds Generator (http://saiashirwad.com)'));

foreach ($rss_modules as $module) {
    $manager = managers_store::locate($module);
    $items = $manager->all();
    foreach ($items as $item) {
        $xml_item = $xmlDoc->createElement('item');
        
        $title = (isset($item['name']) ? $item['name'] : (isset($item['title']) ? $item['title'] : ''));
        if ($module == 'users') {
            $title = $item['fname'] . ' ' . $item['mname'] . ' ' . $item['lname'];
        }
        $xml_item->appendChild($xmlDoc->createElement('title', $title));
        
        $description = (isset($item['description']) ? $item['description'] : '');
        $description = $xmlDoc->createCDATASection($description);
        $descriptionNode = $xmlDoc->createElement('description');
        $descriptionNode->appendChild($description);
        $xml_item->appendChild($descriptionNode);
        
        $module_link = (strrpos($module, 's') == strlen($module) - 1) ? substr(
            $module, 0, strlen($module) - 1) : $module;
        $link = $core->basehref . '/' . $module_link . '/' . name_for_url(
            $title);
        if ($module == 'users') {
            $link = $core->basehref . '/profile/' . $item['username'];
        }
        $xml_item->appendChild($xmlDoc->createElement('link', $link));
        
        $category = str_replace("_", "/", $module);
        $categoryNode = $xmlDoc->createElement('category', $category);
        $categoryNode->setAttribute("domain", "www.ghanerao.in");
        $xml_item->appendChild($categoryNode);
        
        $pubDate = new DateTime();
        $pubDate->setTimestamp(strtotime($item['creation_ts']));
        $xml_item->appendChild(
            $xmlDoc->createElement('pubDate', 
                $pubDate->format("D, d M Y H:i:s P")));
        
        $channel->appendChild($xml_item);
    }
}
$root->appendChild($channel);
$xmlDoc->appendChild($root);

echo $xmlDoc->saveXML();
exit();
