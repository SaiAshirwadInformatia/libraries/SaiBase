<?php
$html = '';
if (isset($_GET['t']) and isset($_GET['a'])) {
    $captcha = new Captcha\Captcha();
    $captcha->setPrivateKey('6Lc97vMSAAAAALEomjl-8pOD3JY9qFqEJgCERgRa');
    $captcha->setPublicKey('6Lc97vMSAAAAAD84G_fozuVPACvoQq7MtEuZN4DU');
    // set a remote IP if the remote IP can not be found via $_SERVER['REMOTE_ADDR']
    if (! isset($_SERVER['REMOTE_ADDR'])) {
        $captcha->setRemoteIp('192.168.1.1');
    }
    $user_manager = managers_store::locate('users');
    try {
        $user = $user_manager->getByToken($_GET['t']);
        
        if (! is_null($user)) {
            $show_recaptcha = true;
            if (isset($_POST['submit'])) {
                $response = $captcha->check();
                
                if (! $response->isValid()) {
                    // What happens when the CAPTCHA was entered incorrectly
                    $html .= "<p>The reCAPTCHA wasn't entered correctly. " .
                         "(reCAPTCHA said: " . $response->getError() . ")</p>";
                } else {
                    $user->is_active = 1;
                    $core->session->setParam('currentUser', $user);
                    $core->db->update(
                        array(
                            'tablename' => 'users',
                            'set' => array(
                                'is_active' => 1
                            ),
                            'where' => array(
                                'data' => array(
                                    'id' => $user->getId()
                                ),
                                'condition' => 'AND'
                            )
                        ));
                    $show_recaptcha = false;
                    $roles_manager = managers_store::locate('roles');
                    $users_roles_manager = managers_store::locate('users_roles');
                    $role = $roles_manager->get('Member');
                    $users_roles = new users_roles();
                    $users_roles->loadObjectInstance(
                        array(
                            'user_id' => $user->getId(),
                            'role_id' => $role->getId()
                        ));
                    $users_roles_manager->create($users_roles);
                    $user->loadRights();
                    $core->session->setParam('currentUser', $user);
                    $core->setUser($user);
                    $html .= '<script>$(document).ready(function () {
					sai.showDialog("Validation Successful","Thank You for confirming your email address.<br />Your account is ready to access now, please close this box",null,function () {window.location = "profile";});
				});</script>';
                }
            }
            if ($show_recaptcha) {
                $html .= '<h2>Please verify your account by entering the following code</h2>';
                $html .= '<form method="post" action="">';
                
                // you got this from the signup page
                $html .= $captcha->html();
                
                $html .= '<input class="btn btn-default" type="submit" name="submit" /></form>';
            }
        }
    } catch (users_exception $e) {
        $html = '<p>Invalid validation code, please contact System Administrator to resend new validation code.</p>';
        $html .= '<p><a href="index">Back to Home</a></p>';
    }
} else {
    header("Location: index");
    exit();
}
$assign['html'] = $html;
