<?php

/**
 * Fixed constant values referenced ahead
 *
 * @author Rohan Sakhale
 * @author Chaitali Patil
 * @copyright 2014 Sai Ashirwad Informatia
 * @since WillingTree v1
 * @version Release: 0.1
 *
 */

/**
 *
 * @var THIRD_PARTY_DIR
 */
define('THIRD_PARTY_DIR', SAI_ABS_PATH . 'vendor');

/**
 *
 * @var LOCALE_DIR
 */
define('LOCALE_DIR', SAI_ABS_PATH . 'locales');

/**
 *
 * @var REGISTRY_DIR
 */
define('REGISTRY_DIR', 
    SAI_ABS_PATH . 'api' . DS . 'library' . DS . 'Sai' . DS . 'Registry');

/**
 *
 * @var JS_ASSETS
 */
define('JS_ASSETS', SAI_ABS_PATH . 'assets' . DS . 'js');

/**
 *
 * @var IMAGES_ASSETS
 */
define('IMAGES_ASSETS', SAI_ABS_PATH . 'assets' . DS . 'images');

/**
 * Smarty Configuration Constants
 */
define('GUI_CONFIG', SAI_ABS_PATH . 'gui' . DS . 'config');
define('GUI_CACHE', SAI_ABS_PATH . 'gui' . DS . 'cache');
define('GUI_TEMPLATE', SAI_ABS_PATH . 'gui' . DS . 'templates');
define('GUI_COMPILE', SAI_ABS_PATH . 'gui' . DS . 'templates_c');

/**
 * GUI Logical include path
 */
define('GUI_LOGIC_INCLUDE', SAI_ABS_PATH . 'api' . DS . 'includes');

/**
 * Object states
 */
define('ACTIVE', 1);
define('INACTIVE', 0);
define('DELETED', - 1);

/**
 * Success status
 */
define('OK', 'OK');
define('NOT_OK', 'NOT_OK');

/**
 *
 * @var MALE
 */
define('MALE', 'm');
/**
 *
 * @var FEMALE
 */
define('FEMALE', 'f');

/**
 */
$sai_allowed_photo_file_type = array(
    "jpg" => "image/jpg",
    "jpeg" => "image/jpeg",
    "png" => "image/png"
);

/**
 */
$sai_object_states = array(
    "active" => ACTIVE,
    "inactive" => INACTIVE,
    "deleted" => DELETED
);

$sai_gender = array(
    "male" => MALE,
    "female" => FEMALE
);

$sai_r_gender = array_flip($sai_gender);

/**
 * Marital Status
 */
$sai_marital_status = array(
    "SINGLE" => "marital_single",
    "MARRIED" => "marital_married",
    "DIVORCED" => "marital_divorced",
    "WIDOWED" => "marital_widowed"
);

/**
 * Supported Time in minutes
 */
$sai_time_minutes = array(
    "5" => "minute_5",
    "10" => "minute_10",
    "15" => "minute_15",
    "20" => "minute_20",
    "30" => "minute_30",
    "45" => "minute_45",
    "60" => "minute_60",
    "75" => "minute_75",
    "90" => "minute_90",
    "120" => "minute_120"
);

/**
 * Supported Address Type
 */
$sai_address_types = array(
    'Residential' => 'residential_address',
    'Office' => 'office_address',
    'Correspondence' => 'correspondence_address'
);

/**
 * Supported Salutations
 */
$sai_salutations = array(
    'master' => 'lbl_master',
    'miss' => 'lbl_miss',
    'mr' => 'lbl_mr',
    'mrs' => 'lbl_mrs',
    'smt' => 'lbl_smt',
    'late' => 'lbl_late'
);

$sai_salutations_linkage = array(
    'm' => 'mr',
    'f' => 'miss',
    'single_m' => 'master',
    'single_f' => 'miss',
    'married_m' => 'mr',
    'married_f' => 'mrs',
    'divorced_m' => 'mr',
    'divorced_f' => 'smt',
    'widowed_m' => 'mr',
    'widowed_f' => 'smt'
);

/**
 * Supported locales
 */
$sai_locales = array(
    "en-US" => "English (US)",
    "hi-IN" => "Hindi (IN)"
);
/**
 */
$sai_admin_nav = array(
    "system" => array(
        "label" => "System Configuration",
        "icon" => "cog",
        "childs" => array(
            "configs" => array(
                "label" => "Site Configuration",
                "icon" => "cog",
                "childs" => array(
                    "add" => array(
                        "label" => "Add Config",
                        "icon" => "floppy-save",
                        "url" => "admincp/configs/create"
                    ),
                    "edit" => array(
                        "label" => "Update Configs",
                        "icon" => "pencil",
                        "url" => "admincp/configs/update"
                    ),
                    "viewall" => array(
                        "label" => "View Configs",
                        "icon" => "eye-open",
                        "url" => "admincp/configs/viewall"
                    )
                )
            ),
            "roles" => array(
                "label" => "Roles",
                "icon" => "user",
                "childs" => array(
                    "create" => array(
                        "label" => "Create Role",
                        "icon" => "floppy-save",
                        "url" => "admincp/roles/create"
                    ),
                    "view_all" => array(
                        "label" => "View All Roles",
                        "icon" => "eye-open",
                        "url" => "admincp/roles/viewall"
                    ),
                    "view_deleted" => array(
                        "label" => "View Deleted Roles",
                        "icon" => "floppy-remove",
                        "url" => "admincp/roles/deleted"
                    )
                )
            ),
            "rights" => array(
                "label" => "Rights",
                "icon" => "user",
                "childs" => array(
                    "create" => array(
                        "label" => "Create Right",
                        "icon" => "floppy-save",
                        "url" => "admincp/rights/create"
                    ),
                    "view_all" => array(
                        "label" => "View All Rights",
                        "icon" => "eye-open",
                        "url" => "admincp/rights/viewall"
                    ),
                    "view_deleted" => array(
                        "label" => "View Deleted Rights",
                        "icon" => "floppy-remove",
                        "url" => "admincp/rights/deleted"
                    )
                )
            ),
            "pages" => array(
                "label" => "Pages",
                "icon" => "book",
                "childs" => array(
                    "create" => array(
                        "label" => "Create Page",
                        "icon" => "floppy-save",
                        "url" => "admincp/pages/create"
                    ),
                    "view_all" => array(
                        "label" => "View All Pages",
                        "icon" => "eye-open",
                        "url" => "admincp/pages/viewall"
                    ),
                    "view_deleted" => array(
                        "label" => "View Deleted Pages",
                        "icon" => "floppy-remove",
                        "url" => "admincp/pages/deleted"
                    )
                )
            ),
            "menus" => array(
                "label" => "Menus",
                "icon" => "th-list",
                "childs" => array(
                    "create" => array(
                        "label" => "Create Menu",
                        "icon" => "floppy-save",
                        "url" => "admincp/menus/create"
                    ),
                    "view_all" => array(
                        "label" => "View All Menus",
                        "icon" => "eye-open",
                        "url" => "admincp/menus/viewall"
                    ),
                    "view_deleted" => array(
                        "label" => "View Deleted Menus",
                        "icon" => "floppy-remove",
                        "url" => "admincp/menus/deleted"
                    )
                )
            ),
            "feedback" => array(
                "label" => "Feedback",
                "icon" => "comment",
                "childs" => array(
                    "create" => array(
                        "label" => "Create Feedback",
                        "icon" => "floppy-save",
                        "url" => "admincp/feedback/create"
                    ),
                    "view_all" => array(
                        "label" => "View All Feedback",
                        "icon" => "eye-open",
                        "url" => "admincp/feedback/viewall"
                    ),
                    "view_deleted" => array(
                        "label" => "View Deleted Feedback",
                        "icon" => "floppy-remove",
                        "url" => "admincp/feedback/deleted"
                    )
                )
            ),
            "translations" => array(
                "label" => "Translations",
                "icon" => "globe",
                "childs" => array(
                    "create" => array(
                        "label" => "Create Translation",
                        "icon" => "floppy-save",
                        "url" => "admincp/translations/create"
                    ),
                    "view_all" => array(
                        "label" => "View All Translation",
                        "icon" => "eye-open",
                        "url" => "admincp/translations/viewall"
                    ),
                    "view_deleted" => array(
                        "label" => "View Deleted Translations",
                        "icon" => "floppy-remove",
                        "url" => "admincp/translations/deleted"
                    )
                )
            )
        )
    ),
    "users" => array(
        "label" => "Users",
        "icon" => "user",
        "childs" => array(
            "create" => array(
                "label" => "Create User",
                "icon" => "floppy-save",
                "url" => "admincp/users/create"
            ),
            "view_all" => array(
                "label" => "View All Users",
                "icon" => "eye-open",
                "url" => "admincp/users/viewall"
            ),
            "view_deleted" => array(
                "label" => "View Deleted Users",
                "icon" => "floppy-remove",
                "url" => "admincp/users/deleted"
            ),
            "awaiting" => array(
                "label" => "Users Awaiting Activation",
                "icon" => "warning-sign",
                "url" => "admincp/users/awaiting"
            )
        )
    ),
    "albums" => array(
        "label" => "Gallery",
        "icon" => "th-large",
        "childs" => array(
            "create_album" => array(
                "label" => "Create Album",
                "icon" => "floppy-save",
                "url" => "admincp/albums/create"
            ),
            "view_all_albums" => array(
                "label" => "View All Albums",
                "icon" => "eye-open",
                "url" => "admincp/albums/viewall"
            ),
            "create_photo" => array(
                "label" => "Create Albums Photo",
                "icon" => "floppy-save",
                "url" => "admincp/albums/createphoto"
            ),
            "view_all_photos" => array(
                "label" => "View All Photos",
                "icon" => "eye-open",
                "url" => "admincp/albums/viewallphotos"
            ),
            "view_deleted_albums" => array(
                "label" => "View Deleted Albums",
                "icon" => "floppy-remove",
                "url" => "admincp/albums/deleted"
            ),
            "view_deleted_photos" => array(
                "label" => "View Deleted Photos",
                "icon" => "floppy-remove",
                "url" => "admincp/albums/deletedphotos"
            )
        )
    ),
    "downloads" => array(
        "label" => "Downloads",
        "icon" => "download-alt",
        "childs" => array(
            "create" => array(
                "label" => "Create Download",
                "icon" => "floppy-save",
                "url" => "admincp/downloads/create"
            ),
            "view_all" => array(
                "label" => "View All Downloads",
                "icon" => "eye-open",
                "url" => "admincp/downloads/viewall"
            ),
            "view_deleted" => array(
                "label" => "View Deleted Downloads",
                "icon" => "floppy-remove",
                "url" => "admincp/downloads/deleted"
            )
        )
    )
);

$sai_bloodgroup = array(
    "A+ve",
    "B+ve",
    "AB+ve",
    "O+ve",
    "A-ve",
    "B-ve",
    "AB-ve",
    "O-ve"
);
