<?php
namespace Sai\Base;

class SmartyPlugins
{

    public $core;

    /**
     * Construct object for Sai Smarty Plugins and instantiate core object
     */
    public function __construct()
    {
        $this->core = Core::getInstance();
    }

    /**
     */
    public function getLang($params, &$smarty)
    {
        if (isset($params['var'])) {
            $strs = array();
            if (strpos($params['s'], ',') !== FALSE) {
                $strs = explode(",", $params['s']);
            } else {
                $strs = array(
                    $params['s']
                );
            }
            $labels = array();
            foreach ($strs as $str) {
                $str = trim($str);
                $labels[$str] = $this->core->i18n->get_lang($str);
            }
            $smarty->assign($params['var'], $labels);
        } else {
            return $this->core->i18n->get_lang($params['s']);
        }
    }

    /**
     *
     * @param unknown $params            
     * @param unknown $smarty            
     * @return string
     */
    public function getBootstrapPageNavigation($params, &$smarty)
    {
        $baseUrl = $params['baseUrl'];
        $currentPage = $params['currentPage'];
        $maxPages = $params['maxPages'];
        $html = '<div><h5>Page ' . $currentPage . ' of ' . $maxPages .
             '</h5><ul class="pagination">';
        $diff = $maxPages - $currentPage;
        if ($currentPage > 4) {
            $html .= '<li><a href="' . $baseUrl .
                 '&page=1" title="Navigate to first page"> &laquo; </a></li>';
        }
        if ($currentPage != 1) {
            $html .= '<li><a href="' . $baseUrl . '&page=' . ($currentPage - 1) .
                 '" title="Navigate to previous page"> &lt; </a></li>';
        }
        if ($maxPages < 9) {
            for ($i = 1; $i <= $maxPages; $i ++) {
                $html .= '<li';
                if ($i == $currentPage) {
                    $html .= ' class="active"';
                }
                $html .= '>';
                $html .= '<a href="' . $baseUrl . '&page=' . $i . '">' . $i .
                     '</a>';
                $html .= '</li> ';
            }
        } elseif ($currentPage > 4 and ($maxPages - $currentPage) > 5) {
            $html .= '<li class="disabled"><a href="#">. . .</a></li>';
            for ($i = $currentPage - 3; $i <= $currentPage + 3; $i ++) {
                $html .= '<li';
                if ($i == $currentPage) {
                    $html .= ' class="active"';
                }
                $html .= '>';
                $html .= '<a href="' . $baseUrl . '&page=' . $i . '">' . $i .
                     '</a>';
                $html .= '</li> ';
            }
            $html .= '<li class="disabled"><a href="#">. . .</a></li>';
        } elseif (($maxPages - $currentPage) <= 5) {
            $html .= '<li class="disabled"><a href="#">. . .</a></li>';
            for ($i = $maxPages - 6; $i <= $maxPages; $i ++) {
                $html .= '<li';
                if ($i == $currentPage) {
                    $html .= ' class="active"';
                }
                $html .= '>';
                $html .= '<a href="' . $baseUrl . '&page=' . $i . '">' . $i .
                     '</a>';
                $html .= '</li> ';
            }
        } else {
            for ($i = 1; $i <= $currentPage + 4; $i ++) {
                $html .= '<li';
                if ($i == $currentPage) {
                    $html .= ' class="active"';
                }
                $html .= '>';
                $html .= '<a href="' . $baseUrl . '&page=' . $i . '">' . $i .
                     '</a>';
                $html .= '</li> ';
            }
            $html .= '<li class="disabled"><a href="#">. . .</a></li>';
            for ($i = $maxPages - 3; $i <= $maxPages; $i ++) {
                if ($i == $currentPage)
                    $html .= '<li class="active">' . $i . '</li> ';
                else {
                    $html .= '<li>';
                    $html .= '<a href="' . $baseUrl . '&page=' . $i . '">' . $i .
                         '</a>';
                    $html .= '</li> ';
                }
            }
        }
        if ($currentPage != $maxPages) {
            $html .= '<li><a href="' . $baseUrl . '&page=' . ($currentPage + 1) .
                 '" title="Navigate to next page"> &gt; </a></li>';
        }
        if ($currentPage < ($maxPages - 3)) {
            $html .= '<li><a href="' . $baseUrl . '&page=' . $maxPages .
                 '" title="Navigate to last page"> &raquo; </a></li>';
        }
        $html .= '</ul></div>';
        return $html;
    }

    /**
     * Truncate string by length and append character at the end
     *
     * @param array $params            
     * @param object $smarty            
     * @return string
     */
    public function getTruncatedString($params, &$smarty)
    {
        $len = 100;
        $str = "";
        $append = "&hellip;";
        if (isset($params['str'])) {
            $str = $params['str'];
        }
        if (isset($params['len'])) {
            $len = $params['len'];
        }
        if (isset($params['append'])) {
            $append = $params['append'];
        }
        return truncate($str, $len, $append);
    }

    /**
     *
     * @param array $params            
     * @param object $smarty            
     * @return mixed
     */
    public function getNameForURL($params, &$smarty)
    {
        $str = "";
        if (isset($params['str'])) {
            $str = $params['str'];
        }
        $str = name_for_url($str);
        return $str;
    }

    /**
     *
     * @param array $params            
     * @param object $smarty            
     * @return (mixed)
     */
    public function getConst($params, &$smarty)
    {
        if (isset($params['name'])) {
            $names = explode(",", $params['name']);
            foreach ($names as $name) {
                $smarty->assign($name, const_get($name));
            }
        } else {
            throw new exception("Missing parameter `name`");
        }
    }

    /**
     *
     * @param array $params            
     * @param object $smarty            
     * @return array
     */
    public function getAge($params, &$smarty)
    {
        if (isset($params['start']) and isset($params['var'])) {
            $start = $params['start'];
            $end = null;
            if (isset($params['end'])) {
                $end = $params['end'];
            }
            $smarty->assign($params['var'], get_age($start, $end));
        }
    }
}
