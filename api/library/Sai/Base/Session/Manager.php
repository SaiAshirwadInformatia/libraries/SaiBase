<?php
namespace Sai\Base\Session;

use Sai\Base;

/**
 *
 * @name Session Manager
 * @abstract Class to manage session related activities and start and destroy session if required
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager
{

    public $ua;

    public $ip;

    private static $instance;

    /**
     * Private constructor to avoid recreation of object
     * Follows Singleton Design Pattern
     */
    private function __construct()
    {
        $this->initSession();
    }

    /**
     * According to Singleton Design Pattern
     * Cloning of object should be disallowed
     * Hence, clone method is made private
     */
    private function __clone()
    {
        throw new Base\Session\Exception(
            Base\Session\Exception::CLONE_NOT_ALLOWED);
    }

    /**
     */
    public function initSession()
    {
        $this->overwrite = true;
        $this->sid_name = "SAI_PHPSESSIONID_" . strtoupper(DB_NAME);
        $this->session_duration = 1800;
        $this->session_max_duration = 3600;
        $this->use_cookie = true;
        $this->hijackBlock = true;
        /**
         * Overwrite Existing Session
         */
        if ($this->overwrite) {
            // Make sure session cookies expire when we want it to expires
            // ini_set('session.cookie_lifetime', $this -> session_duration);
            // set the value of the garbage collector
            ini_set('session.gc_maxlifetime', $this->session_max_duration);
            // set the session name to our fantastic name
            // ini_set('session.name', $this -> sid_name);
            
            session_name($this->sid_name);
            // Set SSL level
            $https = isset($secure) ? $secure : isset($_SERVER['HTTPS']);
            // Set session cookie options
            session_set_cookie_params(0, '/', null, $https, true);
            session_cache_expire(15);
            session_start();
            // Make sure the session hasn't expired, and destroy it if it has
            if ($this->validateSession()) {
                // Check to see if the session is new or a hijacking attempt
                if (! $this->preventHijacking()) {
                    // Reset session data and regenerate id
                    if (isset($_SERVER['REMOTE_ADDR']) and
                         isset($_SERVER['HTTP_USER_AGENT'])) {
                        $_SESSION['IPaddress'] = $_SERVER['REMOTE_ADDR'];
                        $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
                        $_SESSION['currentUser'] = null;
                        $this->regenerateSession();
                    }
                    // Give a 0% chance of the session id changing on any request
                }
            } else {
                session_destroy();
                session_start();
            }
        }
    }

    private function validateSession()
    {
        if (isset($_SESSION['OBSOLETE']) && ! isset($_SESSION['EXPIRES']))
            return false;
        if (isset($_SESSION['EXPIRES']) && $_SESSION['EXPIRES'] < time())
            return false;
        return true;
    }

    private function preventHijacking()
    {
        if (! isset($_SESSION['IPaddress']) || ! isset($_SESSION['userAgent']))
            return false;
        if ($_SESSION['IPaddress'] != $_SERVER['REMOTE_ADDR'])
            return false;
        if ($_SESSION['userAgent'] != $_SERVER['HTTP_USER_AGENT'])
            return false;
        return true;
    }

    private function regenerateSession()
    {
        // If this session is obsolete it means there already is a new id
        if (isset($_SESSION['OBSOLETE']) && $_SESSION['OBSOLETE'] == true)
            return;
            // Set current session to expire in 10 seconds
        $_SESSION['OBSOLETE'] = true;
        $_SESSION['EXPIRES'] = time() + 10;
        // Create new session without destroying the old one
        session_regenerate_id(false);
        // Grab current session ID and close both sessions to allow other scripts to use them
        $newSession = session_id();
        session_write_close();
        // Set session ID to the new one, and start it back up again
        session_id($newSession);
        session_cache_expire(15);
        session_start();
        // Now we unset the obsolete and expiration values for the session we want to keep
        unset($_SESSION['OBSOLETE']);
        unset($_SESSION['EXPIRES']);
    }

    /**
     * By the convention of Singleton design pattern
     * getSession creates single instance if not found and returns the same
     *
     * @return Object of self class
     */
    public static function getSession()
    {
        if (! isset(self::$instance) || is_null(self::$instance)) {
            self::$instance = new Manager();
        }
        return self::$instance;
    }

    /**
     */
    public function setParam($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * Retrieves parameter stored by the given name
     *
     * @param string $name
     *            of the parameter to be stored in session
     * @return mixed value that was stored previously
     */
    public function getParam($name)
    {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    /**
     * Retrieves user object if logged else null is returned
     *
     * @return object of users class or null
     */
    public function getUser()
    {
        return isset($_SESSION['currentUser']) ? $_SESSION['currentUser'] : null;
    }

    /**
     * Checks if any message exists in session like success or error
     *
     * @return boolean if any of message found in session
     */
    public function hasMsg()
    {
        return (isset($_SESSION['error']) and ! empty($_SESSION['error'])) or
             (isset($_SESSION['success']) and ! empty($_SESSION['success']));
    }

    /**
     * Sets error message in session
     *
     * @param string $msg
     *            to be stored in session as error
     */
    public function setError($msg)
    {
        $_SESSION['error'] = $msg;
    }

    /**
     * Retrieves error message if stored in session previously
     *
     * @return string as error message
     */
    public function getError()
    {
        $error = isset($_SESSION['error']) ? $_SESSION['error'] : null;
        return $error;
    }

    /**
     * Sets success message in session
     *
     * @param string $msg
     *            to be stored in session as success
     */
    public function setSuccess($msg)
    {
        $_SESSION['success'] = $msg;
    }

    /**
     * Retrieves success message if stored in session previously
     *
     * @return string as success message
     */
    public function getSuccess()
    {
        $success = isset($_SESSION['success']) ? $_SESSION['success'] : null;
        return $success;
    }

    /**
     * Clears all messages if stored in session
     * Usually this method should be called just before the page end to avoid unwanted clearing of messages
     */
    public function clearMsgs()
    {
        if (array_key_exists('error', $_SESSION)) {
            unset($_SESSION['error']);
        }
        if (array_key_exists('success', $_SESSION)) {
            unset($_SESSION['success']);
        }
    }
}
