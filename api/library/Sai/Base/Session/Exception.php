<?php
namespace Sai\Base\Session;

/**
 *
 * @name Session Exception
 * @abstract Session exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Sai\Base\Exception
{

    const INVALID_SESSION_IDENTIFIED = 1107;

    const CLONE_NOT_ALLOWED = 1108;

    public function __construct($exception_number)
    {
        $this->exception_list = array(
            self::INVALID_SESSION_IDENTIFIED => "excp_session_invalid_identified",
            self::CLONE_NOT_ALLOWED => "excp_session_clone_not_allowed"
        );
        $this->exception_number = $exception_number;
    }
}
