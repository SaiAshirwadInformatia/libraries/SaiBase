<?php
namespace Sai\Base\Translations;

/**
 *
 * @name i18n
 * @abstract Internalization Localization Baby Doll :D
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager
{

    /**
     *
     * @var string $html_locale Parsed value of Locale code for adding over `html lang`
     */
    public $html_locale;

    /**
     *
     * @var array $languages List of allowed or known languages by the system
     */
    public $languages;

    /**
     *
     * @var string $locale Currently selected Locale code
     */
    public $locale;

    /**
     *
     * @var array $g_strings List of strings collected from currently selected locale
     */
    public $g_strings;

    /**
     *
     * @var string $accept_lang Accept-Language from Headers
     */
    public $accept_lang;

    /**
     *
     * @param string $locale
     *            Locale code loaded from the configuration
     * @param string $accept_lang
     *            Locale code identified from Request Headers
     */
    public function __construct($locale = 'en-US', $accept_lang = null)
    {
        $this->locale = $locale;
        $this->accept_lang = $accept_lang;
        
        /**
         * Locale priortization SESSION, COOKIE, System DEFAULT
         */
        $this->locale = isset($_SESSION['locale']) ? $_SESSION['locale'] : (isset(
            $_COOKIE['locale']) ? $_COOKIE['locale'] : $this->locale);
        $this->languages = json_decode(
            file_get_contents(REGISTRY_DIR . DS . 'languages.json'), true);
        $this->checkChangeLocale();
        $this->loadLocales($this->locale);
    }

    /**
     * Parse Locale code for HTML Tag
     */
    public function loadHTMLLocale()
    {
        $l = explode("-", $this->locale);
        $this->html_locale = $l[0];
    }

    /**
     * Save Locale code details in Session + Cookie
     */
    public function saveLocale()
    {
        $_SESSION['locale'] = $this->locale;
        setcookie('locale', $this->locale, time() + 60 * 60 * 24);
        $_COOKIE['locale'] = $this->locale;
    }

    /**
     * Identify if any change of Locale request is raised either from $_GET or from Request Accept-Lang
     */
    public function checkChangeLocale()
    {
        if (isset($_GET['locale']) and
             in_array($_GET['locale'], $this->languages)) {
            $this->changeLocale($_GET['locale']);
        } elseif (! is_null($this->accept_lang) and
             (! isset($_COOKIE['locale']) or ! isset($_SESSION['locale']))) {
            $locale_c = str_replace("_", "-", 
               \Locale::acceptFromHttp($this->accept_lang));
            foreach ($this->languages as $lang) {
                if (strpos($lang, $locale_c) !== false) {
                    $this->changeLocale($lang);
                    break;
                }
            }
        }
    }

    /**
     * Helps to load out strings of language based on locale code
     *
     * @param string $locale
     *            Language strings to be loaded based on this locale code parameter
     */
    public function loadLocales($locale)
    {
        $this->locale = $locale;
        $this->loadHTMLLocale();
        $locale_dir = LOCALE_DIR . DS . $this->locale . DS;
        if (file_exists($locale_dir)) {
            include_once $locale_dir . 'texts.php';
            include_once $locale_dir . 'description.php';
            include_once $locale_dir . 'errors.php';
        }
        $a = get_defined_vars();
        foreach ($a as $_a => $_v) {
            if (strpos($_a, 'sai_i18n_') !== FALSE) {
                $this->g_strings[str_replace('sai_i18n_', '', $_a)] = $_v;
            }
        }
        \Locale::setDefault($this->locale);
    }

    /**
     * If Change occurs previously use this method to change the language strings and also save the locale code
     *
     * @param string $locale_code
     *            Locale code used to change the Language
     */
    public function changeLocale($locale_code)
    {
        if (in_array($locale_code, $this->languages)) {
            $this->locale = $locale_code;
            $this->saveLocale();
            $this->loadLocales($locale_code);
        }
    }

    /**
     * Get the Language string based from Globally loaded strings based on locale selected
     *
     * @param string $key
     *            Uniquely identified string key
     * @return string:|NULL
     */
    public function get_lang($key)
    {
        if (isset($this->g_strings[$key])) {
            return $this->g_strings[$key];
        }
        return $key;
    }
}
