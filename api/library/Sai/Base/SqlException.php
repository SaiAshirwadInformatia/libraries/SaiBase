<?php
namespace Sai\Base;

/**
 *
 * @name SQL Exception
 * @abstract SQL exception handling
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class SqlException extends Exception
{

    const UNIQUE_CONSTRAIN_ERROR = 1062;

    const COLUMN_NOT_FOUND = 1054;

    public $sql_error_map;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->sql_error_map = array();
        $c = get_class($this);
        if (defined($c . '::UNIQUE')) {
            $this->sql_error_map[self::UNIQUE_CONSTRAIN_ERROR] = constant(
                $c . '::UNIQUE');
        }
        if (isset($this->sql_error_map[$exception_number])) {
            $this->exception_number = $this->sql_error_map[$exception_number];
        }
    }
}
