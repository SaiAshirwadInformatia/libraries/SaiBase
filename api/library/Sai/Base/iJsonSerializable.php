<?php
namespace Sai\Base;

/**
 * As JsonSerializable interface is supported from 5.4 and above
 *
 * This interface is introduce to avoid issues due to PHP Version
 * jsonSerialize method is currently supposed to be defined and called manually
 *
 * As the PHP Version would be updated on server and other areas
 * The manual calls would be removed and this interface would extend PHP JsonSerializable
 *
 * This would avoid replacing implement calls
 */
if (class_exists('JsonSerializable')) {

    interface iJsonSerializable extends JsonSerializable
    {
    }
} else {

    interface iJsonSerializable
    {

        public function jsonSerialize();
    }
}