<?php
namespace Sai\Base;

use Sai\Database\MySqliDB;
use Sai\Modules;

/**
 * Core represents the heart of the entire application that pumps out several stuff for every object life cycle.
 *
 * It helps share several things among every object removing out duplicacy and maintaining single place to gather stuff
 *
 * @name Core
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Core
{

    /**
     *
     * @var integer $current_year The Current Year that would be displayed over site footer for copyrights
     */
    public $current_year;

    /**
     *
     * @var object $configs Represents the site configuration which is easily configurable
     */
    public $configs;

    /**
     *
     * @var object $db Represents and maintains Database Connectivity
     */
    public $db;

    /**
     *
     * @var array $http_code Maintains the list of Valid HTTP Status Code with their message
     */
    public $http_code;

    /**
     *
     * @var object $user Currently logged user's object maintained
     */
    public $user;

    /**
     *
     * @var string $basehref Represents Base URL of the application
     */
    public $basehref;

    /**
     *
     * @var string $apihref Represents API URL of the application
     */
    public $apihref;

    /**
     *
     * @var object $session Maintains the session object of the application
     */
    public $session;

    /**
     *
     * @var array $from_email Maintains the details who will send this mail from System Application
     *     
     *      Contains two keys
     *      name => Name of the Sender
     *      email => Email of the Sender
     */
    public $from_email;

    /**
     *
     * @var array $request_headers Captures the special request headers that are parsed explicitly
     */
    public $request_headers;

    /**
     *
     * @var object $i18n Maintains the i18n Object instance to access the strings based on currently selected locale
     */
    public $i18n;

    /**
     *
     * @var array $allowed_modules Contains a list of Allowed Modules for our RESTFul service
     */
    private $allowed_modules;

    /**
     *
     * @var array $allowed_contenttypes Contains a list of Allowed Content Types that our service can parse
     */
    private $allowed_contenttypes;

    /**
     *
     * @var array $no_auth_calls Contains a list of Allowed REST Calls without authenticaiton depending on HTTP METHOD
     */
    private $no_auth_calls;

    /**
     *
     * @var object $_instance Maintains the self object as static private instance
     */
    private static $_instance;

    /**
     * Private constructor to avoid external creation of core object
     * Singleton Design Patter allows avoiding duplication of objects
     *
     * So single instance is maintained within the same class as a private static variable
     *
     * Usage
     * $core = core::getInstance();
     *
     * So getInstance method would only return same reference of core object making it better usable
     *
     * This helps in saving memory and faster execution
     *
     *
     * Core is supposed to instantiate most of core stuff like
     * -	session creation
     * -	database object creation
     * and some more additional activities
     */
    private function __construct()
    {
        $this->startTime = microtime(true);
        $this->session = Session\Manager::getSession();
        $this->db = new MySqliDB();
        $this->configs = SiteConfigs::getInstance($this->db);
        $this->loadRequestHeaders();
        $accept_lang = null;
        if (isset($this->request_headers['accept-language'])) {
            $accept_lang = $this->request_headers['accept-language'];
        }
        
        $this->i18n = new Translations\Manager($this->configs->locale, 
            $accept_lang);
        $this->initializeHttpCode();
        $this->basehref = Common::getBaseHref();
        $this->apihref = $this->basehref . '/api';
        $this->from_email = array(
            'name' => $this->configs->from_email_name,
            'email' => $this->configs->from_email_address
        );
        $this->allowed_modules = array();
        $this->no_auth_calls = array();
        /**
         * Loads modules from registry
         */
        $modules_json = file_get_contents(REGISTRY_DIR . DS . 'modules.json');
        
        if ($modules_json != '' and Common::isJSON($modules_json)) {
            $modules_json = json_decode($modules_json, true);
            $this->allowed_modules = $modules_json['modules_registry'];
            $this->no_auth_calls = $modules_json['no_auth_calls'];
        }
        /**
         */
        $contenttypes_json = file_get_contents(
            REGISTRY_DIR . DS . 'contenttypes.json');
        if ($contenttypes_json != '' and Common::isJSON($contenttypes_json)) {
            $contenttypes_json = json_decode($contenttypes_json, true);
            $this->allowed_contenttypes = $contenttypes_json;
        }
        $this->current_year = date("Y", time());
    }

    /**
     * Check whether the user has the required right or not
     * Since checking rights is done over a user, hence authenticated user is mandatory over which checking can be done
     *
     * If user isn't logged then Authentication Required exception is raised which can be redirected to login screen
     *
     * If user is logged and checking of right fails internally exception is raised stating Access Denied
     *
     * If everything passes out, no exception is raised :)
     *
     * @param string $right_name
     *            Uniquely Identified Right Name to be checked over logged user
     * @throws Sai\Modules\Auth\Exception
     */
    public function checkRight($right_name)
    {
        if ($this->isLogged()) {
            $this->session->getUser()->checkRight($right_name);
        } else {
            throw new Modules\Auth\Exception(
                Modules\Auth\Exception::AUTHENTICATION_REQUIRED);
        }
    }

    /**
     */
    public function hasRight($right_name)
    {
        if ($this->isLogged()) {
            return $this->session->getUser()->hasRight($right_name);
        } else {
            throw new Modules\Auth\Exception(
                Modules\Auth\Exception::AUTHENTICATION_REQUIRED);
        }
    }

    /**
     */
    public function loadRights($rights)
    {
        if (! is_array($rights)) {
            throw new Modules\Users\Rights\Exception(
                Modules\Users\Rights\Exception::LOAD_RIGHTS_ARRAY_REQ);
        }
        $rights_r = array();
        foreach ($rights as $r) {
            $rights_r[$r] = $this->hasRight($r);
        }
        return $rights_r;
    }

    /**
     * Get Instance a static method that returns out self object instance
     *
     * As per Singleton Design Pattern rule
     * getInstance identifies if private instance is initialized
     * if no then it intializes with self constructor
     * and then just returns its self private static instance
     *
     * @return Object of core class
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Core();
        }
        return self::$_instance;
    }

    /**
     * Initializes standard HTTP Code
     *
     * These HTTP Code would be used while setting response status code
     * This method also helps identify what code would mean what
     *
     * P.S. Not all HTTP codes are used, only standard mostly used codes are initialized below
     */
    public function initializeHttpCode()
    {
        $this->http_code = array();
        $status_code_registry = REGISTRY_DIR . DS . 'statuscode.json';
        if (file_exists($status_code_registry)) {
            $this->http_code = json_decode(
                file_get_contents($status_code_registry), true);
        }
    }

    /**
     * Get Allowed Modules Array
     *
     * @return Array of Allowed Modules
     */
    public function getAllowedModules()
    {
        return $this->allowed_modules;
    }

    /**
     * Get Allowed Content Types
     *
     * @return Array of Allowed Content Types
     */
    public function getAllowedContentTypes()
    {
        return $this->allowed_contenttypes;
    }

    /**
     * Get No Auth Calls method details
     *
     * @return Array of no auth call methods
     */
    public function getNoAuthCalls()
    {
        return $this->no_auth_calls;
    }

    /**
     * Sets the user that can be user throughout application
     *
     * if no param is passed, user is set from session
     *
     * @param $user (Object)
     *            should logged in user (Optional)
     *            
     */
    public function setUser($user = null)
    {
        if (is_null($user)) {
            $user = $this->session->getUser();
        }
        $this->user = $user;
        if (! property_exists($this->user, 'is_admin_logged')) {
            $this->user->is_admin_logged = false;
        }
    }

    /**
     * Displays Page with the help of Smarty Template Engine
     * Enhanced by SAI
     *
     * @param $template_arr (Array)
     *            file	==> Template file name
     *            assign	==>	Array of variables to be assigned i.e. passed to Smarty Template
     */
    public function displayPage($template_arr)
    {
        if (is_null($template_arr)) {
            throw new Exception("Template Information Array Required");
        }
        if (! isset($template_arr['file'])) {
            $template_arr['file'] = 'page';
        }
        $smarty = Smarty::getInstance();
        if (isset($template_arr['assign']) and is_array($template_arr['assign']) and
             count($template_arr['assign']) > 0) {
            foreach ($template_arr['assign'] as $k => $v) {
                $smarty->assign($k, $v);
            }
        }
        $smarty->display($template_arr['file']);
    }

    /**
     * Checks if user is logged
     *
     * @return (Boolean) whether the user is logged or not
     */
    public function isLogged()
    {
        return (! is_null($this->session->getUser()) and
             ! is_null($this->session->getUser()->creation_ts));
    }

    /**
     * Checks if admin user is logged
     *
     * @return Boolean where the admin user is logged in or not
     */
    public function isAdminLogged()
    {
        return ($this->isLogged() and
             ! is_null($this->session->getParam('is_admin_logged')));
    }

    /**
     * This is fallback implementation for certain PHP versions not supporting apache function
     *
     * $_SERVER plays the game :D
     * Only drawbback is between name case sensitivity and symbols
     * So using smaller case & underscore forcefully
     *
     * This is non-standard request header accessing approach
     * Still it works pretty well as expected
     */
    public function loadRequestHeaders()
    {
        $rh = array();
        if (function_exists('apache_request_headers')) {
            $rh = apache_request_headers();
        } else {
            foreach ($_SERVER as $k => $v) {
                if (strpos($k, 'HTTP_') === 0) {
                    $rh[$k] = $v;
                }
            }
        }
        foreach ($rh as $k => $v) {
            $this->request_headers[str_replace("_", "-", strtolower($k))] = $v;
        }
    }
}
