<?php
namespace Sai\Base\Restlizer;

use Sai\Base;

/**
 *
 * @name Restlizer
 * @abstract Restlizer would deserialize data from request and serialzie data for response
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager
{

    /**
     *
     * @var array $contenttypes Allowed Type of Content forms to be parsed
     */
    public $contenttype;

    /**
     *
     * @var array $serializers Allowed Serializable Contents
     */
    public $serializers;

    /**
     *
     * @var object $serio_obj Depending on the Content-Type selected instantiate this serio_obj
     */
    public $serio_obj;

    /**
     *
     * @var object $core Core Object
     */
    public $core;

    /**
     *
     * @param string $contenttype            
     * @throws Base\Restlizer\Exception
     */
    public function __construct($contenttype = 'application/json')
    {
        $this->core = Base\Core::getInstance();
        $this->serializers = $this->core->getAllowedContentTypes();
        if ($contenttype == '' or is_null($contenttype)) {
            $contenttype = 'application/json';
        }
        if (strpos($contenttype, ';')) {
            $contenttype = explode(';', $contenttype);
            $contenttype = $contenttype[0];
        }
        if (strpos($contenttype, ',')) {
            $contenttype = explode(',', $contenttype);
            foreach ($contenttype as $ct) {
                if (isset($this->serializers[$ct])) {
                    $contenttype = $ct;
                    break;
                }
            }
        }
        $this->contenttype = $contenttype;
        if (! isset($this->serializers[$this->contenttype])) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::INVALID_ACCEPT);
        }
        $this->serio_obj = new $this->serializers[$this->contenttype]();
    }

    /**
     * Serialize
     */
    public function serializo($obj)
    {
        return $this->serio_obj->serializo($obj);
    }

    /**
     * DeSerialize
     */
    public function deserializo($str)
    {
        return $this->serio_obj->deserializo($str);
    }
}
