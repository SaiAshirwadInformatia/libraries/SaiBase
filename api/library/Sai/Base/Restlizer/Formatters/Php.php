<?php
namespace Sai\Base\Restlizer\Formatters;

use Sai\Base;

/**
 *
 * @name PHP Formatter
 * @abstract PHP related Formatting operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Php implements iFormatter
{

    /**
     */
    public function serializo($obj)
    {
        if (is_string($obj)) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::DESERIALIZE_STRING_NOT_ALLOWED);
        }
        if (is_object($obj)) {
            if (method_exists($obj, 'jsonSerialize')) {
                $obj = $obj->jsonSerialize();
            } else {
                $obj = (array) $obj;
            }
        }
        return serialize($obj);
    }

    /**
     * Deserializes PHP Serialized Object String into PHP Array
     *
     * @param $str (String)
     *            PHP Serialized Object string to be parsed
     *            
     * @return (Array) that is parsed out from PHP Serialized Object String
     */
    public function deserializo($str)
    {
        return unserialize($str);
    }

    /**
     */
    public function beautifio($mixed)
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }

    /**
     */
    public function minimizo($mixed)
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }
}
