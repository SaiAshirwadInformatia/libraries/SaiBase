<?php
namespace Sai\Base\Restlizer\Formatters;

/**
 *
 * @name XML Formatter
 * @abstract XML related Formatting operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Xml implements iFormatter
{

    /**
     */
    public function serializo($obj)
    {
        if (is_string($obj)) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::DESERIALIZE_STRING_NOT_ALLOWED);
        }
        try {
            if (is_object($obj)) {
                if (method_exists($obj, 'jsonSerialize')) {
                    $obj = $obj->jsonSerialize();
                } else {
                    $obj = (array) $obj;
                }
            }
            $xml = LSS\Array2XML::createXML('output', $obj);
            return $xml->saveXML();
        } catch (Exception $e) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::FAILED_XML_SERIALIZE);
        }
    }

    /**
     * Deserializes XML String into PHP Array
     *
     * @param $str (String)
     *            XML string to be parsed
     *            
     * @return (Array) that is parsed out from XML string
     */
    public function deserializo($str)
    {
        try {
            return LSS\XML2Array::createArray($str);
        } catch (Exception $e) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::INVALID_XML);
        }
    }

    /**
     */
    public function beautifio($mixed)
    {
        if (! is_string($mixed)) {
            $mixed = $this->serializo($mixed);
        }
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->loadXML($mixed);
        $dom->formatOutput = true;
        return $dom->saveXML();
    }

    /**
     */
    public function minimizo($mixed)
    {
        try {
            if (is_object($mixed) or is_array($mixed)) {
                $mixed = $this->serializo($mixed);
            } else {
                $mixed = $this->serializo($this->deserializo($mixed));
            }
        } catch (Exception $e) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::INVALID_XML);
        }
        return $mixed;
    }
}
