<?php
namespace Sai\Base\Restlizer\Formatters;

/**
 * YAML Formatter, parses data vice-versa
 *
 * @author Rohan Sakhale
 * @copyright 2014 Sai Ashirwad Informatia
 * @since Ghanerao v1
 *       
 */
class Yaml implements iFormatter
{

    /**
     */
    public function serializo($obj)
    {
        if (is_string($obj)) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::DESERIALIZE_STRING_NOT_ALLOWED);
        }
        try {
            if (is_object($obj)) {
                if (method_exists($obj, 'jsonSerialize')) {
                    $obj = $obj->jsonSerialize();
                } else {
                    $obj = (array) $obj;
                }
            }
            return yaml_emit($obj);
        } catch (Exception $e) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::FAILED_YAML_ENCODE);
        }
    }

    /**
     */
    public function deserializo($str)
    {
        try {
            $deserialized = yaml_parse($str);
            return $deserialized;
        } catch (Exception $e) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::FAILED_YAML_DECODE);
        }
    }

    /**
     */
    public function beautifio($mixed)
    {
        if (is_string($mixed)) {
            $mixed = $this->deserializo($mixed);
        }
        return json_encode($mixed, JSON_PRETTY_PRINT);
    }

    /**
     */
    public function minimizo($mixed)
    {
        try {
            if (is_object($mixed) or is_array($mixed)) {
                $mixed = $this->serializo($mixed);
            } else {
                $mixed = $this->serializo($this->deserializo($mixed));
            }
        } catch (Exception $e) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::INVALID_JSON);
        }
        return $mixed;
    }
}
