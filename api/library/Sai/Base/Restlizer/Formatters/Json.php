<?php
namespace Sai\Base\Restlizer\Formatters;

use Sai\Base;

/**
 *
 * @name JSON Formatter
 * @abstract JSON related Formatting operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Json implements iFormatter
{

    /**
     */
    public function serializo($obj)
    {
        if (is_string($obj)) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::DESERIALIZE_STRING_NOT_ALLOWED);
        }
        try {
            if (is_object($obj)) {
                if (method_exists($obj, 'jsonSerialize')) {
                    $obj = $obj->jsonSerialize();
                } else {
                    $obj = (array) $obj;
                }
            }
            if (defined('JSON_UNESCAPED_SLASHES')) {
                return json_encode($obj, JSON_UNESCAPED_SLASHES);
            } else {
                return json_encode($obj);
            }
        } catch (Exception $e) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::FAILED_JSON_ENCODE);
        }
    }

    /**
     */
    public function deserializo($str)
    {
        try {
            $deserialized = json_decode($str, true);
            if (json_last_error() != JSON_ERROR_NONE) {
                throw new Base\Restlizer\Exception(
                    Base\Restlizer\Exception::INVALID_JSON);
            }
            return $deserialized;
        } catch (Exception $e) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::INVALID_JSON);
        }
    }

    /**
     */
    public function beautifio($mixed)
    {
        if (is_string($mixed)) {
            $mixed = $this->deserializo($mixed);
        }
        return json_encode($mixed, JSON_PRETTY_PRINT);
    }

    /**
     */
    public function minimizo($mixed)
    {
        try {
            if (is_object($mixed) or is_array($mixed)) {
                $mixed = $this->serializo($mixed);
            } else {
                $mixed = $this->serializo($this->deserializo($mixed));
            }
        } catch (Exception $e) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::INVALID_JSON);
        }
        return $mixed;
    }
}
