<?php
namespace Sai\Base\Restlizer\Formatters;

use Sai\Base;

/**
 *
 * @name HTML Formatter
 * @abstract HTML related Formatting operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Html implements iFormatter
{

    /**
     * (non-PHPdoc)
     * 
     * @see \Sai\Base\Restlizer\Formatters\iFormatter::serializo()
     */
    public function serializo($obj)
    {
        if (is_string($obj)) {
            throw new Base\Restlizer\Exception(
                Base\Restlizer\Exception::DESERIALIZE_STRING_NOT_ALLOWED);
        }
        if (is_object($obj)) {
            if (method_exists($obj, 'jsonSerialize')) {
                $obj = $obj->jsonSerialize();
            }
        }
        return $this->getHTMLTable($obj);
    }

    /**
     * Deserializes HTML String into Object not allowed
     *
     * @throws Base\Restlizer\Exception
     */
    public function deserializo($str)
    {
        throw new Base\Restlizer\Exception(
            Base\Restlizer\Exception::HTML_DESERIALIZE_NOT_SUPPORTED);
    }

    /**
     */
    public function getHTMLTable($data)
    {
        $html = '<table>';
        if (! is_null($data)) {
            if (is_object($data)) {
                $data = (array) $data;
            }
            foreach ($data as $key => $value) {
                $html .= '<tr>';
                $html .= '<td>' . $key . '</td>';
                if (is_array($value) or is_object($value)) {
                    $html .= '<td>' . $this->getHTMLTable($data) . '</td>';
                } else {
                    $html .= '<td>' . $value . '</td>';
                }
                $html .= '</tr>';
            }
        }
        $html .= '</table>';
        return $html;
    }

    /**
     */
    public function beautifio($mixed)
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_AVAILABLE);
    }

    /**
     */
    public function minimizo($mixed)
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_AVAILABLE);
    }
}
