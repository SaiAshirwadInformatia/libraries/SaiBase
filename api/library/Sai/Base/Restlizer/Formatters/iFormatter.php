<?php
namespace Sai\Base\Restlizer\Formatters;

/**
 *
 * @name SAI Formatter
 * @abstract Interface to capture base lined methods that should be present for any Formatter
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
interface iFormatter
{

    /**
     * `e` replaced by `o` just for fancy names
     */
    
    /**
     *
     * @param $obj (Object)
     *            that has to be serialized
     *            
     * @return serialized data
     */
    public function serializo($obj);

    /**
     *
     * @param $str (String)
     *            that has to be deserialized
     *            
     * @return deserialized object
     */
    public function deserializo($str);

    /**
     *
     * @param $mixed (mixed)            
     *
     * @return Beautified String
     */
    public function beautifio($mixed);

    /**
     *
     * @param $mixed (mixed)            
     *
     * @return Minimized String
     */
    public function minimizo($mixed);
}
