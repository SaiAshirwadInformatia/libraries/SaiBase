<?php
namespace Sai\Base\Restlizer;

use Sai\Base;

/**
 *
 * @name RestLizer Exception
 * @abstract RestLizer exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */

/**
 * RestLizer exception handling for rest content transformation
 */
class Exception extends Base\Exception
{

    const INVALID_JSON = 1207;

    const INVALID_XML = 1208;

    const INVALID_ACCEPT = 1209;

    const FAILED_JSON_ENCODE = 1210;

    const FAILED_XML_SERIALIZE = 1211;

    const DESERIALIZE_STRING_NOT_ALLOWED = 1212;

    const HTML_DESERIALIZE_NOT_SUPPORTED = 1213;

    const FAILED_YAML_ENCODE = 1214;

    const FAILED_YAML_DECODE = 1215;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::INVALID_JSON => "excp_invalid_json",
            self::INVALID_XML => "excp_invalid_xml",
            self::INVALID_ACCEPT => "excp_invalid_accept",
            self::FAILED_JSON_ENCODE => "excp_failed_json_encode",
            self::FAILED_XML_SERIALIZE => "excp_failed_xml_serialize",
            self::DESERIALIZE_STRING_NOT_ALLOWED => "excp_deserialize_string_not_allowed",
            self::HTML_DESERIALIZE_NOT_SUPPORTED => "excp_html_deserialize_not_allowed",
            self::FAILED_YAML_ENCODE => "excp_yaml_failed_encode",
            self::FAILED_YAML_DECODE => "excp_yaml_failed_decode"
        );
        $this->response_error_code[400] = array_merge(
            $this->response_error_code[400], 
            array(
                self::INVALID_JSON,
                self::INVALID_XML,
                self::HTML_DESERIALIZE_NOT_SUPPORTED
            ));
        $this->response_error_code[406][] = self::INVALID_ACCEPT;
    }
}
