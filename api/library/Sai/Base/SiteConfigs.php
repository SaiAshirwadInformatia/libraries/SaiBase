<?php
namespace Sai\Base;

/**
 *
 * @name Configs
 * @abstract Would represent site configuration
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class SiteConfigs
{

    /**
     *
     * @var array $items Collection of all the configs
     */
    private $items;

    /**
     *
     * @var object $db DB Connection Object
     */
    public $db;

    /**
     *
     * @var object $_instance Private Self Static Instance
     */
    private static $_instance;

    /**
     * Constructs object configs and loads entire configuration from database
     *
     * This configuration is maintained in rich manner, saving and getting configs is easier
     * Ex:
     * $obj = new configs;
     * echo $obj -> sitename; This will return site_name by configured value
     *
     * $obj -> sitename = 'WillingTree';
     * This will set value of site_name to WillingTree
     *
     * $obj -> sitename = array(
     * 'name' => 'sitename',
     * 'val' => 'WillingTree',
     * 'oftype' => 'text',
     * 'options' => ''
     * );
     * This is in-depth saving of existing or new config
     *
     * Final saving of config within db would only happen if save() is called over this object
     */
    private function __construct(&$db)
    {
        $this->db = $db;
        $this->items = array();
        $r = $this->db->selectAll(
            array(
                'tablename' => 'configs'
            ));
        if (! is_null($r)) {
            foreach ($r as $v) {
                $this->items[$v['name']] = $v;
            }
        }
    }

    /**
     * Singleton Design Pattern
     *
     * @param object $db            
     * @return object
     */
    public static function getInstance(&$db)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new siteconfigs($db);
        }
        return self::$_instance;
    }

    /**
     *
     * @param string $k            
     * @return NULL
     */
    public function __get($k)
    {
        $val = null;
        $k = str_replace("_", "-", $k);
        if (isset($this->items[$k])) {
            $val = $this->items[$k]['val'];
        }
        return $val;
    }

    /**
     *
     * @param string $k            
     * @param mixed $v            
     */
    public function __set($k, $v)
    {
        if (isset($this->items[$k])) {
            if (! is_array($v)) {
                $this->items[$k]['val'] = $v;
            } else {
                $this->items[$k] = $v;
            }
        } else {
            if (is_array($v)) {
                $this->items[$k] = $v;
            } elseif (is_string($v) and array_key_exists($k, $this->items)) {
                $this->items[$k]['val'] = $v;
            } else {
                $this->items[$k] = array(
                    'name' => $k,
                    'val' => $v,
                    'oftype' => 'text',
                    'options' => ''
                );
            }
        }
    }

    /**
     *
     * @param string $name            
     * @return Ambigous <NULL, multitype:>
     */
    public function get($name)
    {
        return isset($this->items[$name]) ? $this->items[$name] : null;
    }

    /**
     *
     * @return multitype:
     */
    public function getAll()
    {
        return $this->items;
    }

    /**
     *
     * @param string $name            
     */
    public function remove($name)
    {
        if (isset($this->items[$name])) {
            unset($this->items[$name]);
            $r = $this->db->delete(
                array(
                    'tablename' => 'configs',
                    'where' => array(
                        'condition' => 'AND',
                        'data' => array(
                            'name' => $name
                        )
                    )
                ));
        }
    }

    /**
     */
    public function save()
    {
        if (! is_null($this->items) and count($this->items) > 0) {
            $sqls = array(
                "TRUNCATE `configs`"
            );
            foreach ($this->items as $i) {
                $sqls[] = "INSERT INTO `configs` (`name`, `val`, `oftype`, `options`)
				VALUES ('{$i['name']}', '{$i['val']}', '{$i['oftype']}', '{$i['options']}')
				ON DUPLICATE KEY UPDATE `val` = '{$i['val']}', `oftype` = '{$i['oftype']}', `options` = '{$i['options']}'";
            }
            $e = $this->db->multiQuery(implode(";", $sqls));
        }
    }
}
