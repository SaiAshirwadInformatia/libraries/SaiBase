<?php
namespace Sai\Base;

/**
 *
 * @name Search Exception
 * @abstract Search exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class SearchException extends SqlException
{

    const REQUIRED_ATTR_MISSING = 2601;

    const NOT_FOUND = 2604;

    const INVALID_DATA = 2607;

    const INVALID_CONDITION = 2608;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_search_req_missing",
            self::NOT_FOUND => "excp_search_not_found",
            self::INVALID_DATA => "excp_search_invalid_data",
            self::INVALID_CONDITION => "excp_search_invalid_condition"
        );
        $this->sql_error_map[self::COLUMN_NOT_FOUND] = self::INVALID_DATA;
        if (isset($this->sql_error_map[$exception_number])) {
            $this->exception_number = $this->sql_error_map[$exception_number];
        }
        $this->response_error_code[400][] = self::INVALID_CONDITION;
        $this->response_error_code[400][] = self::INVALID_DATA;
    }
}
