<?php
namespace Sai\Base;

/**
 *
 * @author Rohan Sakhale
 *        
 */
class Icons
{

    private $icons;

    var $icons_css_paths;

    var $icons_js_paths;

    var $core;

    public function __construct()
    {
        $this->core = Core::getInstance();
        $this->icons = array();
        $this->icons_css_paths = array();
        $icons_registry = REGISTRY_DIR . DS . "icons.json";
        if (file_exists($icons_registry)) {
            $icons_registry = json_decode(file_get_contents($icons_registry), 
                true);
            foreach ($icons_registry as $icon_registry_name => $icon_registry) {
                $registry = SAI_ABS_PATH . $icon_registry['json'];
                $this->icons_css_paths[] = $this->core->basehref . '/' .
                     $icon_registry['css'];
                if (isset($icon_registry['js'])) {
                    $this->icons_js_paths[] = $this->core->basehref . '/' .
                         $icon_registry['js'];
                }
                if (file_exists($registry)) {
                    $icon_registry = json_decode(file_get_contents($registry), 
                        true);
                    foreach ($icon_registry as $icon_call => $icon_span) {
                        $this->icons[$icon_call] = $icon_span;
                    }
                }
            }
        }
    }

    /**
     *
     * @param string $icon_name            
     * @return string
     */
    public function __get($icon_name)
    {
        $icon_name = str_replace("_", "-", $icon_name);
        if (isset($this->icons[$icon_name])) {
            return $this->icons[$icon_name];
        }
        return '<span class="btn btn-danger btn-xs">Bad Icon Mann!!!!</span>';
    }

    /**
     *
     * @return Ambigous <multitype:, mixed, string>
     */
    public function all()
    {
        return $this->icons;
    }
}