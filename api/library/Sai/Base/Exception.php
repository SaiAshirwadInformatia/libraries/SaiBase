<?php
namespace Sai\Base;

/**
 *
 * @name Sai Exception
 * @abstract Base Exception class for handling exceptions
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
abstract class Exception extends \Exception implements iJsonSerializable
{

    const STRING_ATTR_NOT_VALID = 999;

    /**
     *
     * @var integer
     */
    public $exception_number;

    /**
     *
     * @var array
     */
    public $exception_list;

    /**
     *
     * @var array
     */
    public $response_error_code;

    /**
     *
     * @var core
     */
    public $core;

    /**
     * Need to re-think how better we can update response status code for any damn thing
     */
    public function __destruct()
    {
        $changed = false;
        if (isset($this->response_error_code)) {
            foreach ($this->response_error_code as $status_code => $error_codes) {
                if (in_array($this->exception_number, $error_codes)) {
                    Common::changeResponseCode($status_code);
                    $changed = true;
                    break;
                }
            }
        }
        if (! $changed) {
            Common::changeResponseCode(500);
        }
    }

    /**
     * Constructs the Exception Object with unique constant
     * This help to display out proper error with localized message
     *
     * Additionally this constructor also builds Error Status Response Code
     *
     * @param integer $exception_number            
     */
    public function __construct($exception_number)
    {
        parent::__construct();
        $c = get_class($this);
        $this->exception_list = array();
        $this->response_error_code = array();
        $this->core = Core::getInstance();
        $default_response_code = array(
            400 => array(
                'REQUIRED_ATTR_MISSING',
                'UNIQUE'
            ),
            404 => array(
                'NOT_FOUND'
            )
        );
        foreach ($this->core->http_code as $code => $error_msg) {
            if ($code >= 400) {
                $this->response_error_code[$code] = array();
            }
            if (array_key_exists($code, $default_response_code)) {
                $default = $default_response_code[$code];
                foreach ($default as $error_constant) {
                    if (defined($c . '::' . $error_constant)) {
                        $this->response_error_code[$code][] = constant(
                            $c . '::' . $error_constant);
                    }
                }
            }
        }
        
        if (! is_null($exception_number)) {
            $this->exception_number = $exception_number;
        }
    }

    /**
     * Serializes the object in pretty array form
     *
     * @see iSaiJsonSerializable::jsonSerialize()
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            "error_code" => $this->exception_number,
            "error_msg" => $this->getExceptionMessage()
        );
    }

    /**
     * Get proper exception message based on error constant set during constructor
     *
     * Also gives our localized string
     *
     * @return NULL|string
     */
    public function getExceptionMessage()
    {
        if (isset($this->exception_list[$this->exception_number])) {
            $l = $this->core->i18n->get_lang(
                $this->exception_list[$this->exception_number]);
            return (isset($l) and ! empty($l)) ? $l : $this->exception_list[$this->exception_number];
        }
        return sprintf(
            "(%d) Looks like something went wrong, we are looking into this", 
            $this->exception_number);
    }
}
