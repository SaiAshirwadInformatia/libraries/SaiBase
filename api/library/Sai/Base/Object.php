<?php
namespace Sai\Base;

/**
 * Would represent the base of objects reusable patterns
 *
 * @name SaiObject
 * @author Rohan Sakhale <rs@saiashirwad.com>
 * @copyright saiashirwad.com
 * @since WillingTree v1
 * @version Release: 0.1
 *         
 */
abstract class Object implements iJsonSerializable
{

    /**
     *
     * @var integer $id
     */
    protected $id;

    /**
     *
     * @var array $arr
     */
    public $arr;

    /**
     *
     * @var string $url_for_link URL for Link that would help to link out over pages
     */
    public $url_for_link;

    /**
     *
     * @var array $idx_key to store representable indexes for object
     */
    public $idx_key;

    /**
     *
     * @var object $core is the heart of application
     */
    public $core;

    /**
     *
     * @var boolean $changelocale
     */
    public $changelocale;

    /**
     * Constructs the most useful base saiobject presently most of modules that we would play with
     *
     * This SAI Object is a very special class that helps represent any class in the form of table
     * Properties of class that inherits this class would act as table's columns
     *
     * SAI Object has ability of reading data from Request and initializing the self object with it
     * Integrated with RestLizer Baby to read multiple forms of request data
     * At the End after loading Arguments it also updates the $_REQUEST array so we can use data anywhere
     *
     * Pretty good class, please help update the baselining stuff w.r.t. objects ;)
     */
    public function __construct($changeLocale = true)
    {
        $this->core = Core::getInstance();
        $this->arr = new stdClass();
        $this->loadDefaults();
        $this->changeLocale = $changeLocale;
    }

    /**
     * Sets the most require default values of the object that has to be ready during instantiation
     */
    public function loadDefaults()
    {
        $this->setId(null);
        if (! is_null($this->idx_key)) {
            foreach ($this->idx_key as $k) {
                if (property_exists($this, $k)) {
                    $this->$k = null;
                }
            }
        }
        $this->url_for_link = '';
    }

    /**
     * A bad function that should be avoided to be used
     * Still in extremem conditions if required only then should be used
     *
     * setId initializes the object id, that should be valid :-O
     * Usually done after database call is made and mostly called in internal constructors
     *
     * @param Number $id
     *            has to be initialized for the object
     *            
     * @throws Base\Modules\Exception if id is not numeric :P
     */
    public function setId($id)
    {
        if (empty($id)) {
            return;
        }
        if (! is_null($id) and ! is_numeric($id)) {
            throw new Modules\Exception(Modules\Exception::ID_NUMERIC_REQ);
        }
        $this->id = $id;
    }

    /**
     * Pretty good function, lets u get the ID of object
     * Usually should be called over its object only ;)
     *
     * Not after jsonSerialize :P
     *
     * @return (Number) id that is supposed to be numeric unique identifier for object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Nullifies all the properties that could be available for the object
     *
     * Properties are identified by idx_key ;)
     */
    public function reset()
    {
        foreach ($this->idx_key as $k) {
            $this->$k = null;
        }
    }

    /**
     */
    public function loadUrl()
    {
        if (empty($this->url_for_link)) {
            if (property_exists($this, 'name')) {
                $this->url_for_link = name_for_url($this->name);
            } elseif (property_exists($this, 'title')) {
                $this->url_for_link = name_for_url($this->title);
            }
        }
    }

    /**
     * A Godly method for every object
     * Helps not only load data from request but also initializes the object instance with the request arr
     *
     * Pretty good saves 2 steps in one shot and some efforts of sending params
     * All Hail loadArgs here :D ;) :)
     */
    public function loadArgs()
    {
        $this->loadFromRequest($this->idx_key);
        $this->loadObjectInstance($this->arr);
    }

    /**
     * Load data from request that could be used with the help of $arr property of this object
     *
     * Additionally this method also updates $_REQUEST to carry the data read from raw-data format
     * Integrated with beautiful RestLizer baby to intake multi-format data
     */
    public function loadFromRequest($values)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $loaded = false;
        if (isset($this->core->request_headers['content-type'])) {
            $req_input = file_get_contents("php://input");
            $allowed_content_types = $this->core->getAllowedContentTypes();
            if (isset(
                $allowed_content_types[$this->core->request_headers['content-type']])) {
                $restlizer_baby = new Restlizer\Manager(
                    $this->core->request_headers['content-type']);
                if (! is_null($req_input) and $req_input != '') {
                    $arr = $restlizer_baby->deserializo($req_input);
                    if (! is_null($arr) and ! empty($arr)) {
                        $_REQUEST = array_merge($_REQUEST, $arr);
                        $loaded = true;
                    }
                }
            }
            if (! $loaded and
                 ($method == 'PUT' || $method == 'DELETE' || $method == 'POST')) {
                $f_query = file_get_contents("php://input");
                if (isset($f_query) and $f_query != '') {
                    $data = null;
                    parse_str($f_query, $data);
                    $_REQUEST = array_merge($_REQUEST, $data);
                }
            }
            /**
             * Load object $arr property with $key matching from given param $values to be considered for loading arr
             * Usually $values would contain idx_key :)
             * Additionally type-casting is taken care to make sure accessing of data is much better
             */
            foreach ($_REQUEST as $key => $val) {
                if (in_array($key, $values) and ! is_null($val)) {
                    if (strpos($key, "is_") !== false) {
                        /**
                         * Sadly now is_active requires to have -1 value for Deleted
                         * Hence additional elseif :-\
                         */
                        $this->arr->$key = ($val == 'true' || $val == 1) ? 1 : (($val ==
                             - 1) ? - 1 : 0);
                    } elseif (is_array($val)) {
                        $this->arr->$key = $val;
                    } elseif (is_int($val)) {
                        $this->arr->$key = intval($val);
                    } elseif (is_bool($val)) {
                        $this->arr->$key = (bool) $val;
                    } elseif (is_string($val)) {
                        $this->arr->$key = trim($val);
                    }
                }
            }
        }
    }

    /**
     * Load object instance helps to load the current object with the passed array as parameter
     * The array index is checked if similar property is available with object
     * If Yes, yo its initialized, else just ignored :P
     *
     * @param $arr (Array)
     *            that has to initialize the current object instance
     */
    public function loadObjectInstance($arr)
    {
        foreach ($arr as $key => $val) {
            if (! is_null($val) and property_exists($this, $key) and (! empty(
                $val) or $val == 0)) {
                if ($key == 'id' and $val != 0) {
                    $this->setId($val);
                } else {
                    $this->$key = $val;
                }
            }
        }
        $this->translate($arr);
    }

    /**
     *
     * @param integer $id            
     */
    public function loadObject($id)
    {
        if (! is_null($id) and is_numeric($id)) {
            $obj = $this->core->db->select(
                array(
                    "tablename" => get_class($this),
                    "where" => array(
                        "condition" => "AND",
                        "data" => array(
                            "id" => $id
                        )
                    )
                ));
            
            if ($this->core->db->getRowNum($obj) > 0) {
                $objVal = $this->core->db->resultToArray($obj);
                $this->loadObjectInstance($objVal[0]);
                $this->translate($objVal[0]);
            }
        }
    }

    /**
     *
     * @param array $data            
     */
    public function loadObjectByDBCol($data)
    {
        if (! is_null($data) and is_array($data) and count($data) > 0) {
            $obj = $this->core->db->select(
                array(
                    "tablename" => get_class($this),
                    "where" => array(
                        "condition" => "AND",
                        "data" => $data
                    )
                ));
            if ($this->core->db->getRowNum($obj) > 0) {
                $objVal = $this->core->db->resultToArray($obj);
                $this->loadObjectInstance($objVal[0]);
                $this->translate($objVal[0]);
            }
        }
    }

    /**
     * Perform a self translation if any keys match out into translations table
     *
     * @param array $objVal            
     */
    public function translate($objVal)
    {
        $this->loadUrl();
        if (! $this->changelocale) {
            return;
        }
        if ($this->core->i18n->locale != 'en-US') {
            $id = null;
            if (is_object($objVal)) {
                if (method_exists($objVal, 'getId')) {
                    $id = $objVal->getId();
                } else 
                    if (property_exists($objVal, 'id')) {
                        $id = $objVal->id;
                    }
            } else {
                if (isset($objVal['id'])) {
                    $id = $objVal['id'];
                }
            }
            if (! is_null($id)) {
                $object_translation = $this->core->db->selectAll(
                    array(
                        "tablename" => "translations",
                        "where" => array(
                            "condition" => "AND",
                            "data" => array(
                                "object_name" => get_class($this),
                                "objectid" => $id
                            )
                        )
                    ));
                
                if (count($object_translation) > 0) {
                    foreach ($object_translation as $obj_t) {
                        if (property_exists($this, $obj_t['object_column'])) {
                            $this->$obj_t['object_column'] = $obj_t['translated'];
                        }
                    }
                }
            }
        }
    }

    /**
     * Usually json_encode can get confuse with properties/functions
     * This will help read easy array that is easy to serialize
     * Making json_encode's life easy
     *
     * Additionally incase where we want object to be read as Array
     * This bro will help us out
     *
     * @return (Array) that is easy to serialize for json_encode
     */
    public function jsonSerialize()
    {
        $resultArr = array();
        foreach ($this->idx_key as $key) {
            if (property_exists($this, $key)) {
                if (is_object($this->$key) and
                     method_exists($this->$key, 'jsonSerialize')) {
                    $resultArr[$key] = $this->$key->jsonSerialize();
                } elseif (strpos($key, "_id") === FALSE) {
                    $resultArr[$key] = $this->$key;
                }
            }
        }
        if (count($resultArr) > 0) {
            $resultArr['url_for_link'] = $this->url_for_link;
        }
        return $resultArr;
    }
}
