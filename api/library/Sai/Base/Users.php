<?php
namespace Sai\Base;

use Sai\Modules;

/**
 *
 * @name Base Users
 * @abstract Would help baseline users object class
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
abstract class Users extends Object
{

    public $user_id;

    public $users;

    public function __construct($changeLocale = true)
    {
        parent::__construct($changeLocale);
        $this->users = new Modules\Users\Object(false);
    }

    public function loadCurrentUser()
    {
        if (isset($this->user_id) and ! isset($this->users->creation_ts)) {
            if (is_null($this->users) or ! is_object($this->users)) {
                $this->users = new Modules\Users\Object(false);
            }
            $this->users->loadObject($this->user_id);
        }
    }

    public function loadArgs()
    {
        parent::loadArgs();
        $this->loadCurrentUser();
        $users_manager = ManagersStore::locate('Sai\Modules\Users');
        if (is_array($this->users)) {
            if (isset($this->users['id'])) {
                $this->users = $users_manager->get($this->users['id']);
            } elseif (isset($this->users['email'])) {
                $this->users = $users_manager->get($this->users['email']);
            }
        } elseif (is_null($this->user_id) and isset($this->arr->users->id) and
             is_null($this->users->creation_ts)) {
            $this->users->loadObject($this->arr['users']['id']);
        }
    }

    public function loadObject($id)
    {
        parent::loadObject($id);
        $this->loadCurrentUser();
    }

    public function loadObjectByDBCol($data)
    {
        parent::loadObjectByDBCol($data);
        $this->loadCurrentUser();
    }

    public function loadObjectInstance($arr)
    {
        parent::loadObjectInstance($arr);
        $this->loadCurrentUser();
    }
}
