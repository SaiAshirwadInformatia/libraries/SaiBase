<?php
namespace Sai\Base;

/**
 *
 * @name Common
 * @abstract Would contain mostly the commonly used operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Common
{

    /**
     *
     * @param $str (String)
     *            to identify if its JSON or NOT
     *            
     * @return Boolean
     */
    static function isJSON($str)
    {
        try {
            json_decode($str);
        } catch (Exception $e) {}
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     *
     * @param $haystack (String)            
     * @param $needle (String)            
     *
     * @return Boolean
     */
    static function contains($haystack, $needle)
    {
        return strpos($haystack, $needle) !== false;
    }

    /**
     */
    static function addGraveAccentToNames($name)
    {
        return "`$name`";
    }

    /**
     * Helps to cast value depending on type of it
     *
     * @param $value (String)
     *            to be casted
     *            
     * @return Object
     */
    static function castValueForDB($value)
    {
        if (is_null($value) and $value != 0) {
            return "''";
        } elseif (is_numeric($value)) {
            settype($value, "double");
            return $value;
        } elseif (is_bool($value)) {
            return (boolean) $value;
        } elseif (is_string($value)) {
            return "'$value'";
        } elseif (is_array($value)) {
            return implode(",", 
                filter_var($value, FILTER_CALLBACK, 
                    array(
                        "options" => "castValueForDB"
                    )));
        }
        return $value;
    }

    /**
     * Helps to change HTTP Response Code in Response Headers
     *
     * @param $code (Number)
     *            as response code
     */
    static function changeResponseCode($code)
    {
        $core = Core::getInstance();
        header("HTTP/1.1 $code " . $core->http_code[$code]);
    }

    /**
     * Redirect URL helps to redirect using JavaScript
     *
     * @param $url (String)            
     */
    static function redirectUrl($url)
    {
        echo '<html><head>
 	<script type="text/javascript">
 	window.location = \'' . $url . '\';
	</script>
 	</head>
 	</html>';
        die();
    }

    /**
     * Get Base URL for the application
     *
     * @return String URL
     */
    static function getBaseHref()
    {
        $url = (isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http') .
             '://' .
             (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : "localhost");
        if (strpos($_SERVER['SCRIPT_NAME'], '/api.php') !== false) {
            $url .= substr($_SERVER['SCRIPT_NAME'], 0, 
                strpos($_SERVER['SCRIPT_NAME'], "/api"));
        } else {
            $url .= substr($_SERVER['SCRIPT_NAME'], 0, 
                strrpos($_SERVER['SCRIPT_NAME'], '/'));
        }
        $folders_check = array(
            "/api",
            "/gui",
            "/install",
            "/docs"
        );
        foreach ($folders_check as $fc) {
            if (strpos($url, $fc) !== false) {
                $url = str_replace($fc, "", $url);
            }
        }
        return $url;
    }

    /**
     * Truncate long string into shorter one to display over UI
     *
     * @param $str (String)
     *            needs to be shortened
     * @param $length (Number)
     *            of maximum characters short string should carry
     * @param $append (String)
     *            to be appended after shortened
     *            
     * @return String i.e. shortened with appended characters
     *        
     */
    static function truncate($str, $length = 100, $append = "&hellip;")
    {
        $str = trim($str);
        
        if (sai_strlen($str) > $length) {
            $str = sai_substr($str, 0, $length);
            $str = explode("\n", $str);
            $str = array_shift($str) . $append;
        }
        
        return $str;
    }

    /**
     * Get globally defined constants
     *
     * @param $name (String)
     *            constant name
     *            
     * @return (mixed) Constant object
     */
    static function getConstant($name)
    {
        $constant_name = "sai_" . $name;
        global $$constant_name;
        $constant = $$constant_name;
        if (isset($constant)) {
            return $constant;
        }
        return null;
    }

    /**
     * Builds the header needed to make the content available for downloading
     *
     * @param string $fileName
     *            the filename
     * @param string $content
     *            the content which should be downloaded
     *            
     */
    static function downloadContentsToFile($filePath, $content = null)
    {
        $fileName = $filePath;
        if (stripos($filePath, '/') !== false) {
            $fileName = substr($filePath, stripos($filePath, '/'));
        }
        ob_get_clean();
        header('Pragma: public');
        header('Content-Type: text/plain; charset=UTF-8; name=' . $fileName);
        header('Content-Transfer-Encoding: BASE64;');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        if (is_null($content) and file_exists($filePath)) {
            readfile($filePath);
        } else {
            echo $content;
        }
    }

    /**
     * Counts string lengther as per charset
     *
     * @param string $str
     *            whose length has to be calculated by charset
     */
    static function saiStrlen($str)
    {
        $sLen = iconv_strlen($str, "UTF-8");
        if ($sLen === false) {
            throw new Exception("Invalid UTF-8 Data detected!");
        }
        return $sLen;
    }

    /**
     * Sub String by charset
     *
     * @param string $str
     *            whose sub string has to be retrieved by charset
     */
    static function saiSubstr($str, $start, $length = null)
    {
        $charset = "UTF-8";
        if (is_null($length)) {
            $length = iconv_strlen($str, $charset);
        }
        return iconv_substr($str, $start, $length, $charset);
    }

    /**
     * Check whether the value passed equates to NULL and is also not numeric to avoid INTEGER `0` as NULL
     *
     * @param mixed $value
     *            The value whose checking has to be done
     * @return boolean
     */
    static function isValueNull($value)
    {
        return $value === NULL && ! is_numeric($value);
    }

    /**
     * Parses the given string into suitable for URL form
     *
     * @param string $str            
     * @return mixed
     */
    static function nameForUrl($str)
    {
        return str_replace(array(
            " "
        ), "-", strtolower($str));
    }

    /**
     * Parses the url string into db suitable name which can be used over LIKE clause
     *
     * @param string $str
     *            The string taken from URL as param
     * @return string
     */
    static function urlToName($str)
    {
        return str_replace(array(
            "-",
            "?"
        ), " ", $str);
    }

    /**
     * Get age from given date into an parsed array form that is easily accesible
     *
     * @param unknown $dob            
     * @return multitype:NULL
     */
    static function getAge($start, $end = null)
    {
        $date = new DateTime($start);
        if (is_null($end)) {
            $end = new DateTime();
        } else {
            $end = new DateTime($end);
        }
        $interval = $end->diff($date);
        return array(
            "years" => $interval->y,
            "months" => $interval->m,
            "days" => $interval->d,
            "hours" => $interval->h,
            "minutes" => $interval->i,
            "seconds" => $interval->s
        );
    }

    static function getYearRange($range, $delimiter = '-')
    {
        if (! is_array($range)) {
            $range = explode($delimiter, $range);
        }
        
        $range = filter_var($range, FILTER_CALLBACK, 
            array(
                'options' => static function ($value)
                {
                    $value = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                    $value = trim($value);
                    return $value;
                }
            ));
        $date = new DateTime();
        $current_year = $date->format("Y");
        $start_range = $current_year - $range[0];
        $end_range = $current_year - $range[1];
        return array(
            'start_range' => $start_range,
            'end_range' => $end_range
        );
    }

    /**
     * Sanitize the HTML from Buffer into minified form
     *
     * @param string $buffer            
     * @return string
     */
    static function sanitizeOutput($buffer)
    {
        /**
         * Explanation on each element
         *
         * 0 => strip whitespaces after tags, except space
         * 1 => strip whitespaces before tags, except space
         * 2 => shorten multiple whitespace sequences
         */
        $search = array(
            '/\>[^\S ]+/s',
            '/[^\S ]+\</s',
            '/(\s)+/s'
        );
        
        $replace = array(
            '>',
            '<',
            '\\1'
        );
        
        $buffer = preg_replace($search, $replace, $buffer);
        
        return $buffer;
    }

    /**
     * Delete all files from folder recursively
     *
     * @param string $folder
     *            - Folder Path
     */
    static function removeFilesFromFolder($folder)
    {
        $compiled_files = glob($folder . '/*');
        if (! is_null($compiled_files) and count($compiled_files) > 0) {
            foreach ($compiled_files as $cf) {
                if (is_dir($cf)) {
                    removeFilesFromFolder($cf);
                    @rmdir($cf);
                } else {
                    @unlink($cf);
                }
            }
            return true;
        }
        return false;
    }

    static function compressImage($source_url, $destination_url, $quality = 70)
    {
        $info = getimagesize($source_url);
        
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source_url);
        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source_url);
        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source_url);
            
            // save file
        imagejpeg($image, $destination_url, $quality);
        
        // return destination file
        return $destination_url;
    }
}