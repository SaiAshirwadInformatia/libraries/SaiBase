<?php
namespace Sai\Base;

/**
 *
 * @name SAI Smarty
 * @abstract Just to extend out functionality of Smarty much specific to SAI Designs ;)
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Smarty extends \Smarty
{

    /**
     *
     * @var object $core Core Object
     */
    public $core;

    /**
     *
     * @var string $template Currently selected template from configs
     */
    public $template;

    /**
     *
     * @var object $_instance Self Private Static Instance
     */
    private static $_instance;

    /**
     * Singleton Design Pattern
     *
     * @return object
     */
    public static function getInstance()
    {
        if (! isset(self::$_instance)) {
            self::$_instance = new sai_smarty();
        }
        return self::$_instance;
    }

    /**
     * Constructs object for Sai Smarty :)
     */
    public function __construct()
    {
        parent::__construct();
        $this->core = core::getInstance();
        $this->template = $this->core->configs->template;
        $this->setTemplateDir(
            GUI_TEMPLATE . '/' . $this->core->configs->template);
        $this->setCacheDir(GUI_CACHE);
        $this->setCompileDir(GUI_COMPILE . '/' . $this->core->configs->template);
        $this->setConfigDir(GUI_CONFIG);
        $this->caching = false;
        
        $this->loadCurrentUrl();
        
        $this->loadDefaultAssigns();
        
        $this->loadFileTypes();
        
        $this->loadIcons();
        
        $this->loadNavbar();
        
        $this->registerFunctionPlugins();
        $this->loadQueryString();
        if ($this->core->configs->trimwhitespace != "no") {
            // $this->loadFilter('output', 'trimwhitespace');
        }
    }

    private function loadFileTypes()
    {
        $this->assign("filetypes", (new filetypes())->all());
    }

    private function loadIcons()
    {
        $this->assign("icons", new icons());
    }

    private function loadNavbar()
    {
        $menus_manager = managers_store::locate('menus');
        $nav = $menus_manager->all();
        $this->loadNavRights($nav);
        $this->assign('nav', $nav);
    }

    private function loadCurrentUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
            $pro = 'https';
        } else {
            $pro = 'http';
        }
        $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":" .
             $_SERVER["SERVER_PORT"]);
        $current_url = $pro . "://" . $_SERVER['SERVER_NAME'] . $port .
             $_SERVER['REQUEST_URI'];
        $this->assign("current_url", $current_url);
    }

    /**
     */
    private function loadDefaultAssigns()
    {
        $this->assign('core', $this->core);
        $this->assign('title', $this->core->configs->sitename);
        $this->assign('meta', 
            array(
                'keywords' => '',
                'description' => '',
                'authors' => ''
            ));
        $this->assign('basehref', $this->core->basehref);
        $this->assign('apihref', $this->core->apihref);
        $this->assign('assets', 
            $this->core->basehref . $this->core->configs->assets);
        $this->assign('js_assets', 
            $this->core->basehref . $this->core->configs->js_assets);
        $this->assign('images_assets', 
            $this->core->basehref . $this->core->configs->images_assets);
        $this->assign('template', 
            $this->core->basehref . $this->core->configs->templates . '/' .
                 $this->template);
        $this->assign('abs_template', GUI_TEMPLATE . DS . $this->template);
        
        /**
         * Load Current User Details
         *
         * By Default let it be null and id as -1
         */
        $this->assign('current_user_id', - 1);
        $this->assign('current_user', null);
        
        if (! is_null($this->core->session->getUser()) and
             ! is_null($this->core->session->getUser()->creation_ts)) {
            $this->assign('current_user_id', 
                $this->core->session->getUser()
                    ->getId());
            $this->assign('current_user', 
                $this->core->session->getUser()
                    ->jsonSerialize());
        }
    }

    /**
     * Registry Functions as Smarty Plugins
     */
    public function registerFunctionPlugins()
    {
        $sai_smarty_plugins = new sai_smarty_plugins();
        $this->registerPlugin('function', 'get_lang', 
            array(
                $sai_smarty_plugins,
                "getLang"
            ));
        $this->registerPlugin('function', 'get_bootstrap_pagination', 
            array(
                $sai_smarty_plugins,
                "getBootstrapPageNavigation"
            ));
        $this->registerPlugin('function', 'truncate_str', 
            array(
                $sai_smarty_plugins,
                "getTruncatedString"
            ));
        $this->registerPlugin('function', 'name_for_url', 
            array(
                $sai_smarty_plugins,
                "getNameForURL"
            ));
        $this->registerPlugin('function', 'get_const', 
            array(
                $sai_smarty_plugins,
                'getConst'
            ));
        $this->registerPlugin('function', 'get_age', 
            array(
                $sai_smarty_plugins,
                'getAge'
            ));
    }

    public function loadNavRights(&$nav)
    {
        foreach ($nav as $k => $v) {
            if (isset($v['permissions']) and ! empty($v['permissions'])) {
                try {
                    $rights = explode(" ", $v['permissions']);
                    foreach ($rights as $r) {
                        $this->core->hasRight($r);
                    }
                } catch (Sai\Modules\Auth\Exception $e) {
                    unset($nav[$k]);
                    continue;
                }
            }
            if (isset($v['childs']) and count($v['childs']) > 0) {
                $this->loadNavRights($v['childs']);
                $nav[$k]['childs'] = $v['childs'];
            }
        }
    }

    public function loadQueryString()
    {
        if (isset($_SERVER['QUERY_STRING']) and $_SERVER['QUERY_STRING'] != '') {
            $query_string = $_SERVER['QUERY_STRING'];
            $query_arr = explode("&", $query_string);
            foreach ($query_arr as $k => $v) {
                if (strpos($v, 'url=') === 0) {
                    unset($query_arr[$k]);
                } elseif (strpos($v, 'locale=') === 0) {
                    unset($query_arr[$k]);
                } elseif (strpos($v, 'page=') === 0) {
                    unset($query_arr[$k]);
                }
            }
            $query_string = implode("&", $query_arr);
            $this->assign('query_string', $query_string);
        }
    }

    /**
     * Displays a Smarty template smartly with SAI Template ;)
     *
     * @param string $template
     *            the resource handle of the template file or template object
     * @param mixed $cache_id
     *            cache id to be used with this template
     * @param mixed $compile_id
     *            compile id to be used with this template
     * @param object $parent
     *            next higher level of Smarty variables
     */
    public function display($template = null, $cache_id = null, $compile_id = null, 
        $parent = null)
    {
        if (strpos($template, '.tpl') === FALSE) {
            $template .= '.tpl';
        }
        parent::display($template, $cache_id, $compile_id, $parent);
    }
}
