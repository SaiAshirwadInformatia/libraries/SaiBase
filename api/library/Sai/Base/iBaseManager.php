<?php
namespace Sai\Base;

/**
 *
 * @name iBaseManager
 * @abstract Interface for Sai Base Manager represents base calls for REST
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */

/**
 * iSaiBaseManager interfaces helps to define the method structure a manager class should posses
 *
 * Methods
 * create	- For performing modular create operation
 * get - For performing modular get operation
 * update	- For performing modular update operation
 * delete	- For performing modular delete operation
 * my		- For getting my (user) related modular details
 * meta	- For getting meta about the module
 */
interface iBaseManager
{

    /**
     * Called on HTTP POST Method
     */
    public function create();

    /**
     * Called on HTTP GET Method
     */
    public function get($id);

    /**
     * Called on HTTP PUT Method
     */
    public function update();

    /**
     * Called on HTTP DELETE Method
     */
    public function delete();

    /**
     * Called on GET {$object}/my
     */
    public function my();

    /**
     * Called on GET {$object}/meta
     */
    public function meta();
}
