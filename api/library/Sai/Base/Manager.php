<?php
namespace Sai\Base;

use Symfony\Component\Validator\Validation;
use Sai\Base;

/**
 *
 * @name Base Manager
 * @abstract Would base out the manager operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager implements iBaseManager
{

    public $core;

    public $object;

    public $object_name;

    public $object_api_href;

    public $validator;

    public function __construct($object = null)
    {
        $this->core = core::getInstance();
        $this->setObject($object);
        $this->validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
    }

    /**
     *
     * @param unknown $object            
     * @throws Sai\Base\Modules\Exception
     * @return void|base_manager
     */
    public function setObject($object)
    {
        if (is_null($object)) {
            return;
        }
        if (is_object($object)) {
            $this->object_name = get_class($object);
            if (strpos($this->object_name, "_manager") !== false) {
                $c = $this->object_name = str_replace("_manager", "", 
                    $this->object_name);
                $this->object = new $c(false);
            } else {
                $this->object = $object;
            }
        } else {
            if (! class_exists($object)) {
                throw new Base\Modules\Exception(
                    Base\Modules\Exception::OBJECT_REQ);
            }
            $this->object = new $object(false);
            $this->object_name = $object;
        }
        $this->object_api_href = $this->core->apihref . '/' .
             str_replace("_", '/', $this->object_name);
        return $this;
    }

    /**
     *
     * @param array $obj_content            
     */
    protected function parseBeforeSQLOperation(&$obj_content)
    {
        if (isset($_FILES) and count($_FILES) > 0) {
            foreach ($_FILES as $file => $file_arr) {
                if (property_exists($this->object, $file)) {
                    $files_manager = managers_store::locate('files');
                    if (! is_array($file_arr['name'])) {
                        try {
                            $f = $files_manager->addFile($file_arr);
                            $obj_content[$file] = $f['url'];
                            if (property_exists($this->object, 'file_id')) {
                                $obj_content['file_id'] = $f['id'];
                                unset($obj_content['files']);
                            }
                            if (isset($f['thumb']) and
                                 property_exists($this->object, 'thumbnail_url')) {
                                $obj_content['thumbnail_url'] = $f['thumb'];
                            }
                            if (isset($f['compress']) and property_exists(
                                $this->object, 'compressed_url')) {
                                $obj_content['compressed_url'] = $f['compress'];
                            }
                        } catch (Base\Exception $e) {}
                    }
                }
            }
        }
        foreach ($obj_content as $k => $v) {
            if (is_array($v)) {
                if (isset($v['id'])) {
                    $_k = $k;
                    if (strrpos($k, 's') === strlen($k) - 1) {
                        $_k = substr($k, 0, strlen($k) - 1);
                    }
                    $_k .= '_id';
                    if (property_exists($this->object, $_k)) {
                        $obj_content[$_k] = $v['id'];
                    }
                }
                unset($obj_content[$k]);
            } elseif (is_object($v)) {
                if (method_exists($v, 'getId')) {
                    $_k = $k;
                    if (strpos($k, 's') === strlen($k) - 1) {
                        $_k = substr($k, 0, strlen($k) - 1);
                    }
                    $_k .= '_id';
                    if (property_exists($this->object, $_k)) {
                        $obj_content[$_k] = $v->getId();
                    }
                }
                unset($obj_content[$k]);
            }
        }
    }

    /**
     * Baselined most common activity of create method for object creation
     *
     * Called on HTTP POST Method
     */
    public function create()
    {
        $this->object->loadArgs();
        if (! method_exists($this->object, 'jsonSerialize')) {
            throw new modules_exception(
                modules_exception::JSON_OBJECT_SERIALIZE_REQ);
        }
        $obj_content = $this->object->jsonSerialize();
        $this->parseBeforeSQLOperation($obj_content);
        
        $ret = $this->core->db->insert(
            array(
                "tablename" => $this->object_name,
                "data" => $obj_content,
                "upsert" => true
            ));
        $c_e = $this->object_name . "_exception";
        if ($ret['status'] === OK) {
            return array(
                "id" => $ret['id'],
                "_self" => $this->object_api_href . "/" . $ret['id'],
                "msg" => $this->core->i18n->get_lang(
                    $this->object_name . "_create_success")
            );
        }
        throw new $c_e($ret['error_code']);
    }

    /**
     * Baselined most common activity of get method to retrieve object
     *
     * Called on HTTP GET METHOD
     */
    public function get($data)
    {
        $c_e = $this->object_name . "_exception";
        $this->object->loadDefaults();
        if (is_numeric($data)) {
            $where_data = array(
                'id' => $data
            );
            $this->object->loadObjectByDBCol($where_data);
        } elseif (is_string($data)) {
            $data = url_to_name($data);
            if (property_exists($this->object, 'name')) {
                $this->object->loadObjectByDBCol(
                    array(
                        'name' => $data
                    ));
            } elseif (property_exists($this->object, 'title')) {
                $this->object->loadObjectByDBCol(
                    array(
                        'title' => $data
                    ));
            } else {
                throw new $c_e($c_e::STRING_ATTR_NOT_VALID);
            }
        }
        if (property_exists($this->object, 'is_active')) {
        /**
         * TO DO
         */
        }
        if (property_exists($this->object, 'is_public') and
             $this->object->is_public != 1) {
            $auth_manager = managers_store::locate('auth');
            if (! $auth_manager->validate()) {
                throw new auth_exception(auth_exception::AUTHENTICATION_REQUIRED);
            }
        }
        
        if (property_exists($this->object, 'creation_ts') and ! is_null(
            $this->object->creation_ts)) {
            return $this->object;
        } elseif (! is_null($this->object->getId())) {
            return $this->object;
        }
        throw new $c_e($c_e::NOT_FOUND);
    }

    /**
     * Baselined most common activity of update method for object updation
     *
     * Called on HTTP PUT Method
     */
    public function update()
    {
        $this->object->loadArgs();
        if (! method_exists($this->object, 'jsonSerialize')) {
            throw new modules_exception(
                modules_exception::JSON_OBJECT_SERIALIZE_REQ);
        }
        $obj_content = $this->object->jsonSerialize();
        $this->parseBeforeSQLOperation($obj_content);
        
        $ret = $this->core->db->update(
            array(
                "tablename" => $this->object_name,
                "set" => $obj_content,
                "where" => array(
                    "data" => array(
                        "id" => $this->object->getId()
                    ),
                    "condition" => "AND"
                )
            ));
        $c_e = $this->object_name . "_exception";
        if ($ret['status'] === OK) {
            return array(
                "id" => $this->object->getId(),
                "_self" => $this->object_api_href . "/" . $this->object->getId(),
                "msg" => $this->core->i18n->get_lang(
                    $this->object_name . "_update_success")
            );
        }
        
        throw new $c_e($ret['error_code']);
    }

    /**
     * Baselined most common activity of delete method to delete object
     *
     * Called on HTTP DELETE METHOD
     */
    public function delete()
    {
        $this->object->loadArgs();
        $c_e = $this->object_name . "_exception";
        if (! is_null($this->object->getId()) and
             is_numeric($this->object->getId())) {
            $this->deleteById();
        } else {
            if (property_exists($this->object, 'name') and
                 ! is_null($this->object->name)) {
                $this->deleteByName();
            } elseif (property_exists($this->object, 'title') and
                 ! is_null($this->object->title)) {
                $this->deleteByName();
            } else {
                throw new $c_e($c_e::NOT_FOUND);
            }
        }
        
        change_response_code(204);
    }

    /**
     * Baselined most common activity of delete method to delete object by its id
     *
     * Called by delete method on HTTP DELETE Method
     */
    protected function deleteById()
    {
        $this->get($this->object->getId());
        $c_e = $this->object_name . "_exception";
        $where = array(
            'data' => array(
                'id' => $this->object->getId()
            ),
            'condition' => 'AND'
        );
        if (property_exists($this->object, 'is_active')) {
            $result = $this->core->db->update(
                array(
                    'tablename' => $this->object_name,
                    'where' => $where,
                    'set' => array(
                        'is_active' => - 1
                    )
                ));
        } else {
            $result = $this->core->db->delete(
                array(
                    "tablename" => $this->object_name,
                    "where" => $where
                ));
        }
        if (is_null($result) or $result['status'] === NOT_OK) {
            throw new $c_e($c_e::DELETE_FAILED);
        }
    }

    /**
     */
    protected function deleteByName()
    {
        $call_property = "";
        $c_e = $this->object_name . "_exception";
        if (property_exists($this->object, 'name')) {
            $call_property = 'name';
        } elseif (property_exists($this->object, 'title')) {
            $call_property = 'title';
        } else {
            throw new $c_e();
        }
        $where = array(
            'data' => array(
                $call_property => $this->object->$call_property
            ),
            'condition' => 'AND'
        );
        if (property_exists($this->object, 'is_active')) {
            $result = $this->core->db->update(
                array(
                    'tablename' => $this->object_name,
                    'where' => $where,
                    'set' => array(
                        'is_active' => - 1
                    )
                ));
        } else {
            $result = $this->core->db->delete(
                array(
                    'tablename' => $this->object_name,
                    'where' => $where
                ));
        }
        if (is_null($result) or $result['status'] === NOT_OK) {
            throw new $c_e($c_e::DELETE_FAILED);
        }
    }

    /**
     * Called on GET {$object}/my
     */
    public function my()
    {
        $auth_manager = managers_store::locate('auth');
        if (! $auth_manager->validate()) {
            throw new auth_exception(auth_exception::AUTHENTICATION_REQUIRED);
        }
        if (property_exists($this->object, 'user_id')) {
            $_REQUEST['data']['user_id'] = $this->core->session->getUser()->getId();
            $result = $this->search();
            return $result['results'];
        }
    }

    /**
     * Called on GET {$object}/meta
     */
    public function meta()
    {
        $sampleData = $this->object->jsonSerialize();
        if (array_key_exists('url_for_link', $sampleData)) {
            unset($sampleData['url_for_link']);
        }
        return array(
            'object' => $this->object_name,
            'href' => $this->object_api_href,
            'sampleData' => $sampleData
        );
    }

    /**
     */
    public function deleted()
    {
        $this->core->hasRight('view_deleted_' . $this->object_name);
        if (isset($_REQUEST['data'])) {
            $_REQUEST['data'] = array_merge($_REQUEST['data'], 
                array(
                    "is_active" => - 1
                ));
        } else {
            $_REQUEST['data'] = array(
                "is_active" => - 1
            );
        }
        
        $deleted = $this->search();
        if (isset($deleted['found']) and $deleted['found'] > 0) {
            return $deleted;
        }
        return array();
    }

    /**
     */
    public function all($data = array())
    {
        $public_objects = array();
        $c_e = $this->object_name . "_exception";
        $where_data = array();
        if (property_exists($this->object, 'is_active')) {
            $where_data['is_active'] = 1;
        }
        if (property_exists($this->object, 'is_public')) {
            $auth_manager = managers_store::locate('auth');
            if (! $auth_manager->validate()) {
                $where_data['is_public'] = 1;
            }
        }
        if (! isset($data['orderby'])) {
            $data['orderby'] = 'id DESC';
        }
        $result = $this->core->db->selectAll(
            array(
                'tablename' => $this->object_name,
                'where' => array(
                    'data' => $where_data,
                    'condition' => 'AND'
                ),
                'orderby' => $data['orderby']
            ));
        
        if (! is_null($result)) {
            foreach ($result as $r) {
                $this->object->loadDefaults();
                $this->object->loadObjectInstance($r);
                $public_objects[] = $this->object->jsonSerialize();
            }
        }
        if (count($public_objects) == 0) {
            throw new $c_e($c_e::NOT_FOUND);
        }
        return $public_objects;
    }

    /**
     *
     * @author Rohan Sakhale
     * @author Chaitali Patil
     * @throws search_exception
     * @return multitype:number NULL unknown string multitype:NULL
     */
    public function search()
    {
        $loadArgs = true;
        if (isset($_REQUEST['data']) and count($_REQUEST['data']) > 0) {
            $loadArgs = false;
        }
        if ($loadArgs) {
            $this->object->loadArgs();
        }
        if (! isset($_REQUEST['data'])) {
            if (property_exists($this->object, "is_active")) {
                $_REQUEST['data'] = array(
                    "is_active" => 1
                );
            }
        } elseif (property_exists($this->object, "is_active") and
             ! array_key_exists("is_active", $_REQUEST['data'])) {
            $_REQUEST['data'] = array_merge($_REQUEST['data'], 
                array(
                    "is_active" => 1
                ));
        }
        $auth_manager = managers_store::locate('auth');
        if (! $auth_manager->validate()) {
            if (! isset($_REQUEST['data'])) {
                if (property_exists($this->object, "is_public")) {
                    $_REQUEST['data'] = array(
                        "is_public" => 1
                    );
                }
            } elseif (property_exists($this->object, "is_public")) {
                $_REQUEST['data'] = array_merge($_REQUEST['data'], 
                    array(
                        "is_public" => 1
                    ));
            }
        }
        
        if (! isset($_REQUEST['data']) or ! is_array($_REQUEST['data'])) {
            throw new search_exception(search_exception::REQUIRED_ATTR_MISSING);
        }
        
        foreach ($_REQUEST['data'] as $data_key => $data_value) {
            if (is_array($data_value) and $data_key != 'id') {
                $object_name = $data_key;
                if (class_exists($object_name)) {
                    $data_object_manager = managers_store::locate($object_name);
                    $object_data = $_REQUEST['data'];
                    $_REQUEST['data'] = $data_value;
                    
                    $object = $data_object_manager->search();
                    if (isset($object['results']) and
                         count($object['results']) > 0) {
                        $obj_id = array();
                        foreach ($object['results'] as $r) {
                            $obj_id[] = $r['id'];
                        }
                        
                        if (strrpos($data_key, 's') == strlen($data_key) - 1) {
                            $data_key = substr($data_key, 0, 
                                strlen($data_key) - 1);
                        }
                        $data_key .= '_id';
                        
                        $object_data[$data_key] = $obj_id;
                    }
                    $_REQUEST['data'] = $object_data;
                }
                unset($_REQUEST['data'][$object_name]);
            }
        }
        
        if (! isset($_REQUEST['condition'])) {
            $_REQUEST['condition'] = 'AND';
        } elseif (! in_array($_REQUEST['condition'], 
            array(
                'AND',
                'OR'
            ))) {
            throw new search_exception(search_exception::INVALID_CONDITION);
        }
        $page = 1;
        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        }
        // $this->object->loadObjectInstance($_REQUEST['data']);
        // $where_filter_data = $this->object->jsonSerialize();
        // $this->parseBeforeSQLOperation($where_filter_data);
        $bad_keys = array(
            'url_for_link',
            'display_picture',
            'dob_range',
            'anniversary_range'
        );
        $where_filter_data = $_REQUEST['data'];
        foreach ($bad_keys as $bk) {
            if (array_key_exists($bk, $where_filter_data)) {
                unset($where_filter_data[$bk]);
            }
        }
        
        $where_data = array(
            "condition" => $_REQUEST['condition'],
            "data" => $where_filter_data
        );
        if (isset($_REQUEST['query'])) {
            $where_data['query'] = $_REQUEST['query'];
            unset($_REQUEST['query']);
        }
        $db_obj = array(
            'tablename' => $this->object_name,
            'where' => $where_data,
            'like' => true
        );
        $total = $this->core->db->count($db_obj);
        
        if ($total == 0) {
            throw new search_exception(search_exception::NOT_FOUND);
        }
        $search_count = $this->core->configs->search_count;
        if (isset($_REQUEST['search_count']) and
             is_numeric($_REQUEST['search_count'])) {
            $search_count = $_REQUEST['search_count'];
        }
        if (isset($_REQUEST['orderby']) and ! empty($_REQUEST['orderby'])) {
            $db_obj['orderby'] = $_REQUEST['orderby'];
        }
        $totalPages = ceil($total / $search_count);
        $lower_limit = ($page - 1) * $search_count;
        $currentPosition = $lower_limit + 1;
        $db_obj['limit'] = array(
            'offset' => $lower_limit,
            'maximum' => $search_count
        );
        $result = $this->core->db->selectAll($db_obj);
        if ($this->core->db->con->errno > 0) {
            throw new search_exception($this->core->db->con->errno);
        }
        if (empty($result)) {
            throw new search_exception(search_exception::NOT_FOUND);
        } else {
            /**
             * In case class named as table exists
             * We should load the object and instantiate it with data acquired from db
             * This will help us achieve jsonSerialize of every object which would in turn format the data as expected
             * Considering objects within objects
             *
             * P.S. fix is depended on loadObjectInstance internally
             * so if thats not done properly we won't achieve what we wanted to
             *
             * @since Ghanerao v1
             */
            
            $resultArr = array();
            foreach ($result as $_r) {
                $this->object->loadDefaults();
                $this->object->loadObjectInstance($_r);
                $resultArr[] = $this->object->jsonSerialize();
            }
            
            return array(
                "results" => $resultArr,
                "found" => count($resultArr),
                "totalFound" => intval($total),
                "page" => $page,
                "totalPages" => $totalPages
            );
        }
    }

    /**
     *
     * @param
     *            unknown
     * @return multitype:Ambigous <NULL, events>
     */
    public function prevnext($object = null)
    {
        $prevevent = null;
        if (! is_null($object)) {
            if (is_array($object)) {
                $this->object->loadObjectInstance($object);
            } elseif (is_object($object)) {
                $this->object = $object;
            }
        } else {
            $this->object->loadArgs();
        }
        $table = str_replace("_manager", "", get_class($this->object));
        $query = "SELECT * FROM `$table` WHERE `id` <  " . $this->object->getId() .
             " ORDER BY `id` DESC LIMIT 0,1";
        
        $r = $this->core->db->exec_Query($query);
        $prev = $this->core->db->resultToArray($r);
        if (isset($prev[0])) {
            $prevevent = new $table(false);
            $prevevent->loadObjectInstance($prev[0]);
        }
        $nextevent = null;
        $query = "SELECT * FROM `$table` WHERE `id` > " . $this->object->getId() .
             " ORDER BY `id` ASC LIMIT 0,1";
        $r = $this->core->db->exec_Query($query);
        $next = $this->core->db->resultToArray($r);
        if (isset($next[0])) {
            $nextevent = new $table(false);
            $nextevent->loadObjectInstance($next[0]);
        }
        
        return array(
            "prev" => $prevevent,
            "next" => $nextevent
        );
    }
}