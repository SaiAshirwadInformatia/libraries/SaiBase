<?php
namespace Sai\Base;

/**
 *
 * @name Managers Store
 * @abstract Would help to maintain all managers together and avoid duplicate manager creation
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class ManagersStore
{

    /**
     *
     * @var object $instance Private static instance of self class
     */
    private static $instance;

    /**
     * Disable object creation from outside
     * Follows Singleton Design Pattern
     */
    private function __construct()
    {}

    /**
     * Static locator
     *
     * Helps store single instance of manager classes in its private instance
     * This way it avoid duplicate creation of manager classes
     * Also helps in better creational way
     *
     * P.S. This class acts like Managers Factory performing in-house managers production and storage
     */
    public static function locate($class_name)
    {
        if (! isset(self::$instance)) {
            self::$instance = new ManagersStore();
        }
        if (! property_exists(self::$instance, $class_name)) {
            $c = $class_name . '_manager';
            if (class_exists($c)) {
                $reflection = new ReflectionClass($c);
                $constructor_params = $reflection->getMethod('__construct')->getParameters();
                if (count($constructor_params) > 0) {
                    self::$instance->$class_name = new $c($class_name);
                } else {
                    self::$instance->$class_name = new $c();
                }
            } else {
                self::$instance->$class_name = new base_manager($class_name);
            }
        }
        return self::$instance->$class_name;
    }
}
