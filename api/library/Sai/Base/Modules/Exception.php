<?php
namespace Sai\Base\Modules;

use Sai\Base;

/**
 *
 * @name Module Exception
 * @abstract Module exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */

/**
 * Manages exception handling for module
 */
class Exception extends Base\Exception
{

    const INVALID_MODULE = 1007;

    const MODULE_NOT_AVAILABLE = 1008;

    const SERVICE_NOT_AVAILABLE = 1009;

    const SERVICE_NOT_ALLOWED = 1010;

    const INVALID_METHOD = 1011;

    const ID_NUMERIC_REQ = 1012;

    const OBJECT_REQ = 1013;

    const JSON_OBJECT_SERIALIZE_REQ = 1014;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::INVALID_MODULE => "excp_modules_invalid_module",
            self::MODULE_NOT_AVAILABLE => "excp_modules_module_not_available",
            self::SERVICE_NOT_AVAILABLE => "excp_modules_service_not_available",
            self::SERVICE_NOT_ALLOWED => "excp_modules_service_not_allowed",
            self::INVALID_METHOD => "excp_modules_invalid_method",
            self::ID_NUMERIC_REQ => "excp_modules_id_numeric_req",
            self::OBJECT_REQ => "excp_modules_module_object_req",
            self::JSON_OBJECT_SERIALIZE_REQ => "excp_modules_json_object_serialize_req"
        );
        $this->exception_number = $exception_number;
        $this->response_error_code[400][] = self::ID_NUMERIC_REQ;
        $this->response_error_code[400][] = self::OBJECT_REQ;
        $not_allowed = array(
            self::SERVICE_NOT_ALLOWED,
            self::INVALID_METHOD,
            self::INVALID_MODULE
        );
        
        if (isset($this->response_error_code[405])) {
            $this->response_error_code[405] = array_merge(
                $this->response_error_code[405], $not_allowed);
        } else {
            $this->response_error_code[405] = array(
                self::SERVICE_NOT_ALLOWED,
                self::INVALID_METHOD,
                self::INVALID_MODULE
            );
        }
        $this->response_error_code[501][] = self::SERVICE_NOT_AVAILABLE;
    }
}
