<?php
namespace Sai\Base\FileTypes;

class Manager
{

    private $filetypes;

    public function __construct()
    {
        $filetypes_registry = REGISTRY_DIR . DS . 'filetypes.json';
        if (file_exists($filetypes_registry)) {
            $filetypes = json_decode(file_get_contents($filetypes_registry), 
                true);
            $this->filetypes = $filetypes;
        }
    }

    public function __get($name)
    {
        if (isset($this->filetypes[$name])) {
            return $this->filetypes[$name];
        } else {
            return '<span class="btn btn-danger btn-xs">Bad Icon Mann!!!!</span>';
        }
    }

    public function all()
    {
        return $this->filetypes;
    }
}