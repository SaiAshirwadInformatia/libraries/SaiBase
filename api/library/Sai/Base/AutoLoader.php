<?php
namespace Sai\Base;

use Doctrine\Common\Annotations\AnnotationRegistry;

/**
 *
 * @name SaiAutoLoad
 * @abstract Load classes dynamically on fly ;)
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class AutoLoader
{

    private static $instance;

    private function __construct()
    {
        /**
         * Using PHP5 AutoLoad Register
         *
         * This way, we multiple register autoload functions
         * This anonymous function would attempt to load undefined classes
         */
        spl_autoload_register(
            function ($class_name)
            {
                $path = SAI_ABS_PATH . 'api' . DS;
                
                /**
                 * Check if class exists in `api` folder
                 */
                if (file_exists($path . $class_name . '.php')) {
                    require_once $path . $class_name . '.php';
                    return;
                }
                
                /**
                 * If class not loaded, check into additional folders within API
                 */
                $look_into_folders = array(
                    'library'
                );
                
                foreach ($look_into_folders as $folder) {
                    if (file_exists($path . $folder . DS . $class_name . '.php')) {
                        require_once $path . $folder . DS . $class_name . '.php';
                        return;
                    }
                }
            });
        
        /**
         * Lets use Composer AutoLoader for loading third party libraries
         */
        if (! defined('THIRD_PARTY_DIR')) {
            define('THIRD_PARTY_DIR', SAI_ABS_PATH . 'vendor');
        }
        if (file_exists(THIRD_PARTY_DIR . DS . 'autoload.php')) {
            require_once THIRD_PARTY_DIR . DS . 'autoload.php';
        }
        
        /**
         * Bad Doctrine is not able to identify Symfony Annotated Classes
         * Hence we register Symfony Constraints namespace explicitly
         */
        if (class_exists('Symfony\\Component\\Validator\\Constraint')) {
            AnnotationRegistry::registerAutoloadNamespace(
                "Symfony\Component\Validator\Constraints", 
                THIRD_PARTY_DIR . DS . "symfony" . DS . "validator");
            AnnotationRegistry::registerAutoloadNamespace(
                "Symfony\Component\Validator\Constraint", 
                THIRD_PARTY_DIR . DS . "symfony" . DS . "validator");
        }
    }

    public static function register()
    {
        if (is_null(self::$instance)) {
            self::$instance = new AutoLoader();
        }
    }
}