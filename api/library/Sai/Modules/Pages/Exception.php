<?php
namespace Sai\Modules\Pages;

use Sai\Base;

/**
 *
 * @name Pages Exception
 * @abstract Pages exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 1401;

    const UNIQUE = 1402;

    const CREATE_FAILED = 1403;

    const NOT_FOUND = 1404;

    const UPDATE_FAILED = 1405;

    const DELETE_FAILED = 1406;

    const NO_PAGES_CREATED = 1407;

    const INVALID_PAGE = 1408;

    const INVALID_HAS_SIDEBAR = 1409;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_pages_req_missing",
            self::UNIQUE => "excp_pages_unique",
            self::CREATE_FAILED => "excp_pages_create_failed",
            self::NOT_FOUND => "excp_pages_not_found",
            self::UPDATE_FAILED => "excp_pages_update_failed",
            self::DELETE_FAILED => "excp_pages_delete_failed",
            self::NO_PAGES_CREATED => "excp_pages_no_pages_created",
            self::INVALID_PAGE => "excp_pages_invalid_page",
            self::INVALID_HAS_SIDEBAR => "excp_pages_invalid_has_sidebar"
        );
        $this->response_error_code[404] = array_merge(
            $this->response_error_code[400], 
            array(
                self::NO_PAGES_CREATED,
                self::INVALID_PAGE
            ));
    }
}
