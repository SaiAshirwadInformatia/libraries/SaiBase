<?php
namespace Sai\Modules\Pages;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Pages Manager
 * @abstract Would help perform page related operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager extends Base\Manager
{

    /**
     * Constructor for Pages Manager
     */
    public function __construct($object = 'pages')
    {
        parent::__construct($object);
    }

    /**
     */
    public function create()
    {
        $this->object->loadArgs();
        $page_content = $this->object->jsonSerialize();
        $page_content['meta_author'] = $page_content['meta']['author'];
        $page_content['meta_keywords'] = $page_content['meta']['keywords'];
        $page_content['meta_description'] = $page_content['meta']['description'];
        if (isset($page_content['meta'])) {
            unset($page_content['meta']);
        }
        $result = $this->core->db->insert(
            array(
                'tablename' => 'pages',
                'data' => $page_content
            ));
        if ($result['status'] == 'OK') {
            return array(
                'msg' => $this->core->i18n->get_lang("pages_create_success"),
                "id" => $result['id'],
                'slug' => $this->object_api_href . '/' . $page_content['slug']
            );
        }
        throw new Modules\Pages\Exception($result['error_code']);
    }

    /**
     */
    public function get($data)
    {
        if (is_numeric($data)) {
            return parent::get($data);
        } else {
            $page = new Modules\Pages\Object(false);
            $auth_manager = Base\ManagersStore::locate('auth');
            $page->loadObjectByDBCol(
                array(
                    'slug' => $data
                ));
            if (! is_null($page->creation_ts)) {
                if (! $auth_manager->validate() and ! $page->is_public) {
                    throw new Modules\Pages\Exception(
                        Modules\Pages\Exception::NOT_FOUND);
                }
            } else {
                throw new Modules\Pages\Exception(
                    Modules\Pages\Exception::NOT_FOUND);
            }
            return $page;
        }
    }

    /**
     */
    public function update()
    {
        $this->object->loadArgs();
        $page_content = $this->object->jsonSerialize();
        $page_content['meta_author'] = $page_content['meta']['author'];
        $page_content['meta_keywords'] = $page_content['meta']['keywords'];
        $page_content['meta_description'] = $page_content['meta']['description'];
        unset($page_content['meta']);
        
        $result = $this->core->db->update(
            array(
                'tablename' => 'pages',
                'set' => $page_content,
                'where' => array(
                    'condition' => 'AND',
                    'data' => array(
                        'id' => $page_content['id']
                    )
                )
            ));
        if ($result['status'] != 'NOT_OK') {
            return array(
                "id" => $page_content['id'],
                "msg" => $this->core->i18n->get_lang("pages_update_success"),
                "_self" => $this->object_api_href . '/' . $page_content['slug']
            );
        }
        throw new Modules\Pages\Exception(Modules\Pages\Exception::UPDATE_FAILED);
    }
}
