<?php
namespace Sai\Modules\Pages;

use Sai\Base;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @name Pages
 * @abstract Would represent the pages object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $title Page Title
     */
    public $title;

    /**
     *
     * @var array $meta Meta Information of the Page represents a collection of Author, Keywords, Description
     */
    public $meta;

    /**
     *
     * @var string $meta_author Meta Author of the Page
     */
    public $meta_author;

    /**
     *
     * @var string $meta_keywords Meta Keywords of the Page
     */
    public $meta_keywords;

    /**
     *
     * @var string $meta_description Meta Description of the Page
     */
    public $meta_description;

    /**
     *
     * @var string $content Content to be displayed over Page
     */
    public $content;

    /**
     *
     * @var string $slug Uniquely identified Page Slug URL
     */
    public $slug;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @var timestamp $lastmodified_ts Represents the database lastmodified timestamp of the object
     */
    public $lastmodified_ts;

    /**
     *
     * @var boolean $has_menubar States whether the page should display a menu bar or not
     */
    public $has_menubar;

    /**
     *
     * @var boolean $has_sidebar States whether the page should display sidebar and on which position
     *     
     *      Possible Values
     *      0 ==> None
     *      1 ==> Left
     *      2 ==> Right
     *     
     *      @Assert\Choice(choices={0,1,2},message="INVALID_HAS_SIDEBAR")
     */
    public $has_sidebar;

    /**
     *
     * @var integer
     */
    public $is_auth_req;

    /**
     *
     * @var integer
     */
    public $is_auth_not_req;

    /**
     *
     * @var integer $is_active Represents the possible state of an object
     *     
     *     
     *      Possible values
     *      1 - Active State
     *      0 - Inactive State
     *      -1 - Deleted State
     */
    public $is_active;

    /**
     *
     * @var integer $is_public Whether this menu is publicly accessible without require of authentication
     */
    public $is_public;

    /**
     * Constructor for Pages Object
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'title',
            'meta',
            'meta_author',
            'meta_keywords',
            'meta_description',
            'content',
            'slug',
            'creation_ts',
            'lastmodified_ts',
            'has_menubar',
            'has_sidebar',
            'is_auth_req',
            'is_auth_not_req',
            'is_active',
            'is_public'
        );
        if ($loadArgs) {
            $this->loadArgs();
            if (! is_null($this->id)) {
                $this->loadObject($this->id);
            }
        }
    }

    /**
     */
    public function loadDefaults()
    {
        parent::loadDefaults();
        $this->meta = array();
        $this->has_menubar = 1;
        $this->has_sidebar = 2;
        $this->is_auth_req = 0;
        $this->is_auth_not_req = 0;
        $this->is_active = 1;
        $this->is_public = 1;
    }

    /**
     */
    public function loadMeta()
    {
        if (isset($this->meta)) {
            if (is_array($this->meta)) {
                if (isset($this->meta['author'])) {
                    $this->meta_author = $this->meta['author'];
                }
                if (isset($this->meta['keywords'])) {
                    $this->meta_keywords = $this->meta['keywords'];
                }
                if (isset($this->meta['description'])) {
                    $this->meta_description = $this->meta['description'];
                }
            } elseif (is_object($this->meta)) {
                if (isset($this->meta->author)) {
                    $this->meta_author = $this->meta->author;
                }
                if (isset($this->meta->keywords)) {
                    $this->meta_keywords = $this->meta->keywords;
                }
                if (isset($this->meta->description)) {
                    $this->meta_description = $this->meta->description;
                }
            }
        }
    }

    /**
     */
    public function loadObject($id)
    {
        parent::loadObject($id);
    }

    /**
     */
    public function loadObjectByDBCol($data)
    {
        parent::loadObjectByDBCol($data);
    }

    /**
     */
    public function loadObjectInstance($arr)
    {
        parent::loadObjectInstance($arr);
        if (is_object($arr)) {
            if (property_exists($arr, 'meta_author')) {
                $this->meta['author'] = $arr->meta_author;
            }
            if (property_exists($arr, 'meta_keywords')) {
                $this->meta['keywords'] = $arr->meta_keywords;
            }
            if (property_exists($arr, 'meta_description')) {
                $this->meta['description'] = $arr->meta_description;
            }
        } elseif (is_array($arr)) {
            if (isset($arr['meta_author'])) {
                $this->meta['author'] = $arr['meta_author'];
            }
            if (isset($arr['meta_keywords'])) {
                $this->meta['keywords'] = $arr['meta_keywords'];
            }
            if (isset($arr['meta_description'])) {
                $this->meta['description'] = $arr['meta_description'];
            }
        }
    }

    /**
     * Overrides parent jsonSerialize
     *
     * Removes security related data from serialized array
     * password
     * acess_token
     */
    public function jsonSerialize()
    {
        $data = parent::jsonSerialize();
        $data['meta'] = array(
            'author' => $data['meta_author'],
            'keywords' => $data['meta_keywords'],
            'description' => $data['meta_description']
        );
        unset($data['meta_author']);
        unset($data['meta_keywords']);
        unset($data['meta_description']);
        return $data;
    }
}
