<?php
namespace Sai\Modules\Email;

use Sai\Base;

/**
 *
 * @name Email Manager
 * @abstract This class would help send emails
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager
{

    public $core;

    public $mailchimp;

    public $mandrill;

    const VERIFY_EMAIL = 174045;

    const FORGOT_PASSWORD = 126513;

    /**
     * Constructor for Email Manager
     *
     * Helps construct third party classes for mailing
     * MailChimp used for template management
     * Mandrill used for sending mails
     */
    public function __construct()
    {
        $this->core = Base\Core::getInstance();
        $this->mailchimp = new Mailchimp($this->core->configs->mailchimp_api_key, 
            array(
                'CURLOPT_FOLLOWLOCATION' => false
            ));
        $this->mandrill = new Mandrill($this->core->configs->mandrill_api_key);
        curl_setopt($this->mandrill->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->mandrill->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->mailchimp->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->mailchimp->ch, CURLOPT_SSL_VERIFYPEER, false);
    }

    /**
     * Send Email through Mandrill Services
     *
     * @param $to (Array)
     *            with name and email
     * @param $from (Array)
     *            with name and email
     * @param $subject (String)
     *            subject for email
     * @param $message (String)
     *            the email body
     * @param $reply_to (String)
     *            email to whom reply should recieve (Optional)
     *            
     *            Default: Config value from DB
     */
    public function sendEmail($from, $to, $subject, $message, $reply_to = null)
    {
        if (is_null($reply_to)) {
            $reply_to = $this->core->configs->email_reply_to;
        }
        $message = str_replace("*|BASEHREF|*", $this->core->basehref, $message);
        $message = str_replace("*|SUBJECT|*", $subject, $message);
        $message = str_replace("*|CURRENT_YEAR|*", $this->core->current_year, 
            $message);
        $message = str_replace("*|LIST:COMPANY|*", 
            $this->core->configs->sitename, $message);
        $message = array(
            'html' => $message,
            'subject' => $subject,
            'from_email' => $from['email'],
            'from_name' => $from['name'],
            'to' => array(
                array(
                    'email' => $to['email'],
                    'name' => $to['name']
                )
            ),
            'headers' => array(
                'Reply-To' => $reply_to
            ),
            'auto_text' => true,
            'important' => false,
            'track_opens' => true,
            'track_clicks' => true,
            'tracking_domain' => 'click.ghanerao.in',
            'signing_domain' => 'ghanerao.in',
            'return_path_domain' => 'click.ghanerao.in',
            'subaccount' => 'ghanerao'
        );
        $async = true;
        $this->mandrill->messages->send($message, $async);
    }

    /**
     * Get template from MailChimp
     *
     * @param $template_id (Number)
     *            unique id for mailchimp template
     *            
     * @return String
     */
    public function getTemplate($template_id)
    {
        $template_info = $this->mailchimp->templates->info($template_id);
        return $template_info['source'];
    }

    /**
     * Perform subscription of user as contact on MailChimp List
     *
     * P.S. if mailChimp wouldn't be used for mailing, this subscription should happen on local db
     *
     * @param $fname (String)
     *            First Name of subscriber
     * @param $lname (String)
     *            Last Name of subscriber
     * @param $email (String)
     *            Email Address of subscriber
     */
    public function subscribe($fname, $lname, $email)
    {
        $this->mailchimp->lists->subscribe("8058fd449c", 
            array(
                "email" => $email
            ), 
            array(
                "FNAME" => $fname,
                "LNAME" => $lname
            ), 'html', false, true, false, false);
    }
}
