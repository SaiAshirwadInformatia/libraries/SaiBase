<?php
namespace Sai\Modules\Auth;

use Sai\Base;

/**
 *
 * @name Authentication Exception
 * @abstract Authentication exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\Exception
{

    const NO_USER_ACCOUNT_ASSOCIATED = 1307;

    const INVALID_EMAIL_PASSWORD = 1308;

    const AWAITING_ACTIVATION = 1309;

    const AUTHENTICATION_REQUIRED = 1310;

    const INVALID_EMAIL = 1311;

    const REQUIRED_ATTR_MISSING = 1312;

    const ACCOUNT_DELETED = 1313;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::NO_USER_ACCOUNT_ASSOCIATED => "excp_auth_no_user_account_associated",
            self::INVALID_EMAIL_PASSWORD => "excp_auth_invalid_email_password",
            self::AWAITING_ACTIVATION => "excp_auth_awaiting_activation",
            self::AUTHENTICATION_REQUIRED => "excp_auth_authentication_required",
            self::INVALID_EMAIL => "excp_auth_invalid_email",
            self::REQUIRED_ATTR_MISSING => "excp_auth_req_missing",
            self::ACCOUNT_DELETED => "excp_auth_account_deleted"
        );
        $this->response_error_code[401] = array(
            self::NO_USER_ACCOUNT_ASSOCIATED,
            self::INVALID_EMAIL,
            self::INVALID_EMAIL_PASSWORD,
            self::AWAITING_ACTIVATION,
            self::AUTHENTICATION_REQUIRED
        );
        $this->response_error_code[403] = array(
            self::ACCOUNT_DELETED
        );
    }
}
