<?php
namespace Sai\Modules\Auth;

use Sai\Base;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @name Auth
 * @abstract Would help represent user authentication
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $email Users valid email address used for authentication
     *     
     *      @Assert\Email(message="INVALID_EMAIL")
     */
    public $email;

    /**
     *
     * @var string $username Users valid username used for authentication
     */
    public $username;

    /**
     *
     * @var string $password Users valid password used for authentication
     *     
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $password;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'email',
            'username',
            'password'
        );
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
        }
    }

    /**
     * Verifies password entered by user and password stored in Database
     *
     * @param string $password            
     * @param string $hash            
     * @return boolean
     */
    public static function verifyPassword($password, $hash)
    {
        return password_verify($password, $hash);
    }

    /**
     * Hashes the string using BCRYPT Algo
     *
     * @param $str (String)
     *            to be hashed
     *            
     * @return String (hashed)
     */
    public static function hashPassword($password)
    {
        // return md5(self::$salt . $str . self::$salt);
        return password_hash($password, PASSWORD_DEFAULT);
    }
}
