<?php
namespace Sai\Modules\Auth;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Auth
 * @abstract Would help perform user authentication
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager extends Base\Manager
{

    /**
     * Contructor for Auth Manager
     */
    public function __construct($object = 'auth')
    {
        parent::__construct($object);
    }

    /**
     * Create helps performing authentication
     *
     * If succeeds, session for the user is created.
     * Else error message are displayed
     *
     * @param
     *            Object of user containing email/password
     *            
     * @return Array
     *
     * @throws Object Modules\Auth\Exception
     */
    public function create()
    {
        $this->object->loadArgs();
        $result = $this->core->db->selectAll(
            array(
                'tablename' => 'users',
                'where' => array(
                    'condition' => 'OR',
                    'data' => array(
                        'email' => $this->object->email,
                        'username' => $this->object->username
                    )
                )
            ));
        
        if (count($result) > 0) {
            $result = $result[0];
            if (Modules\Auth\Object::verifyPassword($this->object->password, 
                $result['password'])) {
                if ($result['is_active'] == - 1) {
                    throw new Modules\Auth\Exception(
                        Modules\Auth\Exception::ACCOUNT_DELETED);
                }
                $user = new users(false);
                $user->loadObject($result['id']);
                $user->loadRights();
                if (! $user->hasRight('login_access_allowed')) {
                    throw new Modules\Auth\Exception(
                        Modules\Auth\Exception::AWAITING_ACTIVATION);
                }
                $this->core->session->setParam('currentUser', $user);
                return array(
                    'msg' => 'Successful',
                    'email' => $user->email,
                    'access_token' => $result['access_token']
                );
            }
            throw new Modules\Auth\Exception(
                Modules\Auth\Exception::INVALID_EMAIL_PASSWORD);
        }
        throw new Modules\Auth\Exception(
            Modules\Auth\Exception::NO_USER_ACCOUNT_ASSOCIATED);
    }

    /**
     */
    public function adminAuth()
    {
        $user = $this->core->session->getUser();
        if (isset($_POST) and count($_POST) > 0) {
            if (isset($_POST['email']) and isset($_POST['password'])) {
                if ($_POST['email'] == $user->email && Modules\Auth\Object::verifyPassword(
                    ($_POST['password']), $user->password)) {
                    $this->core->session->setParam('is_admin_logged', true);
                } else {
                    $this->core->session->setError(
                        'Invalid email/password combination for admin login');
                }
            }
        }
    }

    /**
     */
    public function validateAdmin()
    {
        $this->core->checkRight('login_as_admin');
        $this->adminAuth();
        return $this->core->session->getParam('is_admin_logged') === true;
    }

    /**
     * Validate identifies if user is logged in or not
     *
     * @return Boolean depending on status of user authentication
     */
    public function validate()
    {
        $validation = false;
        $headers = apache_request_headers();
        if (! is_null($this->core->session->getUser()) and
             ! is_null($this->core->session->getUser()->creation_ts)) {
            $user = new Modules\Users\Object(false);
            $user->loadObject(
                $this->core->session->getUser()
                    ->getId());
            $user->loadRights();
            $this->core->session->setParam('currentUser', $user);
            $this->core->setUser($user);
            $validation = true;
        } elseif (isset($headers['user-access-token'])) {
            $user_access_token = $headers['user-access-token'];
            $result = $this->core->db->selectAll(
                array(
                    'tablename' => 'users',
                    'where' => array(
                        'condition' => 'AND',
                        'data' => array(
                            'access_token' => $user_access_token
                        )
                    )
                ));
            if (count($result) > 0) {
                $access_user = new Modules\Users\Object(false);
                $access_user->loadObjectInstance($result[0]);
                $this->core->session->setParam('currentUser', $access_user);
                $this->core->setUser($access_user);
                $validation = true;
            }
        }
        return $validation;
    }

    /**
     * Currently this service is not allowed
     */
    public function get($data)
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }

    /**
     * Currently this service is not allowed
     */
    public function update()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }

    /**
     * Delete helps to logout user session
     *
     * @param
     *            Object of auth but it is not being used
     *            
     * @return String as empty
     */
    public function delete()
    {
        session_destroy();
        change_response_code(204);
        return '';
    }

    /**
     */
    public function invalidateAdmin()
    {
        $this->core->session->setParam('is_admin_logged', false);
    }

    /**
     * Currently this service is not allowed
     */
    public function my()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }

    /**
     * Call No Auth Allowed Method helps identify if the request call from user is allowed with login or not
     */
    public function call_no_auth_allowed_method($obj_class)
    {
        $request_method = $_SERVER['REQUEST_METHOD'];
        $no_auth_calls = $this->core->getNoAuthCalls();
        return (isset($no_auth_calls[$request_method]) and
             in_array($obj_class, $no_auth_calls[$request_method]));
    }
}
