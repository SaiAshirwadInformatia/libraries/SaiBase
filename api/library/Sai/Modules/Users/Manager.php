<?php
namespace Sai\Modules\Users;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Users Manager
 * @abstract Would help perform user related operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */

/**
 * Users Managers helps out operate Users Modular Activities
 * Most commonly helps out manage CRUD operations and additional user related operations
 */
class Manager extends Base\Manager
{

    /**
     * Constructor for Users Manager
     */
    public function __construct($object = 'users')
    {
        parent::__construct($object);
    }

    /**
     * Create helps create users object
     *
     * User creation is public as it acts as user registration
     * Hence user creation doesn't require authentication
     *
     * @param $user (Object)
     *            represents users class object
     *            
     * @return (Array) with id, msg, email and _self url
     *        
     * @throws (Object) Modules\Users\Exception
     *        
     */
    public function create()
    {
        $this->object->loadArgs();
        $user_data = $this->object->jsonSerialize();
        unset($user_data['salutations']);
        $user_data['password'] = isset($_REQUEST['password']) ? Modules\Auth\Object::hashPassword(
            $_REQUEST['password']) : '';
        if (isset($_REQUEST['password'])) {
            unset($_REQUEST['password']);
        }
        $password = $user_data['password'];
        if ($user_data['password'] == '') {
            if ($this->core->isAdminLogged() or (isset(
                $_REQUEST['autogenerate_password']) and
                 $_REQUEST['autogenerate_password'] == true)) {
                $password = mt_rand(1000, 9999);
                $user_data['password'] = Modules\Auth\Object::hashPassword(
                    $password);
            } else {
                throw new Modules\Users\Exception(
                    Modules\Users\Exception::REQUIRED_ATTR_MISSING);
            }
        }
        if (! isset($user_data['access_token']) or
             is_null($user_data['access_token']) or
             $user_data['access_token'] == '') {
            $user_data['access_token'] = Modules\Auth\Object::hashPassword(
                $user_data['email'] . time());
        }
        if (! isset($user_data['gender']) or $user_data['gender'] == '') {
            $user_data['gender'] = 'm';
        }
        if ((! isset($user_data['display_picture']) or
             $user_data['display_picture'] == '') and
             isset($_FILES['display_picture'])) {
            $files_manager = Base\ManagersStore::locate('files');
            $ret = $files_manager->addFile($_FILES['display_picture']);
            $user_data['display_picture'] = $ret['url'];
        }
        if ($user_data['username'] == '') {
            $user_data['username'] = substr($user_data['email'], 0, 
                strpos($user_data['email'], '@'));
        }
        $user_data['gender'] = strtolower($user_data['gender']);
        $user_data['is_active'] = 0;
        if (array_key_exists('age', $user_data)) {
            unset($user_data['age']);
        }
        if (array_key_exists('anniversary_age', $user_data)) {
            unset($user_data['anniversary_age']);
        }
        $result = $this->core->db->insert(
            array(
                'tablename' => "users",
                'data' => $user_data
            ));
        if ($result['status'] == 'OK') {
            try {
                if (isset($_REQUEST['addresses']) and
                     is_array($_REQUEST['addresses']) and
                     count($_REQUEST['addresses']) > 0) {
                    $address = new Modules\Users\Addresses\Object(false);
                    $address_manager = Base\ManagersStore::locate(
                        'users_addresses');
                    foreach ($_REQUEST['addresses'] as $address_type => $address_arr) {
                        if (isset($address_arr['address1']) and
                             ! empty($address_arr['address1'])) {
                            $address_arr['address_type'] = $address_type;
                            $address_arr['user_id'] = $result['id'];
                            $address->loadDefaults();
                            $address->loadObjectInstance($address_arr);
                            $address_manager->setObject($address)->create();
                        }
                    }
                }
                $email_manager = Base\ManagersStore::locate('email');
                
                $validation_url = $this->core->basehref . '/validate?t=' .
                     $user_data['access_token'] . '&a=' .
                     Modules\Auth\Object::hashPassword(time());
                
                $subject = 'Registration - Email Verification - ' .
                     $this->core->configs->sitename;
                $message = $email_manager->getTemplate(
                    Modules\Email\Manager::VERIFY_EMAIL);
                $message = str_replace("*|VALIDATE_URL|*", $validation_url, 
                    $message);
                $message = str_replace("*|EMAIL|*", $user_data['email'], 
                    $message);
                $message = str_replace("*|PASSWORD|*", $password, $message);
                $email_manager->sendEmail($this->core->from_email, 
                    array(
                        'name' => $user_data['fname'] . ' ' . $user_data['lname'],
                        'email' => $user_data['email']
                    ), $subject, $message);
                // $email_manager -> subscribe($user_data['fname'], $user_data['lname'], $user_data['email']);
            } catch (Mandrill_Error $e) {
                throw new Modules\Users\Exception(
                    Modules\Users\Exception::VERIFY_EMAIL_SEND_FAILED);
            }
            
            return array(
                "id" => $result['id'],
                "email" => $user_data['email'],
                "msg" => $this->core->i18n->get_lang("users_create_success"),
                "_self" => $this->object_api_href . '/' . $user_data['username']
            );
        }
        throw new Modules\Users\Exception($result['error_code']);
    }

    /**
     * Get helps retrieve Users Detailed object
     *
     * In case the User is not public and no authentication is found, exception is raised.
     * This is to maintain user privacy and not display user profiles publicly
     *
     * @param $data (String
     *            or Number) Either valid email address or user id can be used for retreiving users object
     *            
     * @return (Object)
     *
     * @throws (Object) Modules\Users\Exception or Modules\Auth\Exception
     */
    public function get($data)
    {
        $user = new Modules\Users\Object(false);
        if (is_numeric($data)) {
            $user->loadObject($data);
        } elseif (is_string($data)) {
            $user->loadObjectByDBCol(
                array(
                    'email' => $data
                ));
        }
        if (is_null($user->creation_ts)) {
            if (is_string($data)) {
                $user->loadObjectByDBCol(
                    array(
                        'username' => $data
                    ));
            }
            if (is_null($user->creation_ts)) {
                throw new Modules\Users\Exception(
                    Modules\Users\Exception::NOT_FOUND);
            }
        }
        $auth_manager = Base\ManagersStore::locate('auth');
        $logged = $auth_manager->validate();
        if (! $logged and (! $user->is_public or ! $user->is_active)) {
            throw new Modules\Auth\Exception(
                Modules\Auth\Exception::AUTHENTICATION_REQUIRED);
        }
        if (! $user->is_active) {
            $this->core->checkRight('view_deleted_profiles');
        }
        if ($logged and
             $user->getId() != $this->core->session->getUser()->getId() and
             ! $user->is_public) {
            $this->core->checkRight('view_non_public_profiles');
        }
        return $user;
    }

    /**
     * Update helps update user profile details
     *
     * Update doesn't support Change Password, special service is built for the same for security reasons
     *
     * @param $user (Object)
     *            represents user class Object
     *            
     * @return (Array) id, email, msg & _self url
     *        
     * @throws (Object) Modules\Users\Exception
     */
    public function update()
    {
        $this->object->jsonSerialize();
        $user_data = $this->object->jsonSerialize();
        if ((! isset($user_data['display_picture']) or
             $user_data['display_picture'] == '') and
             isset($_FILES['display_picture'])) {
            $files_manager = managers_store::locate('files');
            $ret = $files_manager->addFile($_FILES['display_picture']);
            $user_data['display_picture'] = $ret['url'];
        }
        if (! $this->core->isAdminLogged() and
             $user_data['id'] != $this->core->session->getUser()->getId()) {
            $this->core->checkRight('update_others_profile');
        }
        if (isset($user_data['is_public']) and $user_data['is_public'] == 0) {
            $this->core->checkRight('make_profile_non_public');
        }
        if (array_key_exists('age', $user_data)) {
            unset($user_data['age']);
        }
        if (array_key_exists('anniversary_age', $user_data)) {
            unset($user_data['anniversary_age']);
        }
        $result = $this->core->db->update(
            array(
                'tablename' => 'users',
                'set' => $user_data,
                'where' => array(
                    'condition' => 'AND',
                    'data' => array(
                        'id' => $user_data['id']
                    )
                )
            ));
        if ($result['status'] != 'NOT_OK') {
            return array(
                "id" => $user_data['id'],
                "email" => $user_data['email'],
                "msg" => $this->core->i18n->get_lang("users_update_success"),
                "_self" => $this->object_api_href . '/' . $user_data['username']
            );
        }
        throw new Modules\Users\Exception(Modules\Users\Exception::UPDATE_FAILED);
    }

    /**
     * Delete user would inactivate the user state
     * Delete happens either by id or by email
     *
     * @param $user (Object)
     *            of users class
     *            
     * @return null
     *
     * @throws (Object) Modules\Users\Exception
     */
    public function delete()
    {
        $this->object->loadArgs();
        if ($this->object->getId() == $this->core->session->getUser()->getId()) {
            $this->core->checkRight('delete_my_profile');
        } else {
            $this->core->checkRight('delete_others_profile');
        }
        if (! is_null($this->object->email)) {
            $this->deleteByEmail();
        } elseif (! is_null($this->object->username)) {
            $this->deleteByUsername();
        } else {
            parent::delete();
        }
    }

    /**
     * (-) Delete user by its email
     *
     * @param $user (Object)
     *            with Email initialized
     *            
     * @return null
     *
     * @throws (Object) Modules\Users\Exception
     */
    private function deleteByUsername()
    {
        $username = $this->object->username;
        $this->object->loadDefaults();
        $this->object->loadObjectByDBCol(
            array(
                'username' => $username
            ));
        if (is_null($this->object->creation_ts)) {
            throw new Modules\Users\Exception(Modules\Users\Exception::NOT_FOUND);
        }
        $result = $this->core->db->update(
            array(
                'tablename' => 'users',
                'where' => array(
                    'data' => array(
                        'username' => $username
                    ),
                    'condition' => 'AND'
                ),
                'set' => array(
                    'is_active' => - 1
                )
            ));
        if (is_null($result) or $result['status'] == 'NOT_OK') {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::DELETE_FAILED);
        }
    }

    /**
     * (-) Delete user by its email
     *
     * @param $user (Object)
     *            with Email initialized
     *            
     * @return null
     *
     * @throws (Object) Modules\Users\Exception
     */
    private function deleteByEmail()
    {
        $email = $this->object->email;
        $this->object->loadDefaults();
        $this->object->loadObjectByDBCol(
            array(
                'email' => $email
            ));
        if (is_null($this->object->creation_ts)) {
            throw new Modules\Users\Exception(Modules\Users\Exception::NOT_FOUND);
        }
        $result = $this->core->db->update(
            array(
                'tablename' => 'users',
                'where' => array(
                    'data' => array(
                        'email' => $email
                    ),
                    'condition' => 'AND'
                ),
                'set' => array(
                    'is_active' => - 1
                )
            ));
        if (is_null($result) or $result['status'] == 'NOT_OK') {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::DELETE_FAILED);
        }
    }

    /**
     * My helps to retrieve logged in users information
     *
     * @return (Object) of users class
     *        
     * @throws (Object) Modules\Auth\Exception
     */
    public function my()
    {
        $auth_manager = Base\ManagersStore::locate('auth');
        if (! $auth_manager->validate()) {
            throw new Modules\Auth\Exception(
                Modules\Auth\Exception::AUTHENTICATION_REQUIRED);
        }
        return $this->core->session->getUser();
    }

    /**
     * Get users object by valid access token
     *
     * @param $token (String)
     *            valid unique user access token
     *            
     * @return $user (Object) of users class if found
     *        
     * @throws (Object)
     */
    public function getByToken($token)
    {
        $token = trim($token);
        if (is_null($token)) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::TOKEN_NOT_FOUND);
        }
        $user = new Modules\Users\Object(false);
        $user->loadObjectByDBCol(
            array(
                'access_token' => $token
            ));
        if (is_null($user->getId()) or is_null($user->creation_ts)) {
            throw new Modules\Users\Exception(Modules\Users\Exception::NOT_FOUND);
        }
        return $user;
    }

    /**
     * Forgot password helps retrieve password reset instructions over valid user email
     *
     * @return Array if successful
     *        
     * @throws (Object) Modules\Users\Exception
     */
    public function forgotpassword()
    {
        $request_method = $_SERVER['REQUEST_METHOD'];
        $auth_manager = Base\ManagersStore::locate('auth');
        if ($auth_manager->validate()) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::FORGOT_AUTHENTICATED);
        }
        if ($request_method != 'POST') {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::FORGOT_PASSWORD_NOT_ALLOWED);
        }
        $this->object->loadArgs();
        $users = $this->object;
        if (is_null($users->email)) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::FORGOT_PASSWORD_EMAIL_REQ);
        }
        
        $users->loadObjectByDBCol(
            array(
                'email' => $users->email
            ));
        if (is_null($users->getId()) or is_null($users->creation_ts)) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::FORGOT_EMAIL_NOT_REGISTERED);
        }
        $token = Modules\Auth\Object::hashPassword(
            time() . $users->email . $users->getId() . time());
        $t_token = Modules\Auth\Object::hashPassword(
            Modules\Auth\Object::hashPassword($token));
        $result = $this->core->db->update(
            array(
                'tablename' => 'users',
                'where' => array(
                    'condition' => 'AND',
                    'data' => array(
                        'id' => $users->getId()
                    )
                ),
                'set' => array(
                    'access_token' => $token
                )
            ));
        if ($result['status'] == 'OK') {
            $reset_url = $this->core->basehref .
                 "/recoverpassword?a=$token&u={$users->getId()}&t=$t_token";
            try {
                $email_manager = Base\ManagersStore::locate('email');
                $email_template = $email_manager->getTemplate(
                    Modules\Email\Manager::FORGOT_PASSWORD);
                $subject = $users->fname .
                     ", here's the link to reset your password";
                $message = str_replace("*|FNAME|*", $users->fname, 
                    $email_template);
                $message = str_replace("*|SUBJECT|*", $subject, $message);
                $message = str_replace("*|RESET_URL|*", $reset_url, $message);
                $email_manager->sendEmail($this->core->from_email, 
                    array(
                        'email' => $users->email,
                        'name' => $users->getName()
                    ), $subject, $message);
            } catch (Mandrill_Error $e) {
                throw new Modules\Users\Exception(
                    Modules\Users\Exception::FORGOT_SEND_FAILED);
            }
            return array(
                'msg' => 'Forgot password mail sent, please check your inbox',
                'id' => $users->getId()
            );
        }
        throw new Modules\Users\Exception($result['error_code']);
    }

    /**
     * Recover Forggotten password with valid token and user id/email
     *
     * @return Array
     *
     * @throws Modules\Users\Exception
     */
    public function iacvkgj()
    {
        $request_method = $_SERVER['REQUEST_METHOD'];
        if ($request_method != 'POST') {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::FORGOT_REQUEST_NOT_ALLOWED);
        }
        $auth_manager = Base\ManagersStore::locate('auth');
        if ($auth_manager->validate()) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::FORGOT_AUTHENTICATED);
        }
        if (! isset($_REQUEST['password'])) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::RECOVER_PASSWORD_MISSING);
        }
        if (! isset($_REQUEST['confirm_password'])) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::RECOVER_PASSWORD_CONFIRM_MISSING);
        }
        if (! isset($_REQUEST['token'])) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::RECOVER_TOKEN_MISSING);
        }
        if (! isset($_REQUEST['user_id']) and ! isset($_REQUEST['user_email'])) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::RECOVER_USER_MISSING);
        }
        if ($_REQUEST['password'] != $_REQUEST['confirm_password']) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::RECOVER_PASSWORD_MISMATCH);
        }
        $user = $this->getByToken($_REQUEST['token']);
        if (is_null($user)) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::RECOVER_TOKEN_INVALID);
        }
        if (isset($_REQUEST['user_id']) and
             $user->getId() != $_REQUEST['user_id']) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::RECOVER_USER_INVALID);
        } elseif (isset($_REQUEST['user_email']) and
             $user->email != $_REQUEST['user_email']) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::RECOVER_USER_INVALID);
        }
        $password = auth::hashPassword($_REQUEST['password']);
        $new_token = auth::hashPassword($password . $_REQUEST['token'] . time());
        $data = array(
            'password' => $password,
            'access_token' => $new_token,
            'lastmodified_ts' => $this->core->db->now()
        );
        $result = $this->core->db->update(
            array(
                'tablename' => 'users',
                'set' => $data,
                'where' => array(
                    'condition' => 'AND',
                    'data' => array(
                        'id' => $user->getId()
                    )
                )
            ));
        if ($result['status'] == 'OK') {
            return array(
                'id' => $user->getId(),
                'msg' => 'Successfully recovered your password'
            );
        }
        throw new Modules\Users\Exception($result['error_code']);
    }

    /**
     * Change password for user with current and new password
     *
     * @return (Array) if successful with message
     *        
     * @throws (Object) Modules\Users\Exception or Modules\Auth\Exception
     */
    public function changepassword()
    {
        $request_method = $_SERVER['REQUEST_METHOD'];
        if ($request_method != 'POST') {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::CHANGE_PASSWORD_NOT_ALLOWED);
        }
        $auth_manager = Base\ManagersStore::locate('auth');
        if (! $auth_manager->validate()) {
            throw new Modules\Auth\Exception(Modules\Auth\Exception::AUTHENTICATION_REQUIRED);
        }
        $user = $this->core->session->getUser();
        if (! isset($_REQUEST['current_password'])) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::CURRENT_PASSWORD_REQ);
        }
        $current_password = $_REQUEST['current_password'];
        if (Modules\Auth\Object::verifyPassword($current_password, 
            $user->password)) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::CURRENT_PASSWORD_NOT_VALID);
        }
        if (! isset($_REQUEST['new_password']) or
             ! isset($_REQUEST['new_confirm_password'])) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::NEW_CONFIRM_PASSWORD_REQ);
        }
        $new_password = $_REQUEST['new_password'];
        $new_confirm_password = $_REQUEST['new_confirm_password'];
        if ($new_confirm_password != $new_password) {
            throw new Modules\Users\Exception(
                Modules\Users\Exception::NEW_CONFIRM_PASSWORD_NOT_VALID);
        }
        $data = array(
            "password" => Modules\Auth\Object::hashPassword($new_password),
            "lastmodified_ts" => $this->core->db->now()
        );
        $result = $this->core->db->update(
            array(
                "tablename" => "users",
                "set" => $data,
                "where" => array(
                    "condition" => "AND",
                    "data" => array(
                        "id" => $user->getId()
                    )
                )
            ));
        if ($result['status'] == 'OK') {
            return array(
                "msg" => "Successfully updated your password",
                "id" => $user->getId()
            );
        }
        throw new Modules\Users\Exception($result['error_code']);
    }

    /**
     * Change display picture
     *
     * @return (Array) if successful with message
     *        
     * @throws (Object) Modules\Users\Exception or Modules\Auth\Exception
     */
    public function changedisplaypicture()
    {
        $request_method = $_SERVER['REQUEST_METHOD'];
        $auth_manager = Base\ManagersStore::locate('auth');
        if (! $auth_manager->validate()) {
            throw new Modules\Auth\Exception(
                Modules\Auth\Exception::AUTHENTICATION_REQUIRED);
        }
        $user_data = $this->core->session->getUser()->jsonSerialize();
        $process = false;
        switch ($request_method) {
            case "PUT":
            case "POST":
                if (isset($_FILES['disp_pic'])) {
                    $files_manager = Base\ManagersStore::locate('files');
                    $file = $files_manager->addFile($_FILES['disp_pic']);
                    $user_data['disp_pic'] = $file['url'];
                    $process = true;
                }
                break;
            case "GET":
                throw new Modules\Users\Exception(
                    Modules\Users\Exception::CHANGE_DISPLAY_PICTURE_NOT_ALLOWED);
            case "DELETE":
                $file = new Modules\Files\Object();
                $file->path = $user_data['disp_pic'];
                $file->users = $this->core->session->getUser();
                $files_manager = Base\ManagersStore::locate('files');
                $files_manager->delete($file);
                $user_data['disp_pic'] = '';
                $process = true;
                break;
        }
        if ($process) {
            $user_data['lastmodified_ts'] = $this->core->db->now();
            $result = $this->core->db->update(
                array(
                    'tablename' => 'users',
                    'set' => $user_data,
                    'where' => array(
                        'condition' => 'AND',
                        'data' => array(
                            'id' => $user_data['id']
                        )
                    )
                ));
            if ($result['status'] != 'NOT_OK') {
                return array(
                    "id" => $user_data['id'],
                    "msg" => "Successfully updated display picture"
                );
            }
        }
        throw new Modules\Users\Exception(Modules\Users\Exception::UPDATE_FAILED);
    }

    /**
     */
    public function search()
    {
        // $this->core->checkRight('search_profiles'); // To allow public search
        $dob_range = null;
        $anniversary_range = null;
        
        if (isset($_REQUEST['data']['dob_range'])) {
            $dob_range = $_REQUEST['data']['dob_range'];
            unset($_REQUEST['data']['dob_range']);
        }
        
        if (isset($_REQUEST['data']['anniversary_range'])) {
            $anniversary_range = $_REQUEST['data']['anniversary_range'];
            unset($_REQUEST['data']['anniversary_range']);
        }
        $this->object->loadObjectInstance(
            array(
                'dob_range' => $dob_range,
                'anniversary_range' => $anniversary_range
            ));
        
        return parent::search();
    }

    /**
     * Static function to retrieve Anonymous User Object
     *
     * @return (Object) of user class
     */
    public static function getAnonymous()
    {
        $user = new Modules\Users();
        $user->loadObject(1);
        return $user;
    }
}
