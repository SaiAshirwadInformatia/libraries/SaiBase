<?php
namespace Sai\Modules\Users\Roles;

use Sai\Base;

/**
 *
 * @name Users Roles Exception
 * @abstract Users Roles exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\SqlException
{

    const UNIQUE = 2702;

    const INVALID_USER = 2707;

    const INVALID_ROLE = 2708;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::INVALID_USER => "excp_users_roles_invalid_user",
            self::INVALID_ROLE => "excp_users_roles_invalid_role",
            self::UNIQUE => "excp_users_roles_unique"
        );
        $this->response_error_code[400][] = array_merge(
            $this->response_error_code[400], 
            array(
                self::INVALID_ROLE,
                self::INVALID_USER
            ));
    }
}
