<?php
namespace Sai\Modules\Users\Roles;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Users Roles
 * @abstract Would represent the users roles object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Users
{

    /**
     *
     * @var object $roles Referenced to Roles Object
     */
    public $roles;

    /**
     *
     * @var integer $role_id Referenced to Roles Object Id
     */
    public $role_id;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'roles',
            'role_id',
            'users',
            'user_id',
            'creation_ts'
        );
        $this->roles = new Modules\Roles(false);
        if ($loadArgs) {
            $this->setId(null);
            $this->loadArgs();
        }
    }

    /**
     * Load the current Roles Object depending on the data found over Request
     */
    public function loadRoles()
    {
        $roles_manager = Base\ManagersStore::locate('roles');
        if (is_array($this->roles)) {
            if (isset($this->roles['id'])) {
                $this->roles = $roles_manager->get($this->roles['id']);
            } elseif ($this->roles['name']) {
                $this->roles = $roles_manager->get($this->roles['name']);
            }
        } elseif (isset($this->role_id)) {
            $this->roles->loadObject($this->role_id);
        }
    }

    /**
     * Additionally Load Roles from Request if any data found
     *
     * @see base_users::loadArgs()
     */
    public function loadArgs()
    {
        parent::loadArgs();
        $this->loadRoles();
    }
}
