<?php
namespace Sai\Modules\Users\Roles;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Users Roles Manager
 * @abstract Helps operate users roles related operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager extends Base\Manager
{

    /**
     * Constructs pretty Users Roles Manager ;)
     */
    public function __construct($object = 'users_roles')
    {
        parent::__construct($object);
    }

    /**
     * Assign Role to User
     *
     * This not only assigns the role as tag to that particular user
     * But also gives out the rights that particular role would have
     *
     * These rights are assigned individually to the user
     * so controlling would over each of them would have more sense
     *
     * @param $users_roles (Object)
     *            representing user and role details
     *            
     * @return (Array) stating the status of operation
     *        
     * @throws (Object) Modules\Users\Roles\Exception depending on error
     */
    public function create()
    {
        $this->object->loadArgs();
        $users_roles = $this->object;
        $role_id = null;
        if (isset($users_roles->role_id)) {
            $role_id = $users_roles->role_id;
        } elseif (! is_null($users_roles->roles->getId())) {
            $role_id = $users_roles->roles->getId();
        }
        $user_id = $this->core->session->getUser()->getId();
        if (isset($users_roles->user_id)) {
            $user_id = $users_roles->user_id;
        } elseif (isset($users_roles->users->creation_ts)) {
            $user_id = $users_roles->users->getId();
        }
        
        if (is_null($user_id)) {
            throw new Modules\Users\Roles\Exception(
                Modules\Users\Roles\Exception::INVALID_USER);
        }
        if (is_null($role_id)) {
            throw new Modules\Users\Roles\Exception(
                Modules\Users\Roles\Exception::INVALID_ROLE);
        }
        $roles_rights_manager = Base\ManagersStore::locate('roles_rights');
        $rights = $roles_rights_manager->get($role_id);
        
        $linked_right = array();
        foreach ($rights as $r) {
            $result = $this->core->db->insert(
                array(
                    'tablename' => 'users_rights',
                    'data' => array(
                        'user_id' => $user_id,
                        'right_id' => $r['id']
                    )
                ));
            if ($result['status'] == 'OK') {
                $linked_right[] = $r['name'];
            } elseif ($result['error_code'] == 1062) {
                $linked_right[] = $r['name'] . ' - Already linked';
            }
        }
        $result = $this->core->db->insert(
            array(
                'tablename' => 'users_roles',
                'data' => array(
                    'user_id' => $user_id,
                    'role_id' => $role_id
                )
            ));
        if ($result['status'] == 'OK') {
            return array(
                'id' => $result['id'],
                'msg' => 'Successfully gave rights of role to the user',
                'linked_rights' => $linked_right
            );
        }
        throw new Modules\Users\Roles\Exception($result['error_code']);
    }

    /**
     */
    public function get($data)
    {
        $users_manager = Base\ManagersStore::locate('users');
        if (is_null($data) or $data == false) {
            $data = $this->core->session->getUser()->getId();
        }
        $user = $users_manager->get($data);
        $roles = array();
        $result = $this->core->db->selectAll(
            array(
                'tablename' => 'users_roles',
                'where' => array(
                    'condition' => 'AND',
                    'data' => array(
                        'user_id' => $user->getId()
                    )
                )
            ));
        if (! is_null($result) and count($result) > 0) {
            foreach ($result as $r) {}
        }
    }

    public function update()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }

    public function delete()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_AVAILABLE);
    }

    public function my()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_AVAILABLE);
    }
}
