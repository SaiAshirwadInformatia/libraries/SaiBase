<?php
namespace Sai\Modules\Users;

use Sai\Base;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @name Users
 * @abstract Would represent the users object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $salutations Used before Name of individual
     */
    public $salutations;

    /**
     *
     * @var string $fname First Name of User
     *     
     *      @Assert\Length(min=3,minMessage="INVALID_FNAME")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $fname;

    /**
     *
     * @var string $mname Middle Name of User
     *     
     *      @Assert\Length(min=3,minMessage="INVALID_MNAME")
     */
    public $mname;

    /**
     *
     * @var string $lname
     *     
     *      @Assert\Length(min=3,minMessage="1808")
     */
    public $lname;

    /**
     *
     * @var string $username Is the uniquely identified username that can be used to publicly view profile with users special URL
     *     
     *      @Assert\Length(min=3,minMessage="INVALID_USERNAME")
     */
    public $username;

    /**
     *
     * @var string $email
     *     
     *      @Assert\Email(message="INVALID_EMAIL_ADDRESS")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $email;

    /**
     *
     * @var string $password Password of user
     */
    public $password;

    /**
     *
     * @var char $gender Maintains the gender listing of the user
     *     
     *      @Assert\Choice(choices={"m","f","u","n"})
     */
    public $gender;

    /**
     *
     * @var string $blood_group Users blood group
     *     
     *      @Assert\Choice(
     *      choices={"A+ve","B+ve", "AB+ve", "O+ve", "A-ve", "B-ve", "AB-ve", "O-ve"},
     *      message="INVALID_BLOOD_GROUP"
     *      )
     */
    public $blood_group;

    /**
     *
     * @var string $phone1 Users Phone Number
     */
    public $phone1;

    /**
     *
     * @var string $phone2 Users Phone Number
     */
    public $phone2;

    /**
     *
     * @var string $website Users Website
     *     
     *      @Assert\Url(message="INVALID_WEBSITE")
     */
    public $website;

    /**
     *
     * @var date $dob User Date of Birth
     *     
     *      @Assert\Date(message="INVALID_DOB")
     */
    public $dob;

    /**
     *
     * @var mixed $dob_range Age Range for searching users
     */
    public $dob_range;

    /**
     *
     * @var date $anniversary_date Users Anniversary
     *     
     *      @Assert\Date(message="INVALID_ANNIVERSARY_DATE")
     */
    public $anniversary_date;

    /**
     *
     * @var mixed $anniversary_range
     */
    public $anniversary_range;

    /**
     *
     * @var string $display_picture Users Latest Display Picture
     */
    public $display_picture;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @var timestamp $lastmodified_ts Represents the database lastmodified timestamp of the object
     */
    public $lastmodified_ts;

    /**
     *
     * @var unknown
     */
    public $access_token;

    /**
     *
     * @var integer $is_active Represents the possible state of an object
     *     
     *     
     *      Possible values
     *      1 - Active State
     *      0 - Inactive State
     *      -1 - Deleted State
     */
    public $is_active;

    /**
     *
     * @var integer $is_public Whether this menu is publicly accessible without require of authentication
     */
    public $is_public;

    /**
     *
     * @var array $role Currently assigned roles to user
     */
    public $role;

    /**
     *
     * @var array $rights Currently assigned rights to user
     */
    public $rights;

    /**
     *
     * @param boolean $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'salutations',
            'fname',
            'lname',
            'username',
            'email',
            'password',
            'gender',
            'blood_group',
            'phone1',
            'phone2',
            'website',
            'dob',
            'dob_range',
            'anniversary_date',
            'anniversary_range',
            'display_picture',
            'creation_ts',
            'lastmodified_ts',
            'access_token',
            'is_active',
            'is_public'
        );
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            $this->rights = array();
            if (! is_null($this->id)) {
                $this->loadObject($this->id);
            }
        }
    }

    /**
     * (non-PHPdoc)
     *
     * @see saiobject::loadObjectInstance()
     */
    public function loadObjectInstance($arr)
    {
        parent::loadObjectInstance($arr);
        $this->loadAgeRange();
        $this->loadSalutations();
        $this->loadMaritalStatus();
    }

    public function loadDefaults()
    {
        parent::loadDefaults();
        $this->display_picture = $this->core->basehref .
             '/assets/images/profile.png';
    }

    public function loadArgs()
    {
        parent::loadArgs();
        $this->loadAgeRange();
        $this->loadSalutations();
        $this->loadMaritalStatus();
    }

    public function loadObject($id)
    {
        parent::loadObject($id);
        $this->loadAgeRange();
        $this->loadSalutations();
        $this->loadMaritalStatus();
    }

    public function loadObjectByDBCol($data)
    {
        parent::loadObjectByDBCol($data);
        $this->loadAgeRange();
        $this->loadSalutations();
        $this->loadMaritalStatus();
    }

    public function loadMaritalStatus()
    {
        $status = const_get('marital_status');
        if (isset($status[$this->marital_status])) {
            $this->marital_status = $this->core->i18n->get_lang(
                $status[$this->marital_status]);
        }
    }

    public function loadSalutations()
    {
        $salutations_linkage = Base\Common::getConstant('salutations_linkage');
        $salutations = Base\Common::getConstant('salutations');
        $key = array();
        if (isset($this->marital_status) and ! empty($this->marital_status)) {
            $key[] = strtolower($this->marital_status);
        }
        if (isset($this->gender) and ! empty($this->gender)) {
            $key[] = $this->gender;
        } else {
            $key[] = MALE;
        }
        $key = implode("_", $key);
        if (isset($salutations_linkage[$key])) {
            $this->salutations = $this->core->i18n->get_lang(
                $salutations[$salutations_linkage[$key]]);
        }
    }

    /**
     */
    public function loadAgeRange()
    {
        if (! isset($_REQUEST['query'])) {
            $_REQUEST['query'] = array();
        }
        if (isset($this->dob_range)) {
            if (is_array($this->dob_range)) {
                $range = get_year_range($this->dob_range);
            } elseif (strpos($this->dob_range, "-") !== false) {
                $range = get_year_range($this->dob_range);
            } elseif (strpos($this->dob_range, ';' !== false)) {
                $range = get_year_range($this->dob_range, ';');
            }
            
            if (isset($range)) {
                $_REQUEST['query'][] = "`dob` <= '{$range['start_range']}-1-1' AND `dob` >= '{$range['end_range']}-1-1'";
                $this->dob_range = '';
            }
        }
        unset($range);
        if (isset($this->anniversary_range)) {
            if (is_array($this->anniversary_range)) {
                $range = get_year_range($this->anniversary_range);
            } elseif (strpos($this->anniversary_range, "-") !== false) {
                $range = get_year_range($this->anniversary_range);
            } elseif (strpos($this->anniversary_range, ';' !== false)) {
                $range = get_year_range($this->anniversary_range, ';');
            }
            if (isset($range)) {
                $_REQUEST['query'][] = "`anniversary_date` <= '{$range['start_range']}-1-1' AND `anniversary_date` >= '{$range['end_range']}-1-1'";
                $this->anniversary_range = '';
            }
        }
    }

    /**
     */
    public function getName()
    {
        return $this->fname . ' ' . $this->mname . ' ' . $this->lname;
    }

    /**
     * Get Profile Picture URL
     * If not set, then default profile picture is returned
     *
     * @return String
     */
    public function getProfilePic()
    {
        return isset($this->display_picture) ? $this->display_picture : $this->core->basehref .
             '/assets/images/profile.png';
    }

    /**
     * Get User Joining Date in D d/m/Y format
     *
     * @return String as formatted date
     */
    public function getJoinDate()
    {
        return date("D d/m/Y", strtotime($this->creation_ts));
    }

    /**
     * Identify if the user is active or not
     *
     * @return Boolean
     */
    public function isActive()
    {
        return ($this->is_active == true or $this->is_active > 0);
    }

    /**
     * Set user if active or not
     *
     * @param $is_active (Boolean)            
     */
    public function setActive($is_active)
    {
        if ($is_active == true or $is_active > 0) {
            $is_active = 1;
        } else {
            $is_active = 0;
        }
        $this->is_active = $is_active;
    }

    /**
     * Overrides parent jsonSerialize
     *
     * Removes security related data from serialized array
     * password
     * acess_token
     */
    public function jsonSerialize()
    {
        $data = parent::jsonSerialize();
        unset($data['password']);
        unset($data['access_token']);
        if (is_null($data['display_picture']) or $data['display_picture'] == '') {
            $data['display_picture'] = $this->core->basehref .
                 '/assets/images/profile.png';
        }
        $data['age'] = null;
        if (isset($data['dob']) and $data['dob'] != '') {
            $data['age'] = get_age($data['dob']);
        }
        $data['anniversary_age'] = null;
        if (isset($data['anniversary_date']) and $data['anniversary_date'] != '') {
            $data['anniversary_age'] = get_age($data['anniversary_date']);
        }
        $data['url_for_link'] = $data['username'];
        return $data;
    }

    /**
     */
    public function loadRights()
    {
        $query = "SELECT RG.name AS right_name,UR.user_id
				FROM `rights` RG
				JOIN `users_rights` UR ON UR.right_id = RG.id
				WHERE UR.user_id = " . $this->getId();
        $result = $this->core->db->exec_Query($query);
        $this->rights = array();
        if (! is_null($result) and $result->num_rows > 0) {
            $result = $this->core->db->resultToArray($result);
            foreach ($result as $row) {
                $this->rights[] = $row['right_name'];
            }
        }
    }

    /**
     *
     * @param unknown $right_name            
     * @throws Base\Modules\Users\Exception
     */
    public function checkRight($right_name)
    {
        if (! $this->hasRight($right_name)) {
            throw new Base\Modules\Users\Exception(
                Base\Modules\Users\Exception::ACCESS_DENIED);
        }
    }

    /**
     *
     * @param unknown $right_name            
     * @return boolean
     */
    public function hasRight($right_name)
    {
        if (! is_array($this->rights)) {
            return false;
        }
        return in_array($right_name, $this->rights);
    }
}
