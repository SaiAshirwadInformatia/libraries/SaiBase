<?php
namespace Sai\Modules\Users\Addresses;

use Sai\Base;

/**
 *
 * @name Users Rights Exception
 * @abstract Users Rights exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 2801;

    const UNIQUE = 2802;

    const CREATE_FAILED = 2803;

    const NOT_FOUND = 2804;

    const DELETE_FAILED = 2806;

    const UPDATE_NOT_ALLOWED = 2807;

    const LOAD_RIGHTS_ARRAY_REQ = 2908;

    const MY_ALLOWED_WITH_GET = 2909;

    const NO_RIGHTS_GIVEN = 2910;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_users_rights_req_missing",
            self::UNIQUE => "excp_users_rights_unique",
            self::CREATE_FAILED => "excp_users_rights_create_failed",
            self::NOT_FOUND => "excp_users_rights_not_found",
            self::UPDATE_NOT_ALLOWED => "excp_users_rights_update_not_allowed",
            self::LOAD_RIGHTS_ARRAY_REQ => "excp_users_rights_load_rights_array_req",
            self::MY_ALLOWED_WITH_GET => "excp_users_rights_my_allowed_with_get",
            self::NO_RIGHTS_GIVEN => "excp_users_rights_no_rights_given"
        );
        $this->response_error_code[405][] = self::UPDATE_NOT_ALLOWED;
        $this->response_error_code[405][] = self::MY_ALLOWED_WITH_GET;
        $this->response_error_code[400][] = self::LOAD_RIGHTS_ARRAY_REQ;
    }
}
