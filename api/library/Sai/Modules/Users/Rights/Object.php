<?php
namespace Sai\Modules\Users\Rights;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Users Rights
 * @abstract Would help represent rights linked with users object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Users
{

    /**
     *
     * @var object $rights Referenced with Rights object
     */
    public $rights;

    /**
     *
     * @var integer Referenced ID with Rights object
     */
    public $right_id;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @param boolean $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'users',
            'rights',
            'user_id',
            'right_id',
            'creation_ts'
        );
        $this->setId(null);
        $this->rights = new Modules\Rights\Object();
        if ($loadArgs) {
            $this->loadArgs();
            if (! is_null($this->id)) {
                $this->loadObject($this->id);
            }
        }
    }

    /**
     * Load explicitly Roles Rights if found anything relevant in request
     */
    public function loadRolesRights()
    {
        $rights_manager = Base\ManagersStore::locate('rights');
        if (is_array($this->rights)) {
            if (isset($this->rights['id'])) {
                $this->rights = $rights_manager->get($this->rights['id']);
            } elseif ($this->rights['name']) {
                $this->rights = $rights_manager->get($this->rights['name']);
            }
        } elseif (isset($this->right_id)) {
            $this->rights->loadObject($this->right_id);
        }
    }

    /**
     * Load explicitly Roles Rights if found anything relevant in request
     *
     * @throws Modules\Roles\Rights\Exception
     */
    public function loadArgs()
    {
        parent::loadArgs();
        $this->loadRolesRights();
        if (isset($this->id) and isset($this->creation_ts)) {
            if (! is_object($this->rights) or is_null(
                $this->rights->creation_ts)) {
                throw new Modules\Roles\Rights\Exception(
                    Modules\Roles\Rights\Exception::INVALID_RIGHT);
            }
        }
    }

    /**
     *
     * @param unknown $id            
     */
    public function loadObject($id)
    {
        parent::loadObject($id);
        $this->loadRolesRights();
    }
}
