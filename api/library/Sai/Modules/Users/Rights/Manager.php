<?php
namespace Sai\Modules\Users\Addresses;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Users Rights Manager
 * @abstract Helps to operate rights linkage with users
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager extends Base\Manager
{

    /**
     * Constructor
     */
    public function __construct($object = 'users_rights')
    {
        parent::__construct($object);
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Sai\Base\Manager::get()
     */
    public function get($data)
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_AVAILABLE);
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Sai\Base\Manager::update()
     */
    public function update()
    {
        throw new Modules\Users\Rights\Exception(
            Modules\Users\Rights\Exception::UPDATE_NOT_ALLOWED);
    }

    /**
     */
    public function delete()
    {
        $this->object->loadArgs();
        if (is_null($this->object->users) or
             is_null($this->object->users->creation_ts)) {
            $users_rights->users = $this->core->session->getUser();
        }
        $this->core->db->delete(
            array(
                'tablename' => 'users_rights',
                'where' => array(
                    'condition' => 'AND',
                    'data' => array(
                        'user_id' => $this->object->users->getId(),
                        'right_id' => $tthis->object->rights->getId()
                    )
                )
            ));
    }

    /**
     * Get logged users rights as array
     *
     * @return (Array) rights assigned to logged in user
     */
    public function my()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            throw new Modules\Users\Rights\Exception(
                Modules\Users\Rights\Exception::MY_ALLOWED_WITH_GET);
        }
        $query = "SELECT R.*
		FROM `users` U
		JOIN `users_rights` UR ON UR.user_id = U.id
		JOIN `rights` R ON UR.right_id = R.id
		WHERE U.id = " .
             $this->core->session->getUser()->getId();
        $result = $this->core->db->exec_Query($query);
        if ($this->core->db->getRowNum($result) > 0) {
            $users_rights = array();
            $result = $this->core->db->resultToArray($result);
            $right = new Modules\Rights\Object(false);
            foreach ($result as $r) {
                $right->loadObjectInstance($r);
                $r = $right->jsonSerialize();
                unset($r['users']);
                $users_rights[] = $r;
            }
            return $users_rights;
        }
        throw new Modules\Users\Rights\Exception(
            Modules\Users\Rights\Exception::NO_RIGHTS_GIVEN);
    }
}
