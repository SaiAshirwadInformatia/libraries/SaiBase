<?php
namespace Sai\Modules\Users\Workexperience;

use Sai\Base;

/**
 *
 * @name Users Work Experience Exception
 * @abstract Users Work Experience exception handling
 * @author Chirag Babrekar
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 2201;

    const UNIQUE = 2202;

    const CREATE_FAILED = 2203;

    const NOT_FOUND = 2204;

    const UPDATE_FAILED = 2205;

    const DELETE_FAILED = 2206;

    const NO_EXP = 2207;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_users_workexperience_req_missing",
            self::UNIQUE => "excp_users_workexperience_unique",
            self::CREATE_FAILED => "excp_users_workexperience_create_failed",
            self::NOT_FOUND => "excp_users_workexperience_not_found",
            self::UPDATE_FAILED => "excp_users_workexperience_update_failed",
            self::DELETE_FAILED => "excp_users_workexperience_delete_failed",
            self::NO_EXP => "excp_users_workexperience_no_exp"
        );
    }
}
