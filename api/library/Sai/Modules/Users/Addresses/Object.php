<?php
namespace Sai\Modules\Users\Addresses;

use Sai\Base;
use Symfony\Component\Validator\Constraints;

/**
 *
 * @name Users Address
 * @abstract Would help represent users address object
 * @author Chaitali Patil
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Object extends Base\Users
{

    /**
     *
     * @var unknown
     */
    public $address_type;

    /**
     *
     * @var unknown
     */
    public $address1;

    /**
     *
     * @var unknown
     */
    public $address2;

    /**
     *
     * @var unknown
     */
    public $city;

    /**
     *
     * @var unknown
     */
    public $state;

    /**
     *
     * @var unknown
     */
    public $country;

    /**
     *
     * @var unknown
     */
    public $pincode;

    /**
     * @public timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     * @public timestamp $lastmodified_ts Represents the database lastmodified timestamp of the object
     */
    public $lastmodified_ts;

    /**
     * @public integer $is_active Represents the possible state of an object
     *
     *
     * Possible values
     * 1 - Active State
     * 0 - Inactive State
     * -1 - Deleted State
     */
    public $is_active;

    /**
     *
     * @param boolean $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'user_id',
            'users',
            'address_type',
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'pincode',
            'creation_ts',
            'lastmodified_ts',
            'is_active'
        );
        
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            if (! is_null($this->id)) {
                $this->loadObject($this->id);
            }
        }
    }
}
?>