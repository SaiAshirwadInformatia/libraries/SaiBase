<?php
namespace Sai\Modules\Users\Addresses;

use Sai\Base;

/**
 *
 * @name users_addresses Exception
 * @abstract users_addresses exception handling
 * @author Chaitali Patil <cpatil@saiashirwad.com>
 * @copyright saiashirwad.com
 * @since ClientPortal v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 2201;

    const UNIQUE = 2202;

    const CREATE_FAILED = 2203;

    const NOT_FOUND = 2204;

    const UPDATE_FAILED = 2205;

    const DELETE_FAILED = 2206;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_req_address_missing",
            self::UNIQUE => "excp_address_unique",
            self::CREATE_FAILED => "excp_address_create_failed",
            self::NOT_FOUND => "excp_address_not_found",
            self::UPDATE_FAILED => "excp_address_update_failed",
            self::DELETE_FAILED => "excp_address_delete_failed"
        );
    }
}
?>