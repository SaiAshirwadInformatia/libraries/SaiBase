<?php
namespace Sai\Modules\Users;

use Sai\Base;

/**
 *
 * @name Users Exception
 * @abstract Users exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 1801;

    const UNIQUE = 1802;

    const CREATE_FAILED = 1803;

    const NOT_FOUND = 1804;

    const UPDATE_FAILED = 1805;

    const DELETE_FAILED = 1806;

    const INVALID_EMAIL_ADDRESS = 1807;

    const INVALID_FNAME = 1808;

    const INVALID_LNAME = 1809;

    const INVALID_PASSWORD = 1810;

    const INVALID_JOB_TITLE = 1811;

    const VERIFY_EMAIL_SEND_FAILED = 1812;

    const CURRENT_PASSWORD_REQ = 1813;

    const CURRENT_PASSWORD_NOT_VALID = 1814;

    const NEW_CONFIRM_PASSWORD_REQ = 1815;

    const NEW_CONFIRM_PASSWORD_NOT_VALID = 1816;

    const CHANGE_DISPLAY_PICTURE_NOT_ALLOWED = 1817;

    const CHANGE_PASSWORD_NOT_ALLOWED = 1818;

    const FORGOT_PASSWORD_NOT_ALLOWED = 1819;

    const FORGOT_PASSWORD_EMAIL_REQ = 1820;

    const FORGOT_INVALID_EMAIL_ADDRESS = 1821;

    const FORGOT_EMAIL_NOT_REGISTERED = 1822;

    const FORGOT_AUTHENTICATED = 1823;

    const FORGOT_SEND_FAILED = 1824;

    const FORGOT_INVALID_LINK = 1825;

    const FORGOT_REQUEST_NOT_ALLOWED = 1826;

    const RECOVER_PASSWORD_MISSING = 1827;

    const RECOVER_PASSWORD_CONFIRM_MISSING = 1828;

    const RECOVER_TOKEN_MISSING = 1829;

    const RECOVER_USER_MISSING = 1830;

    const RECOVER_TOKEN_INVALID = 1831;

    const RECOVER_USER_INVALID = 1832;

    const RECOVER_PASSWORD_MISMATCH = 1833;

    const TOKEN_NOT_FOUND = 1834;

    const ACCESS_DENIED = 1835;

    const INVALID_WEBSITE = 1836;

    const INVALID_BLOOD_GROUP = 1837;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_users_req_missing",
            self::UNIQUE => "excp_users_unique",
            self::CREATE_FAILED => "excp_users_create_failed",
            self::NOT_FOUND => "excp_users_not_found",
            self::UPDATE_FAILED => "excp_users_update_failed",
            self::DELETE_FAILED => "excp_users_delete_failed",
            self::INVALID_EMAIL_ADDRESS => "excp_users_invalid_email_address",
            self::INVALID_FNAME => "excp_users_invalid_fname",
            self::INVALID_LNAME => "excp_users_invalid_lname",
            self::INVALID_PASSWORD => "excp_users_invalid_password",
            self::INVALID_JOB_TITLE => "excp_users_invalid_job_title",
            self::VERIFY_EMAIL_SEND_FAILED => "excp_users_verify_email_send_failed",
            self::CURRENT_PASSWORD_REQ => "excp_users_change_password_req",
            self::CURRENT_PASSWORD_NOT_VALID => "excp_users_current_password_not_valid",
            self::NEW_CONFIRM_PASSWORD_REQ => "excp_users_new_confirm_password_req",
            self::NEW_CONFIRM_PASSWORD_NOT_VALID => "excp_users_new_confirm_password_not_valid",
            self::CHANGE_DISPLAY_PICTURE_NOT_ALLOWED => "excp_users_change_display_picture_not_allowed",
            self::CHANGE_PASSWORD_NOT_ALLOWED => "excp_users_change_password_not_allowed",
            self::FORGOT_PASSWORD_NOT_ALLOWED => "excp_users_forgot_password_not_allowed",
            self::FORGOT_PASSWORD_EMAIL_REQ => "excp_users_forgot_password_email_req",
            self::FORGOT_INVALID_EMAIL_ADDRESS => "excp_users_forgot_invalid_email_address",
            self::FORGOT_EMAIL_NOT_REGISTERED => "excp_users_forgot_email_not_registered",
            self::FORGOT_AUTHENTICATED => "excp_users_forgot_authenticated",
            self::FORGOT_SEND_FAILED => "excp_users_forgot_send_failed",
            self::FORGOT_INVALID_LINK => "excp_users_forgot_invalid_link",
            self::FORGOT_REQUEST_NOT_ALLOWED => "excp_users_forgot_req_not_allowed",
            self::RECOVER_PASSWORD_MISSING => "excp_users_recover_password_missing",
            self::RECOVER_PASSWORD_CONFIRM_MISSING => "excp_users_recover_password_confirm_missing",
            self::RECOVER_TOKEN_MISSING => "excp_users_recover_token_missing",
            self::RECOVER_USER_MISSING => "excp_users_recover_user_missing",
            self::RECOVER_TOKEN_INVALID => "excp_users_recover_token_invalid",
            self::RECOVER_USER_INVALID => "excp_users_recover_user_invalid",
            self::RECOVER_PASSWORD_MISMATCH => "excp_users_recover_password_missmatch",
            self::TOKEN_NOT_FOUND => "excp_users_token_not_found",
            self::ACCESS_DENIED => "excp_users_access_denied",
            self::INVALID_WEBSITE => "excp_users_invalid_website",
            self::INVALID_BLOOD_GROUP => "excp_users_invalid_blood_group"
        );
        $this->response_error_code[400] = array_merge(
            $this->response_error_code[400], 
            array(
                self::INVALID_EMAIL_ADDRESS,
                self::INVALID_FNAME,
                self::INVALID_JOB_TITLE,
                self::INVALID_LNAME,
                self::INVALID_PASSWORD,
                self::CURRENT_PASSWORD_REQ,
                self::CURRENT_PASSWORD_NOT_VALID,
                self::NEW_CONFIRM_PASSWORD_REQ,
                self::NEW_CONFIRM_PASSWORD_NOT_VALID,
                self::FORGOT_PASSWORD_EMAIL_REQ,
                self::FORGOT_INVALID_EMAIL_ADDRESS,
                self::FORGOT_AUTHENTICATED,
                self::FORGOT_INVALID_LINK,
                self::RECOVER_PASSWORD_MISSING,
                self::RECOVER_PASSWORD_CONFIRM_MISSING,
                self::RECOVER_TOKEN_MISSING,
                self::RECOVER_USER_MISSING,
                self::RECOVER_TOKEN_INVALID,
                self::RECOVER_USER_INVALID,
                self::RECOVER_PASSWORD_MISMATCH
            ));
        $this->response_error_code[403][] = self::ACCESS_DENIED;
        $this->response_error_code[404] = array_merge(
            $this->response_error_code[404], 
            array(
                self::TOKEN_NOT_FOUND,
                self::FORGOT_EMAIL_NOT_REGISTERED
            ));
        $this->response_error_code[405] = array_merge(
            $this->response_error_code[405], 
            array(
                self::CHANGE_DISPLAY_PICTURE_NOT_ALLOWED,
                self::CHANGE_PASSWORD_NOT_ALLOWED,
                self::FORGOT_PASSWORD_NOT_ALLOWED,
                self::FORGOT_REQUEST_NOT_ALLOWED
            ));
    }
}
