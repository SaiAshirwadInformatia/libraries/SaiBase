<?php
namespace Sai\Modules\Rights;

use Sai\Base;

/**
 *
 * @name Rights Exception
 * @abstract Rights exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 1501;

    const UNIQUE = 1502;

    const CREATE_FAILED = 1503;

    const NOT_FOUND = 1504;

    const UPDATE_FAILED = 1505;

    const DELETE_FAILED = 1506;

    const NO_RIGHTS_CREATED = 1507;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_rights_req_missing",
            self::UNIQUE => "excp_rights_unique",
            self::CREATE_FAILED => "excp_rights_create_failed",
            self::NOT_FOUND => "excp_rights_not_found",
            self::DELETE_FAILED => "excp_rights_delete_failed",
            self::UPDATE_FAILED => "excp_rights_update_failed",
            self::NO_RIGHTS_CREATED => "excp_rights_no_rights_created"
        );
        $this->response_error_code[404][] = self::NO_RIGHTS_CREATED;
    }
}
