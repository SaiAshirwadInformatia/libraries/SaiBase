<?php
namespace Sai\Modules\Translations;

use Sai\Base;

/**
 *
 * @name Translations Exception
 * @abstract translations exception handling
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 4401;

    const UNIQUE = 4402;

    const CREATE_FAILED = 4403;

    const NOT_FOUND = 4404;

    const UPDATE_FAILED = 4405;

    const DELETE_FAILED = 4406;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_translations_req_missing",
            self::UNIQUE => "excp_translations_unique",
            self::CREATE_FAILED => "excp_translations_create_failed",
            self::NOT_FOUND => "excp_translations_not_found",
            self::UPDATE_FAILED => "excp_translations_update_failed",
            self::DELETE_FAILED => "excp_translations_delete_failed"
        );
    }
}
