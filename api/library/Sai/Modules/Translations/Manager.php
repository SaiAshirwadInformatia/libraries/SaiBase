<?php
namespace Sai\Modules\Translations;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Translations Manager
 * @abstract Would help operate translations Object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since Ghanerao.in v1
 *       
 */
class Manager extends Base\Manager
{

    /**
     * Contructor for translations Manager
     */
    public function __construct($object = 'translations')
    {
        parent::__construct($object);
    }

    /**
     */
    public function get($data)
    {
        if (is_numeric($data)) {
            return parent::get($data);
        } else {
            $obj = new Modules\Translations\Object(false);
            $obj->loadObjectByDBCol(
                array(
                    'object_key' => $data
                ));
            if (! is_null($obj->creation_ts)) {
                return $obj;
            }
        }
        throw new Modules\Translations\Exception(
            Modules\Translations\Exception::NOT_FOUND);
    }
}
