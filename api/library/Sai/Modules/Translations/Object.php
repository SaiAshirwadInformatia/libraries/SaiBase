<?php
namespace Sai\Modules\Translations;

use Sai\Base;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @name Translations
 * @abstract Would help represent translation object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $object_key Uniquely identified string key
     *     
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $object_key;

    /**
     *
     * @var string $object_name Object name is supposed to be table/module name
     *     
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $object_name;

    /**
     *
     * @var string $object_column Tables column name with whom translation has to be mapped
     *     
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $object_column;

    /**
     *
     * @var integer $object_id Object id whose column has to be uniquely mapped with
     *     
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $objectid;

    /**
     *
     * @var string $original Original string without translation i.e. in en-US form
     *     
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $original;

    /**
     *
     * @var string $translated Translated string from its original source
     *     
     */
    public $translated;

    /**
     *
     * @var string $locale The locale code into which translation should happen
     *     
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $locale;

    /**
     *
     * @var string $type Type of input field this value should provide
     */
    public $type;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @var timestamp $lastmodified_ts Represents the database lastmodified timestamp of the object
     */
    public $lastmodified_ts;

    /**
     *
     * @var integer $is_active Represents the possible state of an object
     *     
     *     
     *      Possible values
     *      1 - Active State
     *      0 - Inactive State
     *      -1 - Deleted State
     */
    public $is_active;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'object_key',
            'object_name',
            'object_column',
            'objectid',
            'original',
            'translated',
            'locale',
            'type',
            'creation_ts',
            'lastmodified_ts',
            'is_active'
        );
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            if ($this->id != null) {
                $this->loadObject($this->id);
            }
        }
    }

    /**
     *
     * @return Boolean
     */
    public function isActive()
    {
        return ($this->is_active == true or $this->is_active > 0);
    }
}
