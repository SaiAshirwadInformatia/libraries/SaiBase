<?php
namespace Sai\Modules\Roles\Rights;

use Sai\Base;

/**
 *
 * @name Roles Rights Exception
 * @abstract Roles Rights exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 1701;

    const CREATE_FAILED = 1703;

    const NOT_FOUND = 1704;

    const DELETE_FAILED = 1706;

    const INVALID_RIGHT = 1707;

    const INVALID_ROLE = 1708;

    const NO_RIGHT_LINKED_WITH_ROLE = 1709;

    const UPDATE_NOT_ALLOWED = 1710;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_rolesrights_req_missing",
            self::CREATE_FAILED => "excp_rolesrights_create_failed",
            self::NOT_FOUND => "excp_rolesrights_not_found",
            self::DELETE_FAILED => "excp_rolesrights_delete_failed ",
            self::INVALID_RIGHT => "excp_rolesrights_invalid_right",
            self::INVALID_ROLE => "excp_rolesrights_invalid_role",
            self::NO_RIGHT_LINKED_WITH_ROLE => "excp_rolesrights_no_right_linked_with_role",
            self::UPDATE_NOT_ALLOWED => "excp_rolesrights_update_not_allowed"
        );
        $this->response_error_code[400][] = self::INVALID_RIGHT;
        $this->response_error_code[400][] = self::INVALID_ROLE;
        $this->response_error_code[404][] = self::NO_RIGHT_LINKED_WITH_ROLE;
        $this->response_error_code[405][] = self::UPDATE_NOT_ALLOWED;
    }
}
