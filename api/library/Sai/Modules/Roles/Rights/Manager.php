<?php
namespace Sai\Modules\Roles\Rights;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Roles Manager
 * @abstract Would help perform role related operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager extends Base\Manager
{

    /**
     * Constructor for Roles Rights Manager
     */
    public function __construct($object = 'roles_rights')
    {
        parent::__construct($object);
    }

    /**
     * (non-PHPdoc)
     *
     * @see base_manager::get()
     */
    public function get($data)
    {
        $roles_manager = Base\ManagersStore::locate('roles');
        $rights_manager = Base\ManagersStore::locate('rights');
        $roles = $roles_manager->get($data);
        $result = $this->core->db->selectAll(
            array(
                'tablename' => 'roles_rights',
                'where' => array(
                    'condition' => 'AND',
                    'data' => array(
                        'role_id' => $roles->getId()
                    )
                )
            ));
        $rights = array();
        if (count($result) > 0) {
            foreach ($result as $r) {
                $rights[] = $rights_manager->get($r['right_id'])->jsonSerialize();
            }
            return $rights;
        }
        throw new Modules\Roles\Rights\Exception(
            Modules\Roles\Rights\Exception::NO_RIGHT_LINKED_WITH_ROLE);
    }

    public function update()
    {
        throw new Modules\Roles\Rights\Exception(
            Modules\Roles\Rights\Exception::UPDATE_NOT_ALLOWED);
    }

    public function delete()
    {
        $this->object->loadArgs();
        if (! is_null($this->object->creation_ts)) {
            $roles_rights_contents = $this->object->jsonSerialize();
            $right_id = $roles_rights_contents['rights']['id'];
            $role_id = $roles_rights_contents['roles']['id'];
            if (! is_null($right_id) and ! is_null($role_id)) {
                $result = $this->core->db->delete(
                    array(
                        'tablename' => 'roles_rights',
                        'where' => array(
                            'data' => array(
                                'right_id' => $right_id,
                                "role_id" => $role_id
                            ),
                            'condition' => 'AND'
                        )
                    ));
                if (is_null($result) or $result['status'] == 'NOT_OK') {
                    throw new Modules\Roles\Rights\Exception(
                        Modules\Roles\Rights\Exception::DELETE_FAILED);
                }
            }
        } else {
            throw new Modules\Roles\Rights\Exception(
                Modules\Roles\Rights\Exception::NOT_FOUND);
        }
        change_response_code(204);
    }
}
