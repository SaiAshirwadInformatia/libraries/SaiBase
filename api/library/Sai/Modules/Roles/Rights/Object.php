<?php
namespace Sai\Modules\Roles\Rights;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Roles Rights
 * @abstract Would help represent rights linked with roles objects
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var object $roles Referenced to Roles object
     */
    public $roles;

    /**
     *
     * @var integer $role_id Referenced ID to Roles object
     */
    public $role_id;

    /**
     *
     * @var object $rights Referenced to Rights Object
     */
    public $rights;

    /**
     *
     * @var integer $right_id Referenced ID to Rights Object
     */
    public $right_id;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'roles',
            'rights',
            'role_id',
            'right_id',
            'creation_ts'
        );
        $this->setId(null);
        $this->rights = new Modules\Rights\Object(false);
        $this->roles = new Modules\Roles\Object(false);
        if ($loadArgs) {
            $this->loadArgs();
            if (! is_null($this->id)) {
                $this->loadObject($this->id);
            }
        }
    }

    /**
     */
    public function loadRolesRights()
    {
        $roles_manager = Base\ManagersStore::locate('roles');
        if (is_array($this->roles)) {
            if (isset($this->roles['id'])) {
                $this->roles = $roles_manager->get($this->roles['id']);
            } elseif (isset($this->roles['name'])) {
                $this->roles = $roles_manager->get($this->roles['name']);
            }
            if (is_object($this->roles)) {
                $this->role_id = $this->roles->getId();
            }
        } elseif (isset($this->role_id)) {
            $this->roles->loadObject($this->role_id);
        }
        $rights_manager = Base\ManagersStore::locate('rights');
        if (is_array($this->rights)) {
            if (isset($this->rights['id'])) {
                $this->rights = $rights_manager->get($this->rights['id']);
            } elseif (isset($this->rights['name'])) {
                $this->rights = $rights_manager->get($this->rights['name']);
            }
            if (is_object($this->rights)) {
                $this->right_id = $this->rights->getId();
            }
        } elseif (isset($this->right_id)) {
            $this->rights->loadObject($this->right_id);
        }
    }

    /**
     */
    public function loadArgs()
    {
        parent::loadArgs();
        $this->loadRolesRights();
        if (isset($this->id) and isset($this->creation_ts)) {
            if (! is_object($this->rights) or is_null(
                $this->rights->creation_ts)) {
                throw new Modules\Roles\Rights\Exception(
                    Modules\Roles\Rights\Exception::INVALID_RIGHT);
            }
            if (! is_object($this->roles) or is_null($this->roles->creation_ts)) {
                throw new Modules\Roles\Rights\Exception(
                    Modules\Roles\Rights\Exception::INVALID_ROLE);
            }
        }
    }

    /**
     */
    public function loadObject($id)
    {
        parent::loadObject($id);
        $this->loadRolesRights();
    }
}
