<?php
namespace Sai\Modules\Roles;

use Sai\Base;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @name Roles
 * @abstract Would help represent roles objects
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $name Name of the Role that can be assigned to user
     *     
     *      @Assert\Length(min=3,minMessage="INVALID_NAME")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $name;

    /**
     *
     * @var string $description Description of the Role
     *     
     *      @Assert\Length(min=15,minMessage="INVALID_DESCRIPTION")
     */
    public $description;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @var timestamp $lastmodified_ts Represents the database lastmodified timestamp of the object
     */
    public $lastmodified_ts;

    /**
     *
     * @var integer $is_active Represents the possible state of an object
     *     
     *     
     *      Possible values
     *      1 - Active State
     *      0 - Inactive State
     *      -1 - Deleted State
     */
    public $is_active;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'name',
            'description',
            'users',
            'user_id',
            'creation_ts',
            'lastmodified_ts',
            'is_active'
        );
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            if (! is_null($this->id)) {
                $this->loadObject($this->id);
            }
        }
    }
}
