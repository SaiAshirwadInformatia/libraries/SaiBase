<?php
namespace Sai\Modules\Roles;

use Sai\Base;

/**
 *
 * @name Roles Exception
 * @abstract Roles exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 1601;

    const UNIQUE = 1602;

    const CREATE_FAILED = 1603;

    const NOT_FOUND = 1604;

    const UPDATE_FAILED = 1605;

    const DELETE_FAILED = 1606;

    const NO_ROLES_CREATED = 1607;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_roles_req_missing",
            self::UNIQUE => "excp_roles_unique",
            self::CREATE_FAILED => "excp_roles_create_failed",
            self::NOT_FOUND => "excp_roles_not_found",
            self::UPDATE_FAILED => "excp_roles_update_failed",
            self::DELETE_FAILED => "excp_roles_delete_failed",
            self::NO_ROLES_CREATED => "excp_roles_no_roles_created"
        );
        $this->response_error_code[404][] = self::NO_ROLES_CREATED;
    }
}
