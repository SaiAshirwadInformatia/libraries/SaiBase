<?php
namespace Sai\Modules\Albums\Photos;

use Sai\Base;

/**
 *
 * @name photos Exception
 * @abstract photos exception handling
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 2701;

    const UNIQUE = 2702;

    const CREATE_FAILED = 2703;

    const NOT_FOUND = 2704;

    const UPDATE_FAILED = 2705;

    const DELETE_FAILED = 2706;

    const PHOTO_REQ = 2707;

    const ALBUM_REQ = 2708;

    const MIN_DESCRIPTION = 2709;

    const INVALID_PHOTO_TYPE = 2710;

    const FILE_SIZE_MORE = 2711;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_albums_photos_req_missing",
            self::UNIQUE => "excp_albums_photos_unique",
            self::CREATE_FAILED => "excp_albums_photos_create_failed",
            self::NOT_FOUND => "excp_albums_photos_not_found",
            self::UPDATE_FAILED => "excp_albums_photos_update_failed",
            self::DELETE_FAILED => "excp_albums_photos_delete_failed",
            self::PHOTO_REQ => "excp_albums_photos_photo_req",
            self::ALBUM_REQ => "excp_albums_photos_album_req",
            self::MIN_DESCRIPTION => "excp_albums_photos_min_description",
            self::INVALID_PHOTO_TYPE => "excp_albums_photos_invalid_photo_type",
            self::FILE_SIZE_MORE => "excp_albums_photos_file_size_more"
        );
    }
}
