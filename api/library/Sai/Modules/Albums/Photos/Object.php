<?php
namespace Sai\Modules\Albums\Photos;

use Symfony\Component\Validator\Constraints as Assert;
use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Albums Photos
 * @abstract Would help represent Albums Photos Object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $description Captions that can be displayed over the photo
     *      @Assert\Length(min=25,minMessage="MIN_DESCRIPTION")
     */
    public $description;

    /**
     *
     * @var string $thumbnail_url Thumbnail of the Albums Photo
     */
    public $thumbnail_url;

    /**
     *
     * @var string $compressed_url Compressed image of the Albums Photo
     */
    public $compressed_url;

    /**
     *
     * @var integer $file_id References with File Object ID
     *      @Assert\GreaterThan(value=0,message="PHOTO_REQ")
     */
    public $file_id;

    /**
     *
     * @var object $files References Photo linked with File Object
     */
    public $files;

    /**
     *
     * @var integer $album_id References with Album Object ID
     *      @Assert\GreaterThan(value=0,message="ALBUM_REQ")
     */
    public $album_id;

    /**
     *
     * @var object $albums References Photo linked with Album Object
     */
    public $albums;

    /**
     *
     * @var integer $views The number of times this photo from album has been viewed
     */
    public $views;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @var integer $is_active Represents the possible state of an object
     *     
     *     
     *      Possible values
     *      1 - Active State
     *      0 - Inactive State
     *      -1 - Deleted State
     */
    public $is_active;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'description',
            'thumbnail_url',
            'compressed_url',
            'file_id',
            'files',
            'album_id',
            'albums',
            'views',
            'creation_ts',
            'is_active'
        );
        $this->albums = new Modules\Albums\Object(false);
        $this->files = new Modules\Files\Object(false);
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            if ($this->id != null) {
                $this->loadObject($this->id);
            }
        }
    }

    /**
     */
    public function loadAlbum()
    {
        if (isset($this->album_id) and ! isset($this->albums->creation_ts)) {
            if (is_null($this->albums) or ! is_object($this->albums)) {
                $this->albums = new Modules\Albums\Object(false);
            }
            $this->albums->loadObject($this->album_id);
        }
    }

    /**
     */
    public function loadFile()
    {
        if (isset($this->file_id) and ! isset($this->files->creation_ts)) {
            if (is_null($this->files) or ! is_object($this->files)) {
                $this->files = new Modules\Files\Object(false);
            }
            $this->files->loadObject($this->file_id);
        }
    }

    /**
     * (non-PHPdoc)
     *
     * @see saiobject::loadArgs()
     */
    public function loadArgs()
    {
        parent::loadArgs();
        $this->loadAlbum();
        $this->loadFile();
        $albums_manager = Base\ManagerStore::locate('albums');
        $files_manager = Base\ManagerStore::locate('files');
        if (is_array($this->albums)) {
            if (isset($this->albums['id'])) {
                $this->albums = $albums_manager->get($this->albums['id']);
            } elseif (isset($this->albums['name'])) {
                $this->albums = $albums_manager->get($this->albums['name']);
            }
        } elseif (is_null($this->album_id) and isset($this->arr->albums['id']) and
             is_null($this->albums->creation_ts)) {
            $this->albums->loadObject($this->arr->albums['id']);
        }
        if (is_array($this->files)) {
            if (isset($this->files['id'])) {
                $this->files = $files_manager->get($this->files['id']);
            } elseif (isset($this->files['name'])) {
                $this->files = $files_manager->get($this->files['name']);
            }
        } elseif (is_null($this->file_id) and ! is_null($this->files->getId()) and
             is_null($this->files->creation_ts)) {
            $this->files->loadObject($this->files->getId());
        }
    }

    public function loadObject($id)
    {
        parent::loadObject($id);
        $this->loadAlbum();
        $this->loadFile();
    }

    public function loadObjectByDBCol($data)
    {
        parent::loadObjectByDBCol($data);
        $this->loadAlbum();
        $this->loadFile();
    }

    public function loadObjectInstance($arr)
    {
        parent::loadObjectInstance($arr);
        $this->loadAlbum();
        $this->loadFile();
    }
}
