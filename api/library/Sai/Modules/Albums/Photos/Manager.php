<?php
namespace Sai\Modules\Albums\Photos;

use Sai\Modules;
use Sai\Base;

/**
 *
 * @name Albums Photos
 * @abstract Would help operate albums_photos Object
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager extends Base\Manager
{

    /**
     * Contructor for Album Photos Manager
     */
    public function __construct($object = 'albums_photos')
    {
        parent::__construct($object);
    }

    public function create()
    {
        if (isset($_FILES) and isset($_FILES['files'])) {
            if ($_FILES['files']['size'] >
                 $this->core->configs->photo_upload_limit * 1024) {
                throw new Modules\Albums\Photos\Exception(
                    Modules\Albums\Photos\Exception::FILE_SIZE_MORE);
            }
            $ext = substr($_FILES['files']['name'], 
                strrpos($_FILES['files']['name'], '.') + 1);
            $allowed_types = const_get('allowed_photo_file_type');
            if (! array_key_exists($ext, $allowed_types) or
                 (! in_array($_FILES['files']['type'], $allowed_types))) {
                throw new Modules\Albums\Photos\Exception(
                    Modules\Albums\Photos\Exception::INVALID_PHOTO_TYPE);
            }
            if (! isset($_REQUEST['compress_image'])) {
                $_REQUEST['compress_image'] = true;
            }
            return parent::create();
        }
    }

    /**
     */
    public function get($data)
    {
        $albums_manager = Base\ManagersStore::locate('albums');
        $album = $albums_manager->get($data);
        
        $albums_photos_arr = array();
        $albums_photos = new Modules\Albums\Photos\Object(false);
        $files = new Modules\Files\Object(false);
        $query = "SELECT F.*, A.id as `album_id`, AP.id AS `albums_photos_id`, 
            AP.description AS `albums_photos_description`, AP.thumbnail_url AS `albums_photos_thumbnail_url`,
            AP.compressed_url AS `albums_photos_compressed_url`, AP.views AS `albums_photos_views`,
            AP.creation_ts AS `albums_photos_creation_ts`, 
            AP.is_active AS `albums_photos_is_active` FROM `albums_photos` AP
            JOIN `files` F ON F.id = AP.file_id
            JOIN `albums` A ON A.id = AP.album_id
            WHERE A.id = " . $album->getId();
        if (! $this->core->hasRight('view_deleted_albums_photos')) {
            $query .= " AND AP.is_active = 1";
        }
        $result = $this->core->db->exec_Query($query);
        $result = $this->core->db->resultToArray($result);
        $photo_ids = array();
        $photo_array = array();
        foreach ($result as $r) {
            $albums_photos->loadDefaults();
            $files->loadDefaults();
            $files->loadObjectInstance($r);
            $albums_photos->loadObjectInstance(
                array(
                    "id" => $r['albums_photos_id'],
                    "description" => $r['albums_photos_description'],
                    "thumbnail_url" => $r['albums_photos_thumbnail_url'],
                    "compressed_url" => $r['albums_photos_compressed_url'],
                    "views" => $r['albums_photos_views'],
                    "files" => $files,
                    "creation_ts" => $r['albums_photos_creation_ts'],
                    "is_active" => $r['albums_photos_is_active']
                ));
            $albums_photo = $albums_photos->jsonSerialize();
            $albums_photo['views'] += 1;
            unset($albums_photo['albums']);
            $photo_array[] = $albums_photo;
            $photo_ids[] = $albums_photo['id'];
        }
        $album = $album->jsonSerialize();
        $album['photos'] = $photo_array;
        $this->viewed($photo_ids);
        return $album;
    }

    public function delete()
    {
        $_REQUEST['increment_photo_view'] = false;
        return parent::delete();
    }

    public function viewed($arr_id = null)
    {
        $is_increment = true;
        
        if (isset($_REQUEST['increment_photo_view']) and
             $_REQUEST['increment_photo_view'] != true) {
            $is_increment = false;
        }
        if ($is_increment) {
            if (is_null($arr_id) and isset($_REQUEST['photos_id'])) {
                $arr_id = $_REQUEST['photos_id'];
            }
            if (! is_array($arr_id) and strpos($arr_id, ',') !== false) {
                $arr_id = explode(',', $arr_id);
            }
            $query = "UPDATE `albums_photos`
            SET views = views + 1
            WHERE id IN (" .
                 implode(",", $arr_id) . ")";
            
            $this->core->db->exec($query);
        }
    }

    public function all($data = array())
    {
        $all_albums = array();
        $albums_mangaer = Base\ManagerStore::locate('albums');
        $albums = $albums_mangaer->all();
        foreach ($albums as $a) {
            $all_albums[] = $this->get($a['id']);
        }
        return $all_albums;
    }

    public function search()
    {
        $final_result = array();
        $albums_manager = Base\ManagersStore::locate(Modules\Albums\Object);
        $result = $albums_manager->search();
        foreach ($result['results'] as $r) {
            $final_result[] = $this->get($r['id']);
        }
        $result['results'] = $final_result;
        return $result;
    }

    public function baseSearch()
    {
        return parent::search();
    }

    public function deleted()
    {
        $this->core->hasRight('view_deleted_' . $this->object_name);
        if (isset($_REQUEST['data'])) {
            $_REQUEST['data'] = array_merge($_REQUEST['data'], 
                array(
                    "is_active" => - 1
                ));
        } else {
            $_REQUEST['data'] = array(
                "is_active" => - 1
            );
        }
        
        $deleted = parent::search();
        if (isset($deleted['found']) and $deleted['found'] > 0) {
            return $deleted;
        }
        return array();
    }
}
