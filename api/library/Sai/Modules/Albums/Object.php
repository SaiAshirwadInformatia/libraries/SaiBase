<?php
namespace Sai\Modules\Albums;

use Sai\Base;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @name Albums
 * @abstract Would help represent Albums Object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $name Name of the album
     *     
     *      @Assert\NotBlank(message="VALID_NAME_REQ")
     *      @Assert\Length(min=3,minMessage="VALID_NAME_REQ")
     */
    public $name;

    /**
     *
     * @var string $description Description of the album
     *     
     *      @Assert\Length(min=20,minMessage="MIN_DESCRIPTION")
     */
    public $description;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @var timestamp $lastmodified_ts Represents the database lastmodified timestamp of the object
     */
    public $lastmodified_ts;

    /**
     *
     * @var integer $is_active Represents the possible state of an object
     *     
     *     
     *      Possible values
     *      1 - Active State
     *      0 - Inactive State
     *      -1 - Deleted State
     */
    public $is_active;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'name',
            'description',
            'creation_ts',
            'lastmodified_ts',
            'is_active'
        );
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            if ($this->id != null) {
                $this->loadObject($this->id);
            }
        }
    }

    /**
     *
     * @return Boolean
     */
    public function isActive()
    {
        return ($this->is_active == true or $this->is_active > 0);
    }
}
