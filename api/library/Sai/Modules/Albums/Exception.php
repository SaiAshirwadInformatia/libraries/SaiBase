<?php
namespace Sai\Modules\Albums;

use Sai\Base;

/**
 *
 * @name Albums Exception
 * @abstract albums exception handling
 * @author Rohan Sakhale26
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 2601;

    const UNIQUE = 2602;

    const CREATE_FAILED = 2603;

    const NOT_FOUND = 2604;

    const UPDATE_FAILED = 2605;

    const DELETE_FAILED = 2606;

    const VALID_NAME_REQ = 2607;

    const MIN_DESCRIPTION = 2608;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_albums_req_missing",
            self::UNIQUE => "excp_albums_unique",
            self::CREATE_FAILED => "excp_albums_create_failed",
            self::NOT_FOUND => "excp_albums_not_found",
            self::UPDATE_FAILED => "excp_albums_update_failed",
            self::DELETE_FAILED => "excp_albums_delete_failed",
            self::VALID_NAME_REQ => "excp_albums_valid_name_req",
            self::MIN_DESCRIPTION => "excp_albums_min_description"
        );
    }
}
