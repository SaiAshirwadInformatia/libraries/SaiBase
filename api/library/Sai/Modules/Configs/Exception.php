<?php
namespace Sai\Modules\Configs;

use Sai\Base;

/**
 *
 * @name Configs Exception
 * @abstract Configs exception handling
 * @author Rohan Sakhale
 * @copyright 2014 Sai Ashirwad Informatia
 * @since Ghanerao.in v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 1401;

    const UNIQUE = 1402;

    const CREATE_FAILED = 1403;

    const NOT_FOUND = 1404;

    const UPDATE_FAILED = 1405;

    const DELETE_FAILED = 1406;

    const STRING_REQ = 1407;

    const INVALID_NAME = 1408;

    const INVALID_OFTYPE = 1409;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_configs_req_missing",
            self::UNIQUE => "excp_configs_unique",
            self::CREATE_FAILED => "excp_configs_create_failed",
            self::NOT_FOUND => "excp_configs_not_found",
            self::UPDATE_FAILED => "excp_configs_update_failed",
            self::DELETE_FAILED => "excp_configs_delete_failed",
            self::STRING_REQ => "excp_configs_string_req",
            self::INVALID_NAME => "excp_configs_invalid_name",
            self::INVALID_OFTYPE => "excp_configs_invalid_oftype"
        );
    }
}
