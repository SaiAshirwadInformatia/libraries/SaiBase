<?php
namespace Sai\Modules\Configs;

use Sai\Base;
use Sai\Modules;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @name Configs
 * @abstract Would help represent configs Object
 * @author Rohan Sakhale
 * @copyright 2014 Sai Ashirwad Informatia
 * @since Ghanerao.in v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $name Name of the configuration uniquely identified value
     *     
     *      @Assert\Length(min=3,minMessage="INVALID_NAME")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $name;

    /**
     *
     * @var string $val Value of the configuration used by the site
     *     
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $val;

    /**
     *
     * @var string $oftype Of What Type the configuration is
     *     
     *      @Assert\Choice(choices={"text","list","boolean"},message="INVALID_OFTYPE")
     */
    public $oftype;

    /**
     *
     * @var $options
     */
    public $options;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'name',
            'val',
            'oftype',
            'options'
        );
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            if ($this->name != null) {
                $this->loadObjectByDBCol(
                    array(
                        "name" => $this->name
                    ));
            }
        }
    }

    /**
     * Overriding setting id base concept
     *
     * @param string $name
     *            acts as the config id
     *            
     * @see saiobject::setId()
     */
    public function setId($name)
    {
        if (! is_null($name) and ! is_string($name)) {
            throw new Modules\Configs\Exception(
                Modules\Configs\Exception::STRING_REQ);
        }
        if (! is_null($name) and ! empty($name)) {
            $this->name = $name;
        }
    }

    /**
     * (non-PHPdoc)
     *
     * @see saiobject::getId()
     */
    public function getId()
    {
        return $this->name;
    }
}
