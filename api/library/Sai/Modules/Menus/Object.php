<?php
namespace Sai\Modules\Menus;

use Sai\Base;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @name Menus
 * @abstract Would help represent Menus Object
 * @author Abhishek Patil
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $name Name of the Menu Uniquely Identified
     *     
     *      @Assert\Length(min=3,minMessage="INVALID_NAME")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $name;

    /**
     *
     * @var string $label Name of the Menu to be Displayed Over UI by Default
     *     
     *      @Assert\Length(min=3,minMessage="INVALID_LABEL")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $label;

    /**
     *
     * @var string $icon Standard Icon Name as stated in Glyphicons
     *     
     *      @Assert\Length(min=3,minMessage="INVALID_ICON")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $icon;

    /**
     *
     * @var string $permissions Rights required to access this menu by the user
     *      Comma separated multiple rights can be placed
     */
    public $permissions;

    /**
     *
     * @var string $url Partial URL where the menu should linked to
     */
    public $url;

    /**
     *
     * @var integer $parent_menu Reference Parent Menu ID
     */
    public $parent_menu;

    /**
     *
     * @var integer $menu_order Order at which the menu should be displayed, this is sequence in number
     */
    public $menu_order;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @var timestamp $lastmodified_ts Represents the database lastmodified timestamp of the object
     */
    public $lastmodified_ts;

    /**
     *
     * @var integer $is_active Represents the possible state of an object
     *     
     *     
     *      Possible values
     *      1 - Active State
     *      0 - Inactive State
     *      -1 - Deleted State
     */
    public $is_active;

    /**
     *
     * @var integer $is_public Whether this menu is publicly accessible without require of authentication
     */
    public $is_public;

    /**
     *
     * @var integer $is_shown_logged_user Whether this menu has to be displayed to logged user
     */
    public $is_shown_logged_user;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'name',
            'label',
            'icon',
            'permissions',
            'url',
            'parent_menu',
            'menu_order',
            'creation_ts',
            'lastmodified_ts',
            'is_active',
            'is_public',
            'is_shown_logged_user'
        );
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            if ($this->id != null) {
                $this->loadObject($this->id);
            }
        }
    }

    /**
     * Identify if the user is active or not
     *
     * @return Boolean
     */
    public function isActive()
    {
        return ($this->is_active == true or $this->is_active > 0);
    }
}
