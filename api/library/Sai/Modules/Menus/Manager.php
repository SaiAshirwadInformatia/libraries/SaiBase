<?php
namespace Sai\Modules\Menus;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Menus
 * @abstract Would help represent menus object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager extends Base\Manager
{

    /**
     * Contructor for Menus Manager
     */
    public function __construct($object = 'menus')
    {
        parent::__construct($object);
    }

    /**
     */
    public function parents()
    {
        $menus = $this->all();
        if (! is_null($menus)) {
            foreach ($menus as $k => $m) {
                if ($m['parent_menu'] > 0) {
                    unset($menus[$k]);
                }
            }
        }
        return $menus;
    }

    /**
     */
    public function all($data = array())
    {
        $menu = new Modules\Menus\Object(false);
        $where_data = array(
            'is_active' => 1
        );
        if (! $this->core->isLogged()) {
            $where_data['is_public'] = 1;
        } else {
            $where_data['is_shown_logged_user'] = 1;
        }
        $menus = $this->core->db->selectAll(
            array(
                'tablename' => 'menus',
                'where' => array(
                    'condition' => 'AND',
                    'data' => $where_data
                )
            ));
        $final_menus = array();
        $pending_menus = array();
        if (! is_null($menus) and count($menus) > 0) {
            foreach ($menus as $m) {
                $menu->loadDefaults();
                if (isset($m['parent_menu']) and $m['parent_menu'] > 0) {
                    if (isset($final_menus[$m['parent_menu']])) {
                        $menu->loadObject($m['id']);
                        $final_menus[$m['parent_menu']]['childs'][] = $menu->jsonSerialize();
                    } else {
                        $pending_menus[] = $m;
                    }
                } else {
                    $menu->loadObject($m['id']);
                    $final_menus[$m['id']] = $menu->jsonSerialize();
                    $final_menus[$m['id']]['childs'] = array();
                }
            }
        }
        if (count($pending_menus) > 0) {
            foreach ($pending_menus as $m) {
                $menu->loadDefaults();
                if (isset($m['parent_menu']) and $m['parent_menu'] > 0) {
                    if (isset($final_menus[$m['parent_menu']])) {
                        $menu->loadObject($m['id']);
                        $final_menus[$m['parent_menu']]['childs'][] = $menu->jsonSerialize();
                    }
                }
            }
        }
        return $final_menus;
    }
}
