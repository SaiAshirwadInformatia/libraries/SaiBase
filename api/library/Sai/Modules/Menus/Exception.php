<?php
namespace Sai\Modules\Menus;

use Sai\Base;

/**
 *
 * @name Menus Exception
 * @abstract Menus exception handling
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 1601;

    const UNIQUE = 1602;

    const CREATE_FAILED = 1603;

    const NOT_FOUND = 1604;

    const UPDATE_FAILED = 1605;

    const DELETE_FAILED = 1606;

    const INVALID_NAME = 1607;

    const INVALID_LABEL = 1608;

    const INVALID_LABEL_KEY = 1609;

    const INVALID_ICON = 1610;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_req_menus_missing",
            self::UNIQUE => "excp_menus_unique",
            self::CREATE_FAILED => "excp_menus_create_failed",
            self::NOT_FOUND => "excp_menus_not_found",
            self::UPDATE_FAILED => "excp_menus_update_failed",
            self::DELETE_FAILED => "excp_menus_delete_failed"
        );
    }
}
