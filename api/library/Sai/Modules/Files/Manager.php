<?php
namespace Sai\Modules\Files;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Files Manager
 * @abstract Would help manage files related operation
 * @author Rohan Sakhale
 * @author Chaitali Patil
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager extends Base\Manager
{

    public $base_upload_path;

    public $url_path;

    public function __construct($object = 'files')
    {
        parent::__construct($object);
        $auth_manager = Base\ManagersStore::locate('auth');
        if ($auth_manager->validate()) {
            $this->base_upload_path = SAI_ABS_PATH . 'upload' . DS .
                 $this->core->session->getParam('currentUser')->getId();
            $this->url_path = $this->core->basehref . '/upload/' .
                 $this->core->session->getParam('currentUser')->getId();
            if (! file_exists($this->base_upload_path)) {
                mkdir($this->base_upload_path);
            }
            if (! file_exists($this->base_upload_path . DS . 'compress')) {
                mkdir($this->base_upload_path . DS . 'compress');
            }
            if (! file_exists($this->base_upload_path . DS . 'thumbs')) {
                mkdir($this->base_upload_path . DS . 'thumbs');
            }
        }
    }

    /**
     */
    public function create()
    {
        $files = array();
        foreach ($_FILES as $f_key => $file_arr) {
            if (! is_array($file_arr['name'])) {
                $files[] = $this->addFile($file_arr);
            } else {
                foreach ($file_arr['name'] as $idx => $f_name) {
                    $files[] = $this->addFile(
                        array(
                            'name' => $f_name,
                            'type' => $file_arr['type'][$idx],
                            'size' => $file_arr['size'][$idx],
                            'error' => $file_arr['error'][$idx],
                            'tmp_name' => $file_arr['tmp_name'][$idx]
                        ));
                }
            }
        }
        return array(
            'msg' => 'Successfully uploaded your file' .
                 (count($files) > 1 ? 's' : ''),
                'files' => $files
        );
    }

    /**
     * Add file to server directory and a record in database
     *
     * @param $file_data array            
     */
    public function addFile($file_data)
    {
        $file = new files();
        $imagine = new Imagine\Gd\Imagine();
        $file->loadFromRequest($file_data);
        if (! is_null($file->name)) {
            $r = $this->core->db->insert(
                array(
                    'tablename' => 'files',
                    'data' => array(
                        'name' => $file->name,
                        'path' => $this->url_path . '/' . $file->name,
                        'type' => $file->type,
                        'size' => $file->size,
                        'user_id' => $this->core->session->getParam(
                            'currentUser')
                            ->getId()
                    )
                ));
            $ret_arr = array(
                'name' => $file->name,
                'url' => $this->core->basehref . '/upload/' .
                     $this->core->session->getParam('currentUser')->getId() . '/' .
                     $file_data['name'],
                    'type' => $file->type
            );
            if ($r['status'] == 'OK') {
                $ret_arr['id'] = $r['id'];
                $ret_arr['self'] = $this->core->apihref . '/files/' . $r['id'];
                move_uploaded_file($file_data['tmp_name'], 
                    $this->base_upload_path . DS . $file_data['name']);
                if (strpos($file_data['type'], 'image') === 0) {
                    if (isset($_REQUEST['compress_image']) and ($_REQUEST['compress_image'] ==
                         'true' or $_REQUEST['compress_image'] == 'yes')) {
                        $ext = substr($_FILES['files']['name'], 
                            strrpos($_FILES['files']['name'], '.') + 1);
                        
                        $options = array(
                            'jpeg_quality' => 50
                        );
                        if ($ext == 'png') {
                            $options = array(
                                'png_compression_level' => 9
                            );
                        }
                        $size = new Imagine\Image\Box(256, 256);
                        
                        $mode = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                        
                        $thumb_path = $this->base_upload_path . DS . 'thumbs' .
                             DS . $file_data['name'];
                        $imagine->open(
                            $this->base_upload_path . DS . $file_data['name'])
                            ->thumbnail($size, $mode)
                            ->save($thumb_path, $options);
                        
                        $compress_path = $this->base_upload_path . DS .
                             'compress' . DS . $file_data['name'];
                        $imagine->open(
                            $this->base_upload_path . DS . $file_data['name'])->save(
                            $compress_path, $options);
                        $ret_arr['thumb'] = $this->url_path . '/thumbs/' .
                             $file_data['name'];
                        $ret_arr['compress'] = $this->url_path . '/compress/' .
                             $file_data['name'];
                    }
                }
                return $ret_arr;
            } else {
                throw new Modules\Files\Exception($r['error_code']);
            }
        }
    }

    /**
     */
    public function get($data)
    {
        $file = parent::get($data);
        
        $query = "UPDATE `files`
            SET hits = hits + 1
            WHERE id = " . $file->getId();
        $this->core->db->exec($query);
        $file_path = SAI_ABS_PATH . 'upload' . DS . $file->users->getId() . DS .
             $file->name;
        Base\Common::downloadContentsToFile($file_path);
    }

    /**
     */
    public function update()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_AVAILABLE);
    }

    /**
     */
    public function delete()
    {
        $this->object->loadArgs();
        if (! is_null($this->object->getId())) {
            $this->deleteById();
        } elseif (! is_null($this->object->path)) {
            if (isset($this->object->users) and is_object($this->object->users)) {
                $this->object->user_id = $this->object->users->getId();
            } elseif (! isset($file->user_id)) {
                $this->object->user_id = $this->core->session->getUser()->getId();
            }
            $this->deleteByPath();
        } else {
            throw new Modules\Files\Exception(
                Modules\Files\Exception::DELETE_FAILED);
        }
        change_response_code(204);
    }

    public function deleteByPath()
    {
        $result = $this->core->db->delete(
            array(
                'tablename' => 'files',
                'where' => array(
                    'data' => array(
                        'path' => $this->object->path,
                        'user_id' => $this->object->user_id
                    ),
                    'condition' => 'AND'
                )
            ));
        if (is_null($result) or $result['status'] == 'NOT_OK') {
            throw new Modules\Files\Exception(
                Modules\Files\Exception::DELETE_FAILED);
        }
    }
}
