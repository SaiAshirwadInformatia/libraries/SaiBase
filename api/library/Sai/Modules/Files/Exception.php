<?php
namespace Sai\Modules\Files;

use Sai\Base;

/**
 *
 * @name Files Exception
 * @abstract Files exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 1901;

    const UNIQUE = 1902;

    const CREATE_FAILED = 1903;

    const NOT_FOUND = 1904;

    const DELETE_FAILED = 1906;

    const FILE_SIZE = 1907;

    public $file_error_map;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_files_req_missing",
            self::UNIQUE => "excp_files_unique",
            self::CREATE_FAILED => "excp_files_create_failed",
            self::NOT_FOUND => "excp_files_not_found",
            self::DELETE_FAILED => "excp_files_delete_failed",
            self::FILE_SIZE => "excp_files_file_size"
        );
        $this->file_error_map = array(
            UPLOAD_ERR_INI_SIZE => self::FILE_SIZE
        );
        if (isset($this->file_error_map[$exception_number])) {
            $this->exception_number = $this->file_error_map[$exception_number];
        }
        $this->response_error_code[413][] = self::FILE_SIZE;
    }
}
