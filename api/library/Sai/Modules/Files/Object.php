<?php
namespace Sai\Modules\Files;

use Sai\Base;

/**
 *
 * @name Files Object
 * @abstract Would represent the base of objects reusable patterns
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Users
{

    /**
     *
     * @var string $name Name of the file to be saved
     */
    public $name;

    /**
     *
     * @var string $path Path where the file is stored on server
     */
    public $path;

    /**
     *
     * @var string $type MIME Type of the File uploaded
     */
    public $type;

    /**
     *
     * @var integer $size Size of file in Bytes
     */
    public $size;

    /**
     *
     * @var integer $hits Number of times this file has been accessed
     */
    public $hits;

    /**
     *
     * @var timestamp $creation_ts Represents the database creation timestamp of the object
     */
    public $creation_ts;

    /**
     *
     * @var integer $is_active Represents the possible state of an object
     *     
     *     
     *      Possible values
     *      1 - Active State
     *      0 - Inactive State
     *      -1 - Deleted State
     */
    public $is_active;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'name',
            'path',
            'type',
            'size',
            'user_id',
            'users',
            'hits',
            'creation_ts',
            'is_active'
        );
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            if ($this->id != null) {
                $this->loadObject($this->id);
            }
        }
    }

    /**
     * Overrides parent loadFromRequest
     *
     * @param $file (Array)
     *            representing Array of $_FILES
     *            
     */
    public function loadFromRequest($file)
    {
        if (isset($file) and isset($file['error']) and
             $file['error'] == UPLOAD_ERR_OK) {
            foreach ($this->idx_key as $k) {
                if (isset($file[$k])) {
                    $this->$k = $file[$k];
                }
            }
        }
    }
}
