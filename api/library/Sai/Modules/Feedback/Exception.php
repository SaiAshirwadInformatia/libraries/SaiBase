<?php
namespace Sai\Modules\Feedback;

use Sai\Base;

/**
 *
 * @name feedback Exception
 * @abstract feedback exception handling
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Exception extends Base\SqlException
{

    const REQUIRED_ATTR_MISSING = 2201;

    const UNIQUE = 2202;

    const CREATE_FAILED = 2203;

    const NOT_FOUND = 2204;

    const UPDATE_FAILED = 2205;

    const DELETE_FAILED = 2206;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_feedback_req_missing",
            self::UNIQUE => "excp_feedback_unique",
            self::CREATE_FAILED => "excp_feedback_create_failed",
            self::NOT_FOUND => "excp_feedback_not_found",
            self::UPDATE_FAILED => "excp_feedback_update_failed",
            self::DELETE_FAILED => "excp_feedback_delete_failed"
        );
    }
}
