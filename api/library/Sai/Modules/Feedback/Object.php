<?php
namespace Sai\Modules\Feedback;

use Sai\Base;

/**
 *
 * @name Feedback
 * @abstract Would help represent feedback Object
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since Ghanerao v1
 *       
 */
class Object extends Base\Users
{

    public $message;

    public $feedback_purpose;

    public $creation_ts;

    /**
     * Constructor for Events object
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'id',
            'message',
            'feedback_purpose',
            'users',
            'user_id',
            'creation_ts'
        );
        $this->setId(null);
        if ($loadArgs) {
            $this->loadArgs();
            if ($this->id != null) {
                $this->loadObject($this->id);
            }
        }
    }
}
