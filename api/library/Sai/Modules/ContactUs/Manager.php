<?php
namespace Sai\Modules\ContactUs;

use Sai\Base;
use Sai\Modules;

/**
 *
 * @name Contact Us Manager
 * @abstract Would help operate Contact Us related operations
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Manager extends Base\Manager
{

    /**
     * Contructor for Contact Us
     */
    public function __construct($object = 'contactus')
    {
        parent::__construct($object);
    }

    /**
     * Create helps contacting site admin
     */
    public function create()
    {
        $this->object->loadArgs();
        $contactus = $this->object;
        $email_manager = Base\ManagersStore::locate('email');
        $msg = nl2br(trim($contactus->message));
        try {
            $email_manager->sendEmail(
                array(
                    'name' => $contactus->name,
                    'email' => $contactus->email
                ), $this->core->from_email, 'Contact Us - WillingTree', $msg);
            return array(
                'status' => 'OK',
                'msg' => 'Message sent successfully'
            );
        } catch (Mandrill_Error $e) {
            return array(
                'status' => 'NOT_OK',
                'msg' => $e->getMessage(),
                'code' => $e->getCode()
            );
        }
    }

    /**
     */
    public function get($data)
    {
        throw new Modules\ContactUs\Exception(
            Modules\ContactUs\Exception::GET_NOT_ALLOWED);
    }

    /**
     */
    public function update()
    {
        throw new Modules\ContactUs\Exception(
            Modules\ContactUs\Exception::UPDATE_NOT_ALLOWED);
    }

    /**
     */
    public function delete()
    {
        throw new Modules\ContactUs\Exception(
            Modules\ContactUs\Exception::DELETE_NOT_ALLOWED);
    }

    /**
     */
    public function my()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }

    /**
     */
    public function all()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }

    /**
     */
    public function deleted()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }

    /**
     */
    public function search()
    {
        throw new Base\Modules\Exception(
            Base\Modules\Exception::SERVICE_NOT_ALLOWED);
    }
}
