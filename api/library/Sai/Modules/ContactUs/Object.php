<?php
namespace Sai\Modules\ContactUs;

use Sai\Base;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @name Contact Us
 * @abstract Would help represent Contact Us Object
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Object extends Base\Object
{

    /**
     *
     * @var string $name Name of the person trying to contact the site administrator
     *     
     *      @Assert\Length(min=3,minMessage="REQUIRED_ATTR_MISSING")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $name;

    /**
     *
     * @var string $email Email of the person trying to contact the site administrator
     *     
     *      @Assert\Email(message="INVALID_EMAIL")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $email;

    /**
     *
     * @var string $message Message to the Site Administrator
     *     
     *      @Assert\Length(min=20,minMessage="MESSAGE_REQ")
     *      @Assert\NotBlank(message="REQUIRED_ATTR_MISSING")
     */
    public $message;

    /**
     *
     * @param string $loadArgs
     *            If found as true load additional arguments from request data
     *            
     *            Defaults to TRUE
     */
    public function __construct($loadArgs = true, $changelocale = true)
    {
        parent::__construct($changelocale);
        $this->idx_key = array(
            'name',
            'email',
            'message'
        );
        if ($loadArgs) {
            $this->loadArgs();
        }
    }
}
