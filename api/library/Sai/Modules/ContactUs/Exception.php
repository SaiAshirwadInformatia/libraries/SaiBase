<?php
namespace Sai\Modules\ContactUs;

use Sai\Base;

/**
 *
 * @name Contact Us Exception
 * @abstract Contact Us exception handling
 * @author Rohan Sakhale
 * @author Chirag Babrekar
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class Exception extends Base\Exception
{

    const REQUIRED_ATTR_MISSING = 2301;

    const CREATE_FAILED = 2303;

    const GET_NOT_ALLOWED = 2307;

    const UPDATE_NOT_ALLOWED = 2308;

    const DELETE_NOT_ALLOWED = 2309;

    const INVALID_EMAIL = 2310;

    const MESSAGE_REQ = 2311;

    public function __construct($exception_number)
    {
        parent::__construct($exception_number);
        $this->exception_list = array(
            self::REQUIRED_ATTR_MISSING => "excp_contactus_req_missing",
            self::CREATE_FAILED => "excp_contactus_create_failed",
            self::GET_NOT_ALLOWED => "excp_contactus_get_not_allowed",
            self::UPDATE_NOT_ALLOWED => "excp_contactus_update_not_allowed",
            self::DELETE_NOT_ALLOWED => "excp_contactus_delete_not_allowed",
            self::INVALID_EMAIL => "excp_contactus_invalid_email",
            self::MESSAGE_REQ => "excp_contactus_message_req"
        );
        $this->response_error_code[405] = array(
            self::GET_NOT_ALLOWED,
            self::UPDATE_NOT_ALLOWED,
            self::DELETE_NOT_ALLOWED
        );
    }
}
