<?php
namespace Sai\Database;

/**
 *
 * @name SQL DB
 * @abstract Would represent the base of SQL operations class
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
abstract class sqldb
{

    /**
     *
     * @var object $con Connection object of database
     */
    public $con;

    /**
     *
     * @var string $tablename Query to be executed over the table
     */
    public $tablename;

    /**
     *
     * @var array $data Insertion data array with Key Value pairs
     */
    public $data;

    /**
     *
     * @var array $where Where clause data array with Key Value pairs
     */
    public $where;

    /**
     *
     * @var boolean $like Identifies if LIKE clause has to be applied over string WHERE clause
     */
    public $like;

    /**
     *
     * @var array $set Set data array used to perform Update
     */
    public $set;

    /**
     *
     * @var array $limit maintaining the `offset` and `maximum` key values
     */
    public $limit;

    /**
     *
     * @var boolean $upsert Identifies if perform UPSERT queries are allowed with INSERT
     */
    public $upsert;

    /**
     *
     * @var string $orderby The order in which result set should be sorted either ASC or DESC
     */
    public $orderby;

    /**
     *
     * @var string $class The name of class that can be generated out from tablename
     */
    public $class;

    /**
     * Initializes the class data members used during execution
     *
     * As we now have a concept of passing Associate Array to operations like
     * INSERT, SELECT, UPDATE, DELETE
     *
     * The array is identify by this method
     * accordingly class data members are loaded, Thanks :)
     *
     * @param $db_obj (Array)
     *            which has to be identified and loaded
     *            
     *            Keys identified from this Associate Array
     *            - tablename
     *            - data
     *            - where
     *            - set
     *            - limit
     *            - orderby
     *            - like ( This is beautiful, uses the LIKE CLAUSE :D )
     *            
     */
    public function init_obj(&$db_obj)
    {
        $keys_to_load = array(
            'tablename',
            'data',
            'where',
            'set',
            'limit',
            'orderby',
            'like',
            'upsert'
        );
        foreach ($keys_to_load as $key => $value) {
            $this->$value = null;
        }
        $this->like = false;
        $this->upsert = false;
        foreach ($keys_to_load as $key => $value) {
            if (isset($db_obj[$value])) {
                if (is_array($db_obj[$value])) {
                    $db_obj[$value] = array_filter($db_obj[$value], 
                        function ($__v)
                        {
                            if (is_object($__v) or is_array($__v)) {
                                return true;
                            } else 
                                if (empty($__v) and ! is_numeric($__v)) {
                                    return false;
                                }
                            return true;
                        });
                }
                $this->$value = $db_obj[$value];
            }
        }
        /**
         * Minor fix for removal of array_filter unwanted stuff :(
         * Default values
         */
        if (! is_null($this->limit)) {
            if (! isset($this->limit['offset'])) {
                $this->limit['offset'] = 0;
            }
            if (! isset($this->limit['maximum'])) {
                $this->limit['maximum'] = 25;
            }
        }
        if (! is_null($this->where)) {
            if (! isset($this->where['condition'])) {
                $this->where['condition'] = 'AND';
            }
        }
        $this->class = null;
        if (! is_null($this->tablename) and class_exists($this->tablename) and
             $this->tablename != 'configs') {
            $c = $this->tablename;
            $this->class = new $c(false);
        }
        if (isset($this->data['url_for_link'])) {
            unset($this->data['url_for_link']);
        }
        if (isset($this->set['url_for_link'])) {
            unset($this->set['url_for_link']);
        }
    }

    /**
     * Builds the WHERE CLAUSE String from $where data member
     *
     * This initializes, identifies the CLAUSE to be used
     * Also identifies the VARIABLE DATA TYPE
     * Accordingly builds the final WHERE CLAUSE string
     *
     * Reduces calculation efforts just pass out the array
     * leave rest to this CLASS for DB execution
     *
     * @return (String) WHERE CLAUSE usable ready STRING
     *         Another beauty is that, if $where is empty
     *         returns `WHERE 1` which means WHERE CLAUSE with TRUE
     *        
     */
    public function buildWhereString()
    {
        $sql = " WHERE 1 ";
        if (! is_null($this->where)) {
            $whereArray = array();
            $data = $this->where['data'];
            foreach ($data as $col_name => $col_value) {
                if ((! is_null($col_value) and $col_value != '') or
                     $col_value === 0) {
                    $whereString = "`$col_name` ";
                    if (is_array($col_value)) {
                        $whereArrayItem = $this->parseWhereArray($whereString, 
                            $col_value);
                        if (! is_null($whereArrayItem)) {
                            $whereArray[] = $whereArrayItem;
                        }
                    } elseif (is_numeric($col_value)) {
                        $whereArray[] = $whereString . ' = ' . $col_value;
                    } else {
                        if ($this->like) {
                            $whereArray[] = $whereString . " LIKE '%" .
                                 $this->escapeString($col_value) . "%'";
                        } else {
                            $whereArray[] = $whereString . " = '" .
                                 $this->escapeString($col_value) . "'";
                        }
                    }
                }
            }
            if (isset($this->where['query'])) {
                $whereArray = array_merge($whereArray, $this->where['query']);
            }
            if (count($whereArray) > 0) {
                $sql = " WHERE " .
                     implode(" {$this -> where['condition']} ", $whereArray);
            }
        }
        return $sql;
    }

    public function parseWhereArray($whereString, &$col_value)
    {
        if (count($col_value) > 0) {
            if (array_key_exists('type', $col_value) and
                 array_key_exists('data', $col_value)) {} else {
                $col_value = array_filter($col_value);
                $col_value = filter_var($col_value, FILTER_CALLBACK, 
                    array(
                        'options' => array(
                            $this,
                            'castValueForDB'
                        )
                    ));
                return $whereString . ' IN (' . implode(", ", $col_value) . ')';
            }
        }
        return null;
    }

    /**
     *
     * @name insert
     * @abstract Insert record in database table
     * @param $db_obj (Mixed)
     *            containing elements as
     *            - tablename
     *            - data
     */
    public function insert($db_obj)
    {
        $this->init_obj($db_obj);
        $this->validateData(
            array(
                'table' => $this->tablename,
                'data' => &$this->data,
                'methodname' => __FUNCTION__
            ));
        $query = "INSERT INTO `{$this->tablename}`";
        
        /**
         * CREATION_TS shouldn't be decided by object :P
         * As mostly when defining tables
         * `CREATION_TS` carries default value as CURRENT_TIMESTAMP
         * Hence, we should be relying on Database for this value
         *
         * Identify
         * if object supports this column
         * then better we unset it to avoid wrong creation_ts
         *
         * Foreceful removal ;)
         */
        if (array_key_exists('creation_ts', $this->data)) {
            unset($this->data['creation_ts']);
        }
        
        /**
         * ID again shouldn't be decided by object :D
         * As mostly when defining tables
         * `ID` is given AUTO_INCREMENT which is INDEXED by DB
         * then better we unset it to avoid wrong ID creation
         *
         * Foreceful removal :)
         */
        if (array_key_exists('id', $this->data)) {
            unset($this->data['id']);
        }
        
        /**
         * Earlier this was missed, which gave NULL or bad Values
         * Hence, a thought came out it would be good to initialize
         * rather leaving empty, hence NOW would be best time it
         *
         * Thought Process was, first creation is even a kinda update
         * Which states this was lastmodified when created
         *
         * Hence this is Forceful assignment
         */
        if ((array_key_exists('lastmodified_ts', $this->data)) or (! is_null(
            $this->class) and property_exists($this->class, 'lastmodified_ts'))) {
            $this->data['lastmodified_ts'] = $this->now();
        }
        
        $cols = filter_var(array_keys($this->data), FILTER_CALLBACK, 
            array(
                "options" => "addGraveAccentToNames"
            ));
        $vals = filter_var(array_values($this->data), FILTER_CALLBACK, 
            array(
                "options" => array(
                    $this,
                    "castValueForDB"
                )
            ));
        $query .= " (" . implode(',', $cols) . ") VALUES (" . implode(',', 
            $vals) . ")";
        if ($this->upsert) {
            $setArray = array();
            foreach ($this->data as $col_name => $col_value) {
                $setString = "`$col_name` = ";
                $setArray[] = $setString . $this->castValueForDB($col_value);
            }
            $query .= " ON DUPLICATE KEY UPDATE " . implode(", ", $setArray);
        }
        if ($this->exec($query)) {
            return array(
                'status' => OK,
                'msg' => 'Inserted successfully',
                'id' => $this->getLastInsert()
            );
        }
        
        return array(
            'status' => NOT_OK,
            'msg' => $this->con->error,
            'error_code' => $this->con->errno
        );
    }

    /**
     *
     * @name update
     * @abstract update's row of database table
     * @param $db_obj (Mixed)
     *            containing elements as
     *            - tablename
     *            - set
     *            - where
     *            
     */
    public function update($db_obj)
    {
        $this->init_obj($db_obj);
        $this->validateData(
            array(
                "table" => $this->tablename,
                "set" => &$this->set,
                "where" => &$this->where,
                "methodname" => __FUNCTION__
            ));
        $query = "UPDATE `{$this->tablename}`";
        $setArray = array();
        
        /**
         * LASTMODIFIED_TS has to be updated on every update
         * Identify
         * if object supports this column
         * then UPDATE the time to NOW ;D
         *
         * Forceful updation
         */
        if (array_key_exists('lastmodified_ts', $this->set) or
             property_exists($this->class, 'lastmodified_ts')) {
            $this->set['lastmodified_ts'] = $this->now();
        }
        
        /**
         * CREATION_TS shouldn't be updated on UPDATE ;)
         * This is forceful removal
         */
        if (array_key_exists('creation_ts', $this->set)) {
            unset($this->set['creation_ts']);
        }
        
        /**
         * ID again shouldn't be updated on UPDATE ;)
         * This is foreceful removal
         */
        if (array_key_exists('id', $this->set)) {
            unset($this->set['id']);
        }
        
        foreach ($this->set as $col_name => $col_value) {
            $setString = "`$col_name` = ";
            $setArray[] = $setString . $this->castValueForDB($col_value);
        }
        $query .= " SET " . implode(", ", $setArray);
        if (count($this->where['data']) == 0) {
            throw new Exception(
                "Missing where clause data will update the entire data :-@ :-X ");
        }
        $query .= $this->buildWhereString();
        if ($this->exec($query)) {
            return array(
                'status' => OK,
                'msg' => 'Updated successfully'
            );
        }
        return array(
            'status' => NOT_OK,
            'msg' => $this->con->error,
            'error_code' => $this->con->errno
        );
    }

    /**
     * Perform select query over the given $db_obj
     *
     * @param $db_obj (Mixed)
     *            containing elements as
     *            - tablename
     *            - limit
     *            - where
     *            
     * @return (Object) of result set that would be given by exec_Query
     */
    public function select($db_obj)
    {
        $this->init_obj($db_obj);
        $validateData = array(
            "table" => $this->tablename,
            "limit" => $this->limit,
            "methodname" => __FUNCTION__
        );
        if (! is_null($this->where) and count($this->where['data']) > 0) {
            $validateData['where'] = &$this->where;
        }
        $this->validateData($validateData);
        $query = sprintf("SELECT * FROM `%s` %s", $this->tablename, 
            $this->buildWhereString());
        if (! is_null($this->orderby)) {
            $query .= sprintf(" ORDER BY %s ", $this->orderby);
        }
        if (! is_null($this->limit) and is_array($this->limit) and
             count($this->limit) > 0) {
            $query .= sprintf(" LIMIT %s, %s", $this->limit['offset'], 
                $this->limit['maximum']);
        }
        return $this->exec_Query($query);
    }

    /**
     * Lets you know the count of number of elements present depending on where clause
     *
     * @return (Number) of rows present in table
     */
    public function count($db_obj)
    {
        $this->init_obj($db_obj);
        $validateData = array(
            "table" => $this->tablename,
            "methodname" => __FUNCTION__
        );
        if (! is_null($this->where) and count($this->where['data']) > 0) {
            $validateData['where'] = $this->where;
        }
        $this->validateData($validateData);
        $query = "SELECT COUNT(*) AS `count` FROM `{$this->tablename}`" .
             $this->buildWhereString();
        
        $result = $this->exec_Query($query);
        $r = $this->resultToArray($result);
        return $r[0]['count'];
    }

    /**
     * Used to perform DELETE query over database
     *
     * @name delete
     * @abstract Delete row's from database table
     * @param
     *            db_obj => Array of Database Object intakes keys as
     *            'tablename' => Name of the table
     *            'where'		=> Array of where clause
     *            
     * @return Array
     */
    public function delete($db_obj)
    {
        $this->init_obj($db_obj);
        $this->validateData(
            array(
                'table' => $this->tablename,
                'where' => $this->where
            ));
        $query = sprintf("DELETE FROM `%s` %s", $this->tablename, 
            $this->buildWhereString());
        if ($this->exec($query)) {
            return array(
                'status' => 'OK',
                'msg' => 'Deleted successfully'
            );
        }
        return array(
            'status' => NOT_OK,
            'msg' => 'Error (' . $this->con->errno . '): ' . $this->con->error,
            'error_code' => $this->con->errno
        );
    }

    /**
     * Cleans PHP object/array data from the given object
     * As only strings or values that are understood by DB should be used
     *
     * Helps to avoid developers mistake of invalid object passing
     */
    public function cleanData(&$data)
    {
        if (! is_null($data) and is_array($data)) {
            foreach ($data as $k => $v) {
                if (is_array($v) or is_object($v) or
                     (is_null($v) and ! is_numeric($v))) {
                    unset($v);
                }
            }
        }
    }

    /**
     * Checks if the data passed for any operation contains all the required items
     *
     * This is to avoid dynamic failures to mistakes by developers
     * Additionally this method would call cleanData, to avoid object/array insertion as string
     * This mistakes usually happens if less awareness of object is seen by developer
     */
    public function validateData($data)
    {
        if (! is_null($data) and is_array($data)) {
            if (! isset($data['classname'])) {
                $data['classname'] = __CLASS__;
            }
            if (! isset($data['methodname'])) {
                $data['methodname'] = 'Not initialized in called method';
            }
            if (isset($data['table']) and
                 (is_null($data['table']) or trim($data['table']) == '')) {
                throw new Exception(
                    'Table name missing in Class: ' . $data['classname'] .
                     ' and Function: ' . $data['methodname']);
            }
            if (isset($data['data'])) {
                if (! is_array($data['data'])) {
                    if (is_object($data['data']) and
                         method_exists($data['data'], 'jsonSerialize')) {
                        $data['data'] = $data['data']->jsonSerialize();
                    } else {
                        throw new Exception(
                            'The data variable is not an associative array in Class: ' .
                                 $data['classname'] . ' and Function: ' .
                                 $data['methodname']);
                    }
                }
                /**
                 * Clean `INSERT` data
                 */
                $this->cleanData($data['data']);
            }
            if (isset($data['where'])) {
                if (is_array($data['where'])) {
                    if (! isset($data['where']['data'])) {
                        throw new Exception(
                            'The data variable of WHERE clause is not an array or not defined in Class: ' .
                                 $data['classname'] . ' and Function: ' .
                                 $data['methodname']);
                    }
                    if (is_object($data['where']['data']) and
                         method_exists($data['where']['data'], 'jsonSerialize')) {
                        $data['where']['data'] = $data['where']['data']->jsonSerialize();
                    }
                    
                    if (count($data['where']['data']) > 0) {
                        if (! isset($data['where']['condition'])) {
                            $data['where']['condition'] = 'AND';
                            /*
                             * throw new Exception('The condition index of WHERE clause is not defined in Class: ' . $data['classname'] . ' and Function: ' . $data['methodname']);
                             */
                        }
                    }
                } else {
                    throw new Exception(
                        'The where clause variable is not an associative array in Class: ' .
                             $data['classname'] . ' and Function: ' .
                             $data['methodname']);
                }
            }
            if (array_key_exists('set', $data)) {
                if (is_null($data['set']) or ! is_array($data['set'])) {
                    if (is_object($data['set']) and
                         method_exists($data['set'], 'jsonSerialize')) {
                        $data['set'] = $data['set']->jsonSerialize();
                    } else {
                        throw new Exception(
                            'The set clause variable is not an associative array in Class: ' .
                                 $data['classname'] . ' and Function: ' .
                                 $data['methodname']);
                    }
                }
                /**
                 * Clean `SET` data
                 */
                $this->cleanData($data['set']);
            }
        }
    }

    /**
     *
     * @param unknown $value            
     * @return Ambigous <string, object, boolean, (String)>
     */
    public function castValueForDB($value)
    {
        $value = castValueForDB($value);
        if ((strpos($value, "'") === 0) and
             (strrpos($value, "'") === (strlen($value) - 1))) {
            $value = sprintf("'%s'", 
                $this->escapeString(substr($value, 1, strlen($value) - 2)));
        }
        return $value;
    }

    /**
     * Get current timestamp that could be used in your SQL Queries
     * Also known as FORMATTED string required by DB
     *
     * @return string - Y-m-d H:i:s ;)
     */
    public function now()
    {
        return date('Y-m-d H:i:s', time());
    }

    abstract public function resultToArray($result);

    abstract public function exec($query);

    abstract public function exec_Query($query);

    abstract public function getLastInsert();

    abstract public function escapeString($str);
}
