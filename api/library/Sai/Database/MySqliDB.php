<?php
namespace Sai\Database;

/**
 *
 * @name MySQLiDB
 * @abstract Would represent the MySQLi operations class
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
class MySqliDB extends sqldb
{

    /**
     * Creates connection with database using MySQLi
     */
    public function __construct()
    {
        $this->con = new \mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if ($this->con->connect_error) {
            die(
                "Connect Error (" . $this->con->connect_errno . ") " .
                     $this->con->connect_error);
        }
        if (! $this->con->set_charset('utf8')) {
            die(
                "Error(" . $this->con->errnon . ") Loading character set utf8: " .
                     $this->con->error);
        }
    }

    /**
     * Performs string escape using MySQLi's mechanism
     *
     * (non-PHPdoc)
     *
     * @see sqldb::escapeString()
     */
    public function escapeString($str)
    {
        return $this->con->escape_string($str);
    }

    /**
     * Provides a good mechanism to perform Bulk Insertion with lesser query execution
     *
     * @param array $db_obj            
     * @return multitype:string NULL |multitype:string
     */
    public function bulkInsert($db_obj)
    {
        $sqls = array();
        $this->init_obj($db_obj);
        $this->validateData(
            array(
                'table' => $this->tablename,
                'data' => $this->data,
                'methodname' => __FUNCTION__
            ));
        $cols = filter_var(array_keys($this->data[0]), "addGraveAccentToNames");
        $vals = array();
        $query = "INSERT INTO `{$this->tablename}`";
        foreach ($this->data as $d) {
            $v = filter_var($d, 
                array(
                    $this,
                    "castValueForDB"
                ));
            $vals[] = "(" . implode(",", $v) . ")";
        }
        if (count($vals) > 50) {
            $vals = array_chunk($vals, 50);
            foreach ($vals as $_v) {
                $sqls[] = $query . " (" . implode(',', $cols) . ") VALUES " .
                     implode(',', $_v);
            }
        } else {
            $sqls[] = $query . " (" . implode(',', $cols) . ") VALUES " .
                 implode(',', $vals);
        }
        $query = implode(";", $sqls);
        $errors = $this->multiQuery($query);
        if (count($errors) == 0) {
            return array(
                'status' => 'OK',
                'msg' => 'Inserted successfully',
                'id' => $this->getLastInsert()
            );
        }
        return array(
            'status' => 'NOT_OK',
            'msg' => 'Error (' . $this->con->errno . '): ' . $this->con->error,
            'errors' => $errors
        );
    }

    /**
     * Helps to perform Select All query over the given table
     *
     * @param array $db_obj            
     * @return multitype:unknown |NULL
     */
    public function selectAll($db_obj)
    {
        $result = $this->select($db_obj);
        if (! is_null($result)) {
            $resultArr = array();
            while ($row = $result->fetch_assoc()) {
                $resultArr[] = $row;
            }
            
            return $resultArr;
        }
        return null;
    }

    /**
     * Helps to perform Multi Query execution and return errors found during execution of individual query
     *
     * @param string $query
     *            Multiple SQL queries separated by semicolon (;)
     * @return array of errors
     */
    public function multiQuery($query)
    {
        $return = $this->con->multi_query($query);
        $return_arr = array();
        if ($return) {
            do {
                if ($res = $this->con->store_result()) {
                    while ($row = $res->fetch_row()) {
                        $return_arr[] = $row[0];
                    }
                    $res->free();
                }
            } while ($this->con->more_results() && $this->con->next_result());
        }
        return $return_arr;
    }

    /**
     * Perform query execution and return the result set
     *
     * (non-PHPdoc)
     *
     * @see sqldb::exec_Query()
     */
    public function exec_Query($query)
    {
        if ($result = $this->con->query($query)) {
            return $result;
        }
        return null;
    }

    /**
     * Get the last insertion id generated while insert query execution
     *
     * (non-PHPdoc)
     *
     * @see sqldb::getLastInsert()
     */
    public function getLastInsert()
    {
        return $this->con->insert_id;
    }

    /**
     * Execute the query and return boolean status of it
     *
     * (non-PHPdoc)
     *
     * @see sqldb::exec()
     */
    public function exec($query)
    {
        return ($this->con->query($query) === false) ? false : true;
    }

    /**
     * Convert the result set to an Associate Array
     *
     * (non-PHPdoc)
     *
     * @see sqldb::resultToArray()
     */
    public function resultToArray($result)
    {
        if (! is_null($result)) {
            $resultArr = array();
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $resultArr[] = $row;
            }
            return $resultArr;
        }
        return null;
    }

    /**
     * Get the number of rows available in the given result set
     *
     * @param object $result            
     * @return number
     */
    public function getRowNum($result)
    {
        if (is_null($result)) {
            return 0;
        }
        return $result->num_rows;
    }
}
