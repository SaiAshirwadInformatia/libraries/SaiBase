<?php
require_once '../../../start.php';

$socjs_css = JS_ASSETS . DS . 'socjs' . DS . 'src' . DS . 'sass' . DS . 'soc.css';
if (file_exists($socjs_css)) {
    $socjs_css = file_get_contents($socjs_css);
    preg_match_all("/soc-[a-z-0-9-]+/", $socjs_css, $matches);
    $socjs = array();
    foreach ($matches[0] as $icon) {
        $socjs[$icon] = '<a class="' . $icon . '"></a>';
    }
    if (count($socjs) > 0) {
        $socjs_json = json_encode($socjs, JSON_PRETTY_PRINT);
        file_put_contents(
            SAI_ABS_PATH . "api" . DS . "registry" . DS . "socjs.json", 
            $socjs_json);
        echo 'Generated SocJS';
    } else {
        echo 'Found nothing to generate';
    }
}