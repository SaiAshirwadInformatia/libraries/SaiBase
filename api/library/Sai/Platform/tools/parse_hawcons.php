<?php
require_once '../../../start.php';

$icons_json = json_decode(
    file_get_contents(
        SAI_ABS_PATH . 'assets' . DS . 'images' . DS . 'Hawcons' . DS . 'Font' .
             DS . 'Filled' . DS . 'selection.json'), true);
$hawicons = array();
foreach ($icons_json['icons'] as $icon) {
    $icon_name = $icon['properties']['name'];
    $icon_callable_name = "hawcons-" .
         preg_replace("/icon-[0-9]+-/", "", $icon_name);
    $hawicons[$icon_callable_name] = '<span class="hawcons-' . $icon_name .
         '"></span>';
}
if (count($hawicons) > 0) {
    $hawicons_json = json_encode($hawicons, JSON_PRETTY_PRINT);
    file_put_contents(
        SAI_ABS_PATH . "api" . DS . "registry" . DS . "hawcons.json", 
        $hawicons_json);
    echo 'Generated Hawicons';
} else {
    echo 'Found nothing to generate';
}