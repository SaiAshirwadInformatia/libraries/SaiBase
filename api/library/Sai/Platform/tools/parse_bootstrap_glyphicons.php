<?php
require_once '../../../start.php';

$bootstrap_css = JS_ASSETS . DS . 'bootstrap' . DS . 'dist' . DS . 'css' . DS .
     'bootstrap.css';
if (file_exists($bootstrap_css)) {
    $bootstrap_css = file_get_contents($bootstrap_css);
    preg_match_all("/glyphicon-[a-z-0-9-]+/", $bootstrap_css, $matches);
    $bootstrap_glyphicons = array();
    foreach ($matches[0] as $icon) {
        $bootstrap_glyphicons[$icon] = '<span class="glyphicon ' . $icon .
             '"></span>';
    }
    if (count($bootstrap_glyphicons) > 0) {
        $bootstrap_glyphicons_json = json_encode($bootstrap_glyphicons, 
            JSON_PRETTY_PRINT);
        file_put_contents(
            SAI_ABS_PATH . "api" . DS . "registry" . DS .
                 "bootstrap-glyphicons.json", $bootstrap_glyphicons_json);
        echo 'Generated Bootstrap Glyphicons';
    } else {
        echo 'Found nothing to generate';
    }
}