<?php
require_once '../../../start.php';

$ionicons_css = JS_ASSETS . DS . 'ionicons' . DS . 'css' . DS . 'ionicons.css';
if (file_exists($ionicons_css)) {
    $ionicons_css = file_get_contents($ionicons_css);
    preg_match_all("/ion-[a-z-0-9-]+/", $ionicons_css, $matches);
    $ionicons = array();
    foreach ($matches[0] as $icon) {
        $ionicons[$icon] = '<span class="icon ' . $icon . '"></span>';
    }
    if (count($ionicons) > 0) {
        $ionicons_json = json_encode($ionicons, JSON_PRETTY_PRINT);
        file_put_contents(
            SAI_ABS_PATH . "api" . DS . "registry" . DS . "ionicons.json", 
            $ionicons_json);
        echo 'Generated Ionicons';
    } else {
        echo 'Found nothing to generate';
    }
}