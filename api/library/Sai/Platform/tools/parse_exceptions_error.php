<?php
require_once '../../../start.php';

$exception_class_files = glob(
    SAI_ABS_PATH . "api" . DS . "exceptions" . DS . "*");
$errors_keys = array();
foreach ($exception_class_files as $class_file) {
    $c = substr($class_file, strrpos($class_file, DS) + 1);
    $c = str_replace(".class.php", "", $c);
    $reflection_class = new ReflectionClass($c);
    $arr = $reflection_class->getConstants();
    foreach ($arr as $k => $v) {
        $errors_keys[] = "sai_i18n_excp_" . str_replace("_exception", "", $c) .
             "_" . strtolower($k);
    }
}

$languages = $core->i18n->languages;
foreach ($languages as $locale_code) {
    echo "<hr />Parsing for $locale_code<hr/>";
    $locale_error_file = SAI_ABS_PATH . 'locales' . DS . $locale_code . DS .
         'errors.php';
    echo "<br />Filename: " . $locale_error_file . " Status: " .
         file_exists($locale_error_file) . "<br />";
    $errors_defined = array();
    
    if (file_exists($locale_error_file)) {
        include $locale_error_file;
        $vars = get_defined_vars();
        var_dump($vars);
        die();
        foreach ($vars as $k => $v) {
            if (strpos($k, "sai_i18n_") === 0) {
                $errors_defined[$k] = $v;
            }
        }
    }
    var_dump($errors_defined);
}