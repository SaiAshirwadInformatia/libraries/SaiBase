![WillingTree](images/logo.png)

> WillingTree NGO based project by [Sai Ashirwad Informatia](http://saiashirwad.com)

> WillingTree is a NGO supporting various causes towards welfare of the community

## What WillingTree are for smokers

If a smoker shows his willingness to give up / quit his smoking habit then we buy a sapling and start growing a TREE by his name.
This will be the Caring Tree for that person. Initially all saplings would be maintained in Quadron premises and later transplanted 
into available areas when in a position to afford a gardener to nourish. Later we will provide him the status of his TREE and also 
express our thanks for providing an opportunity for growing a Tree, moreover also seek his feedback around his quitting habit.

## Contributors

| Name            	| Email                     	| Designation                 	|
|-----------------	|---------------------------	|-----------------------------	|
| Rohan Sakhale   	| rs@saiashriwad.com        	| Lead Engineer               	|
| Abhishek Patil  	| abhi@saiashirwad.com      	| Software Developer          	|
| Vikram Sule     	| vikram.s@saiashirwad.com  	| Digital Marketing Executive 	|
| Suyash Jadhav   	| jsuyash@saiashirwad.com   	| UI Developer                	|
| Chirag Babrekar 	| cbabrekar@saiashirwad.com 	| UI Developer                	|



## Credits:

	Development:
		Sai Ashirwad Informatia

	Template:
		Arcana 2.1 by HTML5 UP
		html5up.net | @n33co

	Images:
		fotogrph (http://fotogrph.com)
	
	Other:
		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)
		skelJS (skeljs.org)