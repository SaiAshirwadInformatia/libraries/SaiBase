<?php

/**
 *  Globals
 */
$sai_i18n_year = "Year";
$sai_i18n_years = "Years";
$sai_i18n_start = "Start";
$sai_i18n_end = "End";
$sai_i18n_active = "Active";
$sai_i18n_inactive = "Inactive";
$sai_i18n_deleted = "Deleted";
$sai_i18n_male = "Male";
$sai_i18n_female = "Female";
$sai_i18n_name = "Name";
$sai_i18n_birth_date = "Birth Date";
$sai_i18n_welcome = "Welcome";
$sai_i18n_login = "Login";
$sai_i18n_logout = "Logout";
$sai_i18n_register = "Register";
$sai_i18n_news = "News";
$sai_i18n_advertise = "Advertise";
$sai_i18n_readmore = "Read More";
$sai_i18n_email = "Email";
$sai_i18n_address = "Address";

/**
 * Minutes
 */
$sai_i18n_minute_5 = "5 Mins";
$sai_i18n_minute_10 = "10 Mins";
$sai_i18n_minute_15 = "15 Mins";
$sai_i18n_minute_20 = "20 Mins";
$sai_i18n_minute_30 = "Half an Hour";
$sai_i18n_minute_45 = "45 Mins";
$sai_i18n_minute_60 = "One Hour";
$sai_i18n_minute_75 = "One hour & 15 mins";
$sai_i18n_minute_90 = "One and Half Hours";
$sai_i18n_minute_120 = "Two Hours";

/**
 */
$sai_i18n_marital_single = "Single";
$sai_i18n_marital_married = "Married";
$sai_i18n_marital_divorced = "Divorced";
$sai_i18n_marital_widowed = "Widowed";

/**
 * 404 Not Found
 */
$sai_i18n_404notfound_title = "Page Not Found";

/**
 * Navigation
 */
$sai_i18n_toggle_navigation = "Toggle Navigation";

/*** index ***/
$sai_i18n_index_title = "Introduction";

/*** knowus ***/
$sai_i18n_knowus_title2 = "Contact Us";
$sai_i18n_plch_fullname_txt = "Enter Full Name";
$sai_i18n_plch_email_txt = "Enter email";
$sai_i18n_plch_msg_txt = "Your message here....";
$sai_i18n_contactnow_btn = "Contact Now";

/**
 * Register
 */
$sai_i18n_register_title = "Register";
$sai_i18n_lbl_fname = "First Name*";
$sai_i18n_plch_fname = "(ex. John)";
$sai_i18n_lbl_lname = "Last Name*";
$sai_i18n_plch_lname = "(ex. Smith)";
$sai_i18n_lbl_email = "Email*";
$sai_i18n_plch_email = "(ex. johnsmith@gmail.com)";
$sai_i18n_lbl_password = "Choose Password*";
$sai_i18n_plch_password = "(ex. ******)";
$sai_i18n_lbl_jobtitle = "Job Title";
$sai_i18n_plch_jobtitle = "(ex. Software Engineer)";
$sai_i18n_lbl_companyname = "Company Name";
$sai_i18n_plch_companyname = "(ex. Barclays)";
$sai_i18n_lbl_phone = "Phone";
$sai_i18n_plch_phone = "(ex. +919892098920)";
$sai_i18n_lbl_supportingcauses = "Supporting Causes";
$sai_i18n_registernow_btn = "Register Now";

/*** login ***/
$sai_i18n_login_title = "Login For More...";
$sai_i18n_lbl_email = "Email Address";
$sai_i18n_plch_email = "(ex. johnsmith@gmail.com)";
$sai_i18n_lbl_password = "Password*";
$sai_i18n_plch_password = "(ex. ******)";
$sai_i18n_login_btn = "Login";
$sai_i18n_forgotpassword = "Forgot Password?";
$sai_i18n_joinnow = "Join Now";

/*** forgotpassword **/

$sai_i18n_forgotpassword_title = "Forgot Password? Resetting your password is easy";
$sai_i18n_lbl_forgotpassword = "Please give your email address so we can mail you reset instructions";
$sai_i18n_plch_forgotpassword = "(ex. johnsmith@gmail.com)";
$sai_i18n_forgotpassword_btn = "Submit";

/*** profile ***/
$sai_i18n_profile_title = "User Profile";
$sai_i18n_lbl_profile = "Profile";
$sai_i18n_lbl_activitystream = "Activity Stream";
$sai_i18n_lbl_details = "Details";
$sai_i18n_lbl_joined = "Joined";
$sai_i18n_lbl_bio = "Bio :";
/*$sai_i18n_lbl_phone = "Phone :";*/
$sai_i18n_lbl_website = "Website :";
$sai_i18n_lbl_jobtitle = "Job title :";
$sai_i18n_lbl_companyname = "Comapany Name :";
$sai_i18n_lbl_companyURL = "Company URL: ";
?>