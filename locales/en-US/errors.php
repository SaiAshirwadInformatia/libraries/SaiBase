<?php
/**
 *
 */

/**
 * Authentication Exceptions
 */
$sai_i18n_excp_auth_no_user_account_associated = "Oops, No user account associated with your email address";
$sai_i18n_excp_auth_invalid_email_password = "Oops, that was an invalid email/password combination";
$sai_i18n_excp_auth_awaiting_activation = "Oops, We are awaiting activation of your account";
$sai_i18n_excp_auth_authentication_required = "Oops, we require authenticated user to access this service";
$sai_i18n_excp_auth_invalid_email = "Oops, we require a valid email address";
$sai_i18n_excp_auth_req_missing = "Oops missed something, required fields are Email & Password";
$sai_i18n_excp_auth_account_deleted = "Oops, this account is in delete state but can be recovered back, please contact our Site Administrator";

/**
 * Contact Us Exceptions
 */
$sai_i18n_excp_contactus_req_missing = "Oops, missed out required fields `Name`, `Email`, `Message`";
$sai_i18n_excp_contactus_create_failed = "Oops, something went wrong while performing contact";
$sai_i18n_excp_contactus_get_not_allowed = "Oops, looks like GET HTTP Request method is not allowed over this service";
$sai_i18n_excp_contactus_update_not_allowed = "Oops, looks like UPDATE HTTP Request method is not allowed over this service";
$sai_i18n_excp_contactus_delete_not_allowed = "Oops, looks like DELETE HTTP Request method is not allowed over this service";

/**
 * Files Exceptions
 */
$sai_i18n_excp_files_req_missing = "Oops missed something, required fields is FILE :P";
$sai_i18n_excp_files_unique = "Oops, looks like you have already upload this file";
$sai_i18n_excp_files_create_failed = "Oops, something went wrong while file creation";
$sai_i18n_excp_files_not_found = "Oops, looke like the file you are trying to search is not available";
$sai_i18n_excp_files_delete_failed = "Oops, something went wrong and delete didn't work";
$sai_i18n_excp_files_file_size = "Oops, the file you uploaded was quite big, please upload file less than 1mb";

/**
 * Modules Exceptions
 */
$sai_i18n_excp_invalid_module = "Oops, something seems wrong, you called a wrong URL";
$sai_i18n_excp_module_not_available = "Oops, sorry this module is currently not available";
$sai_i18n_excp_service_not_available = "Oops, sorry this service is currently not available";
$sai_i18n_excp_service_not_allowed = "Oops, sorry this service is not allowed";
$sai_i18n_excp_invalid_method = "Oops, looks like that HTTP Request Method is not recognised by us, please use valid method's like GET, POST, PUT, DELETE";
$sai_i18n_excp_id_numeric_req = "Oops, numeric id only expected untill the new design supports string+id ;)";

/**
 * Pages Exceptions
 */
$sai_i18n_excp_pages_req_missing = "Oops, looks something got missed out page creation requires Title & Content";
$sai_i18n_excp_pages_unique = "Oops, looks like that title for page is already being used, can you please choose some other";
$sai_i18n_excp_pages_create_failed = "Oops, looks something went wrong while page creation";
$sai_i18n_excp_pages_not_found = "Oops, looks like the page you are trying to find is not available";
$sai_i18n_excp_pages_update_failed = "Oops, looks like something went wrong while updating page";
$sai_i18n_excp_pages_delete_failed = "Oops, looks like something went wrong while deleting page";
$sai_i18n_excp_pages_no_pages_created = "Oops, looks like no pages created or in active state";
$sai_i18n_excp_pages_invalid_page = "Oops, looks like the page you are trying to access is not available";

/**
 * Restlizer Exceptions
 */
$sai_i18n_excp_invalid_json = "Oops, looks like the provided input JSON got some error or mistypo's";
$sai_i18n_excp_invalid_xml = "Oops, looks like the provided input XML got some error or mistypo's";
$sai_i18n_excp_invalid_accept = "Oops, looks like the Accept Content-Type is not supported by us, please use some valid format";
$sai_i18n_excp_failed_json_encode = "Oops, something went wrong while forming enocde using JSON form";
$sai_i18n_excp_failed_xml_serialize = "Oops, something went wrong while serialize in XML form";
$sai_i18n_excp_deserialize_string_not_allowed = "Oops, but de-serializing String is not allowed";
$sai_i18n_excp_html_deserialize_not_allowed = "Oops, but de-serialzing HTML is not supported";

/**
 * Rights Exceptions
 */
$sai_i18n_excp_rights_req_missing = "Oops, looks something got missed out Right creation requires Name & Description";
$sai_i18n_excp_rights_unique = "Oops, looks like that Name for Right is already being used, can you please choose some other";
$sai_i18n_excp_rights_create_failed = "Oops, looks like something went wrong while right creation";
$sai_i18n_excp_rights_not_found = "Oops, looks like the Right you are trying to find is not available";
$sai_i18n_excp_rights_delete_failed = "Oops, looks like something went wrong while deleting the right";
$sai_i18n_excp_rights_update_failed = "Oops, looks like something went wrong while updating the right";
$sai_i18n_excp_rights_no_rights_created = "Oops, looks like no rights created or in active state yet :(";

/**
 * Roles Exceptions
 */
$sai_i18n_excp_roles_req_missing = "Oops, looks something got missed out Role creation requires Name & Description";
$sai_i18n_excp_roles_unique = "Oops, looks like that Name for role is already being used, can you please choose some other";
$sai_i18n_excp_roles_create_failed = "Oops, looks like something went wrong while creating role";
$sai_i18n_excp_roles_not_found = "Oops, looks like the role you are trying to find is not available";
$sai_i18n_excp_roles_update_failed = "Oops, looks like something went wrong while updating role";
$sai_i18n_excp_roles_delete_failed = "Oops, looks like somethign went wrong while deleting role";
$sai_i18n_excp_roles_no_roles_created = "Oops, looks like no roles created or in active state";

/**
 * Roles Rights Exceptions
 */
$sai_i18n_excp_rolesrights_req_missing = "Oops, looks something got missed out, link right with role requires each of its id or array of details";
$sai_i18n_excp_rolesrights_create_failed = "Oops, looks something went wrong while link right with role";
$sai_i18n_excp_rolesrights_not_found = "Oops, looks like there are no rights linked with role";
$sai_i18n_excp_rolesrights_delete_failed = "Oops, failed to remove linkage of right with role";
$sai_i18n_excp_rolesrights_invalid_right = "Oops, looks like right details found is not valid";
$sai_i18n_excp_rolesrights_invalid_role = "Oops, looks like role details found is not valid";
$sai_i18n_excp_rolesrights_no_right_linked_with_role = "Oops, looks like no rights linked with this role yet";
$sai_i18n_excp_rolesrights_update_not_allowed = "Oops, looks like updation is not allowed for rights linked with roles";

/**
 * Search Exceptions
 */
$sai_i18n_excp_search_req_missing = "Oops missed something, required field is `Data`";
$sai_i18n_excp_search_not_found = "Oops, looks like the search didn't return any result";
$sai_i18n_excp_search_invalid_data = "Oops, the data you passed is not known to us";
$sai_i18n_excp_search_invalid_condition = "Oops, the condition you gave is invalid use `OR` or `AND` only";

/**
 * Session Exceptions
 */
$sai_i18n_excp_session_invalid_identified = "Oops, Invalid session identified";
$sai_i18n_excp_session_clone_not_allowed = "Damn, cloning of session object not allowed";

/**
 * Users Exceptions
 */
$sai_i18n_excp_users_req_missing = "Oops missed something, required fields are First Name, Last Name, Email, Password";
$sai_i18n_excp_users_unique = "Looks like we already have an user with that email, please choose some other email address";
$sai_i18n_excp_users_create_failed = "Oops, something went wrong while creating user";
$sai_i18n_excp_users_not_found = "Oops, we were not able to find the required user";
$sai_i18n_excp_users_update_failed = "Oops, something went wrong while updating user";
$sai_i18n_excp_users_delete_failed = "Oops, something went wrong while deleting the user";
$sai_i18n_excp_users_invalid_email_address = "Oops, wrong email address, can you please provide valid email address so we can be in touch with you";
$sai_i18n_excp_users_invalid_fname = "Oops, bad First Name, can you please provide your valid first name so we understand you better";
$sai_i18n_excp_users_invalid_lname = "Oops, bad Last Name, can you lease provide your valid last name so we understand you better";
$sai_i18n_excp_users_invalid_password = "Oops, bad password, can you please choose a strong password which is min. 8 chars and contains atleast one small letter, capital letter and a number";
$sai_i18n_excp_users_invalid_job_title = "Oops, bad job title , can you please provide a valid job title inorder to better understand your work";
$sai_i18n_excp_users_verify_email_send_failed = "Oops, something went wrong on our side. Your account was created but we were not able to send verification email";
$sai_i18n_excp_users_change_password_req = "Oops, current password is required to perform password change";
$sai_i18n_excp_users_current_password_not_valid = "Oops, looks like current password you entered doesn't match actually";
$sai_i18n_excp_users_new_confirm_password_req = "Oops, looks like you missed passing new password and its confirmation password";
$sai_i18n_excp_users_new_confirm_password_not_valid = "Oops, looks like new password and its confirm password doesn't match";
$sai_i18n_excp_users_change_display_picture_not_allowed = "Oops, looks like display picture service doesn't accepts GET request";
$sai_i18n_excp_users_change_password_not_allowed = "Oops, looks like change password service only accepts POST request";
$sai_i18n_excp_users_forgot_password_not_allowed = "Oops, looks like forgot password service only accepts POST request";
$sai_i18n_excp_users_forgot_password_email_req = "Oops, we didn't get your email address, can you please submit valid email address";
$sai_i18n_excp_users_forgot_invalid_email_address = "Oops, we need your valid email address to help you reset your password";
$sai_i18n_excp_users_forgot_email_not_registered = "Oops, we didn't find any account associated with this email address, can you please try registering instead";
$sai_i18n_excp_users_forgot_authenticated = "Oops, looks like forgot password service is not allowed for already authenticated user";
$sai_i18n_excp_users_forgot_send_failed = "Oops, something went wrong while sending forgot password email, can you please try again in a while";
$sai_i18n_excp_users_forgot_invalid_link = "Oops, looks like the link used is invalid. Please enter your email below to receive new link";
$sai_i18n_excp_users_forgot_req_not_allowed = "Oops, looks like forgot password reset is only allowed for POST request";
$sai_i18n_excp_users_recover_password_missing = "Oops, but we require you to give new password";
$sai_i18n_excp_users_recover_password_confirm_missing = "Oops, but we require you to re-enter your new password";
$sai_i18n_excp_users_recover_token_missing = "Oops, but we require you to provide with the password reset token";
$sai_i18n_excp_users_recover_user_missing = "Oops, but we require you to provide with user id or email";
$sai_i18n_excp_users_recover_token_invalid = "Oops, token details passed are not valid";
$sai_i18n_excp_users_recover_user_invalid = "Oops, user details passed are not valid";
$sai_i18n_excp_users_recover_password_missmatch = "Oops, your new password don't match with re-entered password";
$sai_i18n_excp_users_token_not_found = "Oops, we require valid token inorder to retrieve user information";
$sai_i18n_excp_users_access_denied = "Oops, looks like your access is denied for this service";

/**
 * Users Rights Exceptions
 */
$sai_i18n_excp_users_rights_req_missing = "Oops, missed out something, to assign right to user both are required ;)";
$sai_i18n_excp_users_rights_unique = "Oops, looks like user is already given this right :D";
$sai_i18n_excp_users_rights_create_failed = "Oops, looks like something went wrong while assiging right to user";
$sai_i18n_excp_users_rights_not_found = "Oops, looks like the user is not given with any right that you are looking for";
$sai_i18n_excp_users_rights_update_not_allowed = "Oops, looks like update is not allowed over users rights module";
$sai_i18n_excp_users_rights_load_rights_array_req = "Oops, looks like inorder to load rights array is required for which all to be loaded";
$sai_i18n_excp_users_rights_my_allowed_with_get = "Oops, looks like invalid method invoked over `MY` call, only GET Request is allowed";
$sai_i18n_excp_users_rights_no_rights_given = "Oops, looks like you don't have any rights yet";

/**
 * Users Roles Exceptions
 */
$sai_i18n_excp_users_roles_invalid_user = "Oops, looks like the provided user details is invalid for assigning role";
$sai_i18n_excp_users_roles_invalid_role = "Oops, looks like no such role is avaiable to assign to the user";
$sai_i18n_excp_users_roles_unique = "Oops, looks like the role you are trying to assign is already assigned";

/**
 * Menus Exceptions
 */
$sai_i18n_excp_menus_req_missing = "Oops, looks like required values name, label, label_key, url";
?>