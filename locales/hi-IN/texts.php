<?php
/**
 * Menu Labels
 */
$sai_i18n_mn_home = "Home";
$sai_i18n_mn_causes = "Causes";
$sai_i18n_mn_trees = "Trees";
$sai_i18n_mn_stories = "Stories";
$sai_i18n_mn_knowus = "Know Us";
$sai_i18n_mn_ourassociates = "Our Associates";
$sai_i18n_mn_register = "Register";
$sai_i18n_mn_login = "Login";
$sai_i18n_mn_myprofile = "My Profile";
$sai_i18n_mn_mylocations = "My Locations";
$sai_i18n_mn_mystories = "My Stories";
$sai_i18n_mn_mytrees = "My Trees";
$sai_i18n_mn_logout = "Logout";

/*** index ***/
$sai_i18n_index_title = "परिचय";

/*** causes ***/
$sai_i18n_causes_title = "कारण";
$sai_i18n_causes_support_link = 'Why don\'t you <a href="register?cause=%1">समर्थन करो</a>';

/*** trees ***/
$sai_i18n_trees_title = "पेड़";

/*** stories ***/
$sai_i18n_stories_title = "कहानियां";

/*** knowus ***/
$sai_i18n_knowus_title1 = "What We Are For Smokers";
$sai_i18n_knowus_title2 = "हमसे संपर्क करें";
$sai_i18n_plch_fullname_txt = "पूरा नाम दर्ज करें";
$sai_i18n_plch_email_txt = "ईमेल दर्ज करें";
$sai_i18n_plch_msg_txt = "यहाँ आपका संदेश ....";
$sai_i18n_contactnow_btn = "संपर्क करें";

/*** ourassociates ***/
$sai_i18n_ourassociates_title = "हमारे सहयोगी";

/*** register ***/
$sai_i18n_register_title = "विल्लिंगट्री परिवार में शामिल हों";
$sai_i18n_lbl_fname = "नाम*";
$sai_i18n_plch_fname = "(उदा. जॉन)";
$sai_i18n_lbl_lname = "अंतिम नाम*";
$sai_i18n_plch_lname = "(उदा. स्मिथ)";
$sai_i18n_lbl_email = "ईमेल*";
$sai_i18n_plch_email = "(उदा. Johnsmith@gmail.com)";
$sai_i18n_lbl_password = "पासवर्ड चुनें*";
$sai_i18n_plch_password = "(उदा. ******)";
$sai_i18n_lbl_jobtitle = "नौकरी शीर्षक";
$sai_i18n_plch_jobtitle = "(उदा. सॉफ्टवेअर अभियंता)";
$sai_i18n_lbl_companyname = "कंपनी का नाम";
$sai_i18n_plch_companyname = "(उदा. बार्कलेज)";
$sai_i18n_lbl_phone = "फोन";
$sai_i18n_plch_phone = "(उदा. +919892098920)";
$sai_i18n_lbl_supportingcauses = "Supporting Causes";
$sai_i18n_registernow_btn = "रजिस्टर करें";

/*** login ***/
$sai_i18n_login_title = "अधिक जानने के लिए लॉगिन ...";
$sai_i18n_lbl_email = "ईमेल";
$sai_i18n_plch_email = "(उदा. johnsmith@gmail.com)";
$sai_i18n_lbl_password = "पासवर्ड*";
$sai_i18n_plch_password = "(उदा. ******)";
$sai_i18n_login_btn = "लॉगिन";
$sai_i18n_forgotpassword = "पासवर्ड भूल गए?";
$sai_i18n_joinnow = "अब सम्मिलित हों";

/*** forgotpassword **/

$sai_i18n_forgotpassword_title = "पासवर्ड भूल गए? यह अपना पासवर्ड रीसेट करने के लिए आसान है";
$sai_i18n_lbl_forgotpassword = "हम आपको निर्देश रीसेट मेल कर सकते हैं तो अपना ईमेल पता दे दीजिए";
$sai_i18n_plch_forgotpassword = "(उदा. johnsmith@gmail.com)";
$sai_i18n_forgotpassword_btn = "जमा करें";

/*** profile ***/
$sai_i18n_profile_title = "प्रयोक्ता परिचायिका";
$sai_i18n_lbl_profile = "परिचायिका";
$sai_i18n_lbl_activitystream = "गतिविधि क्षेत्र";
$sai_i18n_lbl_details = "विवरण";
$sai_i18n_lbl_joined = "Joined";
$sai_i18n_lbl_bio = "चरित्र :";
/*$sai_i18n_lbl_phone = "Phone :";*/
$sai_i18n_lbl_website = "वेबसाइट :";
$sai_i18n_lbl_jobtitle = "कार्य शीर्षक :";
$sai_i18n_lbl_companyname = "कंपनी का नाम :";
$sai_i18n_lbl_companyURL = "कंपनी का यूआरएल: ";

/*** mylocations ***/
$sai_i18n_mylocation_title = "मेरा स्थान";
$sai_i18n_createlocation_btn = "स्थान बनाओ";

/*** mystories ***/
$sai_i18n_mystories_title = "मेरी कहानियों";
$sai_i18n_writeyourstory_btn = "अपनी कहानी लिखें";

/*** mytrees-----nomytrees ***/
$sai_i18n_mytrees_title = "मेरे पेड़";
$sai_i18n_createtree_btn = "मेरा पेड़ बनाएँ";
$sai_i18n_plantmytree_btn = "मेरा पेड़ लगाए";
$sai_i18n_planttree_btn = "पेड़ लगाए";

?>