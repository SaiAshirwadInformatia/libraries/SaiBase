<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Name</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$menus.results item=menu}
		<tr>
			<td><input type="checkbox" name="delete_menus[]" value="{$menu.id}" /></td>
			<td>{$menu.name}</td>
			<td>
				<div class="btn-group pull-right">
					<a class="btn btn-primary btn-xs" href="#"
						title="Created At {$menu.creation_ts}"> <span
						class="glyphicon glyphicon-time"> </span>
					</a>{if !isset($edit) or $edit!='false'} <a
						class="btn btn-default btn-xs" href="edit?id={$menu.id}"> <span
						class="glyphicon glyphicon-pencil"> </span>
					</a>{/if} </a> {if isset($delete) and $delete=='soft_delete'}
					<button class="btn btn-danger btn-xs" data-module="menus"
						data-id="{$menu.id}" title="Delete {$menu.name}"
						data-label="{$menu.name}">{$icons->glyphicon_remove}</button>
					{elseif isset($restore) and $restore=='true'}
					<button class="btn btn-success btn-xs" data-module="menus"
						data-id="{$menu.id}" data-label="{$menu.name}">
						{$icons->glyphicon_repeat}</button>
					{/if}
				</div>
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>