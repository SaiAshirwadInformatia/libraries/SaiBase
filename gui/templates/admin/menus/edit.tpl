<h2>{if $action == 'create'}Create{else}Edit{/if} Menu</h2>
<form action="" method="POST" id="createEditForm" name="menus">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="name">Menu Name</label> <input type="text" name="name"
					id="name" class="form-control" value="{$menus.name}"
					placeholder="(ex: Home)" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="label">Label</label> {if $menus.id > 0}
				<div class="input-group">
					<input type="text" name="label" id="label" class="form-control"
						value="{$menus.label}" placeholder="(ex: Home)" /><span
						class="input-group-addon"
						title="Use Any Translate, to translate this">{$icons->glyphicon_globe}</span>
				</div>
				{else}<input type="text" name="label" id="label"
					class="form-control" value="{$menus.label}"
					placeholder="(ex: Home)" /> {/if} <input type="hidden" name="id"
					id="id" value="{$menus.id}" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<label for="icon">Icon</label> <input type="text" name="icon"
				id="icon" class="form-control" value="{$menus.icon}"
				placeholder="(ex: home)" />
		</div>
		<div class="col-md-6">
			<label for="url">Url</label> <input type="text" name="url" id="url"
				class="form-control" value="{$menus.url}" placeholder="(ex: home)" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<label for="parent_menu">Parent Menu</label> <select
				class="form-control" id="parent_menu" name="parent_menu"> {if
				$menus.parent_menu != '' and $menus.parent_menu > 0}
				<option value="{$menus.parent_menu}" selected></option> {/if}
			</select>
		</div>
		<div class="col-md-6">
			<label for="permissions">Permissions</label> <input type="text"
				name="permissions" id="permissions" class="form-control"
				value="{$menus.permissions}" placeholder="(ex: login_allowed)" />
		</div>
	</div>
	<div class="row" style="margin-top: 10px">
		<div class="col-md-6">
			<div class="form-group">
				<button type="submit" id="createMandal" class="btn btn-default">{if
					$action == 'create'}Create{else}Edit{/if}</button>
			</div>
		</div>
	</div>
</form>
{literal}
<script>
$(document).ready(function(){
    new sai.menus().init();
    new sai.anytranslations().init();
});
</script>
{/literal}
