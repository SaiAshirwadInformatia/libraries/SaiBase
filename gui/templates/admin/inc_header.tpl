<!DOCTYPE HTML>
<!--
----------------------------------------------------------------------------
Developed and Maintained by Sai Ashirwad Informatia
----------------------------------------------------------------------------
----------------------------------------------------------------------------
Authors
----------------------------------------------------------------------------
Rohan Sakhale		rs@saiashirwad.com
Suyash Jadhav		jsuyash@saiashirwad.com
Chirag Babrekar		cbabrekar@saiashriwad.com
Vikram Sule			vikram.s@saiashirwad.com
Chaitali Patil      cpatil@saiashirwad.com
----------------------------------------------------------------------------
-->
<html lang="en">
<head>
<title>{$page->title} - {$core->configs->sitename}</title>
<!-- Page meta by saiashirwad.com -->
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="{$page->meta_description}" />
<meta name="keywords" content="{$page->meta_keywords}" />
<meta name="author" content="{$page->meta_author}" />

<link rel="icon" type="image/png" href="{$images_assets}/icon.png">

<link rel="stylesheet"
	href="{$js_assets}/jquery-ui/themes/south-street/jquery-ui.min.css" />
<link rel="stylesheet"
	href="{$js_assets}/jquery-ui/themes/south-street/theme.css" />
{foreach from=$icons->icons_css_paths item=css_path}
<link rel="stylesheet" href="{$css_path}" />
{/foreach}
<link rel="stylesheet"
	href="{$js_assets}/bootswatch/yeti/bootstrap.min.css" />

<link rel="stylesheet"
	href="{$js_assets}/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet"
	href="{$js_assets}/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" />
<link rel="stylesheet"
	href="{$js_assets}/bootstrap-select/dist/css/bootstrap-select.min.css" />

<link rel="stylesheet" href="{$template}/css/style.css" />

<script src="{$js_assets}/jquery/dist/jquery.min.js"></script>
<script src="{$js_assets}/jquery-ui/jquery-ui.min.js"></script>
<script src="{$js_assets}/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{$js_assets}/bootbox.js/bootbox.js"></script>
<script src="{$js_assets}/moment/min/moment.min.js"></script>
<script
	src="{$js_assets}/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script
	src="{$js_assets}/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script
	src="{$js_assets}/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script
	src="{$js_assets}/bootstrap-filestyle/src/bootstrap-filestyle.js"></script>
<script src="{$js_assets}/functions.js"></script>
<script src="{$js_assets}/saiquery.js"></script>
{literal}
<script>
			sai.externalplugins = {
				"jbimages" : "{/literal}{$js_assets}{literal}/jbimages/plugin.min.js"
			};
			sai.apihref = "{/literal}{$apihref}{literal}";
			sai.basehref = "{/literal}{$basehref}{literal}";
			sai.languages = {/literal}{$core->i18n->languages|json_encode}{literal};
		</script>
{/literal}
</head>
<body>
	<header class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">{$core->configs->sitename} - Admin
					CP</a>
			</div>
			<p class="navbar-text">
				<a href="{$basehref}/index" target="_blank" class="navbar-link">Visit
					Site</a>
			</p>
			{if $page->slug != 'login'}
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Hello, {$current_user.fname} <span
						class="caret"> </span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">My Profile</a></li>
						<li><a href="{$basehref}/exitadmin">Exit Admin</a></li>
						<li class="divider"></li>
						<li><a href="{$basehref}/logout">Log Out</a></li>
					</ul></li>
			</ul>
			{/if}
		</div>
	</header>