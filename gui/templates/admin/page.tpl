{include file='./inc_header.tpl' page=$page}
<div class="container-fluid" id="main-wrapper">
	<div class="row">
		<div class="col-sm-3 col-md-3">{if $page->slug != 'login'} {include
			file='./inc_nav.tpl' title=$title nav=$nav} {/if}</div>
		<div class="col-sm-9 col-md-9">
			{if $core->session->hasMsg()}
			<div
				class="alert {if $core->session->getError()!=''}alert-danger{else}alert-success{/if} alert-dismissible"
				role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true"> &times;</span><span class="sr-only">
						Close</span>
				</button>
				{if $core->session->getError()!=''}
				{$icons->glyphicon_exclamation_sign} {$core->session->getError()}
				{else} {$icons->glyphicon_ok} {$core->session->getSuccess()} {/if}
			</div>
			{/if} {include file=$template_file page=$page}
		</div>
	</div>
</div>
{include file='./inc_footer.tpl'} {$core->session->clearMsgs()}
