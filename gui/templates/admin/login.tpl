<div class="row">
	<div class="col-md-6">
		<img src="{$images_assets}/admin_logo.png" class="img-responsive" />
	</div>
</div>
<div class="row">
	<form action="" method="POST">
		<div class="col-md-6" style="margin-top: 15px">
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-addon">@</div>
					<input type="email" class="form-control" name="email" id="email"
						placeholder="Enter email" value="{$current_user.email}">
				</div>
			</div>
			<div class="form-group">
				<label class="sr-only" for="exampleInputPassword2">Password</label>
				<input autofocus="true" type="password" class="form-control"
					name="password" id="password" placeholder="Password">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-default" name="adminLogin">
					Login</button>
			</div>
		</div>
	</form>
</div>
