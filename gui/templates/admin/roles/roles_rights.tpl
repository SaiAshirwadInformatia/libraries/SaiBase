<h3>{$role.name}</h3>
<hr />
<h4>Currently Assigned Rights</h4>
<form action="" method="POST">
	<div class="well well-lg">
		{if isset($current_rights) and $current_rights|count > 0}
		<div class="checkbox">
			{foreach from=$current_rights item=ct} <label for="{$ct.name}"><input
				type="checkbox" name="{$ct.name}" checked /> {$ct.name}</label>
			{/foreach}
		</div>
		{else}
		<p>No rights assign yet.</p>
		{/if}
	</div>
	<h4>Assign New Rights</h4>
	<div class="well well-lg">
		<div class="checkbox">
			{foreach from=$newrights item=nt} <label for="{$nt.name}"><input
				type="checkbox" name="{$nt.name}" /> {$nt.name}</label> {/foreach}
		</div>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-default">Save</button>
	</div>
</form>