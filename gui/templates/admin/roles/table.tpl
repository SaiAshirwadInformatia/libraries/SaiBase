<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Name</th>
			<th>Description</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$roles.results item=role}
		<tr>
			<td><input type="checkbox" name="delete_roles[]" value="{$role.id}" />
			</td>
			<td>{$role.name}</td>
			<td>{$role.description}</td>
			<td>
				<div class="btn-group pull-right">
					<a class="btn btn-default btn-xs" href="roles_rights?id={$role.id}"
						title="Assign Role">{$icons->glyphicon_pushpin}</a> <a
						class="btn btn-primary btn-xs" href="#"
						title="Created At {$role.creation_ts}"><span
						class="glyphicon glyphicon-time"> </span></a> {if !isset($edit) or
					$edit!='false'}<a class="btn btn-default btn-xs"
						href="edit?id={$role.id}">{$icons->glyphicon_pencil}</a>{/if} {if
					isset($delete) and $delete=='soft_delete'}
					<button class="btn btn-danger btn-xs" data-module="roles"
						data-id="{$role.id}" title="Delete {$role.name}"
						data-label="{$role.name}">{$icons->glyphicon_remove}</button>
					{elseif isset($restore) and $restore=='true'}
					<button class="btn btn-success btn-xs" data-module="roles"
						data-id="{$role.id}" data-label="{$role.name}">
						{$icons->glyphicon_repeat}</button>
					{/if}
				</div>
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>