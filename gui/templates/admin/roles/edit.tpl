<h2>{if $action == 'create'}Create{else}Edit{/if} Role</h2>
<form action="" method="POST">
	<div class="form-group">
		<label for="name">Name</label> <input type="text" name="name"
			id="name" class="form-control" value="{$roles.name}" />
	</div>

	<div class="form-group">
		<label for="description">Description</label>
		<textarea name="description" id="description" class="form-control">{$roles.description}</textarea>
		<input type="hidden" name="user_id" id="user_id"
			value="{$current_user.id}" />
	</div>
	{if $roles.id != ''}
	<div class="form-group">
		<label for="createdBy">Created By</label>
		<div>
			<p class="form-control-static">
				<img src="{$roles.users.display_picture}" style="width: 32px" />
				{$roles.users.fname} {$roles.users.lname}
			</p>
		</div>
	</div>
	{/if}
	<div class="form-group">
		<div class="col-md-1 col-xs-2">
			<button type="submit" class="btn btn-default">{if $action ==
				'create'}Create{else}Save{/if}</button>
		</div>
		{if $roles.id != ''}
		<p class="col-md-3 col-xs-5 form-control-static">
			Created: <br class="visible-xs"> {$roles.creation_ts}
		</p>
		<p class="col-md-4 col-xs-5 form-control-static">
			Last Modified: <br class="visible-xs"> {$roles.lastmodified_ts}
		</p>
		{/if}
	</div>
</form>
<script src="{$js_assets}/tinymce/tinymce.min.js"></script>
{literal}
<script>
	$(document).ready(function() {
		new sai.tinymce().init("textarea", sai.externalplugins);
	}); 
</script>
{/literal}
