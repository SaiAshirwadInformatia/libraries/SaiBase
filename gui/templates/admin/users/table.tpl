{get_const name='r_gender'}
<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Name</th>
			<th>Phone No.</th>
			<th>Email</th>
			<th></th>
		</tr>
	
	
	<tbody>
		{foreach from=$users.results item=user}
		<tr>
			<td><input type="checkbox" name="delete_users[]" value="{$user.id}" />
			</td>
			<td>{$user.fname} {$user.lname}</td>
			<td>{$user.phone1}{if !is_null($user['phone2']) and
				!empty($user['phone2'])} / {$user.phone2}{/if}</td>
			<td>{$user.email}</td>
			<td>
				<div class="btn-group pull-right">
					{if $user.gender!=""} <a href="#" class="btn btn-default btn-xs"> <img
						class="img-responsive" style="width: 18px"
						src="{$images_assets}/{$user.gender}_24.png"
						title="{get_lang s=$r_gender[$user.gender]}" />
					</a> {/if} {if $user.is_public == 1 } <a
						class="btn btn-default btn-xs text-success" href="#"
						title="Public Profile At"> {$icons->glyphicon_globe} </a> {else} <a
						class="btn btn-default btn-xs text-danger" href="#"
						title="Non Public Profile"> {$icons->glyphicon_eye_close} </a>
					{/if} <a class="btn btn-primary btn-xs" href="#"
						title="Created At {$user.creation_ts}"><span
						class="glyphicon glyphicon-time"> </span></a>{if !isset($edit) or
					$edit!='false'} <a class="btn btn-default btn-xs"
						href="edit?id={$user.id}">{$icons->glyphicon_pencil}</a>{/if} {if
					isset($delete) and $delete=='soft_delete'}
					<button class="btn btn-danger btn-xs" data-module="users"
						data-id="{$user.id}" title="Delete {$user.fname} {$user.lname}"
						data-label="{$user.fname} {$user.lname}">
						{$icons->glyphicon_remove}</button>
					{elseif isset($restore) and $restore=='true'}
					<button class="btn btn-success btn-xs" data-module="users"
						data-id="{$user.id}" data-label="{$user.fname} {$user.lname}">
						{$icons->glyphicon_repeat}</button>
					{/if}
				</div>
			</td>
		</tr>
		{/foreach}
	</tbody>

	</thead>
</table>