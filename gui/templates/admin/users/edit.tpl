{get_const name='object_states,bloodgroup,marital_status'}
<h2>{if $action == 'create'}Create{else}Edit{/if} User</h2>

<form action="" method="POST" id="createEditForm" name="users">
	{if $users.id != '' and $users.id > 0 and $users.is_active == -1}
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
				<p>
					{$icons->glyphicon_warning} Looks like this user is currently
					deleted, <a id="retrieveBack" href="#">retrieve back</a>?
				</p>
			</div>
		</div>
	</div>
	{/if}
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label" for="fname">First Name</label> {if
				$users.id > 0}
				<div class="input-group">
					<input type="text" name="fname" id="fname" class="form-control"
						value="{$users.fname}" /><span class="input-group-addon"
						title="Use Any Translate, to translate this">{$icons->glyphicon_globe}</span>
				</div>
				{else} <input type="text" name="fname" id="fname"
					class="form-control" value="{$users.fname}" /> {/if} <input
					type="hidden" id="id" name="id" value="{$users.id}" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label" for="fname">Middle Name</label> {if
				$users.id > 0}
				<div class="input-group">
					<input type="text" name="mname" id="mname" class="form-control"
						value="{$users.mname}" /><span class="input-group-addon"
						title="Use Any Translate, to translate this">{$icons->glyphicon_globe}</span>
				</div>
				{else} <input type="text" name="mname" id="mname"
					class="form-control" value="{$users.mname}" /> {/if}
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label" for="lname">Last Name</label> {if
				$users.id > 0}
				<div class="input-group">
					<input type="text" name="lname" id="lname" class="form-control"
						value="{$users.lname}" /><span class="input-group-addon"
						title="Use Any Translate, to translate this">{$icons->glyphicon_globe}</span>
				</div>
				{else} <input type="text" name="lname" id="lname"
					class="form-control" value="{$users.lname}" /> {/if}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label" for="email">Email Address</label> <input
					type="email" name="email" id="email" class="form-control"
					value="{$users.email}" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label" for="username">User Name</label> <input
					type="text" name="username" id="username" class="form-control"
					value="{$users.username}" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label" for="hotino">Hoti No.</label> <input
					type="text" name="hoti_no" id="hoti_no" class="form-control"
					value="{$users.hoti_no}" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Gender</label>
				<div class="checkbox">
					<label class="control-label"> <input type="radio" name="gender"
						id="gender" value="m" {if $users.gender== 'm'} checked{/if} />
						Male
					</label> <label class="control-label"> <input type="radio"
						name="gender" id="gender" value="f" {if $users.gender== 'f'} checked{/if} />
						Female
					</label>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Bloodgroup</label> <select
					class="form-control" name="blood_group"> {foreach from=$bloodgroup
					item=bg}
					<option value="{$bg}" {if $users.blood_group==$bg}selected{/if}>{$bg}</option>
					{/foreach}
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Phone1</label> <input
					class="form-control" type="text" name="phone1" id="phone1"
					value="{$users.phone1}" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Phone2</label> <input
					class="form-control" type="text" name="phone2" id="phone2"
					value="{$users.phone2}" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Website</label> <input
					class="form-control" type="text" name="website" id="website"
					value="{$users.website}" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">DOB</label>
				<div class='input-group date'>
					<input class="form-control" type="text" name="dob" id="dob"
						value="{$users.dob}" data-date-format="YYYY-MM-DD" /> <span
						class="input-group-addon"> {$icons->glyphicon_calendar} </span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Education</label> <input
					class="form-control" type="text" name="education" id="education"
					value="{$users.education}" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Marital Status</label> <select
					name="marital_status" id="marital_status" class="form-control">
					<option value=""></option> {foreach from=$marital_status
					item=lbl_ms key=ms}
					<option value="{$ms}">{get_lang s=$lbl_ms}</option> {/foreach}
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Anniversary</label>
				<div class='input-group date'>
					<input class="form-control" type="text" name="anniversary_date"
						id="anniversary_date" value="{$users.anniversary_date}"
						data-date-format="YYYY-MM-DD" /> <span class="input-group-addon">
						{$icons->glyphicon_calendar} </span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		{if $users.id > 0}
		<div class="col-md-4">
			<label class="control-label" for="is_active">User State</label> <select
				class="form-control" name="is_active" id="is_active"> {foreach
				from=$object_states key=lbl item=obs}
				<option value="{$obs}" {if $obs==$users.is_active}selected{/if}>{get_lang
					s=$lbl}</option> {/foreach}
			</select>
		</div>
		{/if}
		<div class="col-md-4">
			<div class="form-group">
				<label class="control-label">Is Profile Public?</label>
				<div class="radio">
					<label class="control-label" for="is_public"> <input type="radio"
						id="is_public" name="is_public" value="1" {if $users.is_public== 1} checked{/if} />
						Yes
					</label> <label class="control-label" for="is_public"> <input
						type="radio" id="is_public" name="is_public" value="0"
						{if $users.is_public== 0} checked{/if} /> No
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-1 col-xs-2">
			<button type="submit" id="registerNow" class="btn btn-default">Save</button>
		</div>
		{if $users.id != ''}
		<div class="col-md-4 col-xs-5">
			<div class="form-inline">
				<p class="form-control-static">
					Created At- <br class="visible-xs"> {$users.creation_ts}
				</p>
			</div>
		</div>
		<div class="col-md-4 col-xs-5">
			<div class="form-inline">
				<p class="form-control-static">
					Last Modified At- <br class="visible-xs"> {$users.lastmodified_ts}
				</p>
			</div>
		</div>
		{/if}
	</div>
</form>
{literal}
<script>
	$(document).ready(function() {
		new sai.revertBackObject().init();
		$(".date").datetimepicker({pickTime:false});
		new sai.anytranslations().init();
	})
</script>
{/literal}
