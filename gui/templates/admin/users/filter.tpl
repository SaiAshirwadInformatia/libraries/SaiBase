<form action="" method="GET">
	<div class="form-group">
		<label for="fname">User First Name</label> <input type="text"
			name="data[fname]" id="fname" class="form-control"
			value="{if isset($smarty.get.fname)}{$smarty.get.fname}{/if}" />
	</div>
	<div class="form-group">
		<label for="fname">User Last Name</label> <input type="text"
			name="data[lname]" id="lname" class="form-control"
			value="{if isset($smarty.get.lname)}{$smarty.get.lname}{/if}" />
	</div>
	<div class="form-group">
		<label for="email">User Email</label> <input type="text"
			name="data[email]" id="email" class="form-control"
			value="{if isset($smarty.get.email)}{$smarty.get.email}{/if}" />
	</div>
	<div class="form-group">
		<label for="gender">User Gender</label>
		<div class="radio">
			<label for="gender"> <input type="radio" name="data[gender]"
				value="m" {if isset($smarty.get.gender) and $smarty.get.gender== 'm'} checked{/if} />
				Male
			</label> <label for="gender"> <input type="radio" name="data[gender]"
				value="f" {if isset($smarty.get.gender) and $smarty.get.gender== 'f'} checked{/if} />
				Female
			</label>
		</div>
	</div>
	<div class="form-group">
		<label for="name">Phone No.</label> <input type="text"
			name="data[phone1]" id="phone1" class="form-control"
			value="{if isset($smarty.get.data.phone1)}{$smarty.get.data.phone1}{/if}" />
	</div>
	<button type="submit" id="searchUsers" class="btn btn-default">Filter
		Results</button>
</form>