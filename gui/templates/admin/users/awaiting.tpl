<div class="row clearfix">
	<div class="col-md-9 column">
		<h2>Awaiting Users</h2>
		<div class="table-responsive">
			{if isset($users) and isset($users['results']) and
			count($users.results) > 0} {include file='./table.tpl' users=$users}
			{else}
			<div class="alert alert-danger" role="alert">
				{$icons->glyphicon_exclamation_sign} Looks like no users are deleted
				yet</div>
			{/if}
		</div>
	</div>
	<div class="col-md-3">
		<h3>Filter</h3>
		<hr>
		{include file='./filter.tpl'}
	</div>
</div>
