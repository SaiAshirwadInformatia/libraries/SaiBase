<div class="row clearfix">
	<div class="col-md-9 column">
		<h2>Deleted Photos</h2>
		<div class="table-responsive">
			{if isset($albums_photos) and isset($albums_photos['results']) and
			count($albums_photos.results) > 0} {include file="./tablephotos.tpl"
			restore='true' edit="false"} {else}
			<div class="alert alert-danger" role="alert">
				{$icons->glyphicon_exclamation_sign} Looks like no photos
				are deleted yet</div>
			{/if}
		</div>
	</div>
	<div class="col-md-3">
		{if isset($smarty.get.data) or (isset($albums_photos) and
		isset($albums_photos['results']) and count($albums_photos.results) >
		0)}
		<h2>Filter</h2>
		{include file="./filterphotos.tpl"} {/if}
	</div>
</div>
