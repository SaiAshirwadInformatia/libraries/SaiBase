<div class="row clearfix">
	<div class="col-md-8">
		{if isset($albums) and $albums|count > 0} {if isset($albums.photos)
		and $albums.photos|count > 0}
		<h2>Photos of {$albums.name}</h2>
		<table class="table table-hover">
			<tr>
				<th></th>
				<th>Name</th>
				<th>Description</th>
				<th>Views</th>
				<th></th>
			</tr>
			{foreach from=$albums.photos item=a}
			<tr>
				<td><img style="width: 32px; height: 32px" src="{$a.files.path}"
					class="img-rounded" /></td>
				<td>{$a.files.name}</td>
				<td>{$a.description}</td>
				<td class="text-center">
								<span style="background: #85cffa; color: #000" class="badge">{$a.views}</span>
				</td><td>
					<div class="btn-group pull-right">
						<a class="btn btn-primary btn-xs" href="#"
							title="Created at {$a.creation_ts}"> <span
							class="glyphicon glyphicon-time"> </span>
						</a> {if isset($a.is_active)} {if $a.is_active == 1}
						<button class="btn btn-danger btn-xs" data-module="albums_photos"
							data-id="{$a.id}"
							title="Remove {$a.files.name} from {$albums.name}"
							data-label="{$a.files.name}">{$icons->glyphicon_remove}</button>
						{else}
						<button class="btn btn-success btn-xs" data-module="albums_photos"
							data-id="{$a.id}" data-label="{$a.files.name}">{$icons->glyphicon_repeat}</button>
						{/if} {/if}
					</div>
				</td>
			</tr>
			{/foreach}
		</table>
		{else}
		<div class="alert alert-danger" role="alert">
			<p>Oops, looks like no photo is added yet</p>
			<a href="{$basehref}/admincp/albums/createphoto"
				class="btn btn-success btn-xs">Add Photo Now</a>
		</div>
		{/if} {else}
		<div class="alert alert-danger" role="alert">
			<p>Oops, looks like that is an invalid Album id</p>
		</div>
		{/if}
	</div>
</div>


{literal}
<script>
$(document).ready(function(){
    new sai.deleteOrRestoreItems().init();
});
</script>
{/literal}
