<form action="" method="GET">
	<div class="form-group">
		<label for="name">Album Name</label> <input type="text"
			name="data[name]" id="name" class="form-control" {if
			isset($smarty.get.data.name)} value="{$smarty.get.data.name}" {/if} />
	</div>
	<button type="submit" class="btn btn-default">Filter</button>
</form>
