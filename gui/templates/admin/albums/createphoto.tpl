<h2>Create Photo</h2>
<form action="" method="POST" enctype="multipart/form-data">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="album_id">Select Album</label> <select
					class="form-control" name="album_id">
					<option value=""></option> {foreach from=$albums item=album}
					<option value="{$album.id}"
						{if $album.name==$album.name}selected{/if}>{$album.name}</option>
					{/foreach}
				</select>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="files">Select Files</label> <input type="file"
					name="files" id="files" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				{if $photos.id > 0}
				<div class="input-group">
					<textarea name="description" id="description">{$photos.description}</textarea>
					<span class="input-group-addon"><span
						class="glyphicon glyphicon-globe"
						title="Use Any Translate, to translate this"></span></span>
				</div>
				{else}
				<textarea id="description" name="description" class="form-control">{$photos.description}</textarea>
				{/if}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<button type="submit" id="createMandal" class="btn btn-default">{if
				$action == 'create'}Create{else}Save{/if}</button>
		</div>
	</div>
</form>
<script src="{$js_assets}/tinymce/tinymce.min.js"></script>
{literal}
<script>
	$(document).ready(function() {
		new sai.tinymce().init("#description", sai.externalplugins);
		new sai.anytranslations().init();
	}); 
</script>
{/literal}
