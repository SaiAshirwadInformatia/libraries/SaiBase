<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th>Name</th>
			<th>Type</th>
			<th>Size</th>
			<th>Views</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$albums_photos.results item=photo}
		<tr>
			<td><input type="checkbox" name="delete_albums_photos[]" value="{$photo.id}" />
			</td>
			<td><img style="width: 32px; height: 32px" src="{$photo.files.path}"
				class="img-rounded" /></td>
			<td>{$photo.files.name}</td>
			<td>{$photo.files.type}</td>
			<td>{($photo.files.size /1024 /1024)|string_format:"%.2f"} mb</td>
			<td>{$photo.views}</td>
			<td>
				<div class="btn-group pull-right">
					<a class="btn btn-primary btn-xs" href="#"
						title="Created At {$photo.creation_ts}"> {$icons->glyphicon_time}
					</a> {if isset($delete) and $delete=='soft_delete'}
					<button class="btn btn-danger btn-xs" data-module="albums_photos"
						data-id="{$photo.id}" title="Delete {$photo.files.name}"
						data-label="{$photo.files.name}">{$icons->glyphicon_remove}</button>
					{elseif isset($restore) and $restore=='true'}
					<button class="btn btn-success btn-xs" data-module="albums_photos"
						data-id="{$photo.id}" data-label="{$photo.files.name}">
						{$icons->glyphicon_repeat}</button>
					{/if}
				</div>
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>