<div class="row clearfix">
	<div class="col-md-9 column">
		<h2>All Photos</h2>
		{if isset($albums_photos) and isset($albums_photos['results']) and
		count($albums_photos.results) > 0}  {include file="./tablephotos.tpl"
		delete="soft_delete" restore="false"} {get_bootstrap_pagination
		baseUrl="?$query_string" currentPage=$albums_photos.page
		maxPages=$albums_photos.totalPages} 
		
{literal}
<script>
$(document).ready(function(){
    new sai.deleteOrRestoreItems().init();
});
</script>
{/literal}
		{else}
		<div class="alert alert-danger" role="alert">
			<p>{$icons->glyphicon_exclamation_sign} No records found</p>
		</div>
		{/if}
	</div>
	{if isset($smarty.get.data) or (isset($albums_photos) and
	isset($albums_photos['results']) and count($albums_photos.results) >
	0)}
	<div class="col-md-3">
		<h2>Filter</h2>
		{include file="./filterphotos.tpl"}
	</div>
	{/if}
</div>