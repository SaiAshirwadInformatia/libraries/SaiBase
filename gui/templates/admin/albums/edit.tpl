<h2>{if $action == 'create'}Create{else}Edit{/if} Album</h2>

<form action="" method="POST" enctype="multipart/form-data"
	id="createEditForm" name="albums">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="name">Album Name</label>{if $albums.id > 0}
				<div class="input-group">
					<input type="text" name="name" id="name" class="form-control"
						value="{$albums.name}" /><span class="input-group-addon"><span
						class="glyphicon glyphicon-globe"></span></span>
				</div>
				{else} <input type="text" name="name" id="name" class="form-control"
					value="{$albums.name}" /> {/if} <input type="hidden" id="id"
					name="id" value="{$albums.id}" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="file">Select Files</label> <input type="file"
					name="file" id="file" class="form-control" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="description">Description</label> {if $albums.id > 0}
				<div class="input-group">
					<textarea name="description" id="description">{$albums.description}</textarea>
					<span class="input-group-addon"><span
						class="glyphicon glyphicon-globe"
						title="Use Any Translate, to translate this"></span></span>
				</div>
				{else}
				<textarea name="description" id="description">{$albums.description}</textarea>
				{/if}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<button type="submit" id="createMandal" class="btn btn-default">{if
				$action == 'create'}Create{else}Save{/if}</button>
		</div>
	</div>
</form>
<script src="{$js_assets}/tinymce/tinymce.min.js"></script>
{literal}
<script>
	$(document).ready(function() {
		new sai.tinymce().init("#description", sai.externalplugins);
		$("#eventdatepicker").datetimepicker();
		new sai.anytranslations().init();
	}); 
</script>
{/literal}
