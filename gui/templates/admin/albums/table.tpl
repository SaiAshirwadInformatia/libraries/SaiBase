<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Name</th>
			<th>Description</th>
			<th title="Count of Photos in Album">{$icons->glyphicon_picture}</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$albums.results item=album}
		<tr>
			<td><input type="checkbox" name="delete_albums[]" value="{$album.id}" />
			</td>
			<td>{$album.name}</td>
			<td>{$album.description}</td>
			<td><a href="{$basehref}/admincp/albums/photos/{$album.name}"><span
					class="badge">{$album.photos|count}</span></a></td>
			<td>
				<div class="btn-group pull-right">
					<a class="btn btn-primary btn-xs" href="#"
						title="Created At {$album.creation_ts}"> {$icons->glyphicon_time}
					</a>{if !isset($edit) or $edit!='false'} <a
						class="btn btn-default btn-xs" href="edit?id={$album.id}">
						{$icons->glyphicon_pencil} </a>{/if} {if isset($delete) and
					$delete=='soft_delete'}
					<button class="btn btn-danger btn-xs" data-module="albums"
						data-id="{$album.id}" title="Delete {$album.name}"
						data-label="{$album.name}">{$icons->glyphicon_remove}</button>
					{elseif isset($restore) and $restore=='true'}
					<button class="btn btn-success btn-xs" data-module="albums"
						data-id="{$album.id}" data-label="{$album.name}">
						{$icons->glyphicon_repeat}</button>
					{/if}
				</div>
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>