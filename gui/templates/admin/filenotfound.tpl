<h2>Oops Not Found</h2>
<hr />
{if isset($module_req)} {if isset($file_req)}
<p>
	Please create file named
	<code>{$file_req}</code>
	in module
	<code>{$module_req}</code>
</p>
{else}
<p>
	Please create module named
	<code>{$module_req}</code>
</p>
{/if} {/if}
