<form action="" method="GET">
	<div class="form-group">
		<label for="title">Title</label> <input type="text" name="data[title]"
			id="title" class="form-control" {if
			isset($smarty.get.data.title)} value="{$smarty.get.data.title}" {/if}/>
	</div>
	<button type="submit" class="btn btn-default">Filter</button>
</form>
