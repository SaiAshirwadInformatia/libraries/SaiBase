<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Title</th>
			<th>Slug</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$pages.results item=page}
		<tr>
			<td><input type="checkbox" name="delete_pages[]" value="{$page.id}" /></td>
			<td>{$page.title}</td>
			<td>{$page.slug}</td>
			<td>
				<div class="btn-group pull-right">
					<a class="btn btn-info btn-xs" href="#"><span
						class="glyphicon {if $page.has_sidebar == 0}glyphicon-exclamation-sign"
						title="Has No Sidebar" {elseif $page.has_sidebar==
						1}glyphicon-indent-left" title="Has Left Sidebar"
						{else}glyphicon-indent-right" title="Has Right Sidebar"{/if}></span>
					</a> <a class="btn btn-primary btn-xs" href="#"
						title="Created At {$page.creation_ts}"> <span
						class="glyphicon glyphicon-time"> </span>
					</a>{if !isset($edit) or $edit!='false'} <a
						class="btn btn-default btn-xs" href="edit?id={$page.id}"> <span
						class="glyphicon glyphicon-pencil"> </span>
					</a>{/if} {if isset($delete) and $delete=='soft_delete'}
					<button class="btn btn-danger btn-xs" data-module="pages"
						data-id="{$page.id}" title="Delete {$page.title}"
						data-label="{$page.title}">{$icons->glyphicon_remove}</button>
					{elseif isset($restore) and $restore=='true'}
					<button class="btn btn-success btn-xs" data-module="pages"
						data-id="{$page.id}" data-label="{$page.title}">
						{$icons->glyphicon_repeat}</button>
					{/if}
				</div>
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>