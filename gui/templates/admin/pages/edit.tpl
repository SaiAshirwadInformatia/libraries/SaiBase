<h2>{if $action == 'create'}Create{else}Edit{/if} Pages</h2>
<form action="" method="POST" id="createEditForm" name="pages">
	<div class="form-group">
		<label for="title">Page Title</label> {if $pages.id > 0}
		<div class="input-group">
			<input type="text" name="title" id="title" class="form-control"
				placeholder="(Ex: Home Page)" value="{$pages.title}" /> <span
				class="input-group-addon"
				title="Use Any Translate, to translate this">{$icons->glyphicon_globe}</span>
		</div>
		{else} <input type="text" name="title" id="title" class="form-control"
			placeholder="(Ex: Home Page)" value="{$pages.title}" /> {/if} <input
			type="hidden" name="id" id="id" value="{$pages.id}" />
	</div>
	<fieldset>
		<legend>Page Meta Information</legend>
		<div class="form-group col-md-4">
			<label for="meta_author">Author</label>{if $pages.id > 0}
			<div class="input-group">
				<input type="text" name="meta[author]" id="meta[author]"
					class="form-control" placeholder="(Ex: Rohan Sakhale)"
					value="{$pages.meta.author}" /> <span class="input-group-addon"><span
					class="glyphicon glyphicon-globe"
					title="Use Any Translate, to translate this"></span></span>
			</div>
			{else}<input type="text" name="meta[author]" id="meta[author]"
				class="form-control" placeholder="(Ex: Rohan Sakhale)"
				value="{$pages.meta.author}" /> {/if}
		</div>
		<div class="form-group col-md-4">
			<label for="meta_keywords">Keywords</label>{if $pages.id > 0}
			<div class="input-group">
				<input type="text" name="meta[keywords]" id="meta[keywords]"
					class="form-control"
					placeholder="(ex: Rajasthan, Ghanerao, Village)"
					value="{$pages.meta.keywords}" /> <span class="input-group-addon"><span
					class="glyphicon glyphicon-globe"
					title="Use Any Translate, to translate this"></span></span>
			</div>
			{else}<input type="text" name="meta[keywords]" id="meta[keywords]"
				class="form-control"
				placeholder="(ex: Rajasthan, Ghanerao, Village)"
				value="{$pages.meta.keywords}" />{/if}
		</div>
		<div class="form-group col-md-4">
			<label for="slug">Slug URL</label><input type="text" name="slug"
				id="slug" class="form-control" placeholder="(ex: home)"
				value="{$pages.slug}" />
		</div>
		<div class="form-group col-md-12">
			<label for="meta_description">Description</label> {if $pages.id > 0}
			<div class="input-group">
				<textarea name="meta[description]" id="meta[description]"
					class="form-control"
					placeholder="(ex: Page Detailed Description, Google Loves this)">{$pages.meta.description}</textarea>
				<span class="input-group-addon"><span
					class="glyphicon glyphicon-globe"
					title="Use Any Translate, to translate this"></span></span>
			</div>
			{else}
			<textarea name="meta[description]" id="meta[description]"
				class="form-control"
				placeholder="(ex: Page Detailed Description, Google Loves this)">{$pages.meta.description}</textarea>
			{/if}
		</div>
	</fieldset>
	<fieldset>
		<legend>Page Options</legend>
		<div class="form-group col-md-2">
			<label for="has_menubar">Has Menubar</label>
			<div class="radio">
				<label for="has_menubar"><input type="radio" name="has_menubar"
					value="1" {if $pages.has_menubar== 1} checked{/if} /> Yes</label> <label
					for="has_menubar"><input type="radio" name="has_menubar" value="0"
					{if $pages.has_menubar== 0} checked{/if} /> No</label>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label for="has_sidebar"> Sidebar </label>
			<div class="radio">
				<label for="has_sidebar_none"><input type="radio" name="has_sidebar"
					id="has_sidebar_none" value="0" {if $pages.has_sidebar== 0} checked{/if} />
					None</label> <label for="has_sidebar_left"><input type="radio"
					name="has_sidebar" id="has_sidebar_left" value="1"
					{if $pages.has_sidebar== 1} checked{/if} /> Left</label> <label
					for="has_sidebar_right"><input type="radio" name="has_sidebar"
					id="has_sidebar_right" value="2" {if $pages.has_sidebar== 2} checked{/if} />
					Right</label>
			</div>
		</div>
		<div class="form-group col-md-2">
			<label for="is_auth_req">Force Auth Req</label>
			<div class="radio">
				<label for="is_auth_req_yes"><input type="radio" name="is_auth_req"
					id="is_auth_req_yes" value="1" {if $pages.is_auth_req== 1} checked{/if} />
					Yes</label> <label for="is_auth_req_no"><input type="radio"
					name="is_auth_req" id="is_auth_req_no" value="0"
					{if $pages.is_auth_req !=1} checked{/if} /> No</label>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label for="is_auth_not_req">Force Auth Not Req</label>
			<div class="radio">
				<label for="is_auth_not_req_yes"><input type="radio"
					name="is_auth_not_req" id="is_auth_not_req_yes" value="1" /> Yes</label>
				<label for="is_auth_not_req_no"><input type="radio"
					name="is_auth_not_req" id="is_auth_not_req_no" value="0" checked />
					No</label>
			</div>
		</div>
		<div class="form-group col-md-2">
			<label for="is_public">Public</label>
			<div class="radio">
				<label for="is_public_yes"><input type="radio" name="is_public"
					id="is_public_yes" value="1" checked /> Yes</label> <label
					for="is_public_no"><input type="radio" name="is_public"
					id="is_public_no" value="0" /> No</label>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Page Content</legend>
		<div class="form-group">
			{if $pages.id > 0}
			<div class="input-group">
				<textarea name="content" id="content" class="page_locale_editor">{$pages.content}</textarea>
				<span class="input-group-addon"><span
					class="glyphicon glyphicon-globe"
					title="Use Any Translate, to translate this"></span></span>
			</div>
			{else}
			<textarea name="content" id="content" class="page_locale_editor">{$pages.content}</textarea>
			{/if}
		</div>
	</fieldset>
	<div class="form-group">
		<div class="col-md-1 col-xs-2">
			<button type="submit" class="btn btn-default">{if $action ==
				'create'}Create{else}Save{/if}</button>
		</div>
		{if $pages.id != ''}
		<p class="col-md-3 col-xs-5 form-control-static">
			Created: <br class="visible-xs"> {$pages.creation_ts}
		</p>
		<p class="col-md-4 col-xs-5 form-control-static">
			Last Modified: <br class="visible-xs"> {$pages.lastmodified_ts}
		</p>
		{/if}
	</div>
</form>
<script src="{$js_assets}/tinymce/tinymce.min.js"></script>
{literal}
<script>
	$(document).ready(function() {
		new sai.tinymce().init(".page_locale_editor", sai.externalplugins);
		new sai.anytranslations().init();
	}); 
</script>
{/literal}
