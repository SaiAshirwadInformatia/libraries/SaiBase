<div class="row clearfix">
	<div class="col-md-12 column">
		<h2>Site Configuration</h2>
	</div>
	<table id="configsTable" class="table table-hover">
		<tr class="active">
			<th>Name</th>
			<th>Value</th>
			<th>Type</th>
			<th>Options</th>
			<th></th>
		</tr>
		{foreach from=$configs item=config}
		<tr>
			<td>{$config.name}</td>
			<td>{$config.val}</td>
			<td>{$config.oftype}</td>
			<td>{$config.options}</td>
			<td>
				<div class="btn-group">
					<a class="btn btn-default btn-xs" href="edit?name={$config.name}"
						title="Edit {$config.name}"><span
						class="glyphicon glyphicon-pencil"></span></a>
					<button class="btn btn-danger btn-xs" title="Delete {$config.name}"
						data-module="configs" data-id="{$config.name}"
						data-label="{$config.name}" id="configs_{$config.name}">
						{$icons->glyphicon_trash}</button>
				</div>
			</td>
		</tr>
		{/foreach}
	</table>
</div>
{literal}
<script>
$(document).ready(function(){
    new sai.deleteOrRestoreItems().init();
});
</script>
{/literal}
