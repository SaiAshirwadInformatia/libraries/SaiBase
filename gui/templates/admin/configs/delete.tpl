<div class="row clearfix">
	<div class="col-md-12">
		<h2>Delete Configuration</h2>
		{if isset($config)}
		<form action="" method="POST">
			<div class="form-group">
				<label>Are you sure to delete this config</label>
			</div>
			<div class="form-group">
				<button type="submit" name="deleteNo" class="btn btn-default">No</button>
				<button type="submit" name="deleteYes" class="btn btn-default">Yes</button>
			</div>
		</form>
		{else}
		<div class="alert alert-danger" role="alert">Invalid configuration
			name</div>
		{/if}
	</div>
</div>
