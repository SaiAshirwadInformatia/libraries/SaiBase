<h2>Site Configuration</h2>
<form action="" method="POST">
	<div class="row clearfix">
		{foreach from=$configs item=config}
		<div class="col-md-6">
			<div class="form-group">
				<label for="{$config.name}">{$config.name}</label> {if
				$config.oftype=='text'} <input type="text" class="form-control"
					name="config_{$config.name}" value="{$config.val}" /> {elseif
				$config.oftype=='boolean'}
				<div class="radio">
					<label for="yes"> <input type="radio" name="config_{$config.name}"
						value="yes" {if $config.val== 'yes'} checked{/if}
						/> Yes
					</label> <label for="no"> <input type="radio"
						name="config_{$config.name}" value="no" {if $config.val== 'no'} checked{/if}
						/> No
					</label>
				</div>
				{else} <select name="config_{$config.name}" class="form-control">
					{foreach from=$config.options item=opt}
					<option value="{$opt}" {if $opt==$config.val}selected{/if}>{$opt}</option>
					{/foreach}
				</select> {/if}
			</div>
		</div>
		{/foreach}
	</div>
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-default">Save Configuration</button>
		</div>
	</div>
</form>