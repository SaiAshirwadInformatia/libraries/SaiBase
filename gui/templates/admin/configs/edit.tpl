<h2>{if $action == 'create'}Create{else}Edit{/if} Configuration</h2>

<form action="" method="POST">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="name">Name</label> <input class="form-control"
					type="text" name="name" id="name"
					placeholder="(ex: name_of_config)" value="{$config.name}" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group" id="valueBox">
				<label for="value">Value</label> {if $config.oftype=='boolean'}
				<div class="radio">
					<label for="yesValue" class="col-md-6 col-xs-6"> <input
						type="radio" name="value" id="yesValue" value="yes"
						{if $config.val=="yes"} checked{/if}> <span> Yes</span></label><label
						for="noValue" class="col-md-6 col-xs-6"> <input type="radio"
						name="value" id="noValue" value="no" {if $config.val=="no"} checked{/if}>
						<span> No</span></label>
				</div>
				{else} <input class="form-control" type="text" name="value"
					id="value" placeholder="(ex: config_value)" value="{$config.val}" />
				{/if}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="oftype">Input Type</label> <select class="form-control"
					name="oftype" id="oftype">
					<option value="text" {if $config.oftype=='text'} selected{/if}>Text</option>
					<option value="list" {if $config.oftype=='list'} selected{/if}>List</option>
					<option value="boolean" {if $config.oftype=='boolean'} selected{/if}>Boolean</option>
				</select>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group" id="listOptions">
				{if $config.oftype == 'list'} <label for="options">Options</label> <input
					type="text" id="options" name="options" placeholder="(ex: one|two)"
					class="form-control" value="{$config.options}"> {/if}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-default">{if $config.name != ''}
				Save {else} Create {/if}</button>
		</div>
	</div>
</form>
{literal}
<script>
	$(document).ready(function() {
		new sai.configs().init();
	}); 
</script>
{/literal}
