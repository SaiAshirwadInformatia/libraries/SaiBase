<h2>{if $action == 'create'}Create{else}Edit{/if} Translations</h2>

<form action="" method="POST" enctype="multipart/form-data">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="object_name" class="control-label">Object Name</label> <input
					type="text" name="object_name" id="object_name"
					class="form-control" value="{$translations.object_name}" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="object_column" class="control-label">Object Column</label>
				<input type="text" name="object_column" id="object_column"
					class="form-control" value="{$translations.object_column}" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="object_id" class="control-label">Object ID</label> <input
					type="text" id="objectid" name="objectid" class="form-control"
					value="{$translations.objectid}" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="type" class="control-label">Type</label> <select
					name="type" id="type" class="form-control">
					<option value="text" {if $translations.type=='text'} selected{/if}>Text</option>
					<option value="textarea" {if $translations.type=='textarea'} selected{/if}>Textarea</option>
					<option value="editor" {if $translations.type=='editor'} selected{/if}>Editor</option>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="original">Original</label> <input type="text"
					name="original" id="original" class="form-control"
					value="{$translations.original}" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="translated">Translated</label> <input type="text"
					name="translated" id="translated" class="form-control"
					value="{$translations.translated}" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="locale">Locale</label> <select name="locale" id="locale"
					class="form-control"> {foreach from=$core->i18n->languages
					item=lcle} {if $lcle != 'en-US'}
					<option value="{$lcle}"
						{if $translations.locale==$lcle}selected{/if}>{$lcle}</option>
					{/if} {/foreach}
				</select>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="object_key">Object Key</label> <input type="text"
					name="object_key" id="object_key" class="form-control"
					value="{$translations.object_key}" />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<button type="submit" class="btn btn-default">{if $action ==
				'create'}Create{else}Save{/if}</button>
		</div>
	</div>
</form>
<script src="{$js_assets}/tinymce/tinymce.min.js"></script>
{literal}
<script>
$(document).ready(function(){
	new sai.translations().init();
});
</script>
{/literal}
