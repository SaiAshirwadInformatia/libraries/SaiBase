<form action="" method="GET">
	<div class="form-group">
		<label for="object_key">Object Key</label> <input type="text"
			name="data[object_key]" id="object_key" class="form-control"
			value="{if isset($smarty.get.data.object_key)}{$smarty.get.data.object_key}{/if}" />
	</div>
	<div class="form-group">
		<label for="original">Original</label> <input type="text"
			name="data[original]" id="original" class="form-control"
			value="{if isset($smarty.get.data.original)}{$smarty.get.data.original}{/if}" />
	</div>
	<button type="submit" class="btn btn-default">Filter</button>
</form>