<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Object Key</th>
			<th>Original</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$translations.results item=translation}
		<tr>
			<td><input type="checkbox" name="delete_translations[]"
				value="{$translation.id}" /></td>
			<td>{$translation.object_key}</td>
			<td>{truncate_str str=$translation.original len=50}</td>
			<td><div class="btn-group pull-right">
					<a class="btn btn-primary btn-xs" href="#"
						title="Created At {$translation.creation_ts}"> <span
						class="glyphicon glyphicon-time"> </span>
					</a> <a class="btn btn-default btn-xs"
						href="edit?id={$translation.id}"> <span
						class="glyphicon glyphicon-pencil"> </span>
					</a> {if isset($delete) and $delete=='soft_delete'}
					<button class="btn btn-danger btn-xs" data-module="translations"
						data-id="{$translation.id}"
						title="Delete {$translation.object_key}"
						data-label="{$translation.object_key}">{$icons->glyphicon_remove}
					</button>
					{elseif isset($restore) and $restore=='true'}
					<button class="btn btn-success btn-xs" data-module="translations"
						data-id="{$translation.id}" data-label="{$translation.object_key}">
						{$icons->glyphicon_repeat}</button>
					{/if}
				</div></td>
		</tr>
		{/foreach}
	</tbody>
</table>