<div class="panel-group" id="accordion">
	{foreach from=$nav item=n key=u}
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion"
					href="{if array_key_exists('url',$n)}{$n.url}{else}#{$u}{/if}"> <span
					class="glyphicon glyphicon-{$n.icon}"> </span> {$n.label}
				</a>
			</h4>
		</div>
		{if array_key_exists('childs',$n) and is_array($n.childs)}
		<div id="{$u}"
			class="panel-collapse collapse{if $template_file|strpos:$u !== false} in{/if}">
			<div class="panel-body">
				<table class="table">
					{foreach from=$n.childs key=sm_k item=sm}
					<tr>
						{if array_key_exists('childs',$sm)}{assign var="has_more_childs"
						value="true"}{else}{assign var="has_more_childs"
						value="false"}{/if}
						<td {if $template_file|strpos:$sm_k !== false} class="active"{/if}><a
							{if $has_more_childs== 'true'} data-toggle="collapse"
							data-parent="#{$u}"
							{/if}
							 href="{if array_key_exists('url',$sm)}{$basehref}/{$sm.url}{else}#in_{$sm_k}{/if}"><span
								class="glyphicon glyphicon-{$sm.icon}"> </span> {$sm.label}{if
								array_key_exists('childs',$sm)} <span
								class="glyphicon glyphicon-chevron-down pull-right"></span>{/if}</a>
							{if array_key_exists('childs',$sm) and is_array($n.childs)}
							<div id="in_{$sm_k}"
								class="panel-collapse collapse{if $template_file|strpos:$sm_k !== false} in{/if}">
								<div class="panel-body">
									{if $template_file|strpos:$sm_k !== false} {literal}
									<script>
								    $(document).ready(function(){
									   $("#{/literal}{$u}{literal}").addClass("in");
									    });
								    </script>
									{/literal}{/if}
									<table class="table">
										{foreach from=$sm.childs item=smm}
										<tr>
											<td><a href="{$basehref}/{$smm.url}"><span
													class="glyphicon glyphicon-{$smm.icon}"> </span>
													{$smm.label}</a></td>
										</tr>
										{/foreach}
									</table>
								</div>
							</div> {/if}</td>
					</tr>
					{/foreach}
				</table>
			</div>
		</div>
		{/if}
	</div>
	{/foreach}
</div>