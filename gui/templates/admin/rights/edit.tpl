<h2>{if $action == 'create'}Create{else}Edit{/if} Right</h2>

<form action="" method="POST">
	<div class="form-group">
		<label for="name">Name</label> <input type="text" name="name"
			id="name" class="form-control" value="{$rights.name}" />
	</div>
	<div class="form-group">
		<label for="description">Description</label>
		<textarea name="description" id="description" class="form-control">{$rights.description}</textarea>
	</div>
	{if $rights.id != ''}
	<div class="form-group">
		<label for="createdBy">Created By</label>
		<div>
			<p class="form-control-static">
				<img src="{$rights.users.display_picture}" style="width: 32px" />
				{$rights.users.fname} {$rights.users.lname}
			</p>
		</div>
	</div>
	{/if}
	<div class="form-group">
		<div class="col-md-1 col-xs-2">
			<button type="submit" class="btn btn-default">{if $action ==
				'create'}Create{else}Save{/if}</button>
		</div>
		{if $rights.id != ''}
		<p class="col-md-3 col-xs-5 form-control-static">
			Created: <br class="visible-xs"> {$rights.creation_ts}
		</p>
		<p class="col-md-4 col-xs-5 form-control-static">
			Last Modified: <br class="visible-xs"> {$rights.lastmodified_ts}
		</p>
		{/if}
	</div>
</form>
