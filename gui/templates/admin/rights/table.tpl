<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th>Name</th>
			<th>Description</th>
			<th></th>
		</tr>
	
	
	<tbody>
		{foreach from=$rights.results item=right}
		<tr>
			<td><input type="checkbox" name="delete_rights[]" value="{$right.id}" />
			</td>
			<td>{$right.name}</td>
			<td>{$right.description}</td>
			<td>
				<div class="btn-group pull-right">
					<a class="btn btn-primary btn-xs" href="#"
						title="Created At {$right.creation_ts}"><span
						class="glyphicon glyphicon-time"> </span></a> <a
						class="btn btn-default btn-xs" href="edit?id={$right.id}"><span
						class="glyphicon glyphicon-pencil"> </span></a> </a> {if
					isset($delete) and $delete=='soft_delete'}
					<button class="btn btn-danger btn-xs" data-module="rights"
						data-id="{$right.id}" title="Delete {$right.name}"
						data-label="{$right.name}">{$icons->glyphicon_remove}</button>
					{elseif isset($restore) and $restore=='true'}
					<button class="btn btn-success btn-xs" data-module="rights"
						data-id="{$right.id}" data-label="{$right.name}">
						{$icons->glyphicon_repeat}</button>
					{/if}
				</div>
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>