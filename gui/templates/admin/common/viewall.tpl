<div class="row clearfix">
	<div class="col-md-9 column">
		<h2>All {$module|ucfirst}</h2>
		{if isset($module_results) and isset($module_results['results']) and
		count($module_results.results) > 0} {assign var="$module"
		value=$module_results} {include file="../$module/table.tpl"
		delete="soft_delete" restore="false"} {get_bootstrap_pagination
		baseUrl="?$query_string" currentPage=$module_results.page
		maxPages=$module_results.totalPages} 
		
{literal}
<script>
$(document).ready(function(){
    new sai.deleteOrRestoreItems().init();
});
</script>
{/literal}
		{else}
		<div class="alert alert-danger" role="alert">
			<p>{$icons->glyphicon_exclamation_sign} No records found</p>
		</div>
		{/if}
	</div>
	{if isset($smarty.get.data) or (isset($module_results) and
	isset($module_results['results']) and count($module_results.results) >
	0)}
	<div class="col-md-3">
		<h2>Filter</h2>
		{include file="../$module/filter.tpl"}
	</div>
	{/if}
</div>