<div class="row clearfix">
	<div class="col-md-9 column">
		<h2>Deleted {$module|ucfirst}</h2>
		<div class="table-responsive">
			{if isset($module_results) and isset($module_results['results']) and
			count($module_results.results) > 0}{assign var="$module"
			value=$module_results} {include file="../$module/table.tpl"
			restore='true' edit="false"} {literal}
			<script>
$(document).ready(function(){
    new sai.deleteOrRestoreItems().init();
});
</script>
			{/literal}{else}
			<div class="alert alert-danger" role="alert">
				{$icons->glyphicon_exclamation_sign} Looks like no {$module|ucfirst}
				are deleted yet</div>
			{/if}
		</div>
	</div>
	<div class="col-md-3">
		{if isset($smarty.get.data) or (isset($module_results) and
		isset($module_results['results']) and count($module_results.results) >
		0)}
		<h2>Filter</h2>
		{include file="../$module/filter.tpl"} {/if}
	</div>
</div>
