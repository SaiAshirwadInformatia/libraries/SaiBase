<!-- Footer -->

<footer class="navbar navbar-default navbar-fixed-bottom">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 col-xs-6">
				<p class="navbar-text navbar-left text-center">
					&copy; 2014 Sai Ashirwad Informatia. <br class="visible-xs" />All
					rights reserved.
				</p>
			</div>
			<div class="col-md-6 col-xs-6">
				<p class="navbar-text navbar-right pull-right">
					<span style="font-weight: bold;"> Developed with <span
						class="glyphicon glyphicon-heart"></span> by
					</span><a href="http://saiashirwad.com"> <img
						src="{$images_assets}/sai_32.png" style="vertical-align: middle" />
					</a>
				</p>
			</div>
		</div>
	</div>
</footer>
</body>
</html>
{$core->session->clearMsgs()}
