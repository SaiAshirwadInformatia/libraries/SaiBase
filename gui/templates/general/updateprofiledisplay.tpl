<!-- Main -->

<div id="main-wrapper">
	<div class="container simple-form">
		<h2 style="text-align: left">Edit Profile</h2>
		<div class="row">
			<div class="6u">
				<div class="row">
					<div class="12u">
						<label for="disp_pic">Display Picture</label>
						<input type="file" name="disp_pic" id="disp_pic" />
						<input type="hidden" name="id" id="id" value="{$current_user.id}" />
					</div>
				</div>
				<div class="row">
					<div class="6u">
						<input type="button" name="updateDisplayPicture" id="updateDisplayPicture" value="Update DP" />
					</div>
					<div class="6u">
						<input type="button" name="deleteDisplayPicture" id="deleteDisplayPicture" value="Remove Current DP"/>
					</div>
				</div>
			</div>
			<div class="4u">
				<div  style="text-align: center;display:block">
					<div class="current_display">
						<span>
							Current DP
						</span>
						<img src="{$current_user.disp_pic}" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	{literal}
	$(document).ready(function() {
		new sai.updatepicture().init();
	});
	{/literal}
</script>
