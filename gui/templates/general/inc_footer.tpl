<!-- Footer -->


<footer class="container" id="footer-wrapper">

	<div class="row">
		<div class="col-md-12">
			<div id="copyright">
				&copy; {$core->configs->sitename}. All rights reserved. | Developed &amp; Maintained: <a href="http://saiashirwad.com">SAI</a>
			</div>
		</div>
	</div>
</footer>

</body>
</html>