{get_lang var='labels' s='toggle_navigation'} {assign var=selected
value='false'} {if $page->has_menubar == 1}
<div class="navbar navbar-static-top" role="navigation" id="menubar">
	<div class="container">
		<div class="navbar-header">
			<div class="col-sm-12 col-md-12 visile-xs"
				id="small-device-search-box">
				<form class="">
					<input type="text" placeholder="Search This...">
					<button type="button"
						class="btn btn-danger col-sm-2 col-md-2 col-sm-offset-1 col-md-offset-1">
						{$icons->glyphicon_search}</button>
					<button type="button" class="btn btn-danger col-sm-2 col-md-2 "
						id="close-search-box">{$icons->glyphicon_remove}</button>
				</form>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-3 " id="search-icon-box">
				<button type="button" class="navbar-toggle" data-toggle="collapse">
					<span class="sr-only"> {$labels.toggle_navigation}</span> <span
						class="glyphicon glyphicon-search"
						style="float: left; font-size: 1.4em; color: #FFFFFF; text-align: center;"
						title="Search" id="btnsearch"> </span>
				</button>
			</div>
			<div class="col-xs-8 col-sm-8 col-md-6 tabTitle  ">
				<p class="navbar-toggle">Ghanerao.in</p>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-3 " id="menu-icon-box">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only"> {$labels.toggle_navigation}</span> <span
						class="icon-bar"> </span> <span class="icon-bar"> </span> <span
						class="icon-bar"> </span>
				</button>
			</div>

		</div>
		<!-- Nav Header -->
		<div class="navbar-collapse collapse ">
			<nav id="menu" class="navbar-default ">
				<ul class="nav navbar-nav">
					{foreach from=$nav item=n key=u} {if count($n.childs) > 0} {assign
					var="has_child" value="true"} {else} {assign var="has_child"
					value="false"} {/if}
					<li {if $title|strpos:$n.label !== false and $selected==
						'false'} class="current_page_item{if $has_child == 'true'} dropdown{/if}"
						{assign var=selected value='true' }{elseif $has_child==
						'true'} class="dropdown"{/if}><a href="{$basehref}/{$n.url}"
						{if $has_child== 'true'} class="dropdown-toggle"
						data-toggle="dropdown"{/if}>{$n.label}{if $has_child == 'true'} <span
							class="caret"> </span>{/if}
					</a> {if $has_child == 'true'}
						<ul class="dropdown-menu" role="menu">
							{foreach from=$n.childs item=c key=_c}
							<li><a href="{$basehref}/{$c.url}"><span
									class="inner-menu-circle pull-left inner-menu-circle-left"> </span>
									{$c.label} <span
									class="inner-menu-circle pull-right inner-menu-circle-right"> </span></a>
							</li> {/foreach}
						</ul> {/if}</li> {/foreach}
				</ul>
			</nav>
		</div>
	</div>
</div>
{/if} {include file='./inc_slock.tpl' visibility_property='visible-xs'}
