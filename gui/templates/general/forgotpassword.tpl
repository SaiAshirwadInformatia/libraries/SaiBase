{get_lang var='labels'
s='forgotpassword_title,lbl_forgotpassword,plch_forgotpassword,forgotpassword_btn'}

<!-- Main -->
<div class="container" id="main-wrapper">
	<h2 style="text-align: center">{$labels.forgotpassword_title}</h2>
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="form-group">
				<label for="email">{$labels.lbl_forgotpassword}</label> <input
					type="email" class="form-control" name="email" id="email"
					placeholder="{$labels.plch_forgotpassword}" />
			</div>
			<div class="form-group">
				<input type="button" class="btn btn-default"
					name="forgotPasswordSubmit" id="forgotPasswordSubmit"
					value="{$labels.forgotpassword_btn}" />
			</div>
		</div>
	</div>
</div>
<script>
	{literal}
	$(document).ready(function() {
		new sai.forgotpassword().init();
	}); 
	{/literal}
</script>
