<h2>Follow Us on Twitter</h2>
<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/WillingTree" data-widget-id="471134187102019584">Tweets by @WillingTree</a>
<script>
	{literal}
	! function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
		if (!d.getElementById(id)) {
			js = d.createElement(s);
			js.id = id;
			//js.src = p + "https://platform.twitter.com/widgets.js";
			js.src = "https://platform.twitter.com/widgets.js";
			fjs.parentNode.insertBefore(js, fjs);
		}
	}(document, "script", "twitter-wjs");
	{/literal} 
</script>