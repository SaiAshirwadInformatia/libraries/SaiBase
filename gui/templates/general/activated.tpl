<!-- Main -->

<div id="main-wrapper">
	<div class="container">
		<div class="row">
			<div class="12u" style="font-size: 3em;text-align: center;line-height: 1.5em">
				{$page->content}
			</div>
		</div>
	</div>
</div>