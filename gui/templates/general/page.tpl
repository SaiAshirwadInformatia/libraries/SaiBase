{include file='./inc_header.tpl' page=$page}  {if $page->has_sidebar
!= 0} {assign var='has_sidebar' value='true'} {else} {assign
var='has_sidebar' value='false'} {/if}
<div class="container-fluid">
	<div class="row">
		{if $page->has_sidebar == 1}
		<div class="col-md-3">
			<div class="advertise-news-bar">
				<div class="row">{include file='./inc_sidebar.tpl' page=$page}</div>
			</div>
		</div>
		{/if}
		<div class="{if $has_sidebar=='true'}col-md-9{else}col-md-12{/if}">
			{if file_exists($abs_template|cat:'/'|cat:$template_file)}
			<div class="contentbase">{include file=$template_file page=$page}</div>
			{else}
			<div class="contentbase">
				<h2>{$page->title}</h2>
				{$page->content}
			</div>
			{/if}
		</div>
		{if $page->has_sidebar == 2}
		<div class="col-md-3">
			<div class="advertise-news-bar">
				<div class="row">{include file='./inc_sidebar.tpl' page=$page}</div>
			</div>
		</div>
		{/if}
	</div>
</div>
{include file='./inc_footer.tpl'}
