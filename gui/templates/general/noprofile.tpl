<h2>User Profile</h2>
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-danger" role="alert">
			<p>{$icons->glyphicon_exclamation_sign} Oops, looks like no such user
				profile exists</p>
		</div>
	</div>
</div>