{get_lang var='labels' s='welcome,login,logout,register'}
<!DOCTYPE HTML>
<!--
----------------------------------------------------------------------------
Developed and Maintained by Sai Ashirwad Informatia
----------------------------------------------------------------------------
----------------------------------------------------------------------------
Authors
----------------------------------------------------------------------------
Rohan Sakhale		rs@saiashirwad.com
Suyash Jadhav		jsuyash@saiashirwad.com
Chirag Babrekar		cbabrekar@saiashriwad.com
Vikram Sule			vikram.s@saiashirwad.com
Chaitali Patil      cpatil@saiashirwad.com
----------------------------------------------------------------------------
-->
<html lang="{$core->i18n->html_locale}">
<head>
<title>{$page->title} - {$core->configs->sitename}</title>
<!-- Page meta by saiashirwad.com -->
<meta charset="utf-8" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8" name="viewport"
	content="width=device-width, initial-scale=1">
<meta name="description" content="{$page->meta_description}" />
<meta name="keywords" content="{$page->meta_keywords}" />
<meta name="author" content="{$page->meta_author}" />

<link rel="icon" type="image/png" href="{$images_assets}/icon.png">

		<link rel="stylesheet" href="{$js_assets}/jquery-ui/themes/south-street/jquery-ui.min.css" />
		<link rel="stylesheet" href="{$js_assets}/jquery-ui/themes/south-street/theme.css" />
<link rel="stylesheet" href="{$js_assets}/lightbox/css/lightbox.css" />
{foreach from=$icons->icons_css_paths item=css_path}
<link rel="stylesheet" href="{$css_path}" />
{/foreach}
<link rel="stylesheet"
	href="{$js_assets}/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

<link rel="stylesheet" href="{$template}/css/style.css" />
<link rel="stylesheet" href="{$template}/css/adjust.css" />

<script src="{$js_assets}/jquery/dist/jquery.min.js"></script>
<script src="{$js_assets}/jquery-ui/jquery-ui.min.js"></script>
<script src="{$js_assets}/lightbox/js/lightbox.min.js"></script>
{foreach from=$icons->icons_js_paths item=js_path}
<script src="{$js_path}"></script>
{/foreach}
<script src="{$js_assets}/bootbox.js/bootbox.js"></script>
<script src="{$js_assets}/moment/min/moment.min.js"></script>
<script
	src="{$js_assets}/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="{$js_assets}/jQuery.Marquee/jquery.marquee.min.js"></script>

<script src="{$js_assets}/functions.js"></script>
<script src="{$js_assets}/saiquery.js"></script>
<!--[if lte IE 8]>
		<script src="{$js_assets}/html5shiv/dist/html5shiv.min.js"></script>
		<![endif]-->

{literal}
<script>
			sai.externalplugins = {
				"jbimages" : "{/literal}{$js_assets}{literal}/jbimages/plugin.min.js"
			};
			sai.apihref = '{/literal}{$core->apihref}{literal}';
			sai.basehref = '{/literal}{$core->basehref}{literal}';
		</script>
{/literal}
	</head>
	<body>
	<!-- Header -->
		<header class="container" id="header-wrapper">
			<div class="row">
				<div class="col-md-12">
					<div id="logo">
						<h1>{$core->configs->sitename}</h1>
						<a href="index"><img id="logoImg" src="{$images_assets}/sai_dark_32.png" alt="WillingTree" title="Willing Tree Home" /></a>
					</div>
				</div>
			</div>
		</header>

		{include file='./inc_nav.tpl' page=$page} 
