{get_lang var='labels'
s='change_your_password,current_password,new_password,new_confirm_password,change_password,cancel'
}
<h2>{$labels.change_your_password}</h2>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="current_password">{$labels.current_password}</label> <input
				type="password" name="current_password" id="current_password"
				placeholder="(ex: *******)" class="form-control" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="new_password">{$labels.new_password}</label> <input
				type="password" name="new_password" id="new_password"
				placeholder="(ex: *******)" class="form-control" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="new_confirm_password">{$labels.new_confirm_password}</label>
			<input type="password" name="new_confirm_password"
				id="new_confirm_password" placeholder="(ex: *******)"
				class="form-control" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<button class="btn btn-default" name="changePasswordNow"
				id="changePasswordNow">{$labels.change_password}</button>
			<button class="btn btn-default" name="cancelNow" id="cancelNow">{$labels.cancel}</button>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		new sai.changepassword().init();
	})
</script>
