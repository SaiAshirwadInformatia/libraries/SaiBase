{get_const name='gender,marital_status,bloodgroup'} {get_lang
var='labels'
s='register_title,lbl_fname,plch_fname,lbl_mname,plch_mname,lbl_lname,plch_lname,lbl_email,plch_email,
lbl_password,plch_password,lbl_memberno,plch_memberno,lbl_gender,plch_gender,lbl_maternal_gotra,
plch_maternal_gotra,lbl_bloodgroup,lbl_phone1,plch_phone1,lbl_phone2,plch_phone2,lbl_website,plch_website,lbl_dob,plch_dob,
lbl_education,plch_education,lbl_maritalstatus,plch_maritalstatus,lbl_anniversary,plch_anniversary,registernow_btn,male,female'}
<h2 class="title">{$icons->glyphicon_user} {$labels.register_title}</h2>

<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_fname}</label> <input class="form-control"
				type="text" name="fname" id="fname"
				placeholder="{$labels.plch_fname}" />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_mname}</label> <input class="form-control"
				type="text" name="mname" id="mname"
				placeholder="{$labels.plch_mname}" />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_lname}</label> <input class="form-control"
				type="text" name="lname" id="lname"
				placeholder="{$labels.plch_lname}" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_email}</label> <input class="form-control"
				type="email" name="email" id="email"
				placeholder="{$labels.plch_email}" />
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_password}</label> <input class="form-control"
				type="password" name="password" id="password"
				placeholder="{$labels.plch_password}" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_gender}</label>
			<div class="checkbox">
				{foreach from=$gender key=label_gender item=gen} <label> <input
					type="radio" name="gender" id="gender" value="{$gen}" /> {get_lang
					s=$label_gender}
				</label> {/foreach}
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_bloodgroup}</label> <select class="form-control"
				name="blood_group">
				<option value=""></option>{foreach from=$bloodgroup item=bg}
				<option value="{$bg}">{$bg}</option> {/foreach}
			</select>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_phone1}</label> <input class="form-control"
				type="text" name="phone1" id="phone1"
				placeholder="{$labels.plch_phone1}" />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_phone2}</label> <input class="form-control"
				type="text" name="phone2" id="phone2"
				placeholder="{$labels.plch_phone2}" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_website}</label> <input class="form-control"
				type="text" name="website" id="website"
				placeholder="{$labels.plch_website}" />
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_dob}</label>
			<div class='input-group date'>
				<input class="form-control" type="text" name="dob" id="dob"
					data-date-format="YYYY-MM-DD" /> <span class="input-group-addon">
					{$icons->glyphicon_calendar} </span>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_education}</label> <input class="form-control"
				type="text" name="education" id="education"
				placeholder="{$labels.plch_education}" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_maritalstatus}</label> <select
				name="marital_status" id="marital_status" class="form-control">
				<option value=""></option> {foreach from=$marital_status item=lbl_ms
				key=ms}
				<option value="{$ms}">{get_lang s=$lbl_ms}</option> {/foreach}
			</select>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>{$labels.lbl_anniversary}</label>
			<div class='input-group date'>
				<input class="form-control" type="text" name="anniversary_date"
					id="anniversary_date" data-date-format="YYYY-MM-DD" /> <span
					class="input-group-addon"> {$icons->glyphicon_calendar} </span>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12"">
		<input class="btn btn-default" type="button" id="register"
			value="{$labels.registernow_btn}" />
	</div>
</div>

{literal}
<script>
	$(document).ready(function() {
		$(".date").datetimepicker({pickTime:false});
	})
</script>
{/literal}
