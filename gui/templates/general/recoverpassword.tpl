<!-- Main -->

<div id="main-wrapper">
	<div class="container simple-form">
		<div class="row">
			<div class="12u">
				<h2>Reset Your Password</h2>
			</div>
		</div>
		<div class="row">
			<div class="12u">
				<p>
					To update your password, please enter it once in each field below
				</p>
				<p>
					Passwords are case-sensitive and must be at least 8 characters long. A good password should contain a mix of capital and lower-case letters, numbers and symbols.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="3u">
				<input type="hidden" name="token" id="token" value="{$smarty.get.a}" />
				<input type="hidden" name="user_id" id="user_id" value="{$smarty.get.u}" />
			</div>
			<div class="3u">
				<label for="password">Enter new password</label>
				<input type="password" name="password" id="password" placeholder="**********" autofocus="autofocus" />
			</div>
			<div class="3u">
				<label for="confirm_password">Re-enter new password</label>
				<input type="password" name="confirm_password" id="confirm_password" placeholder="**********" />
			</div>
		</div>
		<div class="row">
			<div class="5u"></div>
			<div class="2u">
				<input type="button" name="resetPassword" id="resetPassword" value="Reset Password" />
			</div>
		</div>
	</div>
</div>
<script>
	{literal}
	$(document).ready(function() {
		new sai.recoverpassword().init();
	});
	{/literal}
</script>
