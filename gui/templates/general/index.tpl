{*

WillingTree Project
Project for a cause

@name Index
@abstract Index page for WillingTree
@author Rohan Sakhale
@copyright saiashirwad.com
@since WillingTree v1

*}

<!-- Main -->

<div id="main-wrapper">
	<div class="container">
		<!-- Banner -->
		<div class="row">
			<section class="12u">
				<div id="banner">
					<a href="#"><img src="{$images_assets}//banner_1.jpg" alt="" /></a>
					<!-- div class="caption">
					<span><strong>Willing Tree</strong>: Cares and Delights your Life</span>
					<a href="#" class="button">Find Out More!</a>
					</div -->
				</div>
				<!-- <iframe id="wow-slider" src="slider.html" marginheight="0" marginwidth="0"></iframe> -->
			</section>
		</div>

		<!-- Features -->

		<div class="row">
			<div class="9u">
				<h2>Introduction </h2>
				{$page->content}
			</div>
		</div>

		<!-- Divider -->

		<!-- div class="row">
		<div class="12u">
		<div class="divider divider-top"></div>
		</div>
		</div -->

		<!-- Highlight Box -->

		<!-- div class="row">
		<div class="12u">
		<div class="highlight-box">
		<h2>AYURBALAM YASHO VARCHAS PRAJA PASHUNWASU NI CHA ||
		<br />
		<br />
		BRAHMA PRAJNAN CHA MEDHA CHA TVAN NO DEHI VANASPATE ||</h2>
		<span>This is a Sanskrit prayer yearning for long life, physical strength, success, sharp intelligence, good progeny above all creativity and universal perspective. </span>
		</div>
		</div>
		</div -->

		<!-- Divider -->

		<!-- div class="row">
		<div class="12u">
		<div class="divider divider-bottom"></div>
		</div>
		</div -->

		<!-- Thumbnails -->

		<!-- div class="row">
		<div class="12u">
		<section class="thumbnails first last">
		<div>
		<div class="row">
		<div class="4u">
		<div class="thumbnail first">
		<a href="#"><img src="{$images_assets}/uday.jpg" alt="" /></a>
		<blockquote>
		Duis neque nisi, dapibus sed mattis et quis, rutrum accumsan sed. Suspendisse eu varius nibh. Suspendapibus sed mattis quis.
		</blockquote>
		<cite><strong>Udayraj Naik</strong> Supporter</cite>
		</div>
		</div>
		<div class="4u">
		<div class="thumbnail">
		<a href="#"><img src="{$images_assets}/abhi.jpg" alt="" /></a>
		<blockquote>
		Duis neque nisi, dapibus sed mattis et quis, rutrum accumsan sed. Suspendisse eu varius nibh. Suspenddapibus sed mattis quis.
		</blockquote>
		<cite><strong>Abhishek Patil</strong> Supporter</cite>
		</div>
		</div>
		<div class="4u">
		<div class="thumbnail">
		<a href="#"><img src="{$images_assets}/rohan.jpg" alt="" /></a>
		<blockquote>
		Duis neque nisi, dapibus sed mattis et quis, rutrum accumsan sed. Suspendisse eu varius nibh. Suspenddapibus sed mattis quis.
		</blockquote>
		<cite><strong>Rohan Sakhale</strong> Supporter</cite>
		</div>
		</div>
		</div>
		</div>
		</section>
		</div>
		</div -->

		<!-- Divider -->

		<!-- div class="row">
		<div class="12u">
		<div class="divider divider-top"></div>
		</div>
		</div -->

		<!-- CTA Box -->

		<!-- div class="row">
		<div class="12u">
		<div class="cta-box">
		<span>Amet lorem varius tempus consequat lorem?</span>
		<a href="#" class="button">Ipsum Consequat</a>
		</div>
		</div>
		</div -->
	</div>
</div>
