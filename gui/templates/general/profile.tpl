<!-- Main -->
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<h2>User Profile</h2>
			<div class="profile_box">
				<div style="display:inline-block">
					<img src="{$user.disp_pic}" style="display:block" />
					{if $myself == true}
					<div style="margin-top: -30px;text-align: center;z-index: 2;">
						<a href="updateprofiledisplay"> <span style="background: rgba(240,240,240,.5);width:100%;display:inline-block">Update</span> </a>
					</div>
					{/if}
				</div>
				<ul>
					<li>
						<strong>{$user.fname} {$user.lname}</strong>
						{if isset($user.gender)}
						<img class="gender_icon" title="{if $user.gender == 'm'}Male User{else}Female User{/if}" src="{$images_assets}/{$user.gender}_24.png" />
						{/if}
					</li>
					{if $myself == true}
					<li>
						{$user.email}
					</li>
					{/if}
					<li>
						Joined: <b>{$user.creation_ts}</b>
					</li>
				</ul>
			</div>
			{if $myself == true}
			<div style="display:inline-block;vertical-align: top;float:right">
				<a href="changepassword" title="Change Password"><img src="{$images_assets}/modify-key-icon.png" title="Change Password" /></a>
				<a href="editprofile" title="Update your profile"><img src="{$images_assets}/picture-settings-icon.png" title="Update your profile" /></a>
			</div>
			{/if}
		</div>
		<div class="col-md-6">
			<h2>Activity Stream</h2>
			<div>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
				dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
				<h2>Details</h2>
	</div>
	</div>
		{if $user.about != ''}
	<div class="row">
			<div class="col-md-2">
			<span class="strong">Bio</span>
		</div>
		<div class="col-md-10">
			<span class="profile_display">{$user.about}</span>
		</div>
	</div>
	{/if}
	<div class="row">
		{if $user.phone != ''}
		<div class="col-md-2">
			<span class="strong">Phone</span>
		</div>
		<div class="col-md-4">
			<span class="profile_display">{$user.phone}</span>
		</div>
		{/if}
		{if $user.website != ''}
		<div class="col-md-2">
			<span class="strong">Website</span>
		</div>
		<div class="col-md-4">
			<span class="profile_display"><a href="{$user.website}" target="_blank">{$user.website}</a></span>
		</div>
		{/if}
	</div>
	<div class="row">
		{if $user.job_title != ''}
		<div class="col-md-2">
			<span class="strong">Job Title</span>
		</div>
		<div class="col-md-4">
			<span class="profile_display">{$user.job_title}</span>
		</div>
		{/if}
		{if $user.company_name != ''}
		<div class="col-md-2">
			<span class="strong">Company Name</span>
		</div>
		<div class="col-md-4">
			<span class="profile_display">{$user.company_name}</span>
		</div>
		{/if}
	</div>
	<div class="row">
		{if $user.company_url != ''}
		<div class="col-md-2">
			<span class="strong">Company URL</span>
		</div>
		<div class="col-md-4">
			<span class="profile_display"><a href="{$user.company_url}" title="{$user.company_name}" target="_blank">{$user.company_url}</a></span>
		</div>
		{/if}
	</div>
</div>
