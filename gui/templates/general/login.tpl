{get_lang var='labels'
s='login_title,lbl_email_username,plch_email_username,lbl_password,plch_password,login_btn'}
<h2>{$labels.login_title}</h2>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label>{$labels.lbl_email_username}</label> <input class="form-control"
				type="text" name="username_email" id="username_email"
				placeholder="{$labels.plch_email_username}" autofocus />
		</div>
		<div class="form-group">
			<label>{$labels.lbl_password}</label> <input class="form-control"
				type="password" name="password" id="password"
				placeholder="{$labels.plch_password}" />
		</div>
		<div class="form-group">
			<input class="btn btn-default" type="button" id="loginNow"
				value="{$labels.login_btn}" /> <a href="forgotpassword">Forgot
				Password?</a>
		</div>
	</div>
</div>
{literal}
<script>
	$(document).ready(function() {
		new sai.login().init();
	}); 
</script>
{/literal}
