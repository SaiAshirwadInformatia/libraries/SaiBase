{get_lang var='labels' s='404notfound_title'}
<h2>{$labels.404notfound_title}</h2>

<div class="row">
	<div class="col-md-12">
		<div class="alert alert-danger" role="alert">{$page->content}</div>
	</div>
</div>
