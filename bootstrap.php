<?php
use Sai\Base\AutoLoader;
use Sai\Base\Core;
/**
 *
 * @name Start
 * @abstract Starting point of application
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */

/**
 * Define Constants that can be used globally in entire application
 */
define('DS', DIRECTORY_SEPARATOR);
define('SAI_ABS_PATH', __DIR__ . DS);

header("Developed-By: Sai Ashirwad Informatia");
header("X-Powered-By: Sai Ashirwad Informatia");
header("Server: Sai Ashirwad Informatia Private Server");

// Tell PHP that we're using UTF-8 strings until the end of the script
mb_internal_encoding('UTF-8');

// Tell PHP that we'll be outputting UTF-8 to the browser
mb_http_output('UTF-8');

/**
 * Turn on output buffering with the gzhandler
 * This will help send compressed data from server to client
 * Note: `zlib` module is required to run gzhandler
 */
ini_set("zlib.output_compression", "On");
/*
 * if (function_exists('ob_start')) { ob_start('ob_gzhandler'); }
 */

/**
 * Check if config_db exists which could state installation is done
 * But this file could also be instantiated by installer, hence check if $installer is set or not
 *
 * If its not an installer and config exists, then include common and authload and instantiate core object
 * Else throw an exception recommending to run an installer
 */
if (file_exists(SAI_ABS_PATH . 'api' . DS . 'cfg' . DS . 'config_db.inc.php') and
     ! isset($installer)) {
    require_once SAI_ABS_PATH . 'api' . DS . 'cfg' . DS . 'config_db.inc.php';
    require_once SAI_ABS_PATH . 'api' . DS . 'cfg' . DS . 'const.inc.php';
    include_once SAI_ABS_PATH . 'api' . DS . 'library' . DS . 'Sai' . DS . 'Base' .
         DS . 'AutoLoader.php';
    AutoLoader::register();
    $core = Core::getInstance();
    if ($core->configs->compress_html_output != "no") {
        ob_start("Sai\Base\Common::sanitizeOutput");
    }
} else 
    if (! isset($installer)) {
        throw new Exception(
            'Configuration not found, please create your configuration from sample file or run the installer');
    }
?>