<?php
use Sai\Modules;
/**
 *
 * @name Page
 * @abstract Page would route out based on database page's available ;)
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
require_once 'bootstrap.php';

$pages_manager = managers_store::locate('pages');
$auth_manager = managers_store::locate('auth');
$sai_smarty = sai_smarty::getInstance();
$page = new pages(false);
$assign = array();
try {
    if (! isset($_GET['url'])) {
        throw new Modules\Pages\Exception(Modules\Pages\Exception::INVALID_PAGE);
    }
    $_url = explode('/', $_GET['url']);
    $url = array_shift($_url);
    $params = array();
    if (count($_url) > 0) {
        $params = $_url;
    }
    unset($_url);
    $page = $pages_manager->get($url);
    
    if (! is_null($page)) {
        if ($page->is_auth_req) {
            if (! $auth_manager->validate() and $page->slug != 'login') {
                header('Location: ' . $core->basehref . '/login');
            }
        }
        if ($page->is_auth_not_req) {
            if ($auth_manager->validate()) {
                header('Location: ' . $core->basehref . '/profile');
            }
        }
        $page->slug = str_replace("-", "_", $page->slug);
        if (file_exists(GUI_LOGIC_INCLUDE . DS . $page->slug . '.php')) {
            require_once GUI_LOGIC_INCLUDE . DS . $page->slug . '.php';
        }
    }
} catch (pages_exception $e) {
    change_response_code(404);
    $page = $pages_manager->get('404notfound');
}

/**
 * Finalize the Page details
 */
$assign['title'] = $page->title;
$assign['page'] = $page;
$assign['template_file'] = './' . $page->slug . '.tpl';
$core->displayPage(array(
    'assign' => $assign
));
?>
