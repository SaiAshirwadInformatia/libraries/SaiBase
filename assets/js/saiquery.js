/**
 *	SAI JavaScript Library
 *
 * 	@author		Rohan Sakhale
 * 	@copyright	Sai Ashirwad Informatia
 * 	@since		WillingTree v1
 */

/**
 * Represents singleton object that would contain most of the variables and
 * method for invocation. If this object is made null through console operation
 * for that particular event would be blocked.
 * 
 * Usage // Declaration sai.variable_name = 10 ==> varaible_name would have 10
 * as a value stored in sai.function_name = function() {....} ==> function_name
 * would be the name of funciton that would store function call // Usage
 * console.log(sai.variable_name); ==> represents sai.function_name() ==>
 * invokes the function_name
 */
var sai = {};

/**
 * Get Query Parameters in easy way Source:
 * http://css-tricks.com/snippets/javascript/get-url-variables/
 * 
 * @param {string}
 *                variable - Query param whose value is required
 * 
 * @return {string}
 */
sai.getQueryVariable = function(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
	var pair = vars[i].split("=");
	if (pair[0] == variable) {
	    return pair[1];
	}
    }
    return (false);
};

/**
 * Functions helps to display up the dialog box on the UI. Optionally this
 * dialog box can contain the buttons for performming any operation activity
 * 
 * @param {string}
 *                title - Displays title of the dialog box on the UI
 * @param {string}
 *                content - Displays content of the dialog box on the UI
 * @param {object}
 *                buttonsObject - Object representing buttons with the
 *                functional operation
 */
sai.showDialog = function(title, content, buttonsObject, closeButton, width,
	height) {
    if (width === undefined) {
	width = 380;
    }
    if (height === undefined) {
	height = 200;
    }
    if (closeButton === undefined || closeButton === null) {
	closeButton = function() {
	    $(this).dialog("close");
	    $(this).remove();
	};
    }
    if (buttonsObject === undefined || buttonsObject === null) {
	buttonsObject = {};
    }
    var dialogTag = document.createElement('div');
    $(dialogTag).attr("title", title);
    $(dialogTag).append(content);
    $(dialogTag).dialog({
	autoOpen : true,
	height : height,
	width : width,
	modal : true,
	buttons : buttonsObject,
	close : closeButton
    });
};

/**
 * Toggle loading displays/removes loading image and hides button
 */
sai.toggleLoading = function() {
    var button = $("input[type=button]");
    if (button.length > 1) {
	button = button[0];
    }
    if ($("#submitLoad").length > 0) {
	$("#submitLoad").remove();
    } else {
	var submitLoad = document.createElement('div');
	$(submitLoad).attr('id', 'submitLoad');
	var submitLoadImg = document.createElement('img');
	$(submitLoadImg).attr('src', 'assets/images/ajax-loader-bar.gif');
	$(submitLoadImg).attr('alt', 'Loading...');
	$(submitLoadImg).attr('title', 'Loading...');
	$(submitLoad).append(submitLoadImg);
	$(button).parent().append(submitLoad);
    }
    $("input[type=button]").each(function() {
	$(this).toggle();
    });
};

/**
 * Adds error message onto UI, optionally appending after option can be shown
 * 
 * @param {string}
 *                errorMsg - Error message to be displayed out
 * @param {string}
 *                id - `ID` to be given to the div block of the error msg
 * @param {string}
 *                appendAfter - jQuery selector for selecting the element which
 *                should be considered for appending after it
 */
sai.addErrorMessage = function(errorMsg, id, appendAfter) {
    if (id === undefined) {
	id = 'submitMsg';
    }
    if ($('#' + id).length > 0) {
	$('#' + id).remove();
    }
    var errorDiv = document.createElement('div');
    var errorIconSpan = document.createElement('span');
    var errorPDiv = document.createElement('p');
    var errorText = document.createTextNode(errorMsg);
    $(errorDiv).attr({
	id : id
    });
    $(errorIconSpan).addClass('ui-icon ui-icon-alert');
    $(errorIconSpan).css('display', 'inline-block');
    $(errorDiv).addClass('ui-widget ui-state-error');
    $(errorPDiv).append(errorIconSpan).append(errorText);
    $(errorDiv).append(errorPDiv);
    if (appendAfter !== undefined) {
	console.log('Apending error after: ' + appendAfter);
	$(appendAfter).after(errorDiv);
    }
    return errorDiv;
};

/**
 * Function to validate email address using regex expression
 * 
 * @param {string}
 *                email - String of the email address to be validated
 * 
 * @return {Boolean} true/false depending on the status of validity of the email
 */
sai.validateEmail = function(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

/**
 * 
 */
sai.validatePassword = function(password) {
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    return re.test(password);
};

sai.profile = function() {
    /**
     * Initialize the events for Profile Update page
     */
    this.init = function() {
	$("#updateProfile").click(submit);
    };

    /**
     * Hides all the errors displayed/non-displayed on the Profile Update page
     */
    var hideErrors = function() {

    };

    /**
     * Resets all the input fields
     */
    var reset = function() {

    };

    /**
     * 
     */
    var submit = function() {
	hideErrors();
	var isValid = true;
	var id = $("#id").val();
	var email = $("#email").val();
	var fname = $("#fname").val();
	var lname = $("#lname").val();
	var gender = $("#gender_m").is(":checked") ? "m" : "f";
	var website = $("#website").val();
	var phone = $("#phone").val();
	var job_title = $("#job_title").val();
	var about = $("#about").val();
	var company_name = $("#company_name").val();
	var company_url = $("#company_url").val();
	if (isValid) {
	    $
		    .ajax({
			async : true,
			cache : false,
			beforeSend : sai.toggleLoading,
			complete : sai.toggleLoading,
			data : {
			    id : id,
			    email : email,
			    fname : fname,
			    lname : lname,
			    gender : gender,
			    website : website,
			    phone : phone,
			    job_title : job_title,
			    about : about,
			    company_name : company_name,
			    company_url : company_url
			},
			type : "PUT",
			url : 'api/users',
			success : function(response) {
			    console.log(response);
			    if (response.msg !== undefined
				    && response.msg == 'Successfully updated user account') {
				if (response.error_code === undefined) {
				    sai
					    .showDialog(
						    'Updated your profile',
						    'Thank You for updating your profile, we appreciate your contribution towards our community',
						    null,
						    function() {
							window.location = sai.basehref
								+ '/profile';
						    }, 350, 240);
				} else {
				    sai
					    .showDialog(
						    "Something Went Wrong",
						    "<p>"
							    + response.responseJSON.error_msg
							    + "</p>");
				}
			    }
			},
			error : function(response) {
			    sai.showDialog("Something Went Wrong", "<p>"
				    + response.responseJSON.error_msg + "</p>");
			}
		    });
	}
    };
};

/**
 * 
 */
sai.updatepicture = function() {
    /**
     * Initialize the events for Update Display Picture page
     */
    this.init = function() {
	$("#updateDisplayPicture").click(update);
	$("#deleteDisplayPicture").click(deleteSubmit);
    };

    /**
     * Hides all the errors displayed/non-displayed on the Display Picture page
     */
    var hideErrors = function() {

    };

    /**
     * 
     */
    var reset = function() {

    };

    /**
     * 
     */
    var update = function() {
	hideErrors();
	var formData = new FormData();
	var isValid = true;
	var id = $("#id").val();
	if (id == null || id == '') {
	    isValid = false;
	}
	if ($($("input[type=file]")[0].files).length != 1) {
	    isValid = false;
	}
	if (isValid) {
	    $($("input[type=file]")[0].files).each(function(i, file) {
		formData.append('display_picture', file);
	    });
	    $
		    .ajax({
			async : true,
			cache : false,
			beforeSend : sai.toggleLoading,
			complete : sai.toggleLoading,
			data : formData,
			url : 'api/users/changedisplaypicture',
			type : 'POST',
			processData : false,
			contentType : false,
			success : function(response) {
			    if (response.error_code === undefined) {
				sai
					.showDialog(
						'Updated your display picture',
						'Thank you for making sure an updated display picture of yourself, we appreciate your efforts towards our community',
						null,
						function() {
						    window.location = sai.basehref
							    + '/profile';
						});
			    } else {
				sai.showDialog("Something Went Wrong", "<p>"
					+ response.responseJSON.error_msg
					+ "</p>");
			    }
			},
			error : function(response) {
			    sai.showDialog("Something Went Wrong", "<p>"
				    + response.responseJSON.error_msg + "</p>");
			}
		    });
	}
    };

    var deleteSubmit = function() {
	sai
		.showDialog(
			"Confirm Remove Display Picture",
			"Are you sure you want to remove the current display picture?",
			{
			    "yes" : function() {
				$
					.ajax({
					    async : true,
					    cache : false,
					    url : 'api/users/changedisplaypicture',
					    type : 'DELETE',
					    success : function(response) {
						$(this).dialog("close");
						$(this).remove();
						sai
							.showDialog(
								"Successful",
								"Your display picture was successfully removed",
								null,
								function() {
								    window.location = sai.basehref
									    + '/profile';
								}, 350, 250);
					    },
					    error : function(response) {
						$(this).dialog("close");
						$(this).remove();
					    }
					});
			    },
			    "no" : function() {
				$(this).dialog("close");
				$(this).remove();
			    }
			}, null, 350, 250);
    };
};

/**
 * 
 */
sai.recoverpassword = function() {
    /**
     * 
     */
    this.init = function() {
	$("#resetPassword").click(submit);
    };

    /**
     * 
     */
    var hideErrors = function() {

    };

    /**
     * 
     */
    var reset = function() {

    };

    /**
     * 
     */
    var submit = function() {
	var isValid = true;
	var password = $("#password").val();
	var confirm_password = $("#confirm_password").val();
	var token = $("#token").val();
	var uid = $("#user_id").val();
	if (password == undefined || password == null) {
	    isValid = false;
	}
	if (confirm_password == undefined || confirm_password == null) {
	    isValid = false;
	}
	if (token == undefined || token == null || token == '') {
	    isValid = false;
	}
	if (uid == undefined || uid == null || uid == '') {
	    isValid = false;
	}
	if (password != confirm_password) {
	    isValid = false;
	}
	if (!sai.validatePassword(password)
		|| !sai.validatePassword(confirm_password)) {
	    isValid = false;
	}
	if (isValid) {
	    $
		    .ajax({
			ajax : true,
			cache : false,
			beforeSend : sai.toggleLoading,
			complete : sai.toggleLoading,
			data : {
			    password : password,
			    confirm_password : confirm_password,
			    token : token,
			    user_id : uid
			},
			type : "POST",
			url : "api/users/iacvkgj",
			success : function(response) {
			    if (response.error_code === undefined) {
				sai
					.showDialog(
						'Woooo, Resetted',
						'Great, you have successfully recovered your password',
						null,
						function() {
						    window.location = sai.basehref
							    + '/login';
						});
			    } else {
				sai.showDialog("Something Went Wrong", "<p>"
					+ response.responseJSON.error_msg
					+ "</p>");
			    }
			},
			error : function() {
			    sai.showDialog("Something Went Wrong", "<p>"
				    + response.responseJSON.error_msg + "</p>");
			}
		    });
	}
    };
};

/**
 * 
 */
sai.forgotpassword = function() {
    /**
     * Initialize the events for Forgot Password page
     */
    this.init = function() {
	$("#forgotPasswordSubmit").click(submit);
    };

    /**
     * 
     */
    var hideErrors = function() {

    };

    /**
     * 
     */
    var reset = function() {

    };

    /**
     * 
     */
    var submit = function() {
	var isValid = true;
	var email = $("#email").val();
	if (email == undefined || email == null || !sai.validateEmail(email)) {
	    isValid = false;
	}
	if (isValid) {
	    $
		    .ajax({
			ajax : true,
			cache : false,
			beforeSend : sai.toggleLoading,
			complete : sai.toggleLoading,
			data : {
			    email : email
			},
			type : "POST",
			url : "api/users/forgotpassword",
			success : function(response) {
			    if (response.error_code === undefined) {
				sai
					.showDialog(
						'Reset Instructions Mailed',
						'Please check your mailbox for password reset instructions',
						null,
						function() {
						    window.location = sai.basehref
							    + '/index';
						});
			    } else {
				sai.showDialog("Something Went Wrong", "<p>"
					+ response.responseJSON.error_msg
					+ "</p>");
			    }
			},
			error : function() {
			    sai.showDialog("Something Went Wrong", "<p>"
				    + response.responseJSON.error_msg + "</p>");
			}
		    });
	}
    };
};

/**
 * 
 */
sai.changepassword = function() {
    /**
     * Initialize the events for Change Password page
     */
    this.init = function() {
	$("#changePasswordNow").click(submit);
	$("#cancelNow").click(function() {
	    window.location = sai.basehref + '/profile';
	});
    };

    /**
     * Hides all the errors displayed/non-displayed on the change password page
     */
    var hideErrors = function() {

    };

    /**
     * 
     */
    var reset = function() {

    };

    /**
     * 
     */
    var submit = function() {
	hideErrors();
	var isValid = true;
	var current_password = $("#current_password").val();
	var new_password = $("#new_password").val();
	var new_confirm_password = $("#new_confirm_password").val();
	if (current_password == null || current_password == ''
		|| new_password == null || new_password == ''
		|| new_confirm_password == null || new_confirm_password == '') {
	    isValid = false;
	}
	if (isValid) {
	    $
		    .ajax({
			ajax : true,
			cache : false,
			beforeSend : sai.toggleLoading,
			complete : sai.toggleLoading,
			data : {
			    current_password : current_password,
			    new_password : new_password,
			    new_confirm_password : new_confirm_password
			},
			type : "POST",
			url : "api/users/changepassword",
			success : function(response) {
			    if (response.error_code === undefined) {
				sai
					.showDialog(
						'Updated your password',
						'We appreciate your efforts for making sure your security by keeping an up-to-date password, Thank You',
						null,
						function() {
						    window.location = sai.basehref
							    + '/profile';
						});
			    } else {
				sai.showDialog("Something Went Wrong", "<p>"
					+ response.responseJSON.error_msg
					+ "</p>");
			    }
			},
			error : function() {
			    sai.showDialog("Something Went Wrong", "<p>"
				    + response.responseJSON.error_msg + "</p>");
			}
		    });
	}
    };
};

sai.login = function() {
    var email = '';
    var username = '';
    var password = '';

    /**
     * Initialize the events for Login page
     */
    this.init = function() {
	$("#loginNow").click(submit);
	$("#username_email").keyup(function(event) {
	    if (event.keyCode == 13) {
		$("#loginNow").click();
	    }
	});
	$("#password").keyup(function(event) {
	    if (event.keyCode == 13) {
		$("#loginNow").click();
	    }
	});
    };

    /**
     * 
     */
    var loadData = function() {
	if ($("#username_email").val().indexOf("@") == -1) {
	    username = $("#username_email").val();
	} else {
	    email = $("#username_email").val();
	}
	password = $("#password").val();
    };

    /**
     * 
     */
    var reset = function() {
	$("#username_email").val('');
	$("#password").val('');
    };

    /**
     * 
     */
    var submit = function() {
	var isValid = true;
	loadData();
	if (isValid) {
	    $.ajax({
		async : true,
		cache : false,
		beforeSend : sai.toggleLoading,
		complete : sai.toggleLoading,
		data : {
		    email : email,
		    username : username,
		    password : password
		},
		type : "POST",
		url : 'api/auth',
		success : function(response) {
		    if (response.msg !== undefined
			    && response.msg == 'Successful') {
			reset();
			window.location = sai.basehref + '/profile';
		    } else if (response.error_msg !== undefined) {
			sai.showDialog("Something Went Wrong", "<p>"
				+ response.error_msg + "</p>");
		    }
		},
		error : function(response) {
		    sai.showDialog("Something Went Wrong", "<p>"
			    + response.responseJSON.error_msg + "</p>");
		}
	    });
	}
    };
};

/**
 * 
 */
sai.reachOut = function() {
    /**
     * Initialize the events for Reach Out page
     */
    this.init = function() {
	$("#name").click(hideErrors);
	$("#email").click(hideErrors);
	$("#message").click(hideErrors);
	$("#submitContact").click(submit);
    };

    /**
     * Hides all the errors displayed/non-displayed on the UI for reach out page
     */
    var hideErrors = function() {
	$("#nameError").hide('fast');
	$("#emailError").hide('fast');
	$("#messageError").hide('fast');
    };

    /**
     * Resets all the fields on the reach put page
     */
    var reset = function() {
	hideErrors();
	$("#name").val('');
	$("#email").val('');
	$("#message").val('');
    };

    /**
     * Submits the reach out form. In case of any errors they are displayed
     * accordingly. On Success it updates the message.
     */
    var submit = function() {
	hideErrors();
	var isValidSubmit = true;
	var name = $("#name").val();
	var email = $("#email").val();
	var message = $("#message").val();
	if (name == undefined || name == null || name.length < 3) {
	    $("#nameError").show('fast');
	    isValidSubmit = false;
	    console.log("Name Error seen");
	}
	if (email == undefined || email == null || !sai.validateEmail(email)) {
	    $("#emailError").show('fast');
	    isValidSubmit = false;
	    console.log("Email Error seen");
	}
	if (message == undefined || message == null || message.length < 15) {
	    $("#messageError").show('fast');
	    isValidSubmit = false;
	    console.log("Message Error seen");
	}
	if (isValidSubmit) {
	    $.ajax({
		async : true,
		cache : false,
		beforeSend : sai.toggleLoading,
		complete : sai.toggleLoading,
		data : {
		    name : name,
		    email : email,
		    message : message,
		    submit : true
		},
		type : "POST",
		url : 'api/contactus',
		success : function(response) {
		    if (response.status == 'OK') {
			reset();
		    }
		    sai.showDialog("Success", "<p>" + response.msg + "</p>");
		}
	    });
	}
    };
};

/**
 * Registration JavaScript Class
 * 
 */
sai.register = function() {
    /**
     * 
     */
    this.init = function() {
	$("#fname").click(hideErrors);
	$("#lname").click(hideErrors);
	$("#password").click(hideErrors);
	$("#email").click(hideErrors);
	$("#job_title").click(hideErrors);
	$("#company_name").click(hideErrors);
	$("#phone").click(hideErrors);
	$("#registerNow").click(submit);
    };

    /**
     * Hides all the errors displayed/non-displayed on the UI for register page
     */
    var hideErrors = function() {
	$("#fnameError").hide('fast');
	$("#lnameError").hide('fast');
	$("#passwordError").hide('fast');
	$("#emailError").hide('fast');
    };

    /**
     * Resets all the fields on register page
     */
    var reset = function() {
	hideErrors();
	$("#fname").val('');
	$("#lname").val('');
	$("#email").val('');
	$("#password").val('');
	$("#job_title").val('');
	$("#company_name").val('');
	$("#phone").val('');
	$("input[type=checkbox]").each(function() {
	    $(this).attr('checked', false);
	});
    };

    /**
     * Perform submit event for registration
     */
    var submit = function() {
	hideErrors();
	var isValidSubmit = true;
	var fname = $("#fname").val();
	var lname = $("#lname").val();
	var email = $("#email").val();
	var password = $("#password").val();
	var job_title = $("#job_title").val();
	var company_name = $("#company_name").val();
	var phone = $("#phone").val();
	var causes = [];
	$("input[type=checkbox]").each(function() {
	    if ($(this).is(':checked')) {
		causes.push($(this).val());
	    }
	});
	if (fname === undefined || fname === null || fname.length < 1) {
	    $("#fnameError").show('fast');
	    isValidSubmit = false;
	}
	if (lname === undefined || lname === null || lname.length < 1) {
	    $("#lnameError").show('fast');
	    isValidSubmit = false;
	}
	if (email === undefined || email === null || !sai.validateEmail(email)) {
	    $("#emailError").show('fast');
	    isValidSubmit = false;
	}
	if (password === undefined || password === null
		|| !sai.validatePassword(password)) {
	    $("#passwordError").show('fast');
	    isValidSubmit = false;
	}
	if (isValidSubmit) {
	    $
		    .ajax({
			async : true,
			cache : false,
			beforeSend : sai.toggleLoading,
			complete : sai.toggleLoading,
			type : "POST",
			url : 'api/users',
			data : {
			    fname : fname,
			    lname : lname,
			    email : email,
			    password : password,
			    job_title : job_title,
			    company_name : company_name,
			    phone : phone
			},
			success : function(response) {
			    if (response.error_msg !== undefined) {
				sai.showDialog("Something went wrong", "<p>"
					+ response.error_msg + "</p>", null,
					function() {
					    $(this).dialog("close");
					    $(this).remove();
					}, 350, 250);
			    } else if (response.id !== undefined
				    && response.id > 0) {
				for ( var causeIdx in causes) {
				    $.ajax({
					async : true,
					cache : false,
					url : 'api/users/causes',
					type : 'POST',
					data : {
					    user_id : response.id,
					    cause_id : causes[causeIdx]
					}
				    });
				}
				sai
					.showDialog(
						"Registration Successful",
						"<p style='text-align: center'>We are just one step away from completing your registration. Please check your inbox/spam box for a verification email we just sent you for confirmation.<p>",
						null,
						function() {
						    $(this).dialog("close");
						    $(this).remove();
						    window.location = sai.basehref
							    + '/index';
						});
			    }
			},
			error : function(response) {
			    console.log(response.responseJSON.error_msg);
			    sai.showDialog("Something Went Wrong", "<p>"
				    + response.responseJSON.error_msg + "</p>");
			}
		    });
	}
    };
};

/**
 * 
 */
sai.menus = function() {
    this.init = function() {
	if ($("select#parent_menu").length > 0) {
	    loadParents();
	}
    };
    var loadParents = function() {
	$.ajax({
	    async : true,
	    cache : false,
	    url : sai.apihref + "/menus/parents",
	    type : "GET",
	    success : function(response) {
		if (response.error_msg === undefined) {
		    var selected = $("select#parent_menu").val();
		    $("select#parent_menu").children("option").remove();
		    $("select#parent_menu").append($('<option value="0">'));
		    for ( var i in response) {
			var option = $("<option>");
			$(option).attr("value", response[i].id);
			$(option).html(response[i].label);
			if (selected == response[i].id) {
			    $(option).attr("selected", "selected");
			}
			$("select#parent_menu").append(option);
		    }
		}
	    }
	});
    };

};

/**
 * 
 */
sai.configs = function() {
    this.init = function() {
	if ($("#oftype").length > 0) {
	    $("#oftype").change(changeoptioninput);
	}
    };

    var changeoptioninput = function() {
	if ($(this)[0].value == 'list') {
	    if ($("#listOptions").children().length == 0) {
		var label = $("<label>").attr("for", "options").html("Options");
		var input = $("<input>").attr({
		    type : "text",
		    id : "options",
		    name : "options",
		    placeholder : "(ex: one|two)",
		    class : "form-control"
		});
		$("#listOptions").append(label).append(input);
	    }
	}
	if ($(this)[0].value == 'boolean' || $(this)[0].value == 'text') {
	    $("#listOptions").children().remove();
	}
	if ($(this)[0].value == 'boolean') {
	    if ($("#valueBox div") > 0) {
		return;
	    }
	    $("#valueBox input").remove();
	    $("#valueBox").append(
		    $("<div>").attr("class", "radio").append(
			    $("<label>").attr({
				'for' : 'yesValue',
				'class' : 'col-md-6 col-xs-6'
			    }).append($("<input>").attr({
				type : 'radio',
				'name' : 'value',
				'id' : 'yesValue',
				value : 'yes'
			    })).append("<span>Yes</span>")).append(
			    $("<label>").attr({
				'for' : 'noValue',
				'class' : 'col-md-6 col-xs-6'
			    }).append($("<input>").attr({
				type : 'radio',
				'name' : 'value',
				'id' : 'noValue',
				value : 'no'
			    })).append("<span>No</span>")));
	} else {
	    if ($("#valueBox input[type=text]").length > 0) {
		return;
	    }
	    $("#valueBox div").remove();
	    $("#valueBox").append($("<input>").attr({
		class : "form-control",
		type : "text",
		name : "value",
		id : "value",
		placeholder : "(ex: config_value)"
	    }));
	}
    };
};


/**
 * 
 */
sai.loadFNameAutocomplete = function(selector, target_selector) {
    if (selector === undefined) {
	selector = "#fname";
    }
    if (target_selector === undefined) {
	target_selector = "#user_id";
    }
    $(selector)
	    .autocomplete(
		    {
			source : function(request, response) {
			    $.ajax({
				url : sai.apihref + "/users/search",
				type : "POST",
				data : {
				    condition : "AND",
				    data : {
					fname : request.term,
					is_active : 1
				    }
				},
				success : function(d) {
				    response(d.results);
				}
			    });
			},
			minLength : 2,
			select : function(event, ui) {
			    event.preventDefault();
			    if (ui.item) {
				$(target_selector).val(ui.item.id);
				$(selector).val(
					ui.item.fname + " " + ui.item.lname);
				$("#userpreview").children().remove();
				$("#userpreview")
					.append(
						"<h3>" + ui.item.fname + " "
							+ ui.item.lname
							+ "</h3>")
					.append(
						$("<div>").addClass(
							"row col-md-12"))
					.append(
						$("<div>")
							.addClass("col-md-4")
							.append(
								$("<img>")
									.addClass(
										"img-responsive img-rounded")
									.attr(
										"src",
										ui.item.display_picture)))
					.append(
						$("<div>")
							.addClass("col-md-8")
							.append(
								"<small>"
									+ "Eduaction: </small>"
									+ ui.item.education)
							.append(
								"<small>"
									+ "<br/>Gender: </small>"
									+ ui.item.gender));
			    }
			},
			open : function() {

			},
			close : function() {

			}
		    }).autocomplete("instance")._renderItem = function(ul, item) {
	return $("<li>").append(
		"<a><img src=\"" + item.display_picture
			+ "\" style=\"width:32px;vertical-align:middle\" /> "
			+ item.fname + " " + item.lname + "</a>").appendTo(ul);
    };
};

/**
 * 
 */
sai.revertBackObject = function() {
    this.init = function() {
	if ($("#retrieveBack").length > 0) {
	    $("#retrieveBack").click(submit);
	}
    };
    var submit = function() {
	if ($("#is_active").length > 0) {
	    $("#is_active").val(1);
	}
	$($("button[type=submit]")[0]).click();
    };
};

/**
 * 
 */
sai.deleteOrRestoreItems = function() {
    var itemLabel = "";
    var itemName = "";
    var objectName = "";

    var loadData = function(obj) {
	itemName = $(obj).data('id');
	itemLabel = $(obj).data('label');
	objectName = $(obj).data('module');
    };
    var confirmDelete = function() {
	$
		.ajax({
		    async : true,
		    cache : false,
		    url : sai.apihref + "/" + objectName + "/" + itemName,
		    type : "DELETE",
		    success : function(response) {
			bootbox
				.dialog({
				    message : "<div class=\"alert alert-success\"><span class=\"glyphicon glyphicon-ok\"></span> Successfully, deleted <code>"
					    + itemLabel
					    + "</code> from <code>"
					    + objectName + "</code></div>",
				    title : "Done",
				    buttons : {
					ok : {
					    label : "<span class=\"glyphicon glyphicon-ok\"></span> OK",
					    className : "btn-primary",
					    callback : function() {
						location.reload();
					    }
					}
				    }
				});
		    },
		    error : function(response) {
			console.log(response);
		    }
		});
    };
    var deleteClick = function() {
	loadData(this);
	bootbox
		.dialog({
		    message : "Are you sure to delete <code>" + itemLabel
			    + "</code> from <code>" + objectName + "</code>?",
		    title : "Delete Confirmation",
		    buttons : {
			no : {
			    label : "<span class=\"glyphicon glyphicon-heart\"></span> No!",
			    className : "btn-primary",
			    callback : function() {
			    }
			},
			yes : {
			    label : "<span class=\"glyphicon glyphicon-trash\"></span> Yes!",
			    className : "btn-danger",
			    callback : confirmDelete
			}
		    }
		});
    };
    var confirmRestore = function() {
	$
		.ajax({
		    async : true,
		    cache : false,
		    url : sai.apihref + "/" + objectName + "/" + itemName,
		    type : "PUT",
		    data : JSON.stringify({
			is_active : 1
		    }),
		    contentType : "application/json",
		    success : function(response) {
			bootbox
				.dialog({
				    message : "<div class=\"alert alert-success\"><span class=\"glyphicon glyphicon-ok\"></span> Successfully, restored back <code>"
					    + itemLabel
					    + "</code> from <code>"
					    + objectName + "</code></div>",
				    title : "Done",
				    buttons : {
					ok : {
					    label : "<span class=\"glyphicon glyphicon-ok\"></span> OK",
					    className : "btn-primary",
					    callback : function() {
						location.reload();
					    }
					}
				    }
				});
		    },
		    error : function(response) {
			console.log(response);
		    }
		});
    };
    var restoreClick = function(event) {
	loadData(this);
	bootbox
		.dialog({
		    message : "So, sure to restore <code>" + itemLabel
			    + "</code> from <code>" + objectName + "</code>?",
		    title : "Restoration Confirmation",
		    buttons : {
			yes : {
			    label : "<span class=\"glyphicon glyphicon-heart\"></span> Yes!",
			    className : "btn-success",
			    callback : confirmRestore
			},
			no : {
			    label : "<span class=\"glyphicon glyphicon-thumbs-down\"></span> No!",
			    className : "btn-primary",
			    callback : function() {
			    }
			}
		    }
		});
    };
    this.init = function(event, index) {
	if ($("button.btn-danger").length > 0) {
	    $("button.btn-danger").each(function(index) {
		$(this).click(deleteClick);
	    });
	}
	if ($("button.btn-success").length > 0) {
	    $("button.btn-success").each(function(index) {
		$(this).click(restoreClick);
	    });
	}
    };
};

/**
 * 
 */
sai.anytranslations = function() {
    var module;
    var inputTypes;
    this.init = function() {
	module = $("#createEditForm").attr("name");
	$(".input-group-addon .glyphicon-globe").click(click);
	inputTypes = [ "text", "textarea", "editor" ];
    };

    var click = function() {
	var inputType = "text";
	if ($(this).parent().parent().children("input").length > 0) {
	    var element = $(this).parent().parent().children("input")[0];
	} else if ($(this).parent().parent().children(".mce-tinymce").length > 0) {
	    var element = $(this).parent().parent().children("textarea")[0];
	    inputType = "editor";
	} else if ($(this).parent().parent().children("textarea").length > 0) {
	    var element = $(this).parent().parent().children("textarea")[0];
	    inputType = "textarea";
	}
	var id = $("#id").val();
	var column = $(element).attr("name");
	var original = $(element).val();
	var divTag = $('<div class="row form-horizontal">');
	/**
	 * Object Name
	 */
	var formGroupTag = $('<div class="form-group col-md-5">');
	var labelTag = $(
		'<label for="translate_object_name" class="col-md-6 control-label">')
		.html("Object Name");
	var inputTag = $('<input type="hidden" name="translate_object_name" id="translate_object_name" value="'
		+ module + '">');
	var pTag = $('<p class="form-control-static">' + module + '</p>')
	$(formGroupTag).append(labelTag).append(
		$('<div class="col-md-6">').append(pTag).append(inputTag));
	$(divTag).append(formGroupTag);

	/**
	 * Object Column
	 */
	var formGroupTag = $('<div class="form-group col-md-7">');
	var labelTag = $(
		'<label for="translate_object_column" class="col-md-5 control-label">')
		.html("Object Column");
	var inputTag = $('<input type="hidden" name="translate_object_column" id="translate_object_column" value="'
		+ column + '">');
	var pTag = $('<p class="form-control-static">' + column + '</p>')
	$(formGroupTag).append(labelTag).append(
		$('<div class="col-md-7">').append(pTag).append(inputTag));
	$(divTag).append(formGroupTag);

	/**
	 * Object ID
	 */
	var formGroupTag = $('<div class="form-group col-md-4">');
	var labelTag = $(
		'<label for="translate_objectid" class="col-md-7 control-label">')
		.html("Object ID");
	var inputTag = $('<input type="hidden" name="translate_objectid" id="translate_objectid" value="'
		+ id + '">');
	var pTag = $('<p class="form-control-static">' + id + '</p>')
	$(formGroupTag).append(labelTag).append(
		$('<div class="col-md-5">').append(pTag).append(inputTag));
	$(divTag).append(formGroupTag);

	/**
	 * Object Key
	 */
	var formGroupTag = $('<div class="form-group col-md-8">');
	var labelTag = $(
		'<label for="translate_object_key" class="col-md-4 control-label">')
		.html("Object Key");
	var inputTag = $('<input type="hidden" name="translate_object_key" id="translate_object_key" value="'
		+ module + "-" + column + "-" + id + '">');
	var pTag = $('<p class="form-control-static">' + module + "-" + column
		+ "-" + id + '</p>')
	$(formGroupTag).append(labelTag).append(
		$('<div class="col-md-8">').append(pTag).append(inputTag));
	$(divTag).append(formGroupTag);

	/**
	 * Type
	 */
	var formGroupTag = $('<div class="form-group col-md-6">');
	var labelTag = $(
		'<label for="translate_type" class="col-md-4 control-label">')
		.html('Type');
	var pTag = $('<p class="form-control-static">');
	$(pTag).html(inputType);
	var inputTag = $('<input type="hidden" name="translate_type" id="translate_type" value="'
		+ inputType + '">');

	$(formGroupTag).append(labelTag).append(
		$('<div class="col-md-8">').append(pTag).append(inputTag));
	$(divTag).append(formGroupTag);

	/**
	 * Locale
	 */
	var formGroupTag = $('<div class="form-group col-md-6">');
	var labelTag = $(
		'<label for="translate_locale" class="col-md-4 control-label">')
		.html("Locale");
	var inputTag = $('<select class="form-control" name="translate_locale" id="translate_locale">');
	for ( var i in sai.languages) {
	    if (sai.languages[i] != 'en-US') {
		inputTag.append($('<option value="' + sai.languages[i] + '">')
			.html(sai.languages[i]));
	    }
	}
	$(formGroupTag).append(labelTag).append(
		$('<div class="col-md-8">').append(inputTag));
	$(divTag).append(formGroupTag);

	/**
	 * Original String
	 */
	var formGroupTag = $('<div class="form-group col-md-12">');
	var labelTag = $(
		'<label for="translate_original" class="control-label">').html(
		"Original");
	var inputTag = $('<input type="hidden" name="translate_original" id="translate_original" value="'
		+ original + '">');
	var pTag = $('<p class="form-control-static">' + original + '</p>')
	$(formGroupTag).append(labelTag).append(pTag).append(inputTag);
	$(divTag).append(formGroupTag);

	/**
	 * Translate
	 */
	var formGroupTag = $('<div class="form-group col-md-12">');
	var labelTag = $(
		'<label for="translate_translated" class="control-label">')
		.html("Translation");
	if (inputType == 'text') {
	    var inputTag = $('<input type="text" class="form-control" name="translate_translated" id="translate_translated">');
	} else if (inputType == 'textarea' || inputType == 'editor') {
	    var inputTag = $('<textarea class="form-control" name="translate_translated" id="translate_translated">');
	}
	$(formGroupTag).append(labelTag).append(inputTag);
	$(divTag).append(formGroupTag);
	bootbox
		.dialog({
		    message : divTag,
		    title : "Any Translate",
		    buttons : {
			save : {
			    label : '<span class="glyphicon glyphicon-floppy-save"></span> Save',
			    className : "btn-primary",
			    callback : function() {
				var fd = {};
				$("[name^='translate_']").each(
					function() {
					    var formIndex = $(this)
						    .attr('name').replace(
							    "translate_", "");
					    fd[formIndex] = $(this).val();
					});
				if (inputType == 'editor') {
				    fd.translated = tinymce.get(
					    "translate_translated")
					    .getContent();
				}
				console.log(fd);
				if (fd['translated'] == undefined
					|| fd.translated == '') {
				    bootbox
					    .alert("Please fill in translated value");
				    return;
				}
				$
					.ajax({
					    type : 'async',
					    cache : false,
					    url : sai.basehref
						    + '/api/translations',
					    type : 'POST',
					    data : fd,
					    success : function(response) {
						if (response.id !== undefined) {
						    bootbox
							    .alert("Done, created translation successfully");
						    return;
						}
					    }
					});
			    }
			}
		    },
		    className : "modal-form"
		});

	if (inputType == 'editor') {
	    new sai.tinymce()
		    .init("#translate_translated", sai.externalplugins);
	}
    };
};

/**
 * 
 */
sai.translations = function() {
    this.init = function() {
	if ($("#type").length > 0 && $("#original").length > 0
		&& $("#translated").length > 0) {
	    $("#type").change(change);
	}
    };

    var change = function() {
	$("#original").remove();
	$("#translated").remove();
	$(".mce-container").remove();
	if ($(this).val() == "text") {
	    $("label[for=original]").after($("<input>").attr({
		type : "text",
		id : "original",
		name : "original",
		class : "form-control",
		placeholder : "(ex: Original String)"
	    }));
	    $("label[for=translated]").after($("<input>").attr({
		type : "text",
		id : "translated",
		name : "translated",
		class : "form-control",
		placeholder : "(ex: Translated String)"
	    }));
	} else if ($(this).val() == "textarea" || $(this).val() == "editor") {
	    $("label[for=original]").after($("<textarea>").attr({
		id : "original",
		name : "original",
		class : "form-control",
		placeholder : "(ex: Original String)"
	    }));
	    $("label[for=translated]").after($("<textarea>").attr({
		id : "translated",
		name : "translated",
		class : "form-control",
		placeholder : "(ex: Translated String)"
	    }));
	}
	if ($(this).val() == "editor") {
	    new sai.tinymce().init("#original", sai.externalplugins);
	    new sai.tinymce().init("#translated", sai.externalplugins);
	}
    };

};

/**
 * 
 */
sai.workexperience = function() {

    this.init = function() {
	if ($("#is_present_company").length > 0) {
	    $("#is_present_company").click(ispresentcompanyCheck);
	}
    };

    var ispresentcompanyCheck = function() {
	if ($(this).is(":checked") == true) {
	    $("#end_month").val("");
	    $("#end_year").val("");
	    $("#end_date").hide();
	} else {
	    $("#end_date").show();
	}
    };
};

/**
 * 
 */
sai.tinymce = function() {
    this.init = function(selector, externalplugins) {
	if (selector.startsWith("#")) {
	    var s_s = selector.substr(1);
	    for ( var i in tinymce.editors) {
		if (tinymce.editors[i].id == s_s) {
		    tinymce.editors[i].remove();
		}
	    }
	}
	tinymce
		.init({
		    selector : selector,
		    external_plugins : externalplugins,
		    plugins : [
			    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			    "save table contextmenu directionality emoticons template paste textcolor jbimages" ],
		    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image jbimages | print preview media fullpage | forecolor backcolor emoticons",
		    relative_urls : true,
		    remove_script_host : false,
		    document_base_url : sai.basehref + '/'
		});
    };
};
