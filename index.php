<?php

/**
 *
 * @name Index
 * @abstract Index page for WillingTree
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *
 */
require_once 'start.php';
change_response_code(301);
header("Location: index");
?>