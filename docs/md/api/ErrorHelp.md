# Willing Tree Errors Help Document

Errors are expected in any program that could would be developed and hence errors are important. A user may get lost if proper attention towards error is not given. Hence WillingTree also focuses on a better way to display error message and encourage user to perform the request with more appropriate required data.

## Error Structure

* `Error Code` is an unique number which is usually a 4 Digit Number. First two digit comprises of module number and second two comprises of possible unique error number
	* Ex: **1001** 
		* `10` means _Modular Exception_
		* `01` means _Invalid Module Invoked_  
* `Error Message` is usually a well descriptive message telling an user what could have went wrong while service invocation.


## Error Module Dictionary

| Module Prefix Code 	| Module Name     	|
|--------------------	|-----------------	|
| 10                 	| Module          	|
| 11                 	| Session         	|
| 12                 	| Restlizer       	|
| 13                 	| Authentication  	|
| 14                 	| Pages           	|
| 15                 	| Rights          	|
| 16                 	| Roles           	|
| 17                 	| RolesRights     	|
| 18                 	| Users           	|
| 19                 	| Files           	|
| 20                 	| Causes          	|
| 21                 	| Locations       	|
| 22                 	| Trees           	|
| 23                 	| UsersTrees      	|
| 24                 	| UsersTreesState 	|
| 25                 	| Stories         	|
| 26                 	| Search          	|
| 27                 	| UsersRoles      	|
| 28                 	| UsersRights     	|
| 29                 	| UsersCauses     	|


## Common Error Dictionary w.r.t. to Modules

| Error Post-fix Code 	| Error Description          	|
|--------------------	|----------------------------	|
| 01                 	| Required Attribute Missing 	|
| 02                 	| Unique Required            	|
| 03                 	| Create Failed              	|
| 04                 	| Not Found                  	|
| 05                 	| Updated Failed             	|
| 06                 	| Delete Failed              	|

> P.S. These are reserved error post-fix code and new ones should start by 07 and henceforth.

## Errors By Module
---
### Module Exceptions

| Error Code 	| Error Name            	| Error Description                                                                                                        	|
|------------	|-----------------------	|--------------------------------------------------------------------------------------------------------------------------	|
| 1007       	| INVALID_MODULE        	| Oops, something seems wrong, you called a wrong URL                                                                      	|
| 1008       	| MODULE_NOT_AVAILABLE  	| Oops, sorry this module is currently not available                                                                       	|
| 1009       	| SERVICE_NOT_AVAILABLE 	| Oops, sorry this service is currently not available                                                                      	|
| 1010       	| SERVICE_NOT_ALLOWED   	| Oops, sorry this service is not allowed                                                                                  	|
| 1011       	| INVALID_METHOD        	| Oops, looks like that HTTP Request Method is not recognised by us, please use valid method's like GET, POST, PUT, DELETE 	|


### Session Exceptions

| Error Code 	| Error Name              	| Error Description                           	|
|------------	|-------------------------	|---------------------------------------------	|
| 1107       	| INVALID_SESSION_CREATED 	| Oops, Invalid session identified            	|
| 1108       	| CLONE_NOT_ALLOWED       	| Damn, cloning of session object not allowed 	|


### Restlizer Exceptions

| Error Code 	| Error Name   	| Error Description                                                    	|
|------------	|--------------	|----------------------------------------------------------------------	|
| 1207       	| INVALID_JSON 	| Oops, looks like the provided input JSON got some error or mistypo's 	|
| 1208       	| INVALID_XML  	| Oops, looks like the provided input XML got some error or mistypo's  	|


### Auth Exceptions

| Error Code 	| Error Name                 	| Error Description                                                                                      	|
|------------	|----------------------------	|--------------------------------------------------------------------------------------------------------	|
| 1307       	| NO_USER_ACCOUNT_ASSOCIATED 	| Oops, No user account associated with your email address                                               	|
| 1308       	| INVALID_EMAIL_PASSWORD     	| Oops, Invalid email/password combination                                                               	|
| 1309       	| AWAITING_ACTIVATION        	| Oops, that was an invalid email/password combination                                                   	|
| 1310       	| AUTHENTICATION_REQUIRED    	| Oops, we require authenticated user to access this service                                             	|
| 1311       	| INVALID_EMAIL              	| Oops, we require a valid email address                                                                 	|
| 1312       	| REQUIRED_ATTR_MISSING      	| Oops missed something, required fields are Email & Password                                            	|
| 1313       	| ACCOUNT_DELETED            	| Oops, this account is in delete state but can be recovered back, please contact our Site Administrator 	|


### Pages Exceptions

| Error Code 	| Error Name            	| Error Description                                                                            	|
|------------	|-----------------------	|----------------------------------------------------------------------------------------------	|
| 1401       	| REQUIRED_ATTR_MISSING 	| Oops, looks something got missed out page creation requires Title & Content                  	|
| 1402       	| UNIQUE                	| Oops, looks like that title for page is already being used, can you please choose some other 	|
| 1403       	| CREATE_FAILED         	| Oops, looks something went wrong while page creation                                         	|
| 1404       	| NOT_FOUND             	| Oops, looks like the page you are trying to find is not available                            	|
| 1405       	| UPDATE_FAILED         	| Oops, looks like something went wrong while updating page                                    	|
| 1406       	| DELETE_FAILED         	| Oops, looks like something went wrong while deleting page                                    	|
| 1407       	| NO_PAGES_CREATED      	| Oops, looks like no pages created or in active state                                         	|


### Rights Exceptions

| Error Code 	| Error Name            	| Error Description                                                                            	|
|------------	|-----------------------	|----------------------------------------------------------------------------------------------	|
| 1501       	| REQUIRED_ATTR_MISSING 	| Oops, looks something got missed out Right creation requires Name & Description              	|
| 1502       	| UNIQUE                	| Oops, looks like that Name for Right is already being used, can you please choose some other 	|
| 1503       	| CREATE_FAILED         	| Oops, looks like something went wrong while right creation                                   	|
| 1504       	| NOT_FOUND             	| Oops, looks like the Right you are trying to find is not available                           	|
| 1505       	| DELETE_FAILED         	| Oops, looks like something went wrong while deleting the right                               	|
| 1506       	| UPDATE_FAILED         	| Oops, looks like something went wrong while updating the right                               	|
| 1507       	| NO_RIGHTS_CREATED     	| Oops, looks like no rights created or in active state yet :(                                 	|


### Roles Exceptions

| Error Code 	| Error Name            	| Error Description                                                                           	|
|------------	|-----------------------	|---------------------------------------------------------------------------------------------	|
| 1601       	| REQUIRED_ATTR_MISSING 	| Oops, looks something got missed out Role creation requires Name & Description              	|
| 1602       	| UNIQUE                	| Oops, looks like that Name for role is already being used, can you please choose some other 	|
| 1603       	| CREATE_FAILED         	| Oops, looks like something went wrong while creating role                                   	|
| 1604       	| NOT_FOUND             	| Oops, looks like the role you are trying to find is not available                           	|
| 1605       	| UPDATE_FAILED         	| Oops, looks like something went wrong while updating role                                   	|
| 1606       	| DELETE_FAILED         	| Oops, looks like somethign went wrong while deleting role                                   	|
| 1607       	| NO_ROLES_CREATED      	| Oops, looks like no roles created or in active state                                        	|


### Roles Rights Exceptions

| Error Code 	| Error Name            	| Error Description                                                                                      	|
|------------	|-----------------------	|--------------------------------------------------------------------------------------------------------	|
| 1701       	| REQUIRED_ATTR_MISSING 	| Oops, looks something got missed out, link right with role requires each of its id or array of details 	|
| 1703       	| CREATE_FAILED         	| Oops, looks something went wrong while link right with role                                            	|
| 1704       	| NOT_FOUND             	| Oops, looks like there are no rights linked with role                                                  	|
| 1706       	| DELETE_FAILED         	| Oops, failed to remove linkage of right with role                                                      	|
| 1707       	| INVALID_RIGHT         	| Oops, looks like right details found is not valid                                                      	|
| 1708       	| INVALID_ROLE          	| Oops, looks like role details found is not valid                                                       	|


### Users

| Error Code 	| Error Name                         	| Error Description                                                                                                                                    	|
|------------	|------------------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------	|
| 1801       	| REQUIRED_ATTR_MISSING              	| Oops missed something, required fields are First Name, Last Name, Email, Password                                                                    	|
| 1802       	| UNIQUE                             	| Looks like we already have an user with that email, please choose some other email address                                                           	|
| 1803       	| CREATE_FAILED                      	| Oops, something went wrong while creating user                                                                                                       	|
| 1804       	| NOT_FOUND                          	| Oops, we were not able to find the required user                                                                                                     	|
| 1805       	| UPDATE_FAILED                      	| Oops, something went wrong while updating user                                                                                                       	|
| 1806       	| DELETE_FAILED                      	| Oops, something went wrong while deleting the user                                                                                                   	|
| 1807       	| INVALID_EMAIL_ADDRESS              	| Oops, wrong email address, can you please provide valid email address so we can be in touch with you                                                 	|
| 1808       	| INVALID_FNAME                      	| Oops, bad First Name, can you please provide your valid first name so we understand you better                                                       	|
| 1809       	| INVALID_LNAME                      	| Oops, bad Last Name, can you lease provide your valid last name so we understand you better                                                          	|
| 1810       	| INVALID_PASSWORD                   	| Oops, bad password, can you please choose a strong password which is min. 8 chars and contains atleast one small letter, capital letter and a number 	|
| 1811       	| INVALID_JOB_TITLE                  	| Oops, bad job title , can you please provide a valid job title inorder to better understand your work                                                	|
| 1812       	| VERIFY_EMAIL_SEND_FAILED           	| Oops, something went wrong on our side. Your account was created but we were not able to send verification email                                     	|
| 1813       	| CURRENT_PASSWORD_REQ               	| Oops, current password is required to perform password change                                                                                        	|
| 1814       	| CURRENT_PASSWORD_NOT_VALID         	| Oops, looks like current password you entered doesn't match actually                                                                                 	|
| 1815       	| NEW_CONFIRM_PASSWORD_REQ           	| Oops, looks like you missed passing new password and its confirmation password                                                                       	|
| 1816       	| NEW_CONFIRM_PASSWORD_NOT_VALID     	| Oops, looks like new password and its confirm password doesn't match                                                                                 	|
| 1817       	| CHANGE_DISPLAY_PICTURE_NOT_ALLOWED 	| Oops, looks like display picture service doesn't accepts GET request                                                                                 	|
| 1818       	| CHANGE_PASSWORD_NOT_ALLOWED        	| Oops, looks like change password service only accepts POST request                                                                                   	|
| 1819       	| FORGOT_PASSWORD_NOT_ALLOWED        	| Oops, looks like forgot password service only accepts POST request                                                                                   	|
| 1820       	| FORGOT_PASSWORD_EMAIL_REQ          	| Oops, we didn't get your email address, can you please submit valid email address                                                                    	|
| 1821       	| FORGOT_INVALID_EMAIL_ADDRESS       	| Oops, we need your valid email address to help you reset your password                                                                               	|
| 1822       	| FORGOT_EMAIL_NOT_REGISTERED        	| Oops, we didn't find any account associated with this email address, can you please try registering instead                                          	|
| 1823       	| FORGOT_AUTHENTICATED               	| Oops, looks like forgot password service is not allowed for already authenticated user                                                               	|
| 1824       	| FORGOT_SEND_FAILED                 	| Oops, something went wrong while sending forgot password email, can you please try again in a while                                                  	|
| 1825       	| FORGOT_INVALID_LINK                	| Oops, looks like the link used is invalid. Please enter your email below to receive new link                                                         	|
| 1826       	| FORGOT_REQUEST_NOT_ALLOWED         	| Oops, looks like forgot password reset is only allowed for POST request                                                                              	|
| 1827       	| RECOVER_PASSWORD_MISSING           	| Oops, but we require you to give new password                                                                                                        	|
| 1828       	| RECOVER_PASSWORD_CONFIRM_MISSING   	| Oops, but we require you to re-enter your new password                                                                                               	|
| 1829       	| RECOVER_TOKEN_MISSING              	| Oops, but we require you to provide with the password reset token                                                                                    	|
| 1830       	| RECOVER_USER_MISSING               	| Oops, but we require you to provide with user id or email                                                                                            	|
| 1831       	| RECOVER_TOKEN_INVALID              	| Oops, token details passed are not valid                                                                                                             	|
| 1832       	| RECOVER_USER_INVALID               	| Oops, user details passed are not valid                                                                                                              	|
| 1833       	| RECOVER_PASSWORD_MISMATCH          	| Oops, your new password don't match with re-entered password                                                                                         	|
| 1834       	| TOKEN_NOT_FOUND                    	| Oops, we require valid token inorder to retrieve user information                                                                                    	|


### Files Exceptions

| Error Code 	| Error Name            	| Error Description                                                           	|
|------------	|-----------------------	|-----------------------------------------------------------------------------	|
| 1901       	| REQUIRED_ATTR_MISSING 	| Oops missed something, required fields is FILE :P                     	|
| 1902       	| UNIQUE                	| Oops, looks like you have already upload this file                          	|
| 1903       	| CREATE_FAILED         	| Oops, something went wrong while file creation                              	|
| 1904       	| NOT_FOUND             	| Oops, looke like the file you are trying to search is not available         	|
| 1906       	| DELETE_FAILED         	| Oops, something went wrong and delete didn't work                           	|
| 1907       	| FILE_SIZE             	| Oops, the file you uploaded was quite big, please upload file less than 1mb 	|


### Causes Exceptions

| Error Code 	| Error Name            	| Error Description                                                                                  	|
|------------	|-----------------------	|----------------------------------------------------------------------------------------------------	|
| 2001       	| REQUIRED_ATTR_MISSING 	| Oops missed something, required fields are Name, Description                                       	|
| 2002       	| UNIQUE                	| Oops, this cause title is already in use, can you please choose other cause title                  	|
| 2003       	| CREATE_FAILED         	| Oops, something went wrong while creating cause                                                    	|
| 2004       	| NOT_FOUND             	| Oops, we were not able to find the required cause                                                  	|
| 2005       	| UPDATE_FAILED         	| Oops, something went wrong while updating cause                                                    	|
| 2006       	| DELETE_FAILED         	| Oops, something went wrong while deleting cause                                                    	|
| 2007       	| INVALID_PHOTO_URL     	| Oops, you just provided an invalid image URL, can you please provide valid image URL               	|
| 2008       	| INVALID_BANNER_URL    	| Oops, you just provided an invalid banner image URL, can you please provide valid banner image URL 	|


### Locations Exceptions

| Error Code 	| Error Name            	| Error Description                                                                      	|
|------------	|-----------------------	|----------------------------------------------------------------------------------------	|
| 2101       	| REQUIRED_ATTR_MISSING 	| Oops missed something, required fields are Name, Address1, City, State & country       	|
| 2102       	| UNIQUE                	| Oops, looks like that Tree is already there, can you please enter some other Tree Name 	|
| 2103       	| CREATE_FAILED         	| Oops, looks like something went wrong while creating the location                      	|
| 2104       	| NOT_FOUND             	| Oops, we were not able to find the required location                                   	|
| 2105       	| UPDATE_FAILED         	| Oops, something went wrong while updating location                                     	|
| 2106       	| DELETE_FAILED         	| Oops, something went wrong while deleting location                                     	|
| 2107       	| INVALID_USER_ID       	| Oops, the user details provided are not valid                                          	|
| 2108       	| NO_LOCATIONS_FOR_YOU  	| Oops, looks like you haven't created any locations for self-tree planting yet          	|


### Trees Exceptions

| Error Code 	| Error Name            	| Error Description                                                                    	|
|------------	|-----------------------	|--------------------------------------------------------------------------------------	|
| 2201       	| REQUIRED_ATTR_MISSING 	| Oops missed something, required fields are Name, Description & Photo Image URL       	|
| 2202       	| UNIQUE                	| Oops, this title is already in use, can you please use some other title              	|
| 2203       	| CREATE_FAILED         	| Oops, something went wrong while creating tree                                       	|
| 2204       	| NOT_FOUND             	| Oops, we were not able to find the required tree                                     	|
| 2205       	| UPDATE_FAILED         	| Oops, something went wrong while updating tree                                       	|
| 2206       	| DELETE_FAILED         	| Oops, something went wrong while deleting tree                                       	|
| 2207       	| UNIQUE_PHOTO_URL      	| Oops, you just provided an invalid image URL, can you please provide valid image URL 	|
| 2208       	| NO_TREES_FOR_YOU      	| Oops, you haven't added any tree yet                                                 	|


### Users Trees Exception

Error Code	Error Name	Error Description
