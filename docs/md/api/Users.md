# Users Module
---

**Services Supported**

- [Create User](#create-user)
- [Get User](#get-user)
- [Update User](#update-user)
- [Delete User](#delete-user)
- [Change Password](#change-password)
- [Change Display Picture](#change-display-picture)
- [Forgot Password](#forgot-password)


## Create User

### Abstract



### URL

`http://willingtree.in/api/users`	

### Method

`POST`

### Request Body

```javascript
{
	"fname" : "John",
	"lname" : "Smith",
	"email" : "johnsmith@gmail.com",
	"password" : "p@$$w0Rd345",
	"disp_pic" : "http://willingtree.in/images/pic2.jpg",
	"phone" : "+91 9892098920",
	"about" : "I am a Banker",
	"website" : "http://www.johnsmith.com",
	"gender" : "m",
	"job_title" : "Analyst",
	"company_name" : "Willing Tree",
	"company_url" : "http://willingtree.in"
}
```

#### Request Parameter Explanation

| Parameter    | Description                                                                                               |
|--------------|-----------------------------------------------------------------------------------------------------------|
| fname        | Valid and real first name                                                                                 |
| lname        | Valid and real last name                                                                                  |
| email        | Valid email address of user                                                                               |
| password     | Valid & strong password of user with having combination of lowercase,uppercase,numeric,special characters |
| disp_pic     | Valid display picture url                                                                                 |
| phone        | Valid Phone or Mobile number with specified country code in it                                            |
| about        | Something about persons profession                                                                        |
| website      | Active website url                                                                                        |
| gender       | Person can enter three options in it i.e m/f/not specified                                                |
| job_title    | Persons job title in which verticle he/she is working                                                     |
| company_name | Real live company name                                                                                    |
| company_url  |                                                                                                           |

### Success Response Body

```javascript
{
	id: 6,
	email: "vikram.s@saiashirwad.com",
	msg: "Successfully created user account",
	_self: "http://willingtree.in/api/users/vikram.s@saiashirwad.com"
}
```

#### Success Response Parameter Explanation

| Parameter | Description                                                                                                          |
|-----------|----------------------------------------------------------------------------------------------------------------------|
| id        | id is an unique identifier that helps identify the created user and can be referenced in any further object creation |
| email     | email is an unique identifier that helps identify the created users valid email address.                             |
| msg       | It displays status of the activity                                                                                   |
| _self     |                                                                                                                      |

#### Success Response Status Code

`201` -> Created

### Error Response Body

```javascript

{
	"error_code" : 411,
	"error_msg" : "Looks like we already have an user with that email, please choose some other email address"
}

```

#### Error Response Parameter Explanation

| Parameters 	| Description                                                         	|
|------------	|---------------------------------------------------------------------	|
| error_code 	| Unique error code which can help developer identify bug easily      	|
| error_msg  	| Normal readable error message that helps identify what can be wrong 	|

#### Error Response Status Code

`412` -> Bad Request 

======

## Get User

### Abstract 

### URL

`http://willingtree.in/api/users/{data}` 

### Method

`GET`

### Request Path Parameter

| Parameter | Description                                                |
|-----------|------------------------------------------------------------|
| data      | it can be either valid registered user id or email address |


### Success Response Body

```javascript
{
	"id" : "1",
	"fname" : "Anonymous",
	"lname" : "User",
	"email" : "support@willingtree.in",
	"disp_pic" : "images/profile.png",
	"phone" : null,
	"about" : null,
	"website" : null,
	"gender" : "m",
	"job_title" : "Bot",
	"company_name" : "WillingTree",
	"company_url" : "http://willingtree.in",
	"recommended_by_user_id" : null,
	"creation_ts" : "2014-07-22 18:04:27",
	"lastmodified_ts" : "0000-00-00 00:00:00",
	"is_active" : "1",
	"is_public" : "1",
	"is_celebrity" : "0"
}
```

#### Success Response Parameter Explanation

| Parameter              | Description                                                                                                          |
|------------------------|----------------------------------------------------------------------------------------------------------------------|
| id                     | 	 |
| fname                  | Valid and real first name                                                                                            |
| lname                  | Valid and real last name                                                                                             |
| email                  | Valid email address of user                                                                                          |
| disp_pic               | Valid display picture url                                                                                            |
| phone                  | Valid Phone or Mobile number with specified country code in it                                                       |
| about                  | Something about persons profession                                                                                   |
| website                | Active website url                                                                                                   |
| gender                 | Person can have three options in it i.e male/female/not specified                                                    |
| job_title              | Persons job title in which verticle he/she is working                                                                |
| company_name           | Real live company name                                                                                               |
| company_url            | Specified company website url                                                                                        |
| recommended_by_user_id |                                                                                                                      |
| creation_ts            |                                                                                                                      |
| lastmodified_ts        |                                                                                                                      |
| is_active              |                                                                                                                      |
| is_public              |                                                                                                                      |
| is_celebrity           |                                                                                                                      |


#### Success Response Status Code

`200` -> Successful

### Error Response Body

```javascript
{
	"error_code" : 409,
	"error_msg" : "Oops, we were not able to find the required user"
}
```


#### Error Response Parameter Explanation

| Parameters 	| Description                                                         	|
|------------	|---------------------------------------------------------------------	|
| error_code 	| Unique error code which can help developer identify bug easily      	|
| error_msg  	| Normal readable error message that helps identify what can be wrong 	|


#### Error Response Status Code

`404` -> User Not Found 

======

## Update User

### Abstract

### URL

`http://willingtree.in/api/users`

**Method** `PUT`

### Request Body

```javascript
{
	"id" : 5,
	"fname" : "Abhishek",
	"lname" : "Patil",
	"email" : "patil.abhishek3@gmail.com",
	"disp_pic" : "images/profile.png",
	"phone" : null,
	"about" : null,
	"website" : null,
	"gender" : "m",
	"job_title" : "Bot",
	"company_name" : "WillingTree",
	"company_url" : "http://willingtree.in",
	"is_active" : "1",
	"is_public" : "1",
	"is_celebrity" : "0"
}
```

#### Request Parameter Explanation

| Parameter | Description                                                                                                          |
|-----------|----------------------------------------------------------------------------------------------------------------------|
| id        | id is an unique identifier that helps identify the created user and can be referenced in any further object creation |
| fname     | Valid and real first name                                                                                            |
| lname     | Valid and real last name                                                                                             |
| email     | Valid email address of user                                                                                          |
| password  | Valid & strong password of user with having combination of lowercase,uppercase,numeric,special characters            |
| is_active |                                                                                                                      |



### Success Response Body

```javascript
{
	"id": 5,
	"email": "patil.abhishek3@gmail.com",
	"msg": "Successfully updated user account",
	"_self": "http://localhost/WillingTree/api/users/patil.abhishek3@gmail.com"
}
```


#### Success Response Parameter Explanation

| Parameter | Description                                                                                                          |
|-----------|----------------------------------------------------------------------------------------------------------------------|
| id        | id is an unique identifier that helps identify the created user and can be referenced in any further object creation |
| email     | email is an unique identifier that helps identify the created users valid email address.                             |
| msg       | It displays status of the activity                                                                                   |
| _self     |                                                                                                                      |



#### Success Response Status Code

`` -> Updated


### Error Response Body

```javascript

{
	"error_code" : 406,
	"error_msg" : "Oops missed something, required fields are First Name, Last Name, Email, Password"
}

```


#### Error Response Parameter Explanation

| Parameters 	| Description                                                         	|
|------------	|---------------------------------------------------------------------	|
| error_code 	| Unique error code which can help developer identify bug easily      	|
| error_msg  	| Normal readable error message that helps identify what can be wrong 	|

#### Error Response Status Code

`412` -> Bad Request


=======

## Delete User

### Abstract

### URL

`http://willingtree.in/api/users`

### Method

`DELETE`

### Request Body

```javascript
{
	"id" : 5 
}
```
 OR

```javascript
{
	"email" : "vikram.s@saiashirwad.com" 
}
```

#### Request Parameter Explanation

| Parameter | Description                                                                                                          |
|-----------|----------------------------------------------------------------------------------------------------------------------|
| id        | id is an unique identifier that helps identify the created user and can be referenced in any further object creation |
| email     | Valid email address of user                                                                                          |


#### Success Response body
`(Empty)`

#### Success Response Status Code

`204` -> No Content


### Error Response Body

```javascript
{
	"error_code" : 409
	"error_msg" : "Oops, we were not able to find the required user"
}
```

#### Error Response Parameter Explanation

| Parameters 	| Description                                                         	|
|------------	|---------------------------------------------------------------------	|
| error_code 	| Unique error code which can help developer identify bug easily      	|
| error_msg  	| Normal readable error message that helps identify what can be wrong 	|


#### Error Response Status Code

`404` -> User Not Found 

======

## Change Password

### Abstract

### URL

`http://willingtree.in/api/users/changepassword`

### Method

`POST`

#### Request body

```javascript
{
	"current_password" : "Welcome@321",
	"new_password" : "Welcome@123",
	"new_confirm_password" : "Welcome@123"
}
```

#### Request Parameter Explanation

| Parameter            | Description                                                                                                   |
|----------------------|---------------------------------------------------------------------------------------------------------------|
| current_password     | Valid current password of user                                                                                |
| new_password         | Valid & strong new password of user with having combination of lowercase,uppercase,numeric,special characters |
| new_confirm_password | Valid & strong new password of user with having combination of lowercase,uppercase,numeric,special characters |



### Success Response Body

```javascript

{
	"msg" : "Successfully updated your password",
	"id" : "3"
}

```

#### Success Response Parameter Explanation

| Parameter | Description                                                                                                          |
|-----------|----------------------------------------------------------------------------------------------------------------------|
| msg       | Normal readable succseful message                                                                                    |
| id        | id is an unique identifier that helps identify the created user and can be referenced in any further object creation |


#### Success Response Status Code

`200` -> Successful


### Error Response Body

```javascript
{
	"error_code" : 414,
	"error_msg" : "Oops, looks like current password you entered doesn't match actually"
}
```


#### Error Response Parameter Explanation

| Parameters 	| Description                                                         	|
|------------	|---------------------------------------------------------------------	|
| error_code 	| Unique error code which can help developer identify bug easily      	|
| error_msg  	| Normal readable error message that helps identify what can be wrong 	|


#### Error Response Status Code

`` -> 


## Change Display Picture

### Abstract

### URL

`http://willingtree.in/api/users/changedisplaypicture`

### Method

`POST`

#### Request body

disp_pic file


#### Request Parameter Explanation

| Parameter | Description               |
|-----------|---------------------------|
| disp_pic  | Valid display picture url |


### Success Response Body

```javascript

{

"id":"3",
"msg":"Successfully updated display picture"

}

```

#### Success Response Parameter Explanation

| Parameter | Description                                                                                                          |
|-----------|----------------------------------------------------------------------------------------------------------------------|
| id        | id is an unique identifier that helps identify the created user and can be referenced in any further object creation |
| msg       | Normal readable message                                                                                              |


#### Success Response Status Code

`200` -> Successful


### Error Response Body

```javascript

{

"error_code":408,
"error_msg":"Oops, something went wrong while updating user details"

}


```


#### Error Response Parameter Explanation

| Parameters 	| Description                                                         	|
|------------	|---------------------------------------------------------------------	|
| error_code 	| Unique error code which can help developer identify bug easily      	|
| error_msg  	| Normal readable error message that helps identify what can be wrong 	|



#### Error Response Status Code

`` -> 

---

## Forgot Password

### Abstract

### URL

`http://willingtree.in/api/users/changepassword`

### Method

`POST`

#### Request body

```javascript

{
  "email" : "rohansakhale@gmail.com"
}

```

#### Request Parameter Explanation

| Parameter | Description                     |
|-----------|---------------------------------|
| email     | Valid email address of the user |


### Success Response Body

```javascript
{
}
```

#### Success Response Parameter Explanation


#### Success Response Status Code

`` -> Successful


### Error Response Body


#### Error Response Parameter Explanation


#### Error Response Status Code

`` -> 
