# Authentication Module
---

## Login

### Abstract
User authentication is required to access user-restricted operations. On successful
authentication, the create session helps user access data that is allowed to him for
a specific inactivity time period.

_Access-Token_ provided on authentication can be used in Request Headers to by-pass login every-time

### URL
`http://willingtree.in/api/auth`

### Method
`POST`

### Request Body

```javascript
{
  "email" : "VALID_EMAIL",
  "password" : "PASSWORD"
}
```

#### Request Parameter Explanation
| Parameters 	| Description                            	|
|------------	|----------------------------------------	|
| Email      	| Valid email address of registered user 	|
| Password   	| Valid password of registered user      	|


### Success Response Body

```javascript
{
	msg: "Successful"
	email: "USER_EMAIL"
	access_token: "483fa0306ef49710347a1a1699f58251"
}
```

#### Success Response Parameter Explanation
| Parameters   	| Description                                                                       	|
|--------------	|-----------------------------------------------------------------------------------	|
| msg          	| Display response message from server                                              	|
| email        	| Confirming email address of logged in user                                        	|
| access_token 	| Unique user access token which can help by pass authentication if user in headers 	|

### Error Response Body
```javascript
{
	error_code: 302,
	error_msg: "Invalid email/password combination"
}
```
#### Error Response Parameter Explanation
| Parameters 	| Description                                                         	|
|------------	|---------------------------------------------------------------------	|
| error_code 	| Unique error code which can help developer identify bug easily      	|
| error_msg  	| Normal readable error message that helps identify what can be wrong 	|


### Error Response Status Code

`403` -> Authentication Required


## Log out

### Abstract
Log out as states, destroys the logged in users session, to avoid unauthorized usage to other users

### URL
`{BASE_URL}/api/auth`

### Method
`DELETE`

### Request Body
`None`

### Response Body
`None`

### Success Response Status Code
`204` -> States no response body generated