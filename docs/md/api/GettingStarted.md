# Willing Tree API Documentation

## RESTful

The WillingTree API has a wonderful RESTful API that can be easily consumed by any third party clients. WillingTree looks forward for more integration points in near future using their RESTFul webservices.

> All API Calls Can be made using `HTTP (POST, GET, PUT, DELETE)`

> You can consider any non-20X HTTP Response code as an error, returned response data will display detail information.

> All modules/sub-modules are accessed by appending its name over `http://willingtree.in/api/{MODULE_NAME}/{SUB-MODULE_NAME|DATA}`

### Passing Request Data

Request data can be passed as JSON Object to the API with appropriate parameters.
Each API call documentation would describe about parameters in detail. 
Alternatively, HTTP Form Data like POST/GET parameters can also be submitted to API calls, but JSON is recommened.

### Output Formats

Currently we only support JSON as response output format, but we would also be supporting more globally accepted formats very soon.

### Official API Clients

WillingTree will very soon support official API clients for various languages starting with PHP and Java ;)

### API Modules

* [Authentication](Authentication.md)
* [Users](Users.md)
* [Locations](Locations.md)
* [Trees](Trees.md)
* [Stories](Stories.md)
* [Causes](Causes.md)

### Error Codes and their meaning

[Error codes document](ErrorHelp.md) will help understand possible errors that could arise while executing any service above, its detailed error message would help identify a better solution towards the same service invocation.

There would be a proper structure maintained in every error that you could see to help identify and also help the developer debug the issue easily.