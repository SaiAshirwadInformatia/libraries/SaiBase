# Users Rights

By default every user would have accessibility towards the website based on the rights given. 
In case user is accessing something over which rights are not given, redirection would be made towards profile with an logged entry.

**Users rights can be given to user by 2 ways**

* Role
* Directly

## Users Roles

Users Roles would be a name of group that would possess certain rights an user can perform.

Example: A `Moderator` named user group would be able to approve Story publication, so here a `Moderator` would posses many such rights

## Rights Listing
---

### User Authentication

1. Login Access Allowed (login_access_allowed)
Given when user validates his/her email address


### Users

1. View non-public profiles (view_non_public_profiles)
1. Update My Profile (update_my_profile)
Allows logged in user to update its own profile
1. User allowed to update others profile (update_others_profile)
1. Make Profile Non-Public (make_profile_non_public)
1. Make Others Profile As Celebrity (make_others_profile_celebrity)
This would allow user to mark any profile as celebrity profile
1. Search others profile (search_profiles)
1. Delete My Profile (delete_my_profile)
Allows deleting self profile
1. Delete Others Profile (delete_others_profile)
Allows user to delete others profile
1. View deleted profiles (view_deleted_profiles)
Allows user to view deleted profiles


### Stories

1. View Non-Public Stories (view_non_public_stories)
Would allow an user to vie non-public stories. Usually once user gets pretty old, this right would be directly given else would need to be earned.
1. Create Story (create_story)
Allows user to create or publish his own story towards the audience
1. Edit Story (edit_story)
Allows user to edit his own stories
1. Edit Others Stories (edit_others_story)
Allows user to edit others stories
1. Delete Self Story (delete_self_story)
Allows user to delete his own stories
1. Delete Others Story (delete_others_story)
Allows user to delete others stories
1. Approve Story (approve_story)
Moderator kinda right which allows user to approve published story for public display


### Trees

1. Create Tree (create_tree)
Allows user to create a tree, which can be used while planting
2. Delete Tree (delete_tree)
Allows user to delete a tree
3. Edit Tree (edit_tree)
Allows user to edit his own tree
4. Palnt My Tree (plant_my_tree)
Allows user to plant his own tree
5. Update My Tree State (update_my_tree_state)
Allows user to update his own tree state
6. View My Tree State (view_my_tree_state)
Allows user to view his own tree state
7. Palnt Tree Public (plant_tree_public)
Allows user to plant tree for others
8. Update Others Tree State (update_others_tree_state)
Allows user to update others tree state


### Location

1. View My Location (view_my_location)
Allows user to view his own location
2. Update My Location (update_my_location)
Allows user to update his own location
3. Delete My Location (delete_my_location)
Allows user to delete his own location
4. Create_location (create_location)
Allows user to create location for himself
