# User guide

## Getting started with  Willing Tree

### Abstract
WillingTree an NGO based website

### Contents

* [About Willing Tree](#about-willing-tree)
* [Registration](#registration)
* [Login Panel](#login-panel)
* [Forgot Password](#forgot-password)
* [Trees](#trees)
* [Stories](#stories)
* [Causes](#causes)
* [Locations](#locations)

### About Willing Tree

> If a smoker shows his willingness to give up / quit his smoking habit then we buy a sapling and start growing a TREE by his name. This will be the Caring Tree for that person. Initially all saplings would be maintained in Quadrant premises and later transplanted into available areas when in a position to afford a gardener to nourish.

> Later we will provide him the status of his TREE and also express our thanks for providing an opportunity for growing a Tree, moreover also seek his feedback around his quitting habit.

> A humble initiative - "quit to grow.." to protect environment and diligently handover it over to the next generation and at the same time spread the awareness around tobacco and it's ill effects. With Just Your Willingness to Quit Smoking Habit, a Tree begins to grow for your joy and care.

	
