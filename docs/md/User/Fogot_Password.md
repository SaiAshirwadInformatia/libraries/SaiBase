### Forgot Password

**Steps to recover your password**

1. Enter in to Willing Tree Website by using **[WWW.WillingTree.in]** URL
2. Click to ***Login*** tab at the top of WillingTree website
3. Click to ***Forgot Password**?* tab present beside Login form
4. Fill the form with your valid email address
5. Visit your mailbox
6. Open WillingTree email in your mailbox (check spam box or promotions tab also)
7. Do the operation as per password reset instructions given in the WillingTree email
