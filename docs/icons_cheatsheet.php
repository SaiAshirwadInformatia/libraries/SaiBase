<?php
require_once '../start.php';
$icons = new icons();
$all_icons = $icons->all();
?>
<!doctype html>
<html lang="en">
<head>
<title>Icons Cheat Sheet that can be used in Sai Projects</title>
<?php
foreach ($icons->icons_css_paths as $css_path) {
    echo '<link rel="stylesheet" href="' . $css_path . '" />';
}
?>
<script src="../assets/js/jquery/dist/jquery.min.js"></script>
<script src="../assets/js/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../assets/js/bootbox.js/bootbox.js"></script>
<script>
var icons = <?php echo json_encode($all_icons);?>;
</script>
<style>
.row {
	-webkit-column-fill: auto;
	-moz-column-fill: auto;
	column-fill: auto;
}

.col-md-2 {
	-webkit-column-break-inside: avoid;
	-moz-column-break-inside: avoid;
	column-break-inside: avoid;
	display: inline-block;
}
</style>
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img
					src="../assets/images/sai-logo_dark.png" /></a>
			</div>
			<p class="navbar-text">Icons Cheat Sheet that can be used in Sai
				Projects</p>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="row">
	<?php
foreach ($all_icons as $name => $icon) {
    echo '<div class="col-md-2"><div class="panel panel-default">';
    echo '<div class="panel-heading"><a href="#" id="' . $name .
         '">Code</a></div>';
    echo '<div class="panel-body text-center" style="font-size: 3em">' . $icon .
         '</div>';
    echo '<div class="panel-footer"><small>' . $name . '</small></div>';
    echo '</div></div>';
}
?>
	</div>
	</div>
	<script>
	var tagsToReplace = {
		    '&': '&amp;',
		    '<': '&lt;',
		    '>': '&gt;'
		};

		function replaceTag(tag) {
		    return tagsToReplace[tag] || tag;
		}

		function safe_tags_replace(str) {
		    return str.replace(/[&<>]/g, replaceTag);
		}
$(document).ready(function(){
    $("a[href=#]").click(function(){
	bootbox
	.dialog({
	    message : "HTML: <code><pre>"
		    + safe_tags_replace(icons[$(this).attr('id')])
		    + "</pre></code> PHP Smarty: <code><pre>{$icons->"+$(this).attr('id').replace(/[-]/g,"_")+"}</pre></code>",
	    title : $(this).attr('id')
	});
	    });
});
</script>
</body>
</html>