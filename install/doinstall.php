<?php
use Sai\Base\Common;
use Sai\Base\AutoLoader;
use Sai\Base\Core;
$installer = true;
require_once '../start.php';
require_once '../api/library/Sai/Base/AutoLoader.php';
require_once '../api/cfg/const.inc.php';

AutoLoader::register();

define('CFG_FOLDER', SAI_ABS_PATH . 'api' . DS . 'cfg');
define('GUI_FOLDER', SAI_ABS_PATH . 'gui');

function checkFolders()
{
    if (! file_exists(SAI_ABS_PATH . 'api')) {
        throw new Exception(
            "Something is wrong with application, API folder is missing ;)");
    }
    if (! file_exists(SAI_ABS_PATH . 'api' . DS . 'library')) {
        throw new Exception(
            "Something is wrong with application, Library folder is missing ;)");
    }
    if (! file_exists(SAI_ABS_PATH . 'api' . DS . 'library' . DS . 'Sai')) {
        throw new Exception(
            "Something is wrong with application, Library\Sai folder is missing ;)");
    }
    if (! file_exists(GUI_FOLDER)) {
        throw new Exception(
            "Something is wrong with application, GUI folder is missing ;)");
    }
    if (! file_exists(GUI_TEMPLATE)) {
        throw new Exception(
            "Something is wrong with application, GUI templates folder is missing ;)");
    }
    if (! file_exists(SAI_ABS_PATH . 'assets')) {
        throw new Exception(
            "Something is wrong with application, ASSETS folder is missing ;)");
    }
}

function clearUnwantedFiles()
{
    return Common::removeFilesFromFolder(GUI_COMPILE);
}

function configfolder()
{
    $valid = true;
    $passed = 'Passed';
    $folder_not_exists = "Folder doesn't exists";
    $not_writable = "Folder is not writable, please give 777 permissions to folder";
    echo 'Checking Folder Configuration';
    echo '<table border="1" width="100%"><tr><th>Activity</th><th>Folder Path</th><th>Validation</th></tr>';
    echo '<tr><td>Config Folder</td><td>api/cfg</td><td>';
    if (! file_exists(CFG_FOLDER)) {
        $valid = false;
        echo $folder_not_exists;
    } else 
        if (! is_writable(CFG_FOLDER)) {
            $valid = false;
            echo $not_writable;
        } else {
            echo $passed;
        }
    echo '</td></tr>';
    echo '<tr><td>GUI Cache Folder</td><td>gui/cache</td><td>';
    if (! file_exists(GUI_CACHE)) {
        $valid = false;
        echo $folder_not_exists;
    } else 
        if (! is_writable(GUI_CACHE)) {
            $valid = false;
            echo $not_writable;
        } else {
            echo $passed;
        }
    echo '</td></tr>';
    echo '<tr><td>GUI Compile Folder</td><td>gui/templates_c</td><td>';
    if (! file_exists(GUI_COMPILE)) {
        $valid = false;
        echo $folder_not_exists;
    } else 
        if (! is_writable(GUI_COMPILE)) {
            $valid = false;
            echo $not_writable;
        } else {
            echo $passed;
        }
    echo '</td></tr>';
    echo '<tr><td>Clear Unwanted Files</td><td>gui/templates_c/</td><td>' .
         ((clearUnwantedFiles()) ? 'Deleted' : 'Nothing to delete') . '</td>';
    echo '<tr><td>Upload Folder</td><td>upload</td><td>';
    
    if (! file_exists(SAI_ABS_PATH . 'upload')) {
        $valid = false;
        echo $folder_not_exists;
    } else 
        if (! is_writable(SAI_ABS_PATH . 'upload')) {
            $valid = false;
            echo $not_writable;
        } else {
            echo $passed;
        }
    echo '</td></tr>';
    echo '</table>';
    return $valid;
}

function createconfig()
{
    if (configfolder()) {
        if (isset($_POST)) {
            if (isset($_POST['db_type']) && isset($_POST['db_host']) &&
                 isset($_POST['db_user']) && isset($_POST['db_pass']) &&
                 isset($_POST['db_name'])) {
                $db_type = $_POST['db_type'];
                $db_host = $_POST['db_host'];
                $db_user = $_POST['db_user'];
                $db_pass = $_POST['db_pass'];
                $db_name = $_POST['db_name'];
                $db_table_prefix = $_POST['db_table_prefix'];
                $str = "<?php
/**
 * Auto-generated Config File by SAI Installer
 *
 * @name config_db.inc.php
 * @author SAI Installer
 * @since WillingTree v1
 * @copyright saiashirwad.com
 */

define('DB_TYPE', '$db_type');
define('DB_HOST', '$db_host');
define('DB_USER', '$db_user');
define('DB_PASS', '$db_pass');
define('DB_NAME', '$db_name');
define('DB_TABLE_PREFIX', '$db_table_prefix');

// Auto Generated on " . date("r", time()) . "
?>";
                file_put_contents(CFG_FOLDER . DS . 'config_db.inc.php', $str);
                return '<p>Created config file</p>';
            }
        }
        throw new Exception('Missing fields for config file creation');
    }
}

function performSQLInstall($filename)
{
    $core = Core::getInstance();
    $table_install_query = file_get_contents(DB_TYPE . DS . $filename . '.sql');
    $table_install_query = str_replace("/* prefix */", DB_TABLE_PREFIX, 
        $table_install_query);
    $re = $core->db->multiQuery($table_install_query);
    if (count($re) > 0) {
        foreach ($re as $_r) {
            echo $_r . '<br />';
        }
        return '<p>Failed during installation of ' . $filename . '</p>';
    }
}

function doDBInstall()
{
    if (file_exists(CFG_FOLDER . DS . 'config_db.inc.php')) {
        include CFG_FOLDER . DS . 'config_db.inc.php';
        performSQLInstall('tables');
        performSQLInstall('data');
        if (isset($_POST['install_test']) and $_POST['install_test'] == 1) {
            performSQLInstall('test');
        }
        return '<p>DB Installation complete...</p><p>Congratulations</p>';
    }
    throw new Exception('DB Installation Failed');
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Installation in Progress</title>
</head>
<body>
	<h1>Installation is in Progress</h1>
		<?php
checkFolders();
echo createconfig() . doDBInstall();
?>
	</body>
</html>
