<?php
/**
 *
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Sai Installer</title>
<style>
label {
	display: block;
	margin: 5px;
	color: #444;
}

div {
	width: 200px;
}

input[type=text], input[type=password], input[type=submit], select {
	width: 100%;
	padding: 10px;
	border: 1px solid #ccc;
}

input[type=submit] {
	margin: 5px auto;
	background: #b5e61d;
	cursor: pointer;
	overflow: hidden;
}

input[type=submit]:hover {
	box-shadow: 0 1px 2px #000;
	transition: ease 0.3s;
}
</style>
</head>
<body>
	<h1>SAI Installer</h1>
	<hr />
	<form action="doinstall.php" method="post">
		<div>
			<label for="db_type">Database Type</label> <select name="db_type">
				<option value="mysql">MySQL</option>
			</select>
		</div>
		<div>
			<label for="db_host">Database Host</label> <input type="text"
				name="db_host" placeholder="(ex: localhost)" value="localhost" />
		</div>
		<div>
			<label for="db_user">Database User</label> <input type="text"
				name="db_user" placeholder="(ex: root)" />
		</div>
		<div>
			<label for="db_pass">Database Password</label> <input type="password"
				name="db_pass" placeholder="(ex: *******)" />
		</div>
		<div>
			<label for="db_name">Database Name</label> <input type="text"
				name="db_name" placeholder="(ex: )" value="" />
		</div>
		<div>
			<label for="db_table_prefix">Database Tables Prefix</label> <input
				type="text" name="db_table_prefix" placeholder="(ex: wt_)" />
		</div>
		<div>
			<label for="options">Additional options</label> <label
				for="install_test"><input type="checkbox" id="install_test"
				name="install_test" value="1" checked /> Install Test Data</label>
		</div>
		<div>
			<input type="submit" name="install_now" value="Install" />
		</div>
	</form>
</body>
</html>
