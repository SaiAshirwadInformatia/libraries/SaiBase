#!/bin/sh
cd ..;
cd ..;

echo "========================================================";
echo "Running Create Folders script";
echo "========================================================";
if [ ! -d "upload" ]; then
	echo "Uploadoy directory doesn't exists";
	echo "Creating upload directory";
	mkdir "upload";
else
	echo "Good!!! Your upload directory exists";
fi
echo "========================================================";

if [ ! -d "gui/cache" ]; then
	echo "GUI Cache directory doesn't exists";
	echo "Creating GUI Cache directory";
	mkdir "gui/cache";
else
	echo "Wonderful, you have GUI cache directory";
fi
echo "========================================================";

if [ ! -d "gui/templates_c" ]; then
	echo "GUI Templates Compile directory doesn't exists";
	echo "Creating GUI Templates Compile directory";
	mkdir "gui/templates_c";
else
	echo "Wonderful, you have GUI Templates Compile directory";
fi

echo "========================================================";
echo "Done, running create folders script";
echo "========================================================";
