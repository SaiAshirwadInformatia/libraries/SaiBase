#!/bin/sh
echo "========================================================";
echo "     SAI INSTALLER";
echo "========================================================";
echo "Sai Linux based installer instantiated";
echo "Should help you to workout directory issues";
echo "========================================================";
echo "Current Directory: ". $PWD;
echo "========================================================";
if [ "omsai" != "$USER" ]; then
	echo "To continue, you must login as OMSAI user";
	echo "========================================================";
	echo "Switching as OMSAI";
	su - omsai -c "cd $PWD/install/scripts/;sh createFolders.sh;sh fixPermissions.sh";
else
	cd $PWD/install/scripts;
	sh "createFolders.sh";
	sh "fixPermissions.sh";
fi

echo "========================================================";
echo "Kool, you are done";
echo "========================================================";

