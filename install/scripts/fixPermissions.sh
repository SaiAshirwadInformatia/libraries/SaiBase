#!/bin/sh
cd ..;
cd ..;

echo "========================================================";
echo "Doing the magic to make upload directory writable";
chmod -R 777 "upload";
echo "Yeah, this is done";

echo "========================================================";
echo "Doing the magic to make GUI Cache directory writable";
chmod -R 777 "gui/cache";
echo "========================================================";

echo "========================================================";
echo "Doing the magic to make GUI Templates Compile directory writable";
chmod -R 777 "gui/templates_c";
echo "========================================================";

echo "========================================================";
echo "Making the API/CFG directory writable so installer can generate config_db file for you";
chmod 777 "api/cfg";
echo "========================================================";
echo "I guess, now everything looks good for installation";
echo "Please proceed with SAI Online Installer";
echo "========================================================";
