--
-- Dumping data for table `albums`
--

INSERT INTO `/* prefix */albums` (`name`, `description`) VALUES
('Gerenal', 'General Photographs of Ghanerao');


--
-- Dumping data for table `users_roles`
--

INSERT INTO `/* prefix */users_roles` (`user_id`, `role_id`) VALUES
(1, 4),
(2, 4);


--
-- Dumping data for table `users_rights`
--

INSERT INTO `/* prefix */users_rights` (`user_id`, `right_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(2, 1),
(2, 2),
(2, 8);

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`fname`, `mname`, `lname`, `username`, `email`, `password`, `hoti_no`, `gender`, `gotra`, `blood_group`, `phone1`, `phone2`, `website`, `dob`, `education`, `maritial_status`, `anniversary_date`, `display_picture`, `access_token`, `is_active`, `is_public`) VALUES
('Rohan', 'Rajan', 'Sakhale', 'rsakhale', 'rohansakhale@gmail.com', '08077750f24f154743ab43dd9cf5aeb6', NULL, 'm', '', 'A+', '', NULL, NULL, NULL, NULL, '', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', '08077750f24f154743ab43dd9cf5aeb7', 1, 1),
('Chirag', NULL, 'Babrekar', 'cbabrekar', 'chiragbabrekar@gmail.com', '08077750f24f154743ab43dd9cf5aeb6', NULL, 'm', '', '', '', NULL, NULL, NULL, NULL, '', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', 'chi77750f24f154743ab43dd9cf5aeb6', 1, 1),
('Abhishek', NULL, 'Patil', 'abhi', 'patil.abhishek3@gmail.com', 'bd743b0d5ed4f29fa8eb49dd356c8020', NULL, 'm', '', '', '', NULL, 'http://abhipatil.com', NULL, 'B.Sc.I.T.', 'unmarried', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', 'd96c7d64025fb4a23b8b57aa16dbea46', 1, 1),
('Vikram', NULL, 'Sule', 'vikram.s', 'sule.vikram281@gmail.com', '084339cd5f0d36db1d1a7be0698df57d', NULL, 'm', '', '', '', NULL, NULL, NULL, 'B.Sc.I.T.', 'unmarried', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', 'a1ae7b14c2a97860c5070df044ee0706', 1, 1),
('Chaitali', NULL, 'Patil', 'cpatil', 'cpatil@saiashirwad.com', '8522a8f6717d7fc0ce16bc01ac4e7dc0', NULL, 'f', '', '', '', NULL, NULL, NULL, NULL, 'unmarried', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', '515131bf93e6cc44edd4bd73f070348c', 1, 1),
('Pranita', NULL, 'Bari', 'pbari', 'pbari@saiashirwad.com', '832e1d145362e43a5b90f20593a81d5c', NULL, 'f', '', '', '', NULL, NULL, NULL, NULL, 'unmarried', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', 'b6dbf6e419e66eeea4ec7c11a0342107', 1, 1),
('Suyash', NULL, 'Jadhav', 'jsuyash', 'jsuyash@saiashirwad.com', '1f5f66adf0495acd6cbe8a6339e64c1b', NULL, 'm', '', '', '', NULL, NULL, NULL, NULL, 'unmarried', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', 'ff32b779e2d0c09ed769557343988365', 1, 1),
('Divya', NULL, 'Save', 'divyas', 'divyas@saiashirwad.com', 'ce1728dc94ea51688b2c4efe6d46e681', NULL, 'f', '', '', '', NULL, NULL, NULL, NULL, 'unmarried', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', '5278b7ccb47a5c0a4ef5859da26fec62', 1, 1),
('Aparna', NULL, 'Jadhav', 'ajadhav', 'ajadhav@saiashriwad.com', '9e03f71e7ac87b4f760b01094815662a', NULL, 'f', '', '', '', NULL, NULL, NULL, NULL, 'unmarried', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', '23d8a5211bbd87bb7c56a892e8c368b9', 1, 1),
('Priyanka', NULL, 'Patil', 'ppatil', 'priya@saiashirwad.com', '78f3167169100f5aae894296a67715bb', NULL, 'f', '', '', '', NULL, NULL, NULL, NULL, 'unmarried', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', '038192b0f4041f1ef42d9098e7edd389', 1, 1),
('User', NULL, 'One', '', 'uone@saiashirwad.com', 'a8d82b158d6d2b13088ea251b2da389f', NULL, 'm', '', '', '', NULL, NULL, NULL, NULL, '', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', '5ab0d97499c3af6fa0adf2f62209a48a', 1, 1),
('User', NULL, 'Two', 'utwo', 'utwo@saiashirwad.com', 'f03329846acc16229a3811f7fa6b0693', NULL, 'f', '', '', '', NULL, NULL, NULL, NULL, '', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', 'b11008b0427d8755186cdf7ceaf542ee', 1, 1),
('User', NULL, NULL, '', '', '00d10cd606065ef723a136875561f179', NULL, 'm', '', '', '', NULL, NULL, NULL, NULL, '', NULL, 'http://localhost/Ghanerao.in/assets/images/profile.png', 'e2366d92e84b5315ab8954550e95506f', 1, 0);

