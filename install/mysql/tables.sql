-- Sai Ashirwad Informatia
-- Tables Create SQL
-- Copyright SAI
-- Created 22nd June 2014
-- Last Modified 22nd June 2014

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `SAI`
--

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `/* prefix */albums`;

CREATE TABLE `/* prefix */albums` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(128) NOT NULL,
 `description` text,
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `lastmodified_ts` timestamp NULL DEFAULT NULL,
 `is_active` int(1) DEFAULT '1',
 PRIMARY KEY (`id`),
 UNIQUE KEY `name` (`name`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `/* prefix */albums_photos`;

CREATE TABLE `/* prefix */albums_photos` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `description` text,
 `file_id` int(11) NOT NULL,
 `album_id` int(11) NOT NULL,
 `views` int(11) NOT NULL DEFAULT '0',
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `is_active` int(1) DEFAULT '1',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

DROP TABLE IF EXISTS `/* prefix */configs`;

CREATE TABLE `/* prefix */configs` (
 `name` varchar(128) NOT NULL,
 `val` text NOT NULL,
 `oftype` varchar(64) NOT NULL,
 `options` text,
 PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Table structure for TABLE `feedback`
--

DROP TABLE IF EXISTS `/* prefix */feedback`;

CREATE TABLE `/* prefix */feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `feedback_purpose` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for TABLE `feedback_comments`
--

DROP TABLE IF EXISTS `/* prefix */feedback_comments`;

CREATE TABLE `/* prefix */feedback_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `feedback_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for TABLE `feedback_purposes`
--

DROP TABLE IF EXISTS `/* prefix */feedback_purposes`;

CREATE TABLE `/* prefix */feedback_purposes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `/* prefix */files`;

CREATE TABLE IF NOT EXISTS `/* prefix */files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `path` text NOT NULL,
  `type` varchar(32) NOT NULL,
  `size` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hits` int(11) DEFAULT '0',
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `/* prefix */menus`;

CREATE TABLE `/* prefix */menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `label` varchar(128) NOT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `permissions` text,
  `url` text NOT NULL,
  `parent_menu` int(1) DEFAULT '0',
  `menu_order` int(11) DEFAULT '1',
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastmodified_ts` timestamp NULL DEFAULT NULL,
  `is_active` int(1) DEFAULT '1',
  `is_public` int(1) DEFAULT '1',
  `is_shown_logged_user` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for TABLE `pages`
--

DROP TABLE IF EXISTS `/* prefix */pages`;

CREATE TABLE `/* prefix */pages` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `title` text NOT NULL,
 `meta_keywords` text,
 `meta_description` text,
 `meta_author` text,
 `content` text DEFAULT NULL,
 `slug` varchar(128) NOT NULL,
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `lastmodified_ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
 `has_menubar` int(1) DEFAULT '1',
 `has_sidebar` int(1) DEFAULT '0',
 `is_auth_req` int(1) DEFAULT '0',
 `is_auth_not_req` int(1) DEFAULT '1',
 `is_active` int(1) DEFAULT '1',
 `is_public` int(1) DEFAULT '1',
 PRIMARY KEY (`id`),
 UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for TABLE `users`
--

DROP TABLE IF EXISTS `/* prefix */users`;

CREATE TABLE `/* prefix */users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` text NOT NULL,
  `lname` text,
  `email` varchar(128) NOT NULL,
  `password` text NOT NULL,
  `disp_pic` text,
  `phone` varchar(32) DEFAULT NULL,
  `about` text,
  `website` text,
  `gender` varchar(1) DEFAULT NULL,
  `job_title` text,
  `company_name` text,
  `company_url` text,
  `recommended_by_user_id` int(11) DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastmodified_ts` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `access_token` text NOT NULL,
  `is_active` int(11) DEFAULT '0',
  `is_public` int(11) DEFAULT '1',
  `is_celebrity` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
--  Table structure for TABLE `files`
--

DROP TABLE IF EXISTS `/* prefix */files`;

CREATE TABLE `/* prefix */files` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(128) NOT NULL,
 `path` text NOT NULL,
 `type` varchar(32) NOT NULL,
 `size` int(11) NOT NULL,
 `user_id` int(11) NOT NULL,
 `hits` int(11) DEFAULT '0',
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `name` (`name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Table structure for TABLE `rights`
--

DROP TABLE IF EXISTS `/* prefix */rights`;

CREATE TABLE `/* prefix */rights` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(128) NOT NULL,
 `description` text NOT NULL,
 `user_id` int(11) NOT NULL,
 `creation_ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
 `lastmodified_ts` timestamp NULL DEFAULT '0000-00-00 00:00:00',
 `is_active` int(1) DEFAULT '1',
 PRIMARY KEY (`id`),
 UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Table structure for TABLE `roles`
--

DROP TABLE IF EXISTS `/* prefix */roles`;

CREATE TABLE `/* prefix */roles` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(128) NOT NULL,
 `description` text NOT NULL,
 `user_id` int(11) NOT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastmodified_ts` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
 UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Table structure for TABLE `roles_rights`
--

DROP TABLE IF EXISTS `/* prefix */roles_rights`;

CREATE TABLE `/* prefix */roles_rights` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `role_id` int(11) NOT NULL,
 `right_id` int(11) NOT NULL,
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `role_id` (`role_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for TABLE `users_roles`
--

DROP TABLE IF EXISTS `/* prefix */users_roles`;

CREATE TABLE `/* prefix */users_roles` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `role_id` int(11) NOT NULL,
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`), 
 UNIQUE KEY `user_id` (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `/* prefix */translations`;

CREATE TABLE `/* prefix */translations` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `object_key` varchar(64) NOT NULL,
 `object_name` varchar(32) NOT NULL,
 `object_column` varchar(32) NOT NULL,
 `objectid` int(11) NOT NULL,
 `original` text NOT NULL,
 `translated` text NOT NULL,
 `locale` varchar(8) NOT NULL,
 `type` varchar(8) NOT NULL DEFAULT 'text',
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `lastmodified_ts` timestamp NULL DEFAULT NULL,
 `is_active` int(1) DEFAULT '1',
 PRIMARY KEY (`id`),
 UNIQUE KEY `key` (`object_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Table structure for TABLE `users_rights`
--

DROP TABLE IF EXISTS `/* prefix */users_rights`;

CREATE TABLE `/* prefix */users_rights` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `right_id` int(11) NOT NULL,
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `user_id` (`user_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;