
--
-- Dumping data for table `config`
--

INSERT INTO `/* prefix */configs` (`name`, `val`, `oftype`, `options`) VALUES
('assets', '/assets', 'text', NULL),
('compress_html_output', 'yes', 'boolean', NULL),
('email_reply_to', 'support@ghanerao.in', 'text', NULL),
('from_email_name', 'Ghanerao.in Support', 'text', NULL),
('from_email_address', 'support@ghanerao.in', 'text', NULL),
('images-assets', '/assets/images', 'text', NULL),
('js-assets', '/assets/js', 'text', NULL),
('locale', 'en-US', 'list', 'en-US|hi-IN'),
('mailchimp-api-key', 'd4293c59b79cbe306d4e4691239a480f-us8', 'text', NULL),
('mandrill-api-key', 'mRrL-p5KpJ241aZvlIHt6Q', 'text', NULL),
('photo-upload-limit', 1000, 'text', NULL),
('rights-reserved', 'All rights reserved', 'text', NULL),
('search-count', '25', 'text', NULL),
('sitename', 'Sai Ashirwad Informatia', 'text', NULL),
('start-year', '2014', 'text', NULL),
('template', 'general', 'list', 'general'),
('templates', '/gui/templates', 'text', NULL);


--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`name`, `label`, `label_key`, `icon`, `permissions`, `url`, `parent_menu`, `menu_order`, `is_active`, `is_public`, `is_shown_logged_user`) VALUES
('Home', 'Home', 'mn_home', 'home', '', 'index', 0, 1,  1, 1, 0),
('Know Us', 'Know Us', 'mn_knowus', 'user', '', 'knowus', 0, 5, 1, 1, 0),
('Register', 'Register', 'mn_register', 'save', '', 'register', 0, 1, 1, 1, 0),
('Login', 'Login', 'mn_login', 'log-in', '', 'login', 0, 1, 1, 1, 0),
('My Profile', 'My Profile', 'mn_myprofile', 'user', '', 'profile', 0, 1, 1, 1, 1),
('Logout', 'Logout', 'mn_logout', 'off', '', 'logout', 0, 1, 1, 1, 1);


--
-- Dumping data for table `pages`
--

INSERT INTO `/* prefix */pages` (`title`, `meta_keywords`, `meta_description`, `meta_author`, `content`, `slug`, `has_menubar`, `has_sidebar`, `is_auth_req`, `is_auth_not_req`) VALUES
('Activated', 'User, Account, Activated', 'User Account Activated', 'Rohan Sakhale', NULL, 'activated', 0, 0, 0, 1),
('Change Password - My Profile', 'user, change, password', 'User change password', 'Rohan Sakhale', NULL, 'changepassword', 1, 2, 1, 0),
('Know Us', 'Know, more, about, us', 'Know more about us', 'Rohan Sakhale', NULL, 'knowus', 1, 2, 0, 1),
('Login', 'user, login', 'User Login', 'Rohan Sakhale', NULL, 'login', 1, 2, 0, 1),
('Forgot Password - Login', 'forgot,password,login', 'Forgot password login page', 'Rohan Sakhale', NULL, 'forgotpassword', 1, 2, 0, 1),
('Register', 'user,register', 'User Registration', 'Rohan Sakhale', NULL, 'register', 1, 2, 0, 1),
('Thank You', 'thank,you', 'Thank You page for user', 'Rohan Sakhale', NULL, 'thankyou', 1, 2, 0, 0),
('Edit My Profile', NULL, NULL, NULL, NULL, 'editprofile', 1, 2, 1, 0),
('My Profile', NULL, NULL, NULL, NULL, 'profile', 1, 2, 1, 0),
('Logout', NULL, NULL, NULL, NULL, 'logout', 0, 2, 1, 0),
('Page Not Found', NULL, NULL, NULL, '<p>Oops, looks like the page you are trying to access is not available</p>', '404notfound', 1, 2, 0, 0),
('Home', NULL, NULL, NULL, NULL, 'index', 1, 2, 0, 0),
('Recover Password - Login', NULL, NULL, NULL, NULL, 'recoverpassword', 0, 2, 0, 1),
('Update Display Picture - Edit My Profile', NULL, NULL, NULL, NULL, 'updateprofiledisplay', 1, 2, 1, 0),
('Account Validation', NULL, NULL, NULL, NULL, 'validate', 0, 2, 0, 1),
('Gallery', NULL, NULL, NULL, NULL, 'gallery', 1, 2, 0, 0),
('Admin CP', NULL, NULL, NULL, NULL, 'admincp', 1, 2, 0, 0),
('Exit Administration', NULL, NULL, NULL, NULL, 'exitadmin', 1, 2, 0, 0),
('Addresses', NULL, NULL, NULL, NULL, 'editaddress', 1, 2, 0, 0);


--
-- Dumping data for table `roles`
--

INSERT INTO `/* prefix */roles` (`name`, `description`, `user_id`) VALUES
('Super Admin', 'Global Controller', 1),
('Administrator', 'System Administrator', 1),
('Moderator', 'Would moderate activities on site', 1),
('Member', 'Just a regular member', 1);


--
-- Dumping data for table `rights`
--

INSERT INTO `/* prefix */rights` (`name`, `description`, `user_id`) VALUES
('login_access_allowed', 'On email validation login access is allowed to user', 1),
('login_as_admin', 'Allows user to login as admin', 1),
('update_my_profile', 'Allows user to update his own profile', 1),
('update_others_profile', 'Allows user to update others profile', 1),
('make_profile_non_public', 'Allows user to make his profile non public', 1),
('delete_my_profile', 'Allows user to delete his own profile', 1),
('view_non_public_profiles', 'Allows user to view others profile', 1),
('search_profiles', 'Allows user to search profiles', 1),
('make_others_profile_celebrity', 'Allows user to make others profile celebrity', 1),
('delete_others_profile', 'Allows user to delete others profile', 1),
('view_deleted_users', 'Allows user to view deleted users', 1),
('create_page', 'Allows user to create a page', 1),
('edit_page', 'Allows user to edit a page', 1),
('delete_page', 'Allows user to delete a page', 1),
('search_page', 'Allows user to search page', 1),
('view_deleted_pages', 'Allows user to view deleted page', 1),
('create_role', 'Allows user to create role', 1),
('edit_role', 'Allows user to edit role', 1),
('view_roles', 'Allows user to view role', 1),
('view_roles_rights', 'Allows user to view role and rights', 1),
('delete_role', 'Allows user to delete role', 1),
('assign_right_to_role', 'Allows user to assign right to role', 1),
('assign_role_to_user', 'Allows user to assign role to user', 1),
('view_deleted_roles', 'Allows user to view deleted roles', 1),
('create_right', 'Allows user to create a right', 1),
('edit_right', 'Allows user to edit a right', 1),
('delete_right', 'Allows user to delete a right', 1),
('view_rights', 'Allows user to view rights', 1),
('assign_right_to_user', 'Allows user to assign right to user', 1),
('view_deleted_rights', 'Allows user to view deleted rights', 1),
('create_config', 'Allows user to create site configuration', 1),
('edit_config', 'Allows user to edit site configuration', 1),
('delete_config', 'Allows user to delete site configuration', 1),
('view_configs', 'Allows user to view site configuration', 1);
