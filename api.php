<?php
/**
 * @name API
 * @abstract Would help route the URL's and perform execution of required module
 * @author Rohan Sakhale
 * @copyright saiashirwad.com
 * @since WillingTree v1
 *       
 */
require_once 'bootstrap.php';
use Symfony\Component\Validator\Validation;
use Sai\Base;
use Sai\Modules;

/**
 * Check if `url` as a query parameter exists
 * Strictly only query parameter is checked here
 *
 * This url can be directly used as below
 * example	==>	api.php?url=users
 *
 * But to make it developer friendly, usage of Apache Mod Rewrite module is done
 * This module helps to rewrite the given form of URL into the required form of URL
 * example	==> api/users	can be rewritten to api.php?url=users
 *
 * This mechanism helps make URLs more friendlier and also easily identifies underlying operations
 */
if (isset($_GET['url'])) {
    /**
     * Following headers would allow CORS request
     */
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: *");
    $url = $_GET['url'];
    
    /**
     * Just to identify classes, we replcae all `/` with `_`
     * As a class can have `_` within it for representing linkage object
     */
    $url = str_replace("/", "_", $url);
    $parts = array();
    
    /**
     * Check whether the entire URL it represents as a class
     * If yes, this should be considered as obj_class further
     */
    if (class_exists($url)) {
        $parts[0] = $url;
    } else {
        $parts[0] = substr($url, 0, strrpos($url, "_"));
        $parts[1] = substr($url, strrpos($url, "_") + 1);
    }
    $obj_class = $parts[0];
    if (isset($parts[1]) and empty($parts[1])) {
        unset($parts[1]);
    }
    $response_data = null;
    try {
        /**
         * Check if the current invoked object is allowed
         */
        if (! in_array($obj_class, $core->getAllowedModules())) {
            throw new Base\Modules\Exception(
                Base\Modules\Exception::INVALID_MODULE);
        }
        /**
         * Check if that object's class is available
         */
        if (! class_exists($obj_class)) {
            throw new Base\Modules\Exception(
                Base\Modules\Exception::MODULE_NOT_AVAILABLE);
        }
        
        /**
         * Auth Manager helps to check with current request should be allowed or not
         *
         * If not, then Base\Modules\Auth\Exception is thrown as AUTHENTICATION_REQUIRED
         */
        $auth_manager = ManagersStore::locate(Sai\Modules\Auth\Object);
        $obj = null;
        $obj_mgr = null;
        $allow_request = $auth_manager->validate() ? true : $auth_manager->call_no_auth_allowed_method(
            $obj_class);
        if (! $allow_request) {
            throw new Modules\Auth\Exception(
                Modules\Auth\Exception::AUTHENTICATION_REQUIRED);
        }
        $obj = new $obj_class();
        $obj_mgr = Base\ManagersStore::locate($obj_class);
        
        /**
         * If trailing part of url makes a method of manager class
         * This method should be called which underlying has its own implementation for CRUD operation
         *
         * This method generates an array response or throws an exception which would be serialized and echoed
         */
        if (isset($parts[1]) and method_exists($obj_mgr, $parts[1])) {
            $response_data = $obj_mgr->$parts[1]();
        } else {
            /**
             * Identify request method call and invoke underlying methods accordingly
             *
             * GET		==>	calls get method
             * POST	==>	calls create method
             * PUT		==> calls update method
             * DELETE	==> calls delete method
             */
            switch ($_SERVER['REQUEST_METHOD']) {
                case "GET":
                    if (isset($parts[1])) {
                        $response_data = $obj_mgr->get($parts[1]);
                        if (! is_array($response_data) and
                             is_object($response_data)) {
                            $response_data = $response_data->jsonSerialize();
                        }
                    } else {
                        if (! is_null($obj->getId()) and
                             ! is_null($obj->creation_ts)) {
                            $response_data = $obj->jsonSerialize();
                        } else {
                            $obj_e = $obj_class . "\Exception";
                            throw new $obj_e($obj_e::NOT_FOUND);
                        }
                    }
                    break;
                case "POST":
                    $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
                    $violations = $validator->validate($obj);
                    if ($violations->count() == 0) {
                        $response_data = $obj_mgr->create();
                        if (isset($response_data['id']) and
                             $response_data['id'] > 0) {
                            change_response_code(201);
                        }
                    } else {
                        $obj_e = $obj_class . "\Exception";
                        $const = $violations->get(0)->getMessage();
                        throw new $obj_e(constant($obj_e . '::' . $const));
                    }
                    break;
                case "PUT":
                    if (isset($parts[1])) {
                        if (is_numeric($parts[1])) {
                            $obj->loadObject($parts[1]);
                        } else {
                            $obj = $obj_mgr->get($parts[1]);
                        }
                    }
                    $obj_e = $obj_class . '\Exception';
                    if (! is_null($obj) and is_null($obj->creation_ts)) {
                        throw new $obj_e($obj_e::NOT_FOUND);
                    }
                    $obj->loadArgs();
                    $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
                    $violations = $validator->validate($obj);
                    if ($violations->count() == 0) {
                        $obj_mgr->setObject($obj);
                        $response_data = $obj_mgr->update();
                    } else {
                        $obj_e = $obj_class . "\Exception";
                        $const = $violations->get(0)->getMessage();
                        throw new $obj_e(constant($obj_e . '::' . $const));
                    }
                    break;
                case "DELETE":
                    if (isset($parts[1]) and $parts[1] !== false) {
                        if (is_numeric($parts[1])) {
                            $obj->loadObject($parts[1]);
                        } else {
                            $obj = $obj_mgr->get($parts[1]);
                        }
                    }
                    $obj_mgr->setObject($obj);
                    $obj_mgr->delete();
                    $response_data = null;
                    break;
                default:
                    throw new Base\Modules\Exception(
                        Base\Modules\Exception::INVALID_METHOD);
            }
        }
    } catch (Base\Exception $e) {
        $response_data = $e->jsonSerialize();
    } catch (\Exception $e) {
        $response_data = array(
            'error_code' => $e->getCode(),
            'error_msg' => $e->getMessage()
        );
    }
    try {
        if (! is_null($response_data) and ! empty($response_data)) {
            if (! is_string($response_data)) {
                $rh = $core->request_headers;
                $accept = 'application/json';
                if (isset($rh['accept']) and
                     strpos($rh['accept'], '*/*') === false) {
                    $accept = $rh['accept'];
                }
                $restlizer_baby = new Base\Restlizer\Manager($accept);
                $response = $restlizer_baby->serializo($response_data);
                header("Content-Type: $accept;charset=UTF-8");
                echo $response;
            } else {
                header("Content-Type: text/plain;charset=UTF-8");
                echo $response_data;
            }
        } else {
            header("Content-Type: text/plain;charset=UTF-8");
            Base\Common::changeResponseCode(204);
        }
    } catch (restlizer_exception $e) {
        header("Content-Type: application/json;charset=UTF-8");
        echo json_encode($e->jsonSerialize());
    }
} else {
    header("HTTP/1.1 403 Forbidden");
    header("Content-Type: text/plain;charset=UTF-8");
    die("Sorry, this file restricted for direct usage");
}
?>