<?php
require_once '../../../start.php';

class important_phones_manager_test extends PHPUnit_Framework_TestCase
{

    public function testCreate()
    {
        try {
            $manager = managers_store::locate('important_phones');
            $obj = new important_phones(false);
            $obj->loadObjectInstance(
                array(
                    "name" => "Apardddnda12",
                    "phone" => "9730228313"
                ));
            $ret = $manager->create($obj);
            $this->assertArrayHasKey('msg', $ret);
        } catch (sai_exception $e) {
            echo "Error: " . $e->getExceptionMessage();
            $this->assertTrue(false,$e->getExceptionMessage());
        }
    }
} 